<?
            //Get the room array and make sure that is has something
            $rooms = Input::get('room');

            if (is_array($rooms)) {

                //Save each room
                foreach ($rooms as $room) {
                    
                    //Validate time, start date must be less than end
                    //2015-01-28 10:00
                    $raw_start_date_time = (string) Input::get('start');
                    $end_time = Input::get('end');

                    //Make sure that the dates for the start and end are the same to avoid data inconsistencies
                    //Le operation....
                    $start_date = explode(' ', $raw_start_date_time);

                    $start_date_time = new DateTime($raw_start_date_time);

                    $end_date_time = $start_date[0] . ' ' . $end_time;
                    
                    if (strtotime($start_date[1]) <= strtotime($end_time)) {

                        $loggedInUser = 1; //Auth::user();
                        //Check whether repeat or not
                        if ($isRepeat == 'NONE' || $isRepeat == "") {

                            //@TODO : Get the room area selected and get the time limits of the room area and if the 
                            //      : Selected is greater set the max to what is provided or less then set the min to this
                            //Single instance
                            ReservationRoom::create(array(
                                'start_time' => $raw_start_date_time,
                                'end_time' => $end_date_time,
                                'user_id' => $loggedInUser,
                                'reservation_name' => Input::get('name'),
                                'reservation_desc' => Input::get('description'),
                                'dept_id' => Input::get('department'),
                                'room_id' => $room
                            ));

                            return Redirect::to('reservation/room/add')->with('notice', 'Room Reservation Made');
                        } else {

                            //Repeat reservation
                            $repeat_end_date = new DateTime(Input::get('repeat-end'));
                            //@TODO : Prevent bookings for the same time on the same resource over the repeat reserve time
                            //Make sure that the end of the repeat is greater than the start date
                            if ($repeat_end_date > $start_date_time) {

                                //Single instance
                                ReservationRoom::create(array(
                                    'start_time' => $raw_start_date_time,
                                    'end_time' => $end_date_time,
                                    'user_id' => $loggedInUser,
                                    'reservation_name' => Input::get('name'),
                                    'reservation_desc' => Input::get('description'),
                                    'dept_id' => Input::get('department'),
                                    'room_id' => $room,
                                    //repeat stuff :D
                                    'repeat_end_date' => Input::get('repeat-end'),
                                    'repeat_id' => Input::get('rtype')
                                ));

                                return Redirect::to('reservation/room/add')->with('notice', 'Room Reservation Made');
                            } else {
                                return Redirect::back()->withInput()->withErrors('Repeat end date must be greater than start date.');
                            }
                        }
                    } else {
                        return Redirect::back()->withInput()->withErrors('Start Time must be less than End.');
                    }
                }

                //Rooms array check
            } else {
                return Redirect::back()->withInput()->withErrors('Room needs to be set.');
            }
