<?php

/*
|--------------------------------------------------------------------------
| All rights reserved - The Strathmore Business School
|--------------------------------------------------------------------------
|
| The SBS Faculty Planner
|
*/

//Home
Breadcrumbs::register('home', function($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'),'<i class="fa fa-dashboard"></i>');
});

/*
 * User Breadcrumbs
 */
Breadcrumbs::register('users', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Users', route('users'));
});
Breadcrumbs::register('user/add', function($breadcrumbs) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Add User', route('user/add'));
});
Breadcrumbs::register('user/edit', function($breadcrumbs) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push('Edit User', route('user/edit'));
});

Breadcrumbs::register('user/u_edit', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Edit Profile', route('user/edit'));
});



/*
 * Role Breadcrumbs
 */
Breadcrumbs::register('roles', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Roles', route('roles'));
});
Breadcrumbs::register('role/add', function($breadcrumbs) {
    $breadcrumbs->parent('roles');
    $breadcrumbs->push('Add Role', route('role/add'));
});
Breadcrumbs::register('role/edit', function($breadcrumbs) {
    $breadcrumbs->parent('roles');
    $breadcrumbs->push('Edit Role', route('role/edit'));
});

/*
 * Department Breadcrumbs
 */
Breadcrumbs::register('departments', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Departments', route('departments'));
});
Breadcrumbs::register('department/add', function($breadcrumbs) {
    $breadcrumbs->parent('departments');
    $breadcrumbs->push('Add Department', route('department/add'));
});
Breadcrumbs::register('department/edit', function($breadcrumbs) {
    $breadcrumbs->parent('departments');
    $breadcrumbs->push('Edit Department', route('department/edit'));
});

/*
 * Builiding Breadcrumbs
 */
Breadcrumbs::register('buildings', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Builidings', route('buildings'));
});
Breadcrumbs::register('building/add', function($breadcrumbs) {
    $breadcrumbs->parent('buildings');
    $breadcrumbs->push('Add Builiding', route('building/add'));
});
Breadcrumbs::register('building/edit', function($breadcrumbs) {
    $breadcrumbs->parent('buildings');
    $breadcrumbs->push('Edit Builiding', route('building/edit'));
});

/*
 * Room Area Breadcrumbs
 */
Breadcrumbs::register('areas', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Room Areas', route('areas'));
});
Breadcrumbs::register('area/add', function($breadcrumbs) {
    $breadcrumbs->parent('areas');
    $breadcrumbs->push('Add Room Area', route('area/add'));
});
Breadcrumbs::register('area/edit', function($breadcrumbs) {
    $breadcrumbs->parent('areas');
    $breadcrumbs->push('Edit Room Area', route('area/edit'));
});


/*
 * Room Breadcrumbs
 */
Breadcrumbs::register('rooms', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Rooms', route('rooms'));
});
Breadcrumbs::register('room/add', function($breadcrumbs) {
    $breadcrumbs->parent('rooms');
    $breadcrumbs->push('Add Room', route('room/add'));
});
Breadcrumbs::register('room/edit', function($breadcrumbs) {
    $breadcrumbs->parent('rooms');
    $breadcrumbs->push('Edit Room', route('room/edit'));
});

/*
 * Course Group Breadcrumbs
 */
Breadcrumbs::register('cgroups', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Course Areas', route('cgroups'));
});
Breadcrumbs::register('cgroup/add', function($breadcrumbs) {
    $breadcrumbs->parent('cgroups');
    $breadcrumbs->push('Add Course Area', route('cgroup/add'));
});
Breadcrumbs::register('cgroup/edit', function($breadcrumbs) {
    $breadcrumbs->parent('cgroups');
    $breadcrumbs->push('Edit Course Area', route('cgroup/edit'));
});

/*
 * Course Categories Breadcrumbs
 */
Breadcrumbs::register('ccategories', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Course Categories', route('ccategories'));
});

/*
 * Course Breadcrumbs
 */
Breadcrumbs::register('courses', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Courses', route('courses'));
});
Breadcrumbs::register('course/add', function($breadcrumbs) {
    $breadcrumbs->parent('courses');
    $breadcrumbs->push('Add Course', route('course/add'));
});
Breadcrumbs::register('course/edit', function($breadcrumbs) {
    $breadcrumbs->parent('courses');
    $breadcrumbs->push('Edit Course', route('course/edit'));
});

/*
 * Reservation Breadcrumbs
 */

Breadcrumbs::register('reservation/room', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Room Reservations', route('reservation/room'));
});
Breadcrumbs::register('reservation/room/add', function($breadcrumbs) {
    $breadcrumbs->parent('reservation/room');
    $breadcrumbs->push('Add Room Reservation', route('reservation/room/add'));
});
Breadcrumbs::register('reservation/room/edit', function($breadcrumbs) {
    $breadcrumbs->parent('reservation/room');
    $breadcrumbs->push('Edit Room Reservation', route('reservation/room/edit'));
});

//Resevation details

Breadcrumbs::register('reservation/details/create', function($breadcrumbs) {
    $breadcrumbs->parent('reservation/room');
    $breadcrumbs->push('Add Reservation Details', route('reservation/details/create'));
});
Breadcrumbs::register('reservation/details/edit', function($breadcrumbs) {
    $breadcrumbs->parent('reservation/room');
    $breadcrumbs->push('Edit Reservation Details', route('reservation/details/edit'));
});

//Mail Templates
Breadcrumbs::register('mails',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Mail Templates',route('mails'));
});
Breadcrumbs::register('mails/create',function($breadcrumbs){
    $breadcrumbs->parent('mails');
    $breadcrumbs->push('Create Mail Templates',route('mails/create'));
});
Breadcrumbs::register('mails/edit',function($breadcrumbs){
    $breadcrumbs->parent('mails');
    $breadcrumbs->push('Edit Mail Templates',route('mails/edit'));
});

//groups
Breadcrumbs::register('groups',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Groups',route('groups'));
});
Breadcrumbs::register('group/add',function($breadcrumbs){
    $breadcrumbs->parent('groups');
    $breadcrumbs->push('Add Groups',route('group/add'));
});
Breadcrumbs::register('group/edit',function($breadcrumbs){
    $breadcrumbs->parent('groups');
    $breadcrumbs->push('Edit Group',route('group/edit'));
});
Breadcrumbs::register('reservation/approval',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Reservation Approvals',route('reservation/approval'));
});
Breadcrumbs::register('reservation/approval/edit',function($breadcrumbs){
    $breadcrumbs->parent('reservation/approval');
    $breadcrumbs->push('Reservation Approval ',route('reservation/approval/edit'));
});
Breadcrumbs::register('announcements',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Announcements',route('announcements'));
});
Breadcrumbs::register('announcement/create',function($breadcrumbs){
    $breadcrumbs->parent('announcements');
    $breadcrumbs->push('Create Announcement',route('announcement/create'));
});
Breadcrumbs::register('announcement/edit',function($breadcrumbs){
    $breadcrumbs->parent('announcements');
    $breadcrumbs->push('Edit Announcement',route('announcement/edit'));
});

//Reports
Breadcrumbs::register('reports',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Reports',route('reports'));
});

Breadcrumbs::register('report/generate',function($breadcrumbs){
    $breadcrumbs->parent('reports');
    $breadcrumbs->push('Generate Reports',route('report/generate'));
});

Breadcrumbs::register('reports/room/util',function($breadcrumbs){
    $breadcrumbs->parent('reports');
    $breadcrumbs->push('Room Utilization Reports',route('reports/room/util'));
});

//Settings
Breadcrumbs::register('settings',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Application Settings',route('settings'));
});

// Lecturer Bookings
Breadcrumbs::register('lecturer/booking',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Lecturer Bookings',route('lecturer/booking'));
});
Breadcrumbs::register('lecturer/confirmed_booking',function($breadcrumbs){
    $breadcrumbs->parent('lecturer/booking');
    $breadcrumbs->push('Lecturer Bookings Summary',route('lecturer/confirmed_booking'));
});
Breadcrumbs::register('lecturer/lecturers',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Lecturer Hours',route('lecturer/lecturers'));
});
Breadcrumbs::register('lecturer/lecturers/add_hours',function($breadcrumbs){
    $breadcrumbs->parent('lecturer/lecturers');
    $breadcrumbs->push('Add Lecturer Hours',route('lecturer/lecturers/add_hours'));
});
Breadcrumbs::register('lecturer/materials',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Lecturer Materials',route('lecturer/materials'));
});
Breadcrumbs::register('lecturer/add_materials',function($breadcrumbs){
    $breadcrumbs->parent('lecturer/materials');
    $breadcrumbs->push('Add Lecturer Materials',route('lecturer/add_materials'));
});
Breadcrumbs::register('lecturer/timesheets',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Timesheets',route('lecturer/timesheets'));
});
Breadcrumbs::register('lecturer/refresh_timesheets',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Refresh Timesheets',route('lecturer/refresh_timesheets'));
});
Breadcrumbs::register('lecturer/viewtimesheets',function($breadcrumbs){
    $breadcrumbs->parent('lecturer/timesheets');
    $breadcrumbs->push('Summary Timesheets',route('lecturer/viewtimesheets'));
});
Breadcrumbs::register('lecturer/workloads',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Workloads',route('lecturer/workloads'));
});
Breadcrumbs::register('lecturer/refresh_workloads',function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Refresh Workloads',route('lecturer/workloads'));
});
Breadcrumbs::register('lecturer/viewworkloads',function($breadcrumbs){
    $breadcrumbs->parent('lecturer/workloads');
    $breadcrumbs->push('Summary Workloads',route('lecturer/viewworkloads'));
});
