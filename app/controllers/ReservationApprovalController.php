<?php

class ReservationApprovalController extends \BaseController
{
    private $mail_model;

    public function __construct()
    {
        $this->mail_model = new Mails();
    }

    /**
     * Display a listing of the resource.
     * GET /reservationapproval
     *
     * @return Response
     */
    public function index()
    {
        return View::make('reservation/approval/index');
    }

    /**
     * Show the form for creating a new resource.
     * GET /reservationapproval/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /reservationapproval
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /reservationapproval/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /reservationapproval/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $reservation = ReservationRoom::find($id);
        if ($reservation) {
            $status = ['' => 'Please select one', 'Approved' => 'Approved', 'Pending Approval' => 'Pending Approval', 'Rejected' => 'Rejected'];
            return View::make('reservation/approval/edit', compact('reservation', 'status'));
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /reservationapproval/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $reservation = ReservationRoom::find($id);
        if ($reservation) {

            $reservation->approval = Input::get('approval');
            if ($reservation->save()) {
                if (is_null($reservation->created_for) || $reservation->created_for == "") {
                    $recipient = $reservation->user_id;
                } else {
                    $recipient = $reservation->created_for;
                }

                $data = $this->mail_model->data($recipient, $reservation->room_id, $reservation->reservation_room_id, '', '');
                $subject = "Reservation response";

                $to = User::find($recipient)->email;

                $tmpl = 'mail_temps/ApprovalResponse';
                Emailr::queue($tmpl,false, $data, $to ,$subject);

                /*Mail::queue(['html' => 'mail_temps/ApprovalResponse'], $data, function ($message) use ($data, $to, $subject) {
                    $message->to($to)->subject($subject);
                    $message->from('sbs_scheduler@strathmore.edu', 'SBS Scheduler');
                });*/

                if (Input::get('approval') == 'Rejected') {
                    if ($reservation->delete()) {
                        return Redirect::to('reservation/approval')->with('notice', 'Reservation' . Input::get('approval'));
                    }

                }
                return Redirect::to('reservation/approval')->with('notice', 'Reservation' . Input::get('approval'));


            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /reservationapproval/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDataTable()
    {
        $rreservations = DB::table('reservation_rooms')->select('reservation_room_id', 'reservation_name', 'reservation_desc', 'fname', 'lname', 'created_for', 'dept_name', 'approval', 'start_time', 'end_time')->join('users', 'reservation_rooms.user_id', '=', 'users.user_id')->join('depts', 'depts.dept_id', '=', 'reservation_rooms.dept_id')->orderBy('reservation_rooms.created_at', 'desc');
        $reserved_by = new \Chumper\Datatable\Columns\FunctionColumn('reserved_by', function ($row) {
            return $row->fname . ' ' . $row->lname;
        });
        $reserved_for = new \Chumper\Datatable\Columns\FunctionColumn('reserved_for', function ($row) {

            $rfor = "-";
            $u_id = $row->created_for;

            if (!is_null($u_id)) {

                $user_reserved_for  = User::find($u_id);
                if ( $user_reserved_for != null ){
                    $rfor = $user_reserved_for->fname . ' ' . $user_reserved_for->lname;
                }else{
                    $rfor = "";
                }
            }
            return $rfor;
        });
        $action_links = new \Chumper\Datatable\Columns\FunctionColumn('action_links',
            function ($row) {
                $rr_id = $row->reservation_room_id;
                $edit_url = URL::to("reservation/approval/" . $rr_id . "/edit");
                $status = $row->approval;
                $return_links = ($status == "Approved") ? "<span class='fa fa-thumbs-o-up' title='Approved' style='color:green' />&nbsp;" : "<div class='action_div'><span title='Not Approved' class='fa fa-thumbs-o-down' style='color:darkred' />&nbsp;";
                $return_links .= "<a title='Change Approval Status' href='$edit_url'><span class='fa fa-edit'/></a>";

                return $return_links;
            });

        return Datatable::query($rreservations)
            ->showColumns('reservation_name', 'reservation_desc', 'reserved_by', 'reserved_for', 'dept_name', 'approval', 'action_links')
            ->addColumn($reserved_by)
            ->addColumn($action_links)
            ->addColumn($reserved_for)
            ->searchColumns('reservation_name', 'dept_name', 'approval', 'fname')
            ->orderColumns('created_at', 'room_name', 'dept_name', 'approval', 'reservation_name')
            ->make();
    }

}
