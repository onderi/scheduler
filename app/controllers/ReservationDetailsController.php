<?php
/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
 */

/**
 * Description of ReservationDetailsController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 *
 * |"Humble thyself in the sight of the LORD, and He will lift you up."
 * || -> Ponder the excellencies of what is offered here.... Selah
 * || -> consider the madness of trying to lift yourself up (you are man!)
 * || -----::> "The power that makes an aeroplane fly is that which sustains it"
 * || -> consider who will lift you up (the Omnipotent, Almighty God)
 * ||| Amazing Grace! This is how God lifts up :: by giving us His Grace
 * ||| ::: "God gives his grace to the humble" :::
 * |
 */
class ReservationDetailsController extends \BaseController
{



    /**
     * @param $room_reservation_id
     * @return \Illuminate\View\View
     */
    public function create($room_reservation_id)
    {

        $rr = ReservationRoom::findOrFail($room_reservation_id);
        $status = DB::table('reservation_statuses')->lists('reservation_status_name', 'reservation_status_id');
        $faculty = User::getFaculty();
        $rr_id = (is_null($rr->rr_id)) ? 0 : $rr->rr_id;
        return View::make('reservation/details/form',
            array(
                'faculty' => $faculty,
                'rr_name' => $rr->reservation_name,
                'rr_id' => $rr_id,
                'reservation' => $room_reservation_id,
                'department' => $rr->dept_id,
                'room' => $rr->room->room_name,
                'user' => array($rr->user->fname . ' ' . $rr->user->lname, $rr->user_id),
                'course' => [],
                'ccategory' => array('' => 'Please select one') + CourseCategory::getCategoriesWithCourses(),
                'start_time' => $rr->start_time,
                'end_time' => $rr->end_time,
                'reservation_repeat_id' => $rr_id,
                'ref' => $rr->reservation_room_id . '/' . $rr->room->room_name . '/' . $rr->dept_id . '/'.$rr_id,
                'status' => $status));
    }

    public function createLec($room_reservation_id)
    {

        $rr = \DB::select('SELECT * FROM sfp_reservations WHERE reservation_id = '.room_reservation_id);
        $status = DB::table('reservation_statuses')->lists('reservation_status_name', 'reservation_status_id');
        $faculty = \DB::select('SELECT CONCAT(fname,lname) FROM sfp_users INNER JOIN sfp_reservations on sfp_users.user_id= sfp_reservations.faculty WHERE sfp_reservations.reservation_id = '.room_reservation_id);
        $rr_id = (is_null($rr->rr_id)) ? 0 : $rr->rr_id;
        return View::make('reservation/details/form',
            array(
                'faculty' => $faculty,
                'rr_name' => $rr->reservation_desc,
                'rr_id' => $rr->reservation_id,
                'reservation' => $rr->reservation_id,
                'department' => $rr->category,
                'room' => $rr->room->room_name,
                'user' => array($faculty, $rr->faculty),
                'course' => [],
                'ccategory' => array('' => 'Please select one') + CourseCategory::getCategoriesWithCourses(),
                'start_time' => $rr->start_time,
                'end_time' => $rr->end_time,
                'reservation_repeat_id' => 1,
                'ref' => $rr->ref_number,
                'status' => $status));
    }


    /**
     * Save the details
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function store_frm()
    {


        $c = 0;
        $v = ReservationRoomDetails::validate(Input::all());

        if ($v->fails()) {
            return Redirect::back()->withInput()->withErrors($v->errors());
        }


        //Make sure that the segments are consistent.
        for ($x = 0; $x < count(Input::get('start')); $x++) {
            if (date('H:i', strtotime(Input::get('start') [$x])) >= date('H:i', strtotime(Input::get('end') [$x]))) {
                return Redirect::back()->withInput()->with('errors', 'Start time ' . $x + 1 . ' or end time ' . $x + 1 . 'is invalid');
            } elseif ($x >= 1 && date('H:i', strtotime(Input::get('start') [$x])) < date('H:i', strtotime(Input::get('end') [$x - 1]))) {
                return Redirect::back()->withInput()->with('errors', 'Invalid start time ' . $x + 1);
            }
        }

        //get the details

        //arrays
        $a_start = Input::get('start');
        $a_end = Input::get('end');
        $a_desc = Input::get('desc');
        $a_courses = Input::get('courses');
        $a_ccategory = Input::get('ccategory');
        $a_status =  Input::get('status');
        $a_faculty = Input::get('faculty');

        $a_first_faculty = Input::get('first_faculty');
        $a_second_faculty = Input::get('second_faculty');

        //single values
        $created_by = Input::get('user_id');
        $rr_id = Input::get('rr');
        $rr_name = Input::get('rr_name');
        $owner = Input::get('owner');

        $ref_str = Input::get('ref');
        $ref = explode(':', $ref_str);
        $ref_number  = $ref[1];


        while ( $c < count(Input::get('desc')) ) {


            $start = explode(" ", $a_start [$c]);
            $end = $start[0] . ' ' . $a_end [$c];

            $stat = $a_status [$c];

            $detail = new ReservationRoomDetails;
            $detail->ref_number = $ref_number;
            $detail->reservation_desc = $a_desc [$c];
            $detail->reservation_room_id = $rr_id;
            $detail->course_id = $a_courses [$c];
            $detail->category = $a_ccategory [$c];
            $detail->created_by = $created_by;
            $detail->start_time = $a_start [$c];
            $detail->end_time = $end;
            $detail->last_modified = date('Y-m-d H:i:s', time());
            $detail->reservation_status_id = $stat;


            //deal with faculty selections
            $first_faculty = $a_first_faculty[$c];
            $second_faculty = $a_second_faculty[$c];

            //if there is a value for first_faculty then it means that we have two faculty members hence we will get the first two array elements as those faculty
            if ( $first_faculty != "" ){

                $facult_str = $a_faculty[0].','.$a_faculty[1];

                //unset the first two elements in $a_faculty
                array_splice($a_faculty,0,2);

                $detail->hours = $first_faculty.','.$second_faculty;
                $detail->faculty = $facult_str;

            }else{

                $detail->faculty = $a_faculty[0];
            }


            if ($detail->save()) {
                $c++;

                /*
                 *    $recipient = Input::get('f_email') [$c];
                    $name = Input::get('facilitator') [$c];
                    $end = Input::get('end') [$c];



                    $keys = array($created_by, '', $rr_id, $a_ccategory[$c], $a_courses[$c]);

                    $subject = $detail->reservation_desc;

                if (Input::get('f_email') [$c - 1] != "") {

                    /*
                     * $keys = array(Auth::user()->user_id, '', '', '', '');
                     * @note : the date format is Y-d-m for so that it can be aligned with google.
                     * $this->model->invite("MBA 8101 - Managerial Accounting",'kinyuawallace@gmail.com', 'Wallace', '2017-05-03 08:00:00', '2017-05-03 12:00:00', $keys);
                     *

                    $this->model->invite($subject, $recipient, $name, $start, $end, $keys);

                }
                */


            } else {
                return 'error';
            }
        }

        return Redirect::to('reservation/room')->with('notice', 'Reservation Room Details Added');
    }

    /**
     * Save the details
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function store()
    {


        $c = 0;
        $v = ReservationRoomDetails::validate(Input::all());


        if ($v->fails()) {

            $errors = $v->errors()->all();

            $msg_str = implode('<br/>',$errors);

            $rtn = array('type' => 'error',  $msg_str);
            return json_encode($rtn);

        }


        //Make sure that the segments are consistent.
        for ($x = 0; $x < count(Input::get('start')); $x++) {

            if (date('H:i', strtotime(Input::get('start') [$x])) >= date('H:i', strtotime(Input::get('end') [$x]))) {
                $rtn = array('type' => 'error',  'Start time ' . $x + 1 . ' or end time ' . $x + 1 . 'is invalid');
                return json_encode($rtn);
            } elseif ($x >= 1 && date('H:i', strtotime(Input::get('start') [$x])) < date('H:i', strtotime(Input::get('end') [$x - 1]))) {
                $rtn = array('type' => 'error',  'Invalid start time ' . $x + 1);
                return json_encode($rtn);
            }
        }

        //get the details

        //arrays
        $a_start = Input::get('start');
        $a_end = Input::get('end');
        $a_desc = Input::get('desc');
        $a_courses = Input::get('courses');
        $a_ccategory = Input::get('ccategory');
        $a_status =  Input::get('status');
        $a_faculty = Input::get('faculty');

        $a_first_faculty = Input::get('first_faculty');
        $a_second_faculty = Input::get('second_faculty');

        //single values
        $created_by = Input::get('user_id');
        $rr_id = Input::get('rr');
        $rr_name = Input::get('rr_name');
        $owner = Input::get('owner');
        $apply_to_all = Input::get('applytoall');


        $ref_str = Input::get('ref');
        $ref = explode(':', $ref_str);
        $ref_number  = $ref[1];
        $ref_number_array = explode('/',$ref_number);
        $reservation_repeat_id = $ref_number_array[3];
        $room_name = $ref_number_array[1];
        $department = $ref_number_array[2];
        $repeated_rooms_array = ReservationRoom::getRoomIds($reservation_repeat_id);
        $repeated_rooms_array_count = count($repeated_rooms_array);
        $details_data = [];
        while ( $c < count(Input::get('desc')) ) {


            $start = explode(" ", $a_start [$c]);
            $end = $start[0] . ' ' . $a_end [$c];

            $stat = $a_status [$c];

            $detail = [];

            $detail['reservation_desc'] = $a_desc [$c];

            $detail['course_id'] = $a_courses [$c];
            $detail['category'] = $a_ccategory [$c];
            $detail['created_by'] = $created_by;

            $detail['last_modified'] = date('Y-m-d H:i:s', time());
            $detail['reservation_status_id'] = $stat;


            //deal with faculty selections
            $first_faculty = $a_first_faculty[$c];
            $second_faculty = $a_second_faculty[$c];

            //if there is a value for first_faculty then it means that we have two faculty members hence we will get the first two array elements as those faculty
            if ( $first_faculty != "" ){

                $facult_str = $a_faculty[0].','.$a_faculty[1];

                //unset the first two elements in $a_faculty
                array_splice($a_faculty,0,2);

                $detail['hours'] = $first_faculty.','.$second_faculty;
                $detail['faculty'] = $facult_str;

            }else{

                $detail['faculty'] = $a_faculty[$c];
            }

            if( $repeated_rooms_array_count > 0 ){

                if($apply_to_all == "1"):
                    foreach($repeated_rooms_array as $room_id_n):

                        $st_time_db = ReservationRoom::findOrFail($room_id_n)->start_time;
                        $current_date = substr($st_time_db,0,strpos($st_time_db,' '));
                        $end_now = $current_date.' '.$a_end [$c];
                        $st_time_ = $a_start [$c];
                        $start_time_n = substr($st_time_,strpos($st_time_,' '),strlen($st_time_));

                        $detail['start_time'] = $current_date.' '.$start_time_n;
                        $detail['end_time'] = $end_now;
                        $detail['reservation_room_id'] = $room_id_n;
                        $detail['ref_number'] = $room_id_n . '/' . $room_name . '/' . $department . '/'.$reservation_repeat_id;
                        array_push($details_data,$detail);
                    endforeach;
                else:

                    $detail['start_time'] = $a_start [$c];
                    $detail['end_time'] = $end;
                    $detail['reservation_room_id'] = $rr_id;
                    $detail['ref_number'] = $ref_number;
                    array_push($details_data,$detail);

                endif;

            }else {
                $detail['start_time'] = $a_start [$c];
                $detail['end_time'] = $end;
                $detail['reservation_room_id'] = $rr_id;
                $detail['ref_number'] = $ref_number;
                array_push($details_data,$detail);

            }

            $c++;


        }
        if (DB::table('reservations')->insert($details_data)) {
            /*
             *    $recipient = Input::get('f_email') [$c];
                $name = Input::get('facilitator') [$c];
                $end = Input::get('end') [$c];


                $keys = array($created_by, '', $rr_id, $a_ccategory[$c], $a_courses[$c]);

                $subject = $detail->reservation_desc;

            if (Input::get('f_email') [$c - 1] != "") {

                /*
                 * $keys = array(Auth::user()->user_id, '', '', '', '');
                 * @note : the date format is Y-d-m for so that it can be aligned with google.
                 * $this->model->invite("MBA 8101 - Managerial Accounting",'kinyuawallace@gmail.com', 'Wallace', '2017-05-03 08:00:00', '2017-05-03 12:00:00', $keys);
                 *

                $this->model->invite($subject, $recipient, $name, $start, $end, $keys);

            }
            */

        } else {
            $rtn = array('type' => 'error', 'Unable to save segment(s)');
            return json_encode($rtn);
        }

        $msg_return = array('type' => 'notice');
        return json_encode($msg_return);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $details = ReservationRoomDetails::where('reservation_room_id','=',$id)->get();
        $ccategory = array('' => 'Please select one') + CourseCategory::getCategoriesWithCourses();
        $status = DB::table('reservation_statuses')->lists('reservation_status_name', 'reservation_status_id');
        $faculty = User::getFaculty();
        //reservation_room
        $r_name = ReservationRoom::where('reservation_room_id', '=', $id)->pluck('reservation_name');
        $r_room = Room::find(ReservationRoom::where('reservation_room_id', '=', $id)->pluck('room_id'))->room_name;
        $r_dept = ReservationRoom::where('reservation_room_id', '=', $id)->pluck('dept_id');

        $user_id = ReservationRoom::where('reservation_room_id', '=', $id)->pluck('user_id');
        $end_time_room = ReservationRoom::where('reservation_room_id', '=', $id)->pluck('end_time');

        $r_user = User::find($user_id)->fname .' '.User::find($user_id)->lname;
        $ref = $id . '/' . $r_room . '/' . $r_dept  . '';
        $owner = $r_user;
        $reservation = $id;

        return View::make('reservation/details/edit', compact('ref','user_id' ,'end_time_room','reservation','owner','details','r_name','ccategory','status','faculty'));
    }


    /**
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function update()
    {

        $c = 0;
        $v = ReservationRoomDetails::validate(Input::all());

        if ($v->fails()) {
            return Redirect::back()->withInput()->withErrors($v->errors());
        }


        //Make sure that the segments are consistent.
        for ($x = 0; $x < count(Input::get('start')); $x++) {
            if (date('H:i', strtotime(Input::get('start') [$x])) >= date('H:i', strtotime(Input::get('end') [$x]))) {
                return Redirect::back()->withInput()->with('errors', 'Start time ' . $x + 1 . ' or end time ' . $x + 1 . 'is invalid');
            } elseif ($x >= 1 && date('H:i', strtotime(Input::get('start') [$x])) < date('H:i', strtotime(Input::get('end') [$x - 1]))) {
                return Redirect::back()->withInput()->with('errors', 'Invalid start time ' . $x + 1);
            }
        }

        //get the details

        //arrays
        $a_start = Input::get('start');
        $a_end = Input::get('end');
        $a_desc = Input::get('desc');
        $a_courses = Input::get('courses');
        $a_ccategory = Input::get('ccategory');
        $a_status =  Input::get('status');
        $a_faculty = Input::get('faculty');

        $a_first_faculty = Input::get('first_faculty');
        $a_second_faculty = Input::get('second_faculty');

        //single values
        $created_by = Input::get('user_id');
        $rr_id = Input::get('rr');
        $rr_name = Input::get('rr_name');
        $owner = Input::get('owner');

        $ref_str = Input::get('ref');
        $ref = explode(':', $ref_str);
        $ref_number  = $ref[1];

        //delete all
        ReservationRoomDetails::where('reservation_room_id','=',$rr_id)->delete();

        //Add again
        while ( $c < count(Input::get('desc')) ) {


            $start = explode(" ", $a_start [$c]);
            $end = $start[0] . ' ' . $a_end [$c];

            $stat = $a_status [$c];

            $detail = new ReservationRoomDetails;
            $detail->ref_number = $ref_number;
            $detail->reservation_desc = $a_desc [$c];
            $detail->reservation_room_id = $rr_id;
            $detail->course_id = $a_courses [$c];
            $detail->category = $a_ccategory [$c];
            $detail->created_by = $created_by;
            $detail->start_time = $a_start [$c];
            $detail->end_time = $end;
            $detail->last_modified = date('Y-m-d H:i:s', time());
            $detail->reservation_status_id = $stat;


            //deal with faculty selections
            $first_faculty = $a_first_faculty[$c];
            $second_faculty = $a_second_faculty[$c];

            //if there is a value for first_faculty then it means that we have two faculty members hence we will get the first two array elements as those faculty
            if ( $first_faculty != "" ){

                $facult_str = $a_faculty[0].','.$a_faculty[1];

                //unset the first two elements in $a_faculty
                array_splice($a_faculty,0,2);

                $detail->hours = $first_faculty.','.$second_faculty;
                $detail->faculty = $facult_str;

            }else{

                $detail->faculty = $a_faculty[$c];
            }


            if ($detail->save()) {
                $c++;

                /*
                 *    $recipient = Input::get('f_email') [$c];
                    $name = Input::get('facilitator') [$c];
                    $end = Input::get('end') [$c];



                    $keys = array($created_by, '', $rr_id, $a_ccategory[$c], $a_courses[$c]);

                    $subject = $detail->reservation_desc;

                if (Input::get('f_email') [$c - 1] != "") {

                    /*
                     * $keys = array(Auth::user()->user_id, '', '', '', '');
                     * @note : the date format is Y-d-m for so that it can be aligned with google.
                     * $this->model->invite("MBA 8101 - Managerial Accounting",'kinyuawallace@gmail.com', 'Wallace', '2017-05-03 08:00:00', '2017-05-03 12:00:00', $keys);
                     *

                    $this->model->invite($subject, $recipient, $name, $start, $end, $keys);

                }
                */


            } else {
                return 'error';
            }
        }

        $msg_return = array('type' => 'notice');
        return json_encode($msg_return);
        //return Redirect::to('reservation/room')->with('notice', 'Reservation Room Details updated');

    }

    /**
     * Remove the specified resource from storage.
     * DELETE /reservationroomdetails/{id}
     *
     * @param  int $id
     * @return Response
     */

    public function destroy()
    {

        $reservation_room_id = Input::get('rr_id');

        if( isset($reservation_room_id) ){

            ReservationRoomDetails::where('reservation_id','=',$reservation_room_id)->delete();

            return '1';

        }

        return '0';

    }

    /**
     * Check whether a given faculty has been booked for the time selected.
     */
    public function hasConflict(){

        $faculty = Input::get('faculty');

        $str_start = Input::get('start');
        $str_end = Input::get('end');

        $response = ['type' => 'error','text' => 'Please select start and end times','timeout' => 5000];

        if ( isset($str_start) && isset($str_end) ){

            $reservations = ReservationRoomDetails::
                select('*')
                    ->whereRaw("start_time < '$str_end' AND end_time > '$str_start'")
                    ->get();

            if( count($reservations) > 0 ){

                $msg = "";

                foreach( $reservations as $r ){


                    $db_faculty = explode(',',$r->faculty);
                    $diff = array_diff($db_faculty,$faculty);

                    $found_faculty = array_diff($db_faculty,$diff);

                    //if it is one or two it means there is a similarity / conflict.
                    if ( (count($diff) == 1 || count($diff) == 0) && count($found_faculty) > 0){


                        if ( isset($diff[0]) && $diff[0] != 0){
                            $msg .= ReservationRoomDetails::getDetails($r,$found_faculty);
                        }else{

                            if( !isset($diff[0]))
                                $msg .= ReservationRoomDetails::getDetails($r,$found_faculty);
                        }

                    }

                }

                if ( $msg != "" ){
                    $msg = "<b>One or more conflict(s) encountered: </b><br/><br/>" . $msg;

                    $response = ['type' => 'error','text' => $msg,'timeout' => 20000];

                    echo json_encode($response);
                }else{
                    $response = ['type' => 'information','text' => 'No conflicts detected','timeout' => 5000];
                    echo json_encode($response);
                }

            }else{

                $response = ['type' => 'information','text' => 'No conflicts detected','timeout' => 5000];
                echo json_encode($response);

            }


            /*

            if ( count($hasOverlap) > 0 ){

                $msg = "";

                foreach( $hasOverlap as $single ){

                    $msg .= ReservationRoomDetails::getDetails($single);

                }


                $msg .= "<b>One or more conflict(s) encountered: </b><br/>";

                $response = ['type' => 'error','text' => $msg];

                echo json_encode($response);
            }else{

                $response = ['type' => 'information','text' => 'No conflicts detected'];
                echo json_encode($response);

            }
*/
        }else{

            echo json_encode($response);

        }


    }

}
