<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of RoleController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class RoleController extends BaseController {
    //put your code here

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $roles = Role::with('users')->get(); //all();

        $roles_final = array();
        $role_users = array();
        $i = 0;
        foreach ($roles as $role):

            $role_name = $role->name;
            $role_id = $role->role_id;
            $role_type = $role->type;
            $sys_type = $role->sys_type;
            $permissions = DB::table('permissions')->select('display_name')->join('role_permissions', 'permissions.permission_id', '=', 'role_permissions.permission_id')->where('role_permissions.role_id', '=', $role_id)->orderBy('display_name')->get();
            $permission = "Not set";

            if ($permissions) {
                //Get the permissions
                $permission = array_pluck($permissions, 'display_name');
            }
            $roles_final[$i] = array('role_id' => $role_id, 'name' => $role_name, 'permissions' => $permission, 'sys_type' => $sys_type, 'type' => $role_type);

            //Users assigned to a certain role
            if (count($role->users) > 0) {

                $prefix = '';
                $list = "";

                foreach ( $role->users as $u ) {

                    $list .= $prefix . $u->fname.' '.$u->lname;
                    $prefix = '&#013;';
                }
                $role_users[$i]['count'] = count($role->users);
                $role_users[$i]['names'] = $list;
            } else {
                $role_users[$i]['count'] = 0;
                $role_users[$i]['names'] = "None Assigned";
            }

            $i++;
        endforeach;
        Return View::make('role/index', array('roles' => $roles_final,'role_users'=>$role_users));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //get the various permissions
        $permissions = DB::table('permissions')->orderBy('display_name')->get();
        Return View::make('role/form_role', array('permissions' => $permissions)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        if (Input::get('permissions')) {
            //get the permissions array
            $permissions = Input::get('permissions');
            //If exists Remove the select-all option
            if (strlen($permissions[0]) == 15) {
                unset($permissions[0]);
            }
            //Get the role name
            $role_name = Input::get('name');

            $role = new Role();
            $role->name = $role_name;
            try {
                if ($role->save()) {
                    //associate the permissions with the role
                    $role->perms()->sync($permissions);
                    return Redirect::to('roles')->with('notice', 'Role added.');
                } else {
                    return Redirect::to('role/add')->withInput()->withErrors($role->errors());
                }
            } catch (Exception $exc) {
                return Redirect::to('role/add')->withInput()->withErrors('The role name must be unique');
            }
        } else {
            return Redirect::to('role/add')->withInput()->withErrors('No Permissions Specified');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
        $role = Role::find($id);
        $permissions_all = DB::table('permissions')->orderBy('display_name')->get();
        if ($role) {
            $role_id = $role->role_id;
            $permissions = DB::table('role_permissions')->select('permission_id as permission_id')->where('role_id', '=', $role_id)->get();
            $permission = "0";

            if ($permissions) {
                //Get the permissions
                $permission = array_pluck($permissions, 'permission_id');
                //Convert into nice string
                $permission = implode(',', $permission);
            }
            return View::make('role/form_role_edit', array('role' => $role, 'permission_selected' => $permission, 'permissions' => $permissions_all));
        } else {
            return Redirect::to('roles');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $role = Role::find($id);

        if ($role) {
            
            
            
            //get the permissions array
            $permissions = Input::get('permissions');
            //If exists Remove the select-all option
            if (strlen($permissions[0]) == 15) {
                unset($permissions[0]);
            }
            try {
                $role->name = Input::get('name');

                if ($role->save()) {
                    //associate the permissions with the role
                    $role->perms()->sync($permissions);
                    return Redirect::to('roles')->with('notice', 'Role saved.');
                } else {
                    return Redirect::to("role/$id/edit")->withInput()->withErrors($role->errors());
                }
            } catch (Exception $exc) {
                //dd($exc->getMessage());
                return Redirect::to("role/$id/edit")->withInput()->withErrors('The role name must be unique');
            }
            
        } else {
            return Redirect::to('roles');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        $role = Role::find($id);

        if ($role) {

            try {
                $role->delete();
                return Redirect::to('roles')->with('notice', 'Role deleted.');
            } catch (Exception $exc) {
                //echo $exc->getTraceAsString();
                return Redirect::to('roles')->with('error', 'Cannot Delete role! It has users assigned to it.');
            }
        } else {
            return Redirect::to('roles');
        }
    }

    /**
     * @return string
     */
    public function getUsers(){

        $role_id = Input::get('role_id');

        $users = User::where('status_id','=','1')->whereHas(
            'roles', function($q) use($role_id){
            $q->where('roles.role_id', '=',$role_id);
        }
        )->orderby('fname','asc')->get();


        $users_str = (count($users) > 0 ) ? "<option value='0'>All</option>" : "<option value='None'>None found</option>" ;

        foreach($users as $u){
            $id = $u['user_id'];
            $name = $u['fname']. ' '. $u['lname'];

            $users_str .= "<option value='$id'>$name</option>";
        }

        return $users_str;
    }

    public function missingMethod($parameters = array()) {
        //
        Redirect::to('roles');
    }


}
