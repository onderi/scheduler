<?php
/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of BuildingController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class BuildingController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $buildings = Building::with('areas')->get();
        Return View::make('building/index', array('buildings' => $buildings));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
        Return View::make('building/form_bldng');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
        $v = Building::validate(Input::all());

        if ($v->passes()) {

            Building::create(array(
                'building_name' => Input::get('name'),
                'building_description' => Input::get('description'),
                'type' => Input::get('type')
            ));

            return Redirect::to('buildings')->with('notice','Building added.');
        } else {
            return Redirect::to('building/add')->withInput()->withErrors($v);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
        $building = Building::find($id);

        if ($building) {
            return View::make('building/form_bldng_edit', array('building' => $building));
        } else {
            return Redirect::to('buildings');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
        $v = Building::validate(Input::all());

        if ($v->passes()) {

            $building = Building::find($id);
            $building->building_name = Input::get('name');
            $building->building_description = Input::get('description');
            $building->type = Input::get('type');
            $building->save();

            return Redirect::to('buildings')->with('notice','Building updated.');
        } else {
            return Redirect::to("building/$id/edit")->withInput()->withErrors($v);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        $building = Building::find($id);

        if ($building) {
            
            try {
                $building->delete();
                return Redirect::to('buildings')->with('notice','Building deleted.');
            } catch (Exception $exc) {
                //echo $exc->getTraceAsString();
               return Redirect::to('buildings')->with('error','Cannot Delete building! It has areas assigned to it.');
            }
            
        } else {
            return Redirect::to('buildings');
        }
    }

    public function missingMethod($parameters = array()) {
        //
        Redirect::to('buildings');
    }

}
