<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of CourseController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class CourseController extends BaseController{

    /**
     * @return \Illuminate\View\View
     */
    public function index(){
       
        return View::make('course/index',array('courses'=> Course::with('ccategory')->get()));
    }

    /**
     * Synchronize courses on demand
     */
    public function SynchronizeCourses()
    {

        if( Auth::user()->isSysAdmin() || Auth::user()->isSystemPlanningAdmin()){
            if( Course::SynchronizeCourses())
                echo "success";
        }else{
            echo "error-p";
        }



    }



}
