<?php

class GroupController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /group
	 *
	 * @return Response
	 */
	public function index()
	{
		$groups=Group::with('SArea')->get();
		return View::make('group/index',compact('groups'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /group/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$SAreas=array(''=>'Please select one') + SArea::lists('subject_area_abbrev','subject_area_id');
		return View::make('group/form',compact('SAreas'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /group
	 *
	 * @return Response
	 */
	public function store()
	{
		$v=Group::validate(Input::all());
		if($v->passes()){
			$group=new Group();
				$group->group_name=Input::get('name');
				$group->period=Input::get('period');
				$group->subject_area_id=Input::get('SArea');
			try{
				if($group->save()){
					return Redirect::to('groups')->with('notice','Group Added');
				}
				else{
					return Redirect::back()->withInput()->withErrors($group->errors());
				}


				
			}catch(Exception $exc){
				return Redirect::to('groups')->with('error','Unable to add group');
			}

		}
		else{
			return Redirect::to('group/add')->withInput()->withErrors($v->errors());
		}
	}

	/**
	 * Display the specified resource.
	 * GET /group/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /group/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$group=Group::find($id);
		if($group){
			$SAreas=array(''=>'Please select one') + SArea::lists('subject_area_abbrev','subject_area_id');
			return View::make('group/edit',compact('group','SAreas'));

		}
		else{
			return Redirect::to('groups')->with('error','Could not find model');
		}
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /group/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$group=Group::find($id);
		if($group){
		$v=Group::validate(Input::all());
		if($v->passes()){
			try{
				
				$group->group_name=Input::get('name');
				$group->period=Input::get('period');
				$group->subject_area_id=Input::get('SArea');

				if($group->save()){
					return Redirect::to('groups')->with('notice','Group details successfully updated');
				}

			}catch(Exception $exc){
				return Redirect::to()->with('error','Unable to update group');
			}

		}else{
			return Redirect::to('group/$id/edit')->withInput()->withErrors($v->errors());
		}
	}else{
		return Redirect::to('groups')->with('error','Group model could not be found');
	}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /group/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$group=Group::find($id);

		if($group){
			try{
				$group->delete();
				return Redirect::to('groups')->with('notice','Group Deleted');
				}
				catch(Exception $exc){
					return Redirect::to('groups')->with('error','Could not delete model');
				}

		}else{
			return Redirect::to('groups')->with('error','Could not find the groups model');
		}
	}

}