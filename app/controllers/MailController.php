<?php

class MailController extends \BaseController {

	private $mail_model;

	public function __construct(){
		$this->mail_model=new Mails();
	}

	/**
	 * Display a listing of the resource.
	 * GET /mail
	 *
	 * @return Response
	 */
	public function index()
	{
		$mails=Mails::get();
		return View::make('mails/index',compact('mails'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /mail/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('mails/form');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /mail
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator=Mails::validate(Input::all());

		if($validator->passes()){
			$mail=new Mails();
			$mail->name=Input::get('name');
			$mail->description=Input::get('description');
			$mail->message=Input::get('message');
			try{
				if($mail->save()){
					return Redirect::to('mails')->with('notice','Successfully added the message');
				}
				else{
					return Redirect::back()->withInput()->withErrors($mail->errors());
				}

			}
			catch(Exception $exc){
				return Redirect::back()->withinput()->withErrors('Exception caught');

			}

		}
		else{
			return Redirect::back()->withInput()->withErrors($validator->errors());
		}
		
	}

	/**
	 * Display the specified resource.
	 * GET /mail/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /mail/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$mail=Mails::find($id);

		if($mail){
			return View::make('mails/edit',compact('mail'));

		}
		else{
			return Redirect::to('mails')->withInput()->withErrors('Model could not be found');
		}
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /mail/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$mail=Mails::find($id);

		if($mail){
			$validator=Mails::validate(Input::all());
			if($validator->passes()){
			

			try{
				$mail->name=Input::get('name');
			$mail->description=Input::get('description');
			$mail->message=Input::get('message');

			if($mail->save()){
				return Redirect::to('mails')->with('notice','Update successful');

			}
			else{
				return Redirect::back()->withInput()->withErrors("Could not update model");
			}


			}
			catch(Exception $exc){
				return Redirect::to('mails/$id/edit')->withInput()->withErrors('Exception caught');

			}
		}
		else{
			return Redirect::back()->withInput()->withErrors($validator->errors());
		}
	}

		else{
			return Redirect::to('mails')->withInput()->withErrors('Model Could not be found');
		}

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /mail/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		$mail=Mails::find($id);

		if($mail){
			try{
				if($mail->delete()){
					return Redirect::to('mails')->with('notice','Delete successful');
				}
			}
			catch(Exception $exc){
				return Redirect::to('mails')->withInput()->withErrors('Delete could not be completed');
			}
			}
		else{
			return Redirect::to('mails')->withInput()->withErrors('Model Could not be found');
		}

			
		
	}

public function render($mail_id){
		$mail=Mails::find($mail_id);

		return View::make('mail_temps/general',compact('mail'));

	}
	public function mailSend(){

	}


	

	
	


}