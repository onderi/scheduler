<?php

class AnnouncementController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /announcement
	 *
	 * @return Response
	 */
	public function index()
	{
		$announcements=Announcement::with('user')->get();

        return View::make('announcement/index',compact('announcements'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /announcement/create
	 *
	 * @return Response
	 */
	public function create()
	{
	    $priorities=[
        ''=>'Please select one...',
        1=>'Urgent',
        2=>'Important',
        3=>'Normal Priority',
        4=>'Low Priority',
        5=>'Informative'];
		return View::make('announcement/form',compact('priorities'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /announcement
	 *
	 * @return Response
	 */
	public function store()
	{
		$v=Announcement::validate(Input::all());
        if($v->passes()){
            $start=date('Y-m-d',strtotime(Input::get('start_date')));
            $end=date('Y-m-d',strtotime(Input::get('end_date')));

            $announcement=new Announcement();
            $announcement->text=Input::get('text');
            $announcement->priority=Input::get('priority');
            $announcement->start_date=$start;
            $announcement->end_date=$end;
            $announcement->user_id=Auth::user()->user_id;
            if($announcement->save()){
                return Redirect::to('announcements')->with('notice','Announcement has been added');
            }

        }
        else{
            return Redirect::to('announcement/create')->withInput()->withErrors($v->errors());
        }
	}

	/**
	 * Display the specified resource.
	 * GET /announcement/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /announcement/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$announcement=Announcement::find($id);
        if($announcement){
            $priorities=[
        ''=>'Please select one...',
        1=>'Urgent',
        2=>'Important',
        3=>'Normal Priority',
        4=>'Low Priority',
        5=>'Informative'];

        return View::make('announcement/edit',compact('priorities','announcement'));
        }
        else{
            return Redirect::to('announcements')->with('error','Model could not be found');
        }
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /announcement/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$announcement=Announcement::find($id);
        if($announcement){
            $v=Announcement::validate(Input::all());
            if($v->passes()){
                $announcement->text=Input::get('text');
                $announcement->user_id=Auth::user()->user_id;
                $announcement->priority=Input::get('priority');

                $start=date('Y-m-d',strtotime(Input::get('start_date')));
                $end= date('Y-m-d',strtotime(Input::get('end_date')));

                $announcement->start_date=$start;
                $announcement->end_date=$end;

                if($announcement->save()){
                    return Redirect::to('announcements')->with('message','Announcement updated');
                }
                else{
                    return Redirect::to('announcements')->with('error','Announcement could not be updated');
                }

            }
            else{
                return Redirect::action('announcement/edit',$id)->withInput()->withErrors($v->errors());
            }
        }
        else{
            return Redirect::to('announcements')->with('error','Model could not be found');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /announcement/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$announcement=Announcement::find($id);
        if($announcement){
            if($announcement->delete()){
                return Redirect::to('announcements')->with('notice','Announcement deleted');
            }
            else{
                return Redirect::to('announcements')->with('error','Model could not be deleted');
            }
        }
	}

}