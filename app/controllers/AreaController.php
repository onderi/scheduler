<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of AreaController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class AreaController extends BaseController {

    /**
     * View all Room areas
     * @return type
     */
    public function index() {
        return View::make('area/index', array('areas' => Area::with('rooms')->get())
        );
    }

    /**
     * Show form to add new Room area
     * @return type
     */
    public function create() {
        $buildings=Building::lists('building_name','building_id');
        return View::make('area/form_area',compact('buildings'));
    }

    /**
     * Save the edited details
     * @return type
     */
    public function store() {
        $v = Area::validate(Input::all());
        //If validation passes

        if ($v->passes()) {

            try {
               

                $area=Area::create(array(
                    'area_name' => Input::get('name'),
                    'area_desc' => Input::get('description'),
                    'default_duration' => Input::get('default_duration') * 60,//Convert the default duration and the resolution into seconds
                    'resolution' => Input::get('resolution') * 60,
                    'start_interval' => Input::get('start_interval'),
                    'end_interval' => Input::get('end_interval'),
                    'building_id'=>Input::get('building_id')
                )); 

                return Redirect::to('areas')->with('notice', 'Room Area added');
            } catch (Exception $exc) {
                return Redirect::to('area/add')->with('error', 'Unable to add Room Area');
            }
        } else {

            return Redirect::to('area/add')->withInput()->withErrors($v->errors());
        }
    }

    /**
     * Show edit form 
     * @param type $id
     * @return type
     */
    public function edit($id) {


        $area = Area::find($id);
        $buildings=Building::lists('building_name','building_id');

         $selected=$area->building_id;
        //If exists
        if ($area) {

            return View::make('area/form_area_edit', array('area' => $area),compact('buildings','selected'));
        } else {
            return Redirect::to('areas');
        }
    }
    /**
     * Update the Room area
     * @param type $id
     * @return type
     */
    public function update($id) {
        $area = Area::find($id);

        if ($area) {

            $v = Area::validate(Input::all());

            if ($v->passes()) {

                $area->area_name = Input::get('name');
                $area->area_desc = Input::get('description');
                $area->building_id = Input::get('building_id');
                $area->default_duration = Input::get('default_duration') * 60;//Convert the default duration and the resolution into seconds
                $area->resolution = Input::get('resolution') * 60;
                $area->start_interval = Input::get('start_interval');
               // $area->private_enabled = Input::get('privacy'); Private booking feature diabled...
                try {

                    $area->save();
                    return Redirect::to('areas')->with('notice', 'Room Area updated');
                } catch (Exception $exc) {
                    return Redirect::to('area/' . $id . '/edit')->withInput()->with('error', 'Unable to save Room area');
                }
            } else {
                return Redirect::to('area/' . $id . '/edit')->withInput()->withErrors($v->errors());
            }
        } else {
            return Redirect::to('areas');
        }
    }
    /**
     * Delete Room Area
     * @param type $id
     * @return type
     */
    public function destroy($id){
        
        $area = Area::find($id);
        //if exists
        if( $area ){
            
            try {
                $area->delete();
                return Redirect::to('areas');
            } catch (Exception $exc) {
               // echo $exc->getMessage();
                return Redirect::to('areas')->with('error','Unable to delete room area,It has rooms assigned to it.');
            }
                    
        }else{
            return Redirect::to('areas');
        }
        
    }

    /**
     * Get the Rooms in a certain Area
     * @return JSON or String
     */
    public function getRoomsInArea(){

        $id = Input::get('id');

        $rooms = Room::where('area_id','=',$id)->get();//lists('room_name','room_id');
        
        if(count($rooms) > 0 ){
            $formatted_rooms = array();
            $i = 0;
            foreach( $rooms as $room ){
                $formatted_rooms[$i]['label'] = $room->room_name;
                $formatted_rooms[$i]['value'] = (String) $room->room_id;
                $i++;
            }
            Return json_encode($formatted_rooms,JSON_HEX_TAG);
        }else{
            Return 'none';
        }
    }
    
}
