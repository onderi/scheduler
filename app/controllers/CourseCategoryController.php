<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of CourseCategoryController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class CourseCategoryController extends BaseController {

    /**
     * @return \Illuminate\View\View
     */
    public function index() {

        $nested_course_categories = CourseCategory::getNestedCourseCategory(0,true);

        return View::make('ccategory/index', array( 'ccategories' => $nested_course_categories ));
    }

    /**
     * Synchronize course categories on demand
     */
    public function SynchronizeCourseCategories(){


        if( Auth::user()->isSysAdmin() || Auth::user()->isSystemPlanningAdmin() ) {

            if (CourseCategory::SynchronizeCourseCategories())
                echo 'success';

        }else{
            echo "error-p";

        }
    }

    public function getCategoryCourses(){

        $cat_id_string = Input::get('id');

        $cat_id = explode('_',$cat_id_string)[1];

        //get the category name
        $category = CourseCategory::find($cat_id)->name;


        $response = ['title' => $category,'body' => 'Not found'];

        if ( isset( $cat_id ) ){

            $courses = Course::select('*')->where('category','=',$cat_id)->get();

            $course_str = ( count($courses) > 0 ) ? "" : "Not found";

            foreach( $courses as $course ){

                $course_str = $course_str . "<p> ⋆ $course->course_title</p>";

            }


            $response['body'] = $course_str;


        }


        echo json_encode($response);

    }

    /**
     * Get the courses of a certain category for populating a select.
     */
    public function getCategoryCoursesSelect(){

        $cat_id = Input::get('id');

        $response = ['id' => '', 'text' => 'No Courses Found'];

        if ( isset( $cat_id ) ) {

            $courses = Course::select('*')->where('category', '=', $cat_id)->get();

            if( count($courses) > 0 ){
                $response = []; //init

                foreach( $courses as $course ){

                    $course_id = $course->course_id;
                    $course_title = $course->course_title;

                    array_push($response,['id' => $course_id,'text' => $course_title]);

                }

            }

        }


        echo json_encode($response);


    }



    }
