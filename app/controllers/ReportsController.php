<?php

class ReportsController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /reports
     *
     * @return Response
     */
    public function index()
    {
        $programs = Course::orderBy('course_title', 'asc')->lists("course_title", "course_id");
        return View::make("reports/index", compact("programs"));
    }

    /**
     * Show the form for creating a new resource.
     * GET /reports/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /reports
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /reports/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /reports/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /reports/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /reports/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id){}


    public function room_util(){

        $areas = array('0'=>'All') + Area::lists('area_name', 'area_id') ;

        $default_area = 2; //@TODO - Get the default area from the settings
        $rooms = Calendar::_getRooms($default_area);
        $room_ids = Room::_getRoomsArray ($default_area);
        $room_ids_str = Room::_getRoomsString($default_area);

        $start = Reports::getDefaultStart();
        $end = Reports::getDefaultEnd();

        /**
         * Default is first member in array
         */
        $report_type = array("# Room Bookings"=>'# Room Bookings',"# Hours"=>"# Hours","Reservation type"=>"Reservation type");

        $chartdata = Reports::getUsageData($room_ids,$report_type['Reservation type'],$start,$end);

        return View::make('reports/room/util',array(
            'areas' => $areas,
            'rooms' => $rooms,
            'room_ids' => $room_ids_str,
            'rooms_array' =>   Calendar::_getRooms($default_area,'array'),
            'default_area' => $default_area,
            'start_date' => $start,
            'end_date' => $end,
            'report_type' => $report_type,
            'default_type' => 'Reservation type',
            'chartdata' => $chartdata
        ));

    }

    public function generate()
    {
        $program = Course::find(Input::get("program"));
        $rooms = Room::get();
        $start_date_limit = new datetime(Input::get("start_limit"));
        $end_date_limit = new datetime(Input::get("end_limit"));
        $end_date_limit = $end_date_limit->modify('+1 day');
        $start_time = new datetime("00:00");
        $end_time = new datetime("21:00");

        $date_interval = new dateinterval("P1D");
        $time_interval = new dateinterval("PT1H");

        $date_period = new dateperiod($start_date_limit, $date_interval, $end_date_limit);
        $time_period = new dateperiod($start_time, $time_interval, $end_time);

        $sessions_sql = "SELECT * FROM sfp_reservations
WHERE course_id=" . $program->course_id . " AND
 ((start_time between '" . $start_date_limit->format('Y-m-d H:i:s') . "' AND '" . $end_date_limit->format('Y-m-d H:i:s') . "')
OR (end_time between '" . $start_date_limit->format('Y-m-d H:i:s') . "' AND '" . $end_date_limit->format('Y-m-d H:i:s') . "')
OR (start_time >= '" . $start_date_limit->format('Y-m-d H:i:s') . "' AND end_time <= '" . $end_date_limit->format('Y-m-d H:i:s') . "')
OR (start_time <= '" . $start_date_limit->format('Y-m-d H:i:s') . "' AND end_time >= '" . $end_date_limit->format('Y-m-d H:i:s') . "'))
order by start_time";

        $sessions = DB::select(DB::raw($sessions_sql));
//        dd($sessions_sql);

        return View::make('reports/generate', compact('sessions', 'date_period', 'time_period', 'program', 'rooms','start_date_limit','end_date_limit'));

    }
    public function timetable_pdf($course_id,$start,$end){
        $program = Course::find($course_id);

        $start_date_limit = new datetime($start);
        $end_date_limit = new datetime($end);
        $date_interval = new dateinterval("P1D");

        $date_period = new dateperiod($start_date_limit, $date_interval, $end_date_limit);



        $sessions_sql = "SELECT * FROM sfp_reservations
WHERE course_id=" . $program->course_id . " AND
 ((start_time between '" . $start_date_limit->format('Y-m-d H:i:s') . "' AND '" . $end_date_limit->format('Y-m-d H:i:s') . "')
OR (end_time between '" . $start_date_limit->format('Y-m-d H:i:s') . "' AND '" . $end_date_limit->format('Y-m-d H:i:s') . "')
OR (start_time >= '" . $start_date_limit->format('Y-m-d H:i:s') . "' AND end_time <= '" . $end_date_limit->format('Y-m-d H:i:s') . "')
OR (start_time <= '" . $start_date_limit->format('Y-m-d H:i:s') . "' AND end_time >= '" . $end_date_limit->format('Y-m-d H:i:s') . "'))
order by start_time";

        $sessions = DB::select(DB::raw($sessions_sql));

        //return View::make('reports/timetable',compact('sessions','program','date_period','start_date_limit','end_date_limit'));
        //$html_ = $html->render();

        //$pdf->loadHTML('<h1>Test</h1>');
//        $pdf = new \Thujohn\Pdf\Pdf();
//        return PDF::load(View::make('reports/haha',compact('sessions','program','date_period','start_date_limit','end_date_limit')))->stream('testing');
//        PDF::clear();


        $param=array();
        $param['sessions']=$sessions;
        $param['program']=$program;
        $param['date_period']=$date_period;
        $param['start_date_limit']=$start_date_limit;
        $param['end_date_limit']=$end_date_limit;

        //$pdf=PDF::loadView('reports/timetable',$param);
        //$pdf=PDF::loadView('reports/haha',compact('sessions','program','date_period','start_date_limit','end_date_limit'));
        return PDF::loadView('reports/timetable',$param)->stream('Timetable.pdf');



    }

    /**
     * Return the chart data based - ajax
     * @return json string
     */
    public function refetchChartData(){
        $report_type = Input::get('report_type');
        $rooms = Input::get("rooms");
        $rooms_array = explode(',',$rooms);
        $from = Input::get('from');
        $to = Input::get('to');


        if ( $report_type != "" && $rooms != "" && $from != "" && $to != ""){

            if( $rooms == "0" ){
                $rooms_array = Room::lists('room_id');
                $chartdata = Reports::getUsageData($rooms_array,$report_type,$from,$to,true);
            }else{
                $chartdata = Reports::getUsageData($rooms_array,$report_type,$from,$to);
            }


            echo $chartdata;
        }else{
           echo '0';
        }

    }
}
