<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */
use Illuminate\Support\Facades\Log;
/**
 * Description of ReservationRoomController - Handle both Room and Segmentation features
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class ReservationController extends BaseController
{

    private $r_model;
    private $mail;

    //initialise the reservation model.
    public function __construct()
    {
        $this->r_model = new ReservationRoom();
        $this->mail = new Mails();
    }

    /**
     * Return view to reservation room view to user
     * @return type
     */
    public function rr_index()
    {
        return View::make('reservation/room/index');
    }

    /**
     * [removePipeFromDate Remove unwanted char from our date string]
     * @param  [Strng] $date [description]
     * @return [Strng]       [date]
     */
    private function removePipeFromDate($date)
    {
        return str_replace('|', ' ', $date);
    }

    /**
     * Display form to add new
     * @return type
     */
    public function rr_create($room_id, $start, $end)
    {

        $prev_url = URL::previous();

        $from_login = strpos($prev_url, 'login');
        //prevent direct viewing
        if ($from_login !== false)
            return Redirect::to('/');

        //separate the time and date
        if (empty($start) || empty($end)) {

            $start = date('Y-m-d') . '|' . date('H:i');
            $end = date('Y-m-d') . '|' . date('H:i', time() + 3600);
        }

        $formatted_start = $this->removePipeFromDate($start);
        $end_time = explode('|', $end);
        $day_number = date('N', strtotime($formatted_start));

        $area_id = ReservationRoom::_getAreaId($room_id);

        //get the Room Area and Room
        //test overlap
        // $this->checkOverlap(4, '2015-01-06 10:00:00', '2015-01-06 11:00:00', true, '2015-01-25');
        $rep_times = array_combine(range(1, 20), range(1, 20));


        return View::make('reservation/room/form_room', array(
                'departments' => array('' => 'Please select one') + Department::orderBy('dept_abbrev')->lists('dept_abbrev', 'dept_id'),
                'areas' => array('' => 'Please select one') + Area::lists('area_name', 'area_id'),
                'r_type' => Repeat::lists('repeat_name', 'repeat_id'),
                'start_date' => $formatted_start,
                'end_time' => $end_time[1],
                'area' => $area_id,
                'room' => $room_id,
                'rep_times' => $rep_times,
                'day_number' => $day_number,
                'users' => array('' => 'Please select one') + User::select('user_id', DB::raw("CONCAT(fname,' ', lname) AS full_name"))->orderBy('fname')->lists('full_name', 'user_id')

            )
        );

    }

    /**
     * Save the reservation
     * @return type
     */
    public function rr_store()
    {

        //Check whether the repeat checkbox is checked first
        $isRepeatChecked = Input::get('repeat') == "repeat" ? true : false;

        //Set repeat only if the checkbox is checked, else set it to none.
        $isRepeat = $isRepeatChecked ? Repeat::getNameById(Input::get('rtype')) : "NONE";

        //Boolean if weekly and n-weekly repeat to indicate that we should check the days when repeats occur
        $isDaySelected = ($isRepeat == "WEEKLY" || $isRepeat == "n-WEEKLY") && $isRepeat ? true : false;

        $validator = ReservationRoom::validate(Input::all(), Input::get('allday'), $isRepeat, $isDaySelected, 'add');

        //Validate our fields
        if ($validator->passes()) {

            //Get the rooms NB : They are in an array format.
            $rooms = Input::get('room');

            //Get and clean all the variables
            $raw_start_date_time = (string)Input::get('start');
            $start_date = explode(' ', $raw_start_date_time);

            //Check whether it is a whole day booking, if so, set start and end to room area's start and end
            if (Input::get('allday') == '1') {

                $room_area = Input::get('area');
                $area_defaults = ReservationRoom::getRoomAreaDefaults($room_area);

                $_start_dt = $start_date[0] . ' ' . $area_defaults['start_interval'];
                $_end_dt = $start_date[0] . ' ' . $area_defaults['end_interval'];
            } else {
                $end_time = Input::get('end');

                //Make sure that the dates for the start and end are the same to avoid data inconsistencies
                //Le operation....
                $_start_dt = $raw_start_date_time;
                $_end_dt = $start_date[0] . ' ' . $end_time;
            }

            //Deal with repeat params
            $r_reserve = $isRepeat != "NONE" && $isRepeatChecked ? true : false;
            $r_type = $isRepeat;
            $repeat_end_date = "";
            if ($r_reserve) {

                $repeat_end_date = Input::get('repeat_end');
                $r_days = Input::get('rep_day');

                $repeat_times = Input::get('rep_times_input');

                $rr_repeat = array(
                    'repeat_type' => $r_type,
                    'repeat_end' => $repeat_end_date,
                    'repeat_days' => $r_days,
                    'repeat_times' => $repeat_times
                );


            } else {
                $rr_repeat = "";
            }

            //Clean variables
            $start_date_time = $_start_dt;
            $end_date_time = $_end_dt;
            $r_name = Input::get('name');
            $r_desc = Input::get('description');
            $r_room = $rooms;
            $r_dept = Input::get('department');
            $rr_type = Input::get('rr_type');
            $r_repeat = $rr_repeat;
            $r_for = Input::get('user');


            //validate date ranges
            $validDateRanges = $this->validateDateRanges($start_date_time, $end_date_time, $repeat_end_date);

            if (is_bool($validDateRanges) && $validDateRanges == true) {

                //remove the "multiselect-all" from rooms array
                if (($key = array_search("multiselect-all", $r_room)) !== false) {
                    unset($r_room[$key]);
                }

                //Send them

                $savedReservation = $this->createReservation($start_date_time, $end_date_time, $r_name, $r_desc, $r_room, $r_dept, $rr_type, $r_repeat, $r_for);
                //If a boolean is returned then we know that we have triumphed.
                if (is_bool($savedReservation) == true) {

                    //add notification to the person created_for
                    //Send notification
                    if ($r_for != Auth::user()->user_id) {
                        $data = $this->mail->data_add($r_for, $r_room,$start_date_time,$end_date_time);
                        $subject = "Room Reservation Creation";

                        $to_for = User::findOrFail($r_for)->email;
                        $to = User::findOrFail(Auth::user()->user_id)->email;

                        //if there is an email
                        if(isset($to_for) && $to_for != null ):
                          $tmpl = 'mail_temps/reservationRequest';
                          Emailr::queue($tmpl,true, $data, $to ,$subject,$to_for);
                        endif;
                    }

                    $msg_return = array('type' => 'notice');
                    echo json_encode($msg_return);
                } else {
                    $rtn = array('type' => 'error', $savedReservation);
                    echo json_encode($rtn);
                }
            } else {
                echo json_encode(array('type' => 'error', $validDateRanges));
            }

            //Validator
        } else {
            $validator_errors = $validator->errors()->all();
            $validator_errors['type'] = "error";
            echo json_encode($validator_errors);
        }
    }

    public function rr_edit($reservation_id)
    {
        $prev_url = URL::previous();

        $from_login = strpos($prev_url, 'login');
        //prevent direct viewing
        if ($from_login !== false)
            return Redirect::to('/');


        $from_table = strpos($prev_url, 'room');

        if ($from_table === false) {
            Session::set('from_cal', 'yes');
        } else {
            Session::set('from_cal', 'No');
        }

        if (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin())
            $reservation = ReservationRoom::findOrFail($reservation_id);
        else
            $reservation = ReservationRoom::where('user_id', '=', Auth::user()->user_id)->where('reservation_room_id', '=', $reservation_id)->first();

        if ($reservation && count($reservation) > 0) {
            $areas = array('' => 'Please select one') + Area::lists('area_name', 'area_id');

            $rooms = array('' => 'Please select one') + Room::lists('room_name', 'room_id');

            $departments = array('' => 'Please select one') + Department::lists('dept_abbrev', 'dept_id');
            $users = array('' => 'Please select one') + User::select('user_id', DB::raw("CONCAT(fname,' ', lname) AS full_name"))->orderBy('fname')->lists('full_name', 'user_id');

            $rep_times = array_combine(range(1, 20), range(1, 20));

            $r_type = Repeat::lists('repeat_name', 'repeat_id');

            $r_type_sel = $reservation->repeat_id;
            $start_date = date('Y-m-d H:i', strtotime($reservation->start_time));
            $area = $reservation->room->area_id;
            $department = $reservation->dept_id;
            $end_time = date('H:i', strtotime($reservation->end_time));
            $room = $reservation->room_id;
            $user = $reservation->created_for;


            $external = $reservation->rr_type;

            $day_number = date('N', strtotime($start_date));

            return View::make('reservation.room.form_room_edit', compact('reservation',
                'areas',
                'area',
                'departments',
                'department',
                'r_type',
                'r_type_sel',
                'start_date',
                'end_time',
                'room',
                'rep_times',
                'day_number',
                'rooms',
                'users',
                'external',
                'user'));
        } else {
            return Redirect::to('/');
        }

    }

    public function rr_update($id)
    {

        if (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin())
            $reservation = ReservationRoom::findOrFail($id);
        else
            $reservation = ReservationRoom::where('user_id', '=', Auth::user()->user_id)->where('reservation_room_id', '=', $id)->first();

        if ($reservation && count($reservation) > 0) {


            //Check whether the repeat checkbox is checked first
            $isRepeatChecked = Input::get('repeat') == "repeat" ? true : false;

            //Set repeat only if the checkbox is checked, else set it to none.
            $isRepeat = $isRepeatChecked ? Repeat::getNameById(Input::get('rtype')) : "NONE";

            //Boolean if weekly and n-weekly repeat to indicate that we should check the days when repeats occur
            $isDaySelected = ($isRepeat == "WEEKLY" || $isRepeat == "n-WEEKLY") && $isRepeat ? true : false;

            $validator = ReservationRoom::validate(Input::all(), Input::get('allday'), $isRepeat, $isDaySelected, 'update');

            //Validate our fields
            if ($validator->passes()) {

                $department = Input::get('department');
                $room = Input::get('room');

                try {

                    //Get and clean all the variables
                    $raw_start_date_time = (string)Input::get('start');
                    $start_date = explode(' ', $raw_start_date_time);

                    //Check whether it is a whole day booking, if so, set start and end to room area's start and end
                    if (Input::get('allday') == '1') {

                        $room_area = Input::get('area');
                        $area_defaults = ReservationRoom::getRoomAreaDefaults($room_area);

                        $_start_dt = $start_date[0] . ' ' . $area_defaults['start_interval'];
                        $_end_dt = $start_date[0] . ' ' . $area_defaults['end_interval'];
                    } else {
                        $end_time = Input::get('end');

                        //Make sure that the dates for the start and end are the same to avoid data inconsistencies
                        //Le operation....
                        $_start_dt = $raw_start_date_time;
                        $_end_dt = $start_date[0] . ' ' . $end_time;
                    }

                    $old_reservation_room = $reservation->room_id;
                    $old_reservation_name = $reservation->reservation_name;
                    $old_reservation_start = $reservation->start_time;
                    $old_reservation_end = $reservation->end_time;
                    $to = User::find($reservation->user_id)->email;
                    $old_reservation_user = $reservation->user_id;

                    //validate first
                    //check for overlaps in all the rooms
                    $has_no_overlap = $this->r_model->hasNoOverlapEdit(array($room), $_start_dt, $_end_dt, $id);
                    //If a boolean is returned i.e. true, then there is no overlap
                    if (is_bool($has_no_overlap) == true) {
                        $reservation->start_time = $_start_dt;
                        $reservation->end_time = $_end_dt;
                        $reservation->reservation_name = Input::get('name');
                        $reservation->reservation_desc = Input::get('description');
                        $reservation->dept_id = $department;
                        $reservation->room_id = $room;
                        $creatd_for = Input::get('user');
                        $reservation->created_for = $creatd_for;
                        $reservation->rr_type = Input::get('rr_type');


                        if ($reservation->save()) {

                            //Send notification
                            if ($reservation->user_id != Auth::user()->user_id) {
                                $data = $this->mail->data_edit($reservation->user_id, $reservation->room_id, $reservation->reservation_room_id, '', '',$old_reservation_room,$old_reservation_start,$old_reservation_end,$old_reservation_name);
                                $subject = "Room Reservation Update";

                                $to_for = User::findOrFail($creatd_for)->email;
                                $tmpl = 'mail_temps/edited';
                                Emailr::queue($tmpl,false, $data, $to ,$subject,$to_for);
                            }
                            $msg_return = array('type' => 'notice');
                            echo json_encode($msg_return);
                        } else {
                            echo 'Your update cannot be performed due to conflicts'; //return Redirect::back()->withInput()->withErrors('error', 'Your update is seeming to bring up a problem');
                        }
                    } else {
                        $rtn = array('type' => 'error', $has_no_overlap);
                        echo json_encode($rtn);
                    }

                } catch (Exception $exc) {
                    echo $exc;
                }


            } else {
                $validator_errors = $validator->errors()->all();
                $validator_errors['type'] = "error";
                echo json_encode($validator_errors);
            }


        } else {
            return Redirect::to('reservation/room');
        }

    }


    public function rr_destroy($id)
    {
        if (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin()) {

            $reservation = ReservationRoom::findOrFail($id);
            if ($reservation) {
                try {
                    if ($reservation->user_id != Auth::user()->user_id) {
                        $data = $this->mail->data($reservation->user_id, $reservation->room_id, $reservation->reservation_room_id, '', '');
                        $subject = "Room Reservation Deletion";
                        $to = User::find($reservation->user_id)->email;

                        $to_for = User::findOrFail($reservation->created_for)->email;

                        $tmpl = 'mail_temps/deleted';
                        Emailr::queue($tmpl,false, $data, $to ,$subject,$to_for);
                    }
                    $reservation->delete();
                    return Redirect::to('reservation/room')->with('notice', 'Reservation deleted.');
                } catch (Exception $exc) {
                    return Redirect::to('reservation/room')->with('error', 'Unable to delete reservation. Segmentation has already been done');
                }
            } else {
                return Redirect::to('reservation/room');
            }


        } else {
            return Redirect::to('reservation/room');
        }
    }

    public function delete_ajax()
    {
        Log::alert('entered using function');
        if (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin()) {
          Log::alert('entered if');
            $id_str = Input::get('d_string');
            $id = explode('_', $id_str)[1];

            $reservation = ReservationRoom::findOrFail($id);

            if ($reservation) {

                try {
                    if ($reservation->user_id != Auth::user()->user_id) {
                        $data = $this->mail->data($reservation->user_id, $reservation->room_id, $reservation->reservation_room_id, '', '');
                        $subject = "Room Reservation Deletion";
                        $to = User::find($reservation->user_id)->email;
                        $to_for = User::findOrFail($reservation->created_for)->email;

                        $tmpl = 'mail_temps/deleted';
                        Emailr::queue($tmpl,false, $data, $to ,$subject,$to_for);
                    }
                    $reservation->delete();
                    return '1';
                } catch (Exception $exc) {
                    Log::alert($exc);
                    return $exc;
                }
            } else {
              Log::alert('reached here then failed');
                return '0';
            }

        } else {
          Log::alert(0);
            return '0';
        }

    }

    /**
     *
     * @param type $start_date
     * @param type $end_date
     * @param type $rr_end_date
     * @return boolean|string
     */
    public function validateDateRanges($start_date, $end_date, $rr_end_date = '')
    {

        //Convert them to the neccessary formats
        $_start_dtm = strtotime($start_date);
        $_end_dtm = strtotime($end_date);
        $_rr_end_dt = strtotime($rr_end_date);

        if ($_start_dtm < time()) {
            return "You can not select a past date";

        } elseif ($_start_dtm >= $_end_dtm) {
            return 'Start Time must be less than End.';
        } else {

            if ($rr_end_date != '') {
                if ($_start_dtm >= $_rr_end_dt || $_end_dtm >= $_rr_end_dt) {
                    return 'Repeat end date must be greater than start date ';
                }
            }
            return true;
        }
    }

    public function createReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $rr_type, $repeat_arr = "", $r_for)
    {

        //Separate repeat
        if (!is_array($repeat_arr)) {
            //Single entry
            return $this->createSingleReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $r_for, $rr_type);
        } else {
            //repeat reservation
            return $this->createRepeatReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $repeat_arr, $r_for, $rr_type);
        }
    }

    /**
     * Create a single entry reservation
     * @param $start_dt
     * @param $end_dt
     * @param $r_name
     * @param $r_desc
     * @param $r_rooms
     * @param $r_dept
     * @param $r_for
     * @param $rr_type
     * @return bool
     */
    public function createSingleReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $r_for, $rr_type)
    {
        return $this->r_model->createSingleReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $r_for, $rr_type);
    }

    /**
     * Create a repeat reservation
     * @param type $start_dt
     * @param type $end_dt
     * @param type $r_name
     * @param type $r_desc
     * @param type $r_rooms
     * @param type $r_dept
     * @param type $repeat_arr
     * @return type
     */
    public function createRepeatReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $repeat_arr, $r_for, $rr_type)
    {
        return $this->r_model->createRepeatReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $repeat_arr, $r_for, $rr_type);
    }

    /**
     * Get the reservation table for index
     */
    public function getDatatable()
    {

        if (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin()) {
            $rreservations = DB::table('reservation_rooms')->select('reservation_room_id', 'reservation_name', 'room_name', 'fname', 'lname', 'dept_name', 'approval', 'start_time', 'end_time', 'reservation_rooms.created_at as rdate')->join('rooms', 'reservation_rooms.room_id', '=', 'rooms.room_id')->join('users', 'reservation_rooms.user_id', '=', 'users.user_id')->join('depts', 'depts.dept_id', '=', 'reservation_rooms.dept_id')->orderBy('reservation_rooms.created_at', 'desc');
            $reserved_by = new \Chumper\Datatable\Columns\FunctionColumn('reserved_by', function ($row) {
                return $row->fname . ' ' . $row->lname;
            });
            $duration = new \Chumper\Datatable\Columns\FunctionColumn('duration', function ($row) {
                return date('jS F (D) Y', strtotime($row->start_time)) . ' @ ' . date('g:ia', strtotime($row->start_time)) . ' - ' . date('g:ia', strtotime($row->end_time));
            });
            $rdate = new \Chumper\Datatable\Columns\FunctionColumn('rrdate', function ($row) {
                return date('jS F Y', strtotime($row->rdate));
            });
            $action_links = new \Chumper\Datatable\Columns\FunctionColumn('action_links',
                function ($row) {
                    $rr_id = $row->reservation_room_id;
                    $edit_url = URL::to("reservation/room/" . $rr_id . "/edit");
                    $status = $row->approval;
                    $return_links = ($status == "Approved") ? "<div class='action_div'><span class='fa fa-thumbs-o-up' title='Approved' style='color:green' />&nbsp;" : "<div class='action_div'><span title='Not Approved' class='fa fa-thumbs-o-down' style='color:darkred' />&nbsp;";
                    $return_links .= "<a class='delete_rr' title='Delete' href='#' id='delete_$rr_id'><span class='fa fa-trash-o'/></a>&nbsp;";

                    $detail = ReservationRoomDetails::where('reservation_room_id', '=', $rr_id)->count();

                    if ($detail == 0) {
                        $edit_dt_url = URL::to('reservation/details/create/' . $rr_id);
                        $return_links .= "<a title='Add Details' href='$edit_dt_url'><span class='fa fa-plus-circle'/></a></div>";
                    } else {
                        $edit_dt_url2 = URL::to('reservation/details/' . $rr_id . '/edit');
                        $return_links .= "<a title='Edit details recorded' href='$edit_dt_url2'><span class='fa fa-pencil-square-o'/></a></div>";
                    }

                    return $return_links;
                });

            return Datatable::query($rreservations)
                ->showColumns('room_name', 'reservation_name', 'duration', 'rrdate', 'reserved_by', 'dept_name', 'action_links')
                ->addColumn($reserved_by)
                ->addColumn($duration)
                ->addColumn($rdate)
                ->addColumn($action_links)
                ->searchColumns('room_name', 'reservation_name', 'dept_name', 'approval', 'fname')
                ->orderColumns('created_at', 'room_name', 'dept_name', 'approval', 'reservation_name')
                ->make();
        } else {
            //@TODO : Enforce permissions - own reservations? Departmental access? Segmentation ?
            $rreservations = DB::table('reservation_rooms')->select('reservation_room_id', 'reservation_name', 'room_name', 'fname', 'lname', 'dept_name', 'approval', 'start_time', 'end_time')->join('rooms', 'reservation_rooms.room_id', '=', 'rooms.room_id')->join('users', 'reservation_rooms.user_id', '=', 'users.user_id')->join('depts', 'depts.dept_id', '=', 'reservation_rooms.dept_id')->where('users.user_id', Auth::user()->user_id)->orderBy('reservation_rooms.created_at', 'desc');
            $reserved_by = new \Chumper\Datatable\Columns\FunctionColumn('reserved_by', function ($row) {
                return $row->fname . ' ' . $row->lname;
            });
            $duration = new \Chumper\Datatable\Columns\FunctionColumn('duration', function ($row) {
                return date('jS F (D) Y', strtotime($row->start_time)) . ' @ ' . date('g:ia', strtotime($row->start_time)) . ' - ' . date('g:ia', strtotime($row->end_time));
            });
            $action_links = new \Chumper\Datatable\Columns\FunctionColumn('action_links',
                function ($row) {
                    $rr_id = $row->reservation_room_id;
                    $edit_url = URL::to("reservation/room/" . $rr_id . "/edit");
                    $status = $row->approval;
                    $return_links = ($status == "Approved") ? "<div class='action_div'><span class='fa fa-thumbs-o-up' title='Approved' style='color:green' />&nbsp;" : "<div class='action_div'><span title='Not Approved' class='fa fa-thumbs-o-down' style='color:darkred' />&nbsp;";
                    $return_links .= "<a title='Edit' href='$edit_url'><span class='fa fa-edit'/></a>&nbsp;";

                    $detail = ReservationRoomDetails::where('reservation_room_id', '=', $rr_id)->count();
                    if ($detail == 0) {
                        $edit_dt_url = URL::to('reservation/details/create/' . $rr_id);
                        $return_links .= "<a title='Add Details' href='$edit_dt_url'><span class='fa fa-pencil'/></a></div>";
                    } else {
                        $set_detail = ReservationRoomDetails::where('reservation_room_id', '=', $rr_id)->pluck('reservation_id');
                        $edit_dt_url2 = URL::to('reservation/details/' . $set_detail . '/edit');
                        $return_links .= "<a title='Edit details recorded' href='$edit_dt_url2'><span class='fa fa-file'/></a></div>";
                    }

                    return $return_links;
                });

            return Datatable::query($rreservations)
                ->showColumns('room_name', 'reservation_name', 'duration', 'reserved_by', 'dept_name', 'action_links')
                ->addColumn($reserved_by)
                ->addColumn($duration)
                ->addColumn($action_links)
                ->searchColumns('room_name', 'reservation_name', 'dept_name', 'approval', 'fname')
                ->orderColumns('created_at', 'room_name', 'dept_name', 'approval', 'reservation_name')
                ->make();


        }

    }

}
