<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of SettingsController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */

class SettingsController extends BaseController
{

    public function index(){

        $company_name =  Settings::getVal('company_name');
        $application_name =  Settings::getVal('application_name');
        $email =  Settings::getVal('email');
        $holidays =  Settings::getCurrentHolidays( Settings::getVal('holidays') );
        $calendar_end = Settings::getVal('calendar_end');
        $calendar_start = Settings::getVal('calendar_start');
        $company_website = Settings::getVal('company_website');
        $events = Events::get();

        return View::make('settings/index',
            [
             'event_subscribers' => Subscription::get(),
             'company_name' => $company_name,
             'application_name' => $application_name,
             'email' => $email,
             'holidays' => $holidays,
             'calendar_start' => $calendar_start,
             'calendar_end' => $calendar_end,
             'company_website' => $company_website,
             'events' => $events,
             'users' => User::getUserDropdown(),
             'notification_event' => Events::where('enabled','=','1')->get(),
             'notification_enabled' => [ 1 => 'Yes', 0 => 'No' ],
             'reminder_interval' => ['3d'=>'3 Days Before','5d'=>'5 Days Before','6d' => '6 Days Before','weekly' => 'Weekly','monthly'=>'Monthly']
            ]);

    }

    public function add_reminder(){

        return View::make('settings/add_reminder',[
            'reminder_interval' => Events::getReminderIntervals(),
            'faculty' => User::getFaculty(),
            'rooms' => Room::lists('room_name','room_id'),
            'users' => User::getUserDropdown(),
            'roles' => Role::lists('name','role_id'),
            'events' => Events::where('type','=',Subscription::TYPE_REMINDER_SUBSCRIPTION)->where('enabled','=','1')->lists('description','id'),

        ]);
    }
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(){

        $company_name = Input::get('company_name');
        $application_name = Input::get('application_name');
        $email = Input::get('email');
        $calendar_start = Input::get('calendar_start');
        $calendar_end = Input::get('calendar_end');
        $company_website = Input::get('company_website');
        $notifications_enabled = Input::get('notification_enabled');
        $notification_events = Input::get('notification_events');

        $holidays = Settings::getHolidayDBString(Input::get('input_holidays'));


        Settings::updateVal('company_name',$company_name);
        Settings::updateVal('application_name',$application_name);
        Settings::updateVal('email',$email);
        Settings::updateVal('holidays',$holidays);
        Settings::updateVal('calendar_start',$calendar_start);
        Settings::updateVal('calendar_end',$calendar_end);
        Settings::updateVal('company_website', $company_website);
        Settings::updateVal('notifications_enabled',$notifications_enabled);

        //update statuses of selected
        Events::updateStatuses($notification_events);

        return Redirect::to('settings')->with('notice','Settings updated.');;

    }

    /**
     * @return string
     */
    public function addEventSubscription(){
        $subscriber = Input::get('user');
        $subscribed_to = Input::get('event');
        $event_id = $subscribed_to;

        return Subscription::addEventSubscription($subscriber,$subscribed_to);
    }

    /**
     * Add reminderSubscription
     */
    public function addReminderSubscription(){

        $role = Input::get('roles');
        $user = Input::get('user');
        $reminder = Input::get('reminder');
        $reminder_interval = Input::get('reminder_interval');
        $rooms = Input::get('rooms');
        $faculty = Input::get('faculty');

        $subscriber = $role.':'.$user;

        $room_str = ( count($rooms) > 0 ) ? implode(',',$rooms) : "";

        $subscribed_to = ($room_str != "") ? $room_str : $faculty;
        $event_id = $reminder;

        $recurring = ($reminder_interval == "weekly" || $reminder_interval == "monthly") ? 1 : 0;


        if ( $user != $faculty ){

            Subscription::create([
                'subscriber' => $subscriber,
                'subscribed_to' => $subscribed_to,
                'event_id' => $event_id,
                'recurring' => $recurring,
                'interval' => $reminder_interval
            ]);

            echo json_encode(['type' => 'notice']);
        }else{
            echo json_encode(array('type' => 'error', 'Error! User cannot be subscribed to their own notifications - This exists by default!'));
        }

    }

}