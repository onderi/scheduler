<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of RoomController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class RoomController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $rooms = Room::with('building')->with('area')->get();
        Return View::make('room/index', array('rooms' => $rooms));
    }

    /**
     * Display Add new Room form
     * @return type
     */
    public function create() {
        Return View::make('room/form_room', array(
                    'areas' => array('' => 'Please select one') + Area::lists('area_name', 'area_id')
        ));
    }

    /**
     * Save the new room
     * @return type
     */
    public function store() {
        //validate the user input
        $v = Room::validate(Input::all());

        if ($v->passes()) {
            Room::create(array(
                'room_name' => Input::get('name'),
                'capacity' => Input::get('capacity'),
                'area_id' => Input::get('area'),
                'ccode'=>Input::get('ccode')
                //'building_id' => Input::get('building')
            ));
            return Redirect::to('rooms')->with('notice', 'Room added');
        } else {
            return Redirect::to('room/add')->withInput()->withErrors($v->errors());
        }
    }

    /**
     * Display the edit form
     * @return type
     */
    public function edit($id) {

        $room = Room::with('building')->with('area')->find($id);

        //if it exists
        if ($room) {
            return View::make('room/form_room_edit', array(
                        'room' => $room,
                        //'buildings' => Building::lists('building_name', 'building_id'),
                        'areas' => Area::lists('area_name', 'area_id')
            ));
        } else {
            return Redirect::to('rooms');
        }
    }

    /**
     * Save the edited room
     * @param type $id
     * @return type
     */
    public function update($id) {

        $room = Room::find($id);

        //if it exists
        if ($room) {
            //validate
            $v = Room::validate(Input::all());
            if ($v->passes()) {
                $room->room_name = Input::get('name');
                $room->capacity = Input::get('capacity');
                $room->area_id = Input::get('area');
                //$room->building_id > Input::get('building');

                try {
                    $room->save();
                    return Redirect::to('rooms')->with('notice', 'Room updated');
                } catch (Exception $ex) {
                    return Redirect::to('room/' . $id . '/edit')->withInput()->withErrors('Room not updated');
                }
            } else {
                return Redirect::to("room/$id/edit")->withInput()->withErrors($v->errors());
            }
        } else {
            return Redirect::to('rooms');
        }
    }

    /**
     * Delete Room
     * @param type $id
     * @return type
     */
    public function destroy($id) {
        $room = Room::find($id);

        if ($room) {
            try {
                $room->delete();
                return Redirect::to('rooms')->with('notice', 'Room deleted');
            } catch (Exception $exc) {
                return Redirect::to('rooms')->with('error', 'Unable to delete Room');
            }
        } else {
            return Redirect::to('rooms');
        }
    }

}
