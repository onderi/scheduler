<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of UserController
 *
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 *
 *
 * @CodeGems : Isaiah 50:7
 * =======================
 *
 *  -- Because the Lord God is my helper I will not be dismayed;
 *  -- Therefore I have set my face like a flint to do his will and I know that I will triumph.
 *
 *     (NIV)
 *  -- Because the Sovereign LORD helps me, I will not be disgraced.
 *  -- Therefore have I set my face like flint, and I know I will not be put to shame.
 */
class UserController extends BaseController {
    public function __construct(){
        $this->beforeFilter('csrf',array('on'=>'post'));
    }


    public function index() {
        $users = User::with('status')->with('departments')->with('type')->get();
//        dd($users[0]);

        Return View::make('user/index', array('users' => $users));
    }

    /**
     * Displays the form for account creation
     */
    public function add() {


        $statuses = array('' => 'Please Select One');
        return View::make('user/form_user', array(
                'departments' => Department::lists('dept_abbrev', 'dept_id'),
                'statuses' => $statuses + Status::lists('status_name', 'status_id'),
                'roles' => Role::lists('name', 'role_id'),
                'user_type' => UserType::orderBy('name')->lists('name', 'id')
            )
        );
    }

    /**
     * Stores new account
     *
     */
    public function store() {
        $v = User::validate(Input::all());

        if ($v->passes()) {

                $user = User::create(array(
                    'fname' => Input::get('fname'),
                    'lname' => Input::get('lname'),
                    'username' => Input::get('username'),
                    'email' => Input::get('email'),
                    'phone' => Input::get('phone'),
                    'status_id' => Input::get('status'),
                    'gender' => Input::get('gender'),
                    'notification' => Input::get('notification'),
                    'user_type' => Input::get('user_type')
                ));

                if (Input::get('department')) {
                    $user->departments()->sync(Input::get('department'));
                }
                //Attach roles to user.

                $user->attachRoles(Input::get('roles'));
                /*foreach(Input::get('roles')as $role){
                DB::table('user_roles')->insert([
                    'user_id'=>$user->user_id,
                    'role_id'=>$role]
                    );
            }*/
                return Redirect::to('users')->with('notice', 'User added.');

        } else {
            return Redirect::to('user/add')->withInput()->withErrors($v);
        }
    }

    /**
     * Display form to edit specific user
     * @param type $id
     * @return type
     */
    public function edit($id) {
        $user = User::find($id);


        if ($user) {

            //Current Department(s)
            if (count($user->departments) > 0) {
                $current_dept = $user->departments;
                //Get the permissions
                $current_department = array_pluck($current_dept, 'dept_id');
                //Convert into nice string
                $final_current_department = implode(',', $current_department);
            } else {
                $final_current_department = '0';
            }

            //Current Role(s)
            if (count($user->roles) > 0) {
                $current_role = $user->roles;
                //Get the permissions
                $current_roles = array_pluck($current_role, 'role_id');
                //Convert into nice string
                $final_current_role = implode(',', $current_roles);
            } else {
                $final_current_role = '0';
            }
            $role=DB::table('user_roles')->where('user_id','=',$id)->lists('role_id');


            return View::make('user/form_user_edit', array(
                'user' => $user,
                'roles' => Role::lists('name', 'role_id'),
                'departments' => Department::lists('dept_abbrev', 'dept_id'),
                'statuses' => Status::lists('status_name', 'status_id'),
                'current_dept' => $final_current_department,
                'current_role' => $final_current_role,
                'role' => $role,
                'user_type' =>  UserType::orderBy('name')->lists('name', 'id')
            ));

        } else {
            return Redirect::to('users');
        }
    }

    public function edit_user($id){
        $user = User::find($id);


        if ($user) {

            //Current Department(s)
            if (count($user->departments) > 0) {
                $current_dept = $user->departments;
                //Get the permissions
                $current_department = array_pluck($current_dept, 'dept_id');
                //Convert into nice string
                $final_current_department = implode(',', $current_department);
            } else {
                $final_current_department = '0';
            }

            //Current Role(s)
            if (count($user->roles) > 0) {
                $current_role = $user->roles;
                //Get the permissions
                $current_roles = array_pluck($current_role, 'role_id');
                //Convert into nice string
                $final_current_role = implode(',', $current_roles);
            } else {
                $final_current_role = '0';
            }
            $role=DB::table('user_roles')->where('user_id','=',$id)->lists('role_id');

            return View::make('user/form_user_edit_normal', array(
                'user' => $user,
                'departments' => Department::lists('dept_abbrev', 'dept_id'),
                'current_dept' => $final_current_department,
                'current_role' => $final_current_role,
                'user_type' =>  UserType::orderBy('name')->lists('name', 'id')
            ));
        } else {
            return Redirect::to('users');
        }
    }

    public function update($id) {

        $arr = array('user_id' => $id);

        $v = User::validate(Input::all() + $arr, TRUE);

        if ($v->passes()) {

            $user = User::find($id);

            //fields Only
            $user->fname = Input::get('fname');
            $user->lname = Input::get('lname');
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->phone = Input::get('phone');
            $user->gender = Input::get('gender');
            $user->notification_status = Input::get('notification');
            $user->status_id = Input::get('status');
            $user->user_type = Input::get('user_type');

            //departments
            $user->departments()->sync( Input::get('department') );

            //Update attached roles to user.
            $user->roles()->sync( Input::get('roles') );

            $user->save();

            return Redirect::to('users')->with('notice', 'User updated.');

        } else {
            return Redirect::to('user/' . $id . '/edit')->withInput()->withErrors($v);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function user_update($id){
        $user = User::findOrFail($id);
        if($user){
            $user->fname = Input::get('fname');
            $user->lname = Input::get('lname');
            $user->phone = Input::get('phone');
            $user->gender = Input::get('gender');
            $user->notification_status = Input::get('notification');
            $user->save();
            return Redirect::to('user/' . $id . '/u_edit')->with('notice', 'Your Profile has been updated.');
        }else{
            return Redirect::to('/');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $user = User::find($id);
        $user->roles()->sync([]);
        try{
            $user->delete();

            return Redirect::to('users')->with('notice', 'User deleted.');
        }
        catch (Exception $exc) {
            return Redirect::to('users')->with('error','Integrity constraint violated : User has made bookings');
        }

    }

    /**
     * Activate or deactivate user
     * @return Result String
     * Ajax Method
     */
    public function status() {
        $status = Input::get('status');
        $userid = Input::get('userid');

        //Apply the passed status
        if ($status) {
            //select id where status name is $status
            $status_id = Status::where('status_name', '=', $status)->first();
            try {
                DB::table('users')->where('user_id', $userid)->update(array('status_id' => $status_id->status_id));
                echo 'saved';
            } catch (Exception $exc) {
                echo 'error';
            }
        } else {
            //return Redirect::to('users');
            echo 'error';
        }
    }

    /**
     * View Current User Roles
     * Ajax
     */
    public function assigned_roles() {
        $userid = Input::get('userid');
        $user = User::find($userid);
        if ($user) {
            //Current Role(s)
            if (count($user->roles) > 0) {
                $current_role = $user->roles;
                //Get the permissions
                $current_roles = array_pluck($current_role, 'name');

                //Convert into nice formatted html string
                $roles = '<u ><h4>' . $user->fname . ' ' . $user->lname . "'s Role(s)</h4></u>";
                foreach ($current_roles as $r) {
                    $roles = $roles . '<li>' . $r . '</li>';
                }
                echo $roles;
            } else {
                echo '<li>None Assigned</li>';
            }
        } else {
            echo 'error';
        }
    }

    /**
     * Displays the login form
     *
     */
    public function login() {
        if(Auth::check()){
            return Redirect::to('/');
        }
        else{
            Session::remove('error-perms');
            return View::make('user/login');
        }
    }

    /**
     * Attempt to do login
     *
     */
    public function do_login() {
        include ( base_path() . '/vendor/adLDAP/adLDAP.php' );

        try {

            $adldap = new adLDAP();

            $username = Input::get('username');
            $password = Input::get('password');
            $ldap_auth = $adldap->user()->authenticate($username, $password);

            if ($ldap_auth) {
                //Check whether the user is in the DB.
                $docs_user =   User::where('username', '=', $username)->get();
                if (count( $docs_user ) == 0 ) {

                    $ldap_user_info = $adldap->user()->info($username);

                    $full_name = $ldap_user_info[0]["displayname"][0];
                    $full_name_array = explode(' ', $full_name);

                    $fname = $full_name_array[0];
                    $lname = $full_name_array[1];
                    $email = $username . '@strathmore.edu';

                    //Create user if not exist.
                    $user = new User();
                    $user->username = $username;
                    $user->fname = $fname;
                    $user->lname = $lname;
                    $user->email = $email;
                    $user->status_id='1';
                    //set a default password - random string
                    $user->password = Hash::make('sbs@'.date('Y'));

                    //save the user
                    $user->save();

                    //Find the ID of the general role.
                    $role = Role::getRoleByName('User');

                    //Attach the role
                    $user->attachRole($role);

                    $created_user = User::find($user->user_id);
                } else {

                    $user_id = User::where('username', '=', $username)->first()->user_id;
                    $created_user = User::find($user_id);
                }
                //dd($created_user);
                Auth::login($created_user);
                if (Auth::check()){
                    return Redirect::intended('/');
                }
                else{
                    return Redirect::to('login')->with('error', 'Unable to login, unhandled exception, please contact administrator');
                }
            }
            else {
                return Redirect::to('login')->with('error', 'Unable to login, Wrong username / password.');
            }
        } catch (Exception $e) {
            Log::error($e);
            return Redirect::to('login')->with('error',$e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout() {
        Auth::logout();

        return Redirect::to('/')->with('notice','Logged out');
    }

}
