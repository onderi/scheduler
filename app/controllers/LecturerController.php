<?php

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LecturerController extends \BaseController {

	// View all yet to be confirmed bookings
	public function booking()
	{
		$faculty = \DB::select('SELECT `sfp_reservations`.`reservation_id`,
								    `sfp_reservations`.`reservation_desc`,
								    `sfp_reservation_statuses`.`reservation_status_name`,
								    `sfp_reservations`.`reservation_room_id`,
								    `sfp_courses`.`course_title`,
								    `sfp_course_categories`.`name`,
								    `a`.`fname`,
								    `a`.`lname`,
								    `sfp_reservations`.`start_time`,
								    `sfp_reservations`.`end_time`,
								    `sfp_reservations`.`ref_number`,
								    `sfp_reservations`.`last_modified`,
								    `sfp_reservations`.`created_at`,
								    `sfp_reservations`.`updated_at`,
								    `b`.`fname`,
								    `b`.`lname`,
								    `sfp_reservations`.`hours`,
								    `sfp_reservations`.`status`
								FROM `scheduler`.`sfp_reservations`
								INNER JOIN sfp_users a ON a.user_id = sfp_reservations.created_by
								INNER JOIN sfp_users b ON b.user_id = sfp_reservations.faculty
								INNER JOIN sfp_courses ON sfp_courses.course_id = sfp_reservations.course_id
								INNER JOIN sfp_course_categories ON sfp_course_categories.id = sfp_reservations.category
								INNER JOIN sfp_reservation_statuses ON sfp_reservation_statuses.reservation_status_id = sfp_reservations.reservation_status_id
								WHERE status = 4
								ORDER BY end_time DESC;
								');

		$state = [
			'' => 'Please select one...',
			1 => 'Booked',
			2 => 'Not Booked'];


		return View::make('lecturer/booking', array('faculty' => $faculty, 'state' => $state));
	}

	// Confirm or reject a booking
	public function confirm_booking($reservation_id){
		// Update confirmed booking
		$faculty_fname = "David";
		$faculty_lname = "Mathuva";
		$program_name = "SMLP 2017";
		$date = "21/11/2017";

        DB::table('reservations')
        	->where('reservation_id', $reservation_id)
        	->update([
        		'approved_by' => 132,
        		'status' => 3]);

        // Send confirmation email
        $data = ['faculty_fname' => $faculty_fname, 'faculty_lname' => $faculty_lname, 'program_name' => $program_name, 'date' => $date, 'reservation_id' => $reservation_id];

        Mail::send('lecturer/mail/confirm_booking', $data, function($message)
        {
            $message->to('kkinoti@strathmore.edu')->subject('SBS Faculty Planner');
            $message->from('sfplanner@strathmore.edu','SBS Faculty Planner');
        });

		return Redirect::to('lecturer/booking')->with('notice','Booking has been confirmed');
	}

	public function reject_booking($reservation_id){
		// Reject a booking
		DB::table('reservations')
			->where('reservation_id', $reservation_id)
			->update([
				'approved_by' => 132,
				'status' => 5]);

		// Send confirmation email
		$data = ['name' => 'Kennedy Kinoti'];

		Mail::send('lecturer/mail/reject_booking', $data, function($message)
		{
		    $message->to('kkinoti@strathmore.edu')->subject('SBS Faculty Planner');
		    $message->from('sfplanner@strathmore.edu','SBS Faculty Planner');
		});

		return Redirect::to('lecturer/booking')->with('notice','Booking has been cancelled');
	}

	// View only confirmed and rejected bookings
	public function confirmed_booking()
	{
		$faculty = \DB::select('SELECT `sfp_reservations`.`reservation_id`,
								    `sfp_reservations`.`reservation_desc`,
								    `sfp_reservation_statuses`.`reservation_status_name`,
								    `sfp_reservations`.`reservation_room_id`,
								    `sfp_courses`.`course_title`,
								    `sfp_course_categories`.`name`,
								    `a`.`fname`,
								    `a`.`lname`,
								    `sfp_reservations`.`start_time`,
								    `sfp_reservations`.`end_time`,
								    `sfp_reservations`.`ref_number`,
								    `sfp_reservations`.`last_modified`,
								    `sfp_reservations`.`created_at`,
								    `sfp_reservations`.`updated_at`,
								    `b`.`fname`,
								    `b`.`lname`,
								    `sfp_reservations`.`hours`,
								    `sfp_reservations`.`status`
								FROM `scheduler`.`sfp_reservations`
								INNER JOIN sfp_users a ON a.user_id = sfp_reservations.created_by
								INNER JOIN sfp_users b ON b.user_id = sfp_reservations.faculty
								INNER JOIN sfp_courses ON sfp_courses.course_id = sfp_reservations.course_id
								INNER JOIN sfp_course_categories ON sfp_course_categories.id = sfp_reservations.category
								INNER JOIN sfp_reservation_statuses ON sfp_reservation_statuses.reservation_status_id = sfp_reservations.reservation_status_id
								WHERE status != 4
								ORDER BY end_time DESC;
								');

		$state = [
			'' => 'Please select one...',
			1 => 'Booked',
			2 => 'Not Booked'];

		return View::make('lecturer/confirmed_booking', array('faculty' => $faculty, 'state' => $state));
	}

	// Book a lecturer (Depreciated for now)
	public function book()
	{
		$faculty = \DB::select('SELECT sfp_users.user_id, sfp_users.fname, sfp_users.lname, sfp_depts.dept_abbrev FROM sfp_users
									INNER JOIN sfp_user_depts ON sfp_users.user_id = sfp_user_depts.user_id
									INNER JOIN sfp_depts ON sfp_user_depts.dept_id = sfp_depts.dept_id
									WHERE dept_abbrev = "Faculty"
									ORDER BY fname ASC');

		$courses = \DB::select('SELECT course_id, course_title FROM sfp_courses');

		return View::make('lecturer/book', ['faculty' => $faculty, 'courses' => $courses]);
	}

	// AJAX to get lecturer information
	public function book_hours()
	{

	}

	// Send email for a confirmed booking
	public function book_confirm()
	{
		$data = ['name' => 'Kennedy Kinoti'];

		Mail::send('lecturer/mail/confirm_reservation', $data, function($message)
		{
		    $message->to('kkinoti@strathmore.edu')->subject('SBS Faculty Planner');
		    $message->from('sfplanner@strathmore.edu','SBS Faculty Planner');
		});

		return Redirect::to('lecturer/book')->with('notice','Lecturer Booking Confirmed');
	}

	// A summary of lecturer and their hours
	public function lecturers()
	{

		$faculty = \DB::select('SELECT sfp_users.user_id, sfp_users.fname, sfp_users.lname, sfp_users.email, sfp_depts.dept_abbrev, sfp_user_type.hours, sec_to_time(sum(time_to_sec((timediff(sfp_reservations.end_time,sfp_reservations.start_time))))) as duration, group_concat(distinct(sfp_lecturer_hours.additional_hours)) as added_hours
								FROM sfp_users
								INNER JOIN sfp_user_depts ON sfp_users.user_id = sfp_user_depts.user_id
								INNER JOIN sfp_depts ON sfp_user_depts.dept_id = sfp_depts.dept_id
								INNER JOIN sfp_user_type ON sfp_user_type.id = sfp_users.user_type
								INNER JOIN sfp_lecturer_hours ON sfp_lecturer_hours.lecturer_id = sfp_users.user_id
								JOIN sfp_reservations ON sfp_reservations.faculty = sfp_users.user_id
								WHERE dept_abbrev = "Faculty"
								GROUP BY faculty

							');

		return View::make('lecturer/lecturers', array('faculty' => $faculty));
	}

	// Add a specific lecturer hours
	public function add_hours($user_id)
	{
		$faculty = \DB::select('SELECT sfp_users.user_id, sfp_users.fname, sfp_users.lname, sfp_users.email, sfp_depts.dept_abbrev, sfp_user_type.hours, sec_to_time(sum(time_to_sec((timediff(sfp_reservations.end_time,sfp_reservations.start_time))))) as duration, group_concat(sfp_lecturer_hours.additional_hours) as added_hours
								FROM sfp_users
								INNER JOIN sfp_user_depts ON sfp_users.user_id = sfp_user_depts.user_id
								INNER JOIN sfp_depts ON sfp_user_depts.dept_id = sfp_depts.dept_id
								INNER JOIN sfp_user_type ON sfp_user_type.id = sfp_users.user_type
								INNER JOIN sfp_lecturer_hours ON sfp_lecturer_hours.lecturer_id = sfp_users.user_id
								JOIN sfp_reservations ON sfp_reservations.faculty = sfp_users.user_id
								WHERE dept_abbrev = "Faculty" && sfp_users.user_id = '.$user_id.'
								GROUP BY faculty

							');

		return View::make('lecturer/add_hours', array('faculty' => $faculty));
	}

	// Update the specific lecturer hours
	public function update_lec_hours(){
		// Add a lecturer more teaching hours
		$lecturer_id = Input::get('lecturer_id');
		$time = Input::get('no_hours');

		\DB::update('UPDATE `scheduler`.`sfp_lecturer_hours` SET `additional_hours`=`additional_hours`+'.$time.' WHERE `lecturer_id`='.$lecturer_id);

		return Redirect::to('lecturer/lecturers')->with('notice','Lecturer Added Teaching Hours');

	}


	public function materials()
	{
		$urgency = [
			'' => 'Please select one...',
			1 => 'Urgent',
			2 => 'Important',
			3 => 'Normal Priority',
			4 => 'Low'];

		$faculty = \DB::select('SELECT sfp_users.user_id, sfp_users.fname, sfp_users.lname, sfp_users.email, sfp_depts.dept_abbrev FROM sfp_users
				INNER JOIN sfp_user_depts ON sfp_users.user_id = sfp_user_depts.user_id
				INNER JOIN sfp_depts ON sfp_user_depts.dept_id = sfp_depts.dept_id
			 WHERE dept_abbrev = "Faculty"');

		$materials = \DB::select('SELECT name FROM sfp_materials');

		$course = \DB::select('SELECT course_title, category FROM sfp_courses');

		return View::make('lecturer/materials', array('urgency' => $urgency, 'faculty' => $faculty, 'course' => $course, 'materials' => $materials));
	}

	public function materials_request()
	{

		// Save data to database
        $course = Input::get('course');
        $lecturer = Input::get('lecturer');
        $materials = Input::get('materials');
        $urgency = Input::get('urgency');
        $start_date = Input::get('start_date');
        $end_date = Input::get('end_date');

        $data = array('lecturer' => $lecturer, 'course' => $course, 'urgency' => $urgency, 'materials' => $materials, 'start_date' => $start_date, 'end_date' => $end_date);

		Mail::send('lecturer/mail/materials_request', $data, function($message)
		{
		    $message->to('kkinoti@strathmore.edu')->subject('SBS Faculty Planner');
		    $message->from('sfplanner@strathmore.edu','SBS Faculty Planner');
		});

       	\DB::insert('INSERT INTO `scheduler`.`sfp_materials_req` (`course`,`lecturer`,`materials`,`priority`,`start_date`,`end_date`) VALUES (?, ?, ?, ?, ?, ?)', [$course, $lecturer, $materials, $urgency, $start_date, $end_date]);

        return Redirect::to('lecturer/materials')->with('notice','Materials Request Sent');

	}

	public function add_materials()
	{
		return View::make('lecturer/add_materials');
	}

	public function save_materials()
	{
		$material_name = Input::get('material_name');

		\DB::insert('INSERT INTO `scheduler`.`sfp_materials` (`name`) VALUES (?)',[$material_name]);

		return Redirect::to('lecturer/materials')->with('notice','New Materials Added');
	}
// timesheets section
	public function timesheets()
	{
		$faculty = \DB::select('SELECT faculty as id,
									`b`.`fname` as lec_fname,
									`b`.`lname` as lec_lname,
								    sec_to_time(sum(time_to_sec((timediff(end_time,start_time))))) as duration
								FROM `scheduler`.`sfp_reservations`
								INNER JOIN sfp_users b ON b.user_id = sfp_reservations.faculty
								WHERE status = 3
								GROUP BY faculty,fname, lname
								ORDER BY duration DESC');

		$state = [
			'' => 'Please select one...',
			1 => 'Booked',
			2 => 'Not Booked'];

		return View::make('lecturer/timesheets', array('faculty' => $faculty, 'state' => $state));
	}

	public function refresh_timesheets(){
		$faculty = \DB::select('SELECT faculty as id,
									`b`.`fname` as lec_fname,
									`b`.`lname` as lec_lname,
								    sec_to_time(sum(time_to_sec((timediff(end_time,start_time))))) as duration
								FROM `scheduler`.`sfp_reservations`
								INNER JOIN sfp_users b ON b.user_id = sfp_reservations.faculty
								WHERE start_time >= "'.Input::get("from").'" and end_time <= "'.Input::get("to").'" && faculty = '.Input::get("lecturer").'
								GROUP BY faculty,fname, lname
								ORDER BY duration DESC');


			// $view = View::make('lecturer/timesheettable',compact('faculty','state'))->render();
		return View::make('lecturer/timesheettable',array('faculty' => $faculty))->render();
		// return "ball";
	}

	public function viewtimesheets($id)
	{
		$faculty = \DB::select('SELECT `sfp_users`.`fname`,
								 `sfp_users`.`lname`,
								 `sfp_reservations`	.`faculty`,
								 `sfp_courses`.`course_title`,
								 `sfp_course_categories`.`name`,
								  timediff(end_time,start_time) as duration,
								  `sfp_reservations`.`ref_number`,
                                  `a`.`fname` as created_fname,
								  `a`.`lname` as created_lname,
                                  `b`.`fname` as approver_fname,
								  `b`.`lname` as approver_lname
								FROM scheduler.sfp_reservations
								INNER JOIN sfp_users ON user_id = sfp_reservations.faculty
								INNER JOIN sfp_courses ON sfp_courses.course_id = sfp_reservations.course_id
                                INNER JOIN sfp_users a ON a.user_id = sfp_reservations.created_by
                                INNER JOIN sfp_users b ON b.user_id = sfp_reservations.approved_by
								INNER JOIN sfp_course_categories ON sfp_course_categories.id = sfp_reservations.category
								WHERE status = 3 && faculty = ' .$id);

		return View::make('lecturer/viewtimesheets', array('faculty' => $faculty));
	}
	//workloads section of the controller
	public function workloads()
	{
		$faculty = \DB::select('SELECT faculty as id,
									`b`.`fname` as lec_fname,
									`b`.`lname` as lec_lname,
								    sec_to_time(sum(time_to_sec((timediff(end_time,start_time))))) as duration
								FROM `scheduler`.`sfp_reservations`
								INNER JOIN sfp_users b ON b.user_id = sfp_reservations.faculty
								WHERE reservation_status_id = 3
								GROUP BY faculty,fname, lname
								ORDER BY duration DESC');
								// selects the sum of durations from the db
								$total= \DB::select('SELECT sec_to_time(sum(time_to_sec(duration))) as dir from ( SELECT
													sec_to_time(sum(time_to_sec((timediff(end_time,start_time))))) as duration
													FROM `scheduler`.`sfp_reservations`
													INNER JOIN sfp_users b ON b.user_id = sfp_reservations.faculty
													WHERE reservation_status_id = 3
													GROUP BY faculty,fname, lname
													ORDER BY duration DESC ) as d');

		$state = [
			'' => 'Please select one...',
			1 => 'Booked',
			2 => 'Not Booked'];

		return View::make('lecturer/workloads', array('faculty' => $faculty,'total'=>$total, 'state' => $state));
	}
// activated when the refresh button is pressed
	public function refresh_workloads(){

	//	dd('hhe');

		$faculty = \DB::select('SELECT faculty as id,
									`b`.`fname` as lec_fname,
									`b`.`lname` as lec_lname,
								    sec_to_time(sum(time_to_sec((timediff(end_time,start_time))))) as duration
								FROM `scheduler`.`sfp_reservations`
								INNER JOIN sfp_users b ON b.user_id = sfp_reservations.faculty
								WHERE reservation_status_id = 3 and start_time >= "'.Input::get("from").'" and end_time <= "'.Input::get("to").'" && faculty = '.Input::get("lecturer").'
								GROUP BY faculty,fname, lname
								ORDER BY duration DESC');
								// selects the sum of durations from the db
			$total= \DB::select('SELECT sec_to_time(sum(time_to_sec(duration))) as dir from ( SELECT
								sec_to_time(sum(time_to_sec((timediff(end_time,start_time))))) as duration
								FROM `scheduler`.`sfp_reservations`
								INNER JOIN sfp_users b ON b.user_id = sfp_reservations.faculty
								WHERE reservation_status_id = 3 and start_time >= "'.Input::get("from").'" and end_time <= "'.Input::get("to").'" && faculty = '.Input::get("lecturer").'
								GROUP BY faculty,fname, lname
								ORDER BY duration DESC) as d');

		$state = [
			'' => 'Please select one...',
			1 => 'Booked',
			2 => 'Not Booked'];


		return View::make('lecturer/workloadstable',array('faculty' => $faculty,'total'=>$total))->render();

	}

	public function viewworkloads($id)
	{
		$faculty = \DB::select('SELECT `sfp_users`.`fname`,
								 `sfp_users`.`lname`,
								 `sfp_reservations`	.`faculty`,
								 `sfp_courses`.`course_title`,
								 `sfp_course_categories`.`name`,
								  timediff(end_time,start_time) as duration,
								  `sfp_reservations`.`ref_number`,
                                  `a`.`fname` as created_fname,
								  `a`.`lname` as created_lname
								FROM scheduler.sfp_reservations
								INNER JOIN sfp_users ON user_id = sfp_reservations.faculty
								INNER JOIN sfp_courses ON sfp_courses.course_id = sfp_reservations.course_id
                INNER JOIN sfp_users a ON a.user_id = sfp_reservations.created_by
								INNER JOIN sfp_course_categories ON sfp_course_categories.id = sfp_reservations.category
								WHERE sfp_reservations.faculty = ' .$id);

								//WHERE status = 3 && faculty = ' .$id

		return View::make('lecturer/viewworkloads', array('faculty' => $faculty));
	}

}
