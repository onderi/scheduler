<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of ImportController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class ImportController extends BaseController
{

    /**
     * @param $str
     * @return bool|string
     */
    protected function _getFormattedDate($str){
        return date('Y-m-d H:i:s',strtotime($str));
    }


    public function _initImport(){
        //memory limit stuff
        ini_set('max_execution_time',0);
        ini_set('memory_limit',-1);

        $filePath = app_path('resources/cal2017.csv');
        Excel::filter('chunk')->load($filePath)->chunk(50, function($results){
       // Excel::load($filePath, function($reader) {

            // Getting all results
           // $results = $reader->get();
            $i = 0;

            foreach($results as $r){

                $st = date('Y-m-d',strtotime($r['start']));
                $en = date('Y-m-d',strtotime($r['end']));
                $r_named = $r['name'];

                if ( $st == $en ){

                    $start_dt = $this->_getFormattedDate($r['start']);
                    $end_dt = $this->_getFormattedDate($r['end']);


                    $r_name = $r['name'];
                    $r_desc = $r_name;
                    $room = trim($r['room']);

                    $r_rooms = Room::getRoomByName($room);
                    $r_dept = Department::getDepartmentByName($r['department']);
                    $r_for = User::getUserByUsername($r['booked_for']);
                    $rr_type = $r['type'];

                    $rreserv = new ReservationRoom();

                    $rreserv->createSingleReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $r_for, $rr_type);

                }else{

                    $start_dt = $this->_getFormattedDate($r['start']);
                    $end_dt = $this->_getFormattedDate($r['end']);


                    $r_name = $r['name'];
                    $r_desc = $r_name;
                    $room = trim($r['room']);

                    $r_rooms = Room::getRoomByName($room);
                    $r_dept = Department::getDepartmentByName($r['department']);
                    $r_for = User::getUserByUsername($r['booked_for']);
                    $rr_type = $r['type'];

                    $rr_repeat = array(
                        'repeat_type' => 'DAILY',
                        'repeat_end' => $end_dt,
                        'repeat_days' => "",
                        'repeat_times' => 1
                    );

                    $rreserv = new ReservationRoom();

                    $rreserv->createRepeatReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $rr_repeat, $r_for, $rr_type);

                }

            $i++;

            }

        });

    }


    public function _cImport(){

        

    }


}