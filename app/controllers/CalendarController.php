<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */
use Illuminate\Support\Facades\Log;
/**
 * Description of CalendarController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class CalendarController extends BaseController {
    private $mail;

    //initialise the reservation model.
    public function __construct() {
        $this->mail=new Mails();
    }
    /**
     * Get all reservation types
     * @return type
     */
    public function getAllReservations() {

        $start = strtotime(Input::get('start'));//strtotime("2016-02-01");//
        $end = strtotime(Input::get('end'));//strtotime("2016-02-28");//
        $group = Input::get('room_area');

        $group = explode('_',$group);

        $room_area = $group[0];
        $room_g = $group[1];
        $room = $room_g;
        if( $room_g == "undefined" || $room_g == "" )
            $room = "";

        return Calendar::_getReservations($start,$end,$room_area,$room);
    }

    public function getAllCourseReservations(){


        $start = strtotime(Input::get('start'));//strtotime("2016-02-01");//
        $end = strtotime(Input::get('end'));//strtotime("2016-02-28");//
        $group = Input::get('room_area');

        $group = explode('_',$group);

        $room_area = $group[0];
        $room_g = $group[1];
        $room = $room_g;
        if( $room_g == "undefined" || $room_g == "" )
            $room = "";

        return Calendar::_getReservationsSegments($start,$end,$room_area,$room);


    }
    public function getAllTimesheets(){


        $start = strtotime(Input::get('start'));//strtotime("2016-02-01");//
        $end = strtotime(Input::get('end'));//strtotime("2016-02-28");//
        $group = Input::get('room_area');

        $group = explode('_',$group);

        $room_area = $group[0];
        $room_g = $group[1];
        $room = $room_g;
        if( $room_g == "undefined" || $room_g == "" )
            $room = "";
            // Log::alert('i was called');
        return Calendar::_getTimesheetReservationsSegments($start,$end,$room_area,$room);


    }

    /**
     * Get Calendar Resources based on the room area i.e basically the roooms
     */
    public function getResources(){
        $area = Input::get('area');
        return Calendar::_getResources($area);
    }

    public function getFacultyResources(){
        $area = Input::get('area');
        return Calendar::_getFacultyResources($area);
    }

    /**
     * Get Calendar Resources based on the room area i.e basically the roooms
     */
    public function getResourcesFromArray(){
        $area = Input::get('area');
        return Calendar::_getResourcesFromArray($area);
    }

    /**
     *
     * @return type
     */
    public function getAllRoomAreas() {
        return Calendar::_getAllRoomAreas();
    }

    /**
     *
     * @param type $area
     * @return type
     */
    public function getAllRooms($area) {
        return Calendar::_getRooms($area);
    }

    /**
     * Delete Reservation Ajax function
     */
    public function deleteCalendarReservation(){
      Log::alert(0);
        if( Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ){
            $reservation_rooom_id = Input::get('ud');
            Log::alert($reservation_rooom_id);
            $reservation=ReservationRoom::findOrFail($reservation_rooom_id);
            if($reservation){

                try{

                    // if($reservation->user_id != Auth::user()->user_id) {
                    //     $data = $this->mail->data($reservation->user_id, $reservation->room_id, $reservation->reservation_room_id, '', '');
                    //     $subject = "Room Reservation Deletion";
                    //     $to = User::find($reservation->user_id)->email;
                    //     $to_for = User::findOrFail($reservation->created_for)->email;
                    //
                    //     $tmpl = 'mail_temps/deleted';
                    //     Emailr::queue($tmpl,false, $data, $to ,$subject,$to_for);
                    // }

                    $reservation->delete();
                    DB::table('reservations')->where('reservation_room_id',$reservation_rooom_id)->delete();
                    return '1';
                }
                catch(Exception $exc){
                  Log::alert($exc);
                    return $exc;
                }
            }
            else{
              Log::alert(1);
                return '0';
            }
        }else{
          Log::alert(2);
            return '0';
        }
    }

}
