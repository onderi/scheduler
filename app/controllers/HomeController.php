<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of HomeController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class HomeController extends BaseController {


    public function index($date = "") {

        //Get the room groups for filtering of the Calendar
        $areas = Area::lists('area_name', 'area_id');
        $facultymembers=Calendar::_getReservedFaculty();
        $default_area = 2; //@TODO - Get the default area from the settings
        $holidays = json_encode(Settings::getHolidayForCalendar());
        $reservation_types = ReservationRoom::getReservationTypes();
        $default_reservation_type = 'room';
        $priorities=[
        1=>'Urgent',
        2=>'Important',
        3=>'Normal Priority',
        4=>'Low Priority',
        5=>'Informative'];
        $announcements=Announcement::where('start_date','<=',date('Y-m-d H:i:s',time()))->where('end_date','>=',date('Y-m-d H:i:s',time()))->orderBy('priority','asc')->get();
        return View::make("home", array(
                    'areas' => $areas,
                    'facultymembers' => $facultymembers,
                    'rooms' => Calendar::_getRooms($default_area),
                    'rooms_array' => Calendar::_getRooms($default_area,'array'),
                    'default_area' => $default_area,
                    'go_date' => $date,
                    'announcements'=>$announcements,
                    'priority'=>$priorities,
                    'holidays' => $holidays,
                    'reservation_type' => $reservation_types,
                    'default_rtype' => $default_reservation_type
        ));
    }

}
