<?php
/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

/**
 * Description of DepartmentController
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class DepartmentController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
        $departments = Department::with('users')->get();
        Return View::make('department/index', array('departments' => $departments));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
        Return View::make('department/form_dept');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
        $v = Department::validate(Input::all());

        if ($v->passes()) {

            Department::create(array(
                'dept_abbrev' => Input::get('abbreviation'),
                'dept_name' => Input::get('name'),
                'dept_desc' => Input::get('description')
            ));

            return Redirect::to('departments')->with('notice','Department added.');;
        } else {
            return Redirect::to('department/add')->withInput()->withErrors($v);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
        $department = Department::find($id);

        if ($department) {
            return View::make('department/form_dept_edit', array('department' => $department));
        } else {
            return Redirect::to('departments');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
        $v = Department::validate(Input::all());

        if ($v->passes()) {

            $department = Department::find($id);
            $department->dept_abbrev = Input::get('abbreviation');
            $department->dept_name = Input::get('name');
            $department->dept_desc = Input::get('description');
            $department->save();

            return Redirect::to('departments')->with('notice','Department updated.');
        } else {
            return Redirect::to("department/$id/edit")->withInput()->withErrors($v);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        $department = Department::find($id);

        if ($department) {
            
            try {
                $department->delete();
                return Redirect::to('departments')->with('notice','Department deleted.');
            } catch (Exception $exc) {
                //echo $exc->getTraceAsString();
               return Redirect::to('departments')->with('error','Cannot Delete department! It has users assigned to it.');
            }
            
        } else {
            return Redirect::to('departments');
        }
    }

    public function missingMethod($parameters = array()) {
        //
        Redirect::to('departments');
    }

}
