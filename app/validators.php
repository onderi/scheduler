<?php

/**
 * Description of validators
 *
 * @author kinyua
 */

#Allow spaces to alphanumeric data
Validator::extend('alpha_spaces', function($attribute, $value) {
    return preg_match('/^[\pL\s]+$/u', $value);
});

#Allow validation of Date and Time in the format YYYY-MM-DD HH:MM
Validator::extend('date_time',function($attribute,$value){
   return preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}$/', $value);
});

#Allow validation of Date and Time in the format HH:MM
Validator::extend('e_time',function($attribute,$value){
   return preg_match('/^\d{2}:\d{2}$/', $value);
});

#Make sure that the room is selected
Validator::extend('room', function($attribute,$value){
    
    if( $value <= 0 ){
        return false;
    }else{
        return true;
    }
    
});

