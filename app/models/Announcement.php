<?php
/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */
class Announcement extends \Eloquent {
    protected $table='announcements';
    protected $primaryKey='announcement_id';
	protected $fillable = [
    'text',
    'priority',
    'user_id',
    'start_date',
    'end_date'];

    public static function validate($input){
        $nice=[
        'text'=>'Text',
        'priority'=>'Priority',
        'user_id'=>'User',
        'start_date'=>'Start Date',
        'end_date'=>'End Date'];
        $rules=[
        'text'=>'required|min:5',
        'priority'=>'required',
        'start_date'=>'required',
        'end_date'=>'required'];
        $validator=Validator::make($input,$rules);
        $validator->setAttributeNames($nice);
        return $validator;
    }
    public function user(){
        return $this->belongsTo('user','user_id');
    }
}