<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */


/**
 * Description of Repeat
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Repeat extends Eloquent{
    
    //Monitor the CRUD fellaz..
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $table = "reservation_repeats";
    protected $primaryKey = "repeat_id";
    protected $fillable = array('repeat_name','repeat_desc');
   
    /**
     * A repeat has many reservation rooms
     * @return type
     */
    public function reservation_room(){
        return $this->hasMany('ReservationRepeat','repeat_id','repeat_id');
    }
    /**
     * Get the repeat name selected
     * @param type $id
     * @return String
     */
    public static function getNameById($id){
        
        return Repeat::where('repeat_id','=',$id)->first()->repeat_name;
        
    }
    /**
     * Get the repeat id by name
     * @param type $name
     * @return type
     */
    public static function getIdByName($name){
        
        return Repeat::where('repeat_name','=',$name)->first()->repeat_id;
        
    }
}
