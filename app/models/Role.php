<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of Role
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
use Zizaco\Entrust\EntrustRole;
class Role extends EntrustRole{
    

public function user(){
    	return $this->belongsToMany('User','user_roles','role_id','user_id');
    }

     public static function getRoleByName($role_name){

      return Role::where('name','=',$role_name)->first() ? : false;

    }
}
