<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */


/**
 * Description of UserDepartment
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class UserDepartment extends Eloquent{
    
     protected $table = "user_depts";
     protected $primaryKey = 'user_dept_id';
     protected $fillable = array('user_id','department_id');
     public $timestamps = false;
}
