<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

class Lecturer extends Eloquent {

	    protected $table='sfp_materials_req';
	    protected $primaryKey='id';
	    // What can be filled
		protected $fillable = [
	    'course',
	    'lecturer',
	    'materials',
	    'priority',
	    'start_date',
	    'end_date'];

	    public static function validate($input){
         $niceNames = array(
             'course' => 'Name',
             'lecturer' => 'Description',
             'materials' => 'Materials',
             'priority' => 'Priority',
             'start_date'=> 'Start Date',
             'end_date' => 'End Date'
         );
        
        // $rules = array(
        //    'name' => 'Required|alpha_spaces',
        //    'description' => 'alpha_spaces',
        //     'type' => 'Required'
        // );
        
        $validator = Validator::make($input);
        $validator->setAttributeNames($niceNames);
        return $validator;

	}

}