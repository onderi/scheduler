<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of CourseCategory
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class CourseCategory extends Eloquent{
    
    protected $table = "course_categories";
    protected $primaryKey = "id";
    protected $fillable = array("id",'name','description','sortorder','parent','coursecount','visible','path','timemodified');

    /**
     * A course category hasMany courses
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses(){
        return $this->hasMany('Course','category');
    }

    /**
     * @param bool $command
     * @return bool
     */
    public static function SynchronizeCourseCategories($command = false){

        if ( $command )
            echo "Course Category Sync Started".PHP_EOL;


        $course_categories = DB::connection('elmysql')->table('course_categories')
            ->select(DB::raw('*'))
            ->where('visible','=','1')
            ->get();

        foreach( $course_categories as $cat ){

            //if exists, update, else insert
            $course_category = CourseCategory::find($cat->id);


            if( $course_category == null ){

                if ( $command )
                    echo ':..::..:: Inserting '.$cat->name.PHP_EOL;

                CourseCategory::create([
                    'id' => $cat->id,
                    'name' => $cat->name,
                    'description' => $cat->description,
                    'sortorder' => $cat->sortorder,
                    'parent' => $cat->parent,
                    'coursecount' => $cat->coursecount,
                    'path' => $cat->path,
                    'timemodified' => $cat->timemodified
                ]);

            }else{

                if ( $command )
                    echo ":..::..:: Updating ".$cat->name.PHP_EOL;

                $course_category->name = $cat->name;
                $course_category->description = $cat->description;
                $course_category->sortorder = $cat->sortorder;
                $course_category->parent = $cat->parent;
                $course_category->coursecount = $cat->coursecount;
                $course_category->path = $cat->path;
                $course_category->timemodified = $cat->timemodified;
                $course_category->save();

            }

        }

        if ( $command )
            echo PHP_EOL.'Course Category Sync Completed';
        else
            return true;

    }

    /**
     * @param $root
     * @param bool $html
     * @return array|string
     */
    public static function getNestedCourseCategory($root,$html = false){

        $course_categories = CourseCategory::select(DB::raw('id,name,coursecount'))->where('parent','=',$root)->get();

        $nest = ($html) ? "" : [];

        if ( $html )
            $nest = '<ul  style="list-style: none;margin: 0;" class="list-group cat_list panel" >';

        foreach( $course_categories as $cat ){


            if( $html ){

                $id = $cat->id;
                $ccount = ($cat->coursecount != 0) ? " <span style='color:#3c8dbc;cursor:help;' class='course_present' id=c_".$id.">(".$cat->coursecount." courses)</span>" : "";
                $name = ucwords($cat->name).$ccount;


                $nest .= '<a data-toggle="collapse" href="#'.$id.'" class="list-group-item" data-parent="accordion"><li>'.$name.'</li></a>';
                $nest .= "<div id='$id' class='panel-collapse collapse'>".CourseCategory::getNestedCourseCategory($id,true)."</div>";

            }else{

                $id = $cat->id;
                $name = ucwords($cat->name);

                $nest[$id] = [$name, CourseCategory::getNestedCourseCategory($id)];
            }

        }

        if( $html )
            $nest .= "</ul>";

        return $nest;

    }

    /**
     * @return array
     */
    public static function getCategoriesWithCourses(){
        $categories =  CourseCategory::select(["id","path"])->where('coursecount','>',0)->orderby('name','desc')->get();

        $categories_array = [];

        foreach( $categories as $cat ){

            $id = $cat->id;
            $path = $cat->path;

            $path_names = CourseCategory::getPathNames($path);

            $categories_array[$id] = $path_names;
        }

        return $categories_array;
    }


    /**
     * @param $pathString e.g /2/3/4/
     * @return string
     */
    public static function getPathNames($pathString){

        $path_array = explode('/',$pathString);
        $path_count = count($path_array);
        $path_names_array = [];

        //we start at one since the first element has nothing
        for( $i = 1; $i <  $path_count; $i++){
            $path_names_array[$i-1] = CourseCategory::find($path_array[$i])->name;

        }

        return implode($path_names_array,' / ');

    }

}
