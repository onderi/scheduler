<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of Building
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Building extends Eloquent {
    protected $table = "buildings";
    protected $primaryKey = 'building_id';
    //What can be filled
    protected $fillable = array('building_name', 'building_description');
    /**
     * Validate Input Data
     * @param type $input
     * @return type
     */
    public static function validate($input){
         $niceNames = array(
             'name' => 'Name',
             'description' => 'Description',
             'type'=> 'Type'
         );
        
        $rules = array(
           'name' => 'Required|alpha_spaces',
           'description' => 'alpha_spaces',
            'type' => 'Required'
        );
        
        $validator = Validator::make($input, $rules);
        $validator->setAttributeNames($niceNames);
        return $validator;
    }
    /**
     * 1 to M Relationship with rooms
     * @return type
     */
    public function areas(){
        return $this->hasMany('Area');
    }
    
}
