<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Faculty Planner
  |
 */

 use Illuminate\Support\Facades\Log;

/**
 * Description of Calendar
 *
 * |---------------------------------------------------------------------------
 *
 * Ajax Full Featured Calendar
 * 	- Add Event To Calendar
 * 	- Edit Event On Calendar
 * 	- Delete Event On Calendar
 * 	- View Event On Calendar
 * 	- Update Event On Rezise
 * 	- Update Event On Drag
 *
 * |---------------------------------------------------------------------------
 * Version: 1.6.4 (July 2014)
 *
 * @Author by Kinyua Wallace <kinyuawallace at gmail.com>
 *
 */
class Calendar {

    /**
     *
     * @return type
     */
    public static function getAllRoomAreas() {

        $roomAreas = Area::select(
                                array(
                                    'area_id AS id',
                                    'area_name AS name'
                        ))
                        ->get()->toArray();

        return json_encode($roomAreas);
    }

   /**
   * Select events from the database and parse as json string ready for calendar display
   * Improved to take note of Start and End time based on the selection of the user i.e the curren calendar view.
   * @param type $start
   * @param type $end
   * @return type
   */

    public static function _getReservations($start,$end,$room_area ="",$room="") {

        if( $room != "" ){
            $room_array = explode(',',$room);
            Log::alert($room_array);
          }

        $start_date = date('Y-m-d',$start);
        $end_date = date('Y-m-d',$end);
        $show_booked_by = [];

        \DB::listen(function($sql) {
          Log::alert($sql);
        });

        if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) ){

            $selection_array = [
                    'reservation_rooms.reservation_name as title'
                    ,'reservation_rooms.room_id as resourceId'//this is the room id
                    ,'ccode as color'
                    ,'rr_type as type'
                    ,'reservation_rooms.rr_id as isRep'
                    ,'rooms.room_name as room'
                    ,'rooms.area_id as area'
                    ,'reservation_rooms.approval as status'
                    ,'reservation_rooms.start_time as start'
                    ,'reservation_rooms.created_at as rdate'
                    ,'reservation_rooms.end_time as end'
                    ,'reservation_rooms.reservation_room_id as id'
                    ,'reservations.reservation_id as segments'
                    ,'users.fname as mby_f'
                    ,'users.lname as mby_l'
                ];

        }else{

            $selection_array = [
                    'reservation_rooms.reservation_name as title'
                    ,'reservation_rooms.room_id as resourceId'//this is the room id
                    ,'ccode as color'
                    ,'rr_type as type'
                    ,'reservation_rooms.rr_id as isRep'
                    ,'rooms.room_name as room'
                    ,'rooms.area_id as area'
                    ,'reservation_rooms.approval as status'
                    ,'reservation_rooms.start_time as start'
                    ,'reservation_rooms.created_at as rdate'
                    ,'reservation_rooms.end_time as end'
                    ,'reservation_rooms.reservation_room_id as id'
                    ,'reservations.reservation_id as segments'

            ];

        }

        //Opted to use fluent approach, simplicity
        $room_reservations_sql = DB::table('reservation_rooms')
                        ->join('rooms', 'reservation_rooms.room_id', '=', 'rooms.room_id')
                        ->join('users','reservation_rooms.user_id','=','users.user_id')
                        ->leftJoin('reservations','reservations.reservation_room_id','=','reservation_rooms.reservation_room_id')
                        ->select($selection_array)->where('reservation_rooms.start_time','>=',$start_date)
                          ->where('reservation_rooms.end_time','<=',$end_date);


                          Log::alert($room_area);

                    if( $room_area != "" )
                        $room_reservations_sql->where('rooms.area_id','=',$room_area);
                    if( $room != "" )
                        $room_reservations_sql->whereIn('rooms.room_id',$room_array);

        $holidays = implode('","',Settings::getHolidayForCalendar());
        $holidays = '"'.$holidays.'"';

         //remove any holiday reservations
        $room_reservations_sql->whereRaw('DATE_FORMAT(`sfp_reservation_rooms`.`start_time`,"%Y-%m-%d") NOT IN ('.$holidays.')');
                    $room_reservations = $room_reservations_sql->groupBy('reservation_rooms.reservation_room_id')->get();

         //Add the allDay false value
         foreach ( $room_reservations as $r ):
             $r->allDay = false;
            if($r->type == '1'){
                $r->borderColor = '#000000';
                $r->textColor = '#000000';
                $r->className = 'externalEvent';
            }
         endforeach;

        // dd($room_reservations);
        return json_encode($room_reservations);
    }


    public static function _getReservationsSegments($start,$end,$room_area ="",$room="") {

        if( $room != "" )
            $room_array = explode(',',$room);

        $start_date = date('Y-m-d',$start);
        $end_date = date('Y-m-d',$end);
        $show_booked_by = [];

        if( Auth::check() /*&& (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() )*/ ){

            $selection_array = [
                'reservations.reservation_desc as title'
                ,'reservation_rooms.room_id as resourceId'//this is the room id
                ,'ccode as color'
                ,'courses.course_title as course'
                ,'rr_type as type' //external or internal
                ,'reservation_rooms.rr_id as isRep'
                ,'rooms.room_name as room'
                ,'rooms.area_id as area'
                ,'reservation_statuses.reservation_status_name as status'
                ,'reservations.start_time as start'
                ,'reservations.created_at as rdate'
                ,'reservations.end_time as end'
                ,'reservations.reservation_id as id'
                ,'users.fname as mby_f'
                ,'users.lname as mby_l'
                ,'reservations.faculty as faculty'
            ];

        }

        //Opted to use fluent approach, simplicity
        $room_reservations_sql = DB::table('reservations')
            ->join('reservation_rooms','reservations.reservation_room_id','=','reservation_rooms.reservation_room_id')
            ->join('courses','courses.course_id','=','reservations.course_id')
            ->join('rooms', 'reservation_rooms.room_id', '=', 'rooms.room_id')
            ->join('users','reservation_rooms.user_id','=','users.user_id')
            ->join('reservation_statuses','reservation_statuses.reservation_status_id','=','reservations.reservation_status_id')
            ->select($selection_array)->where('reservations.start_time','>=',$start_date)
            ->where('reservations.end_time','<=',$end_date);

        if( $room_area != "" )
            $room_reservations_sql->where('rooms.area_id','=',$room_area);
        if( $room != "" )
            $room_reservations_sql->whereIn('rooms.room_id',$room_array);

        $holidays = implode('","',Settings::getHolidayForCalendar());
        $holidays = '"'.$holidays.'"';

        //remove any holiday reservations
        $room_reservations_sql->whereRaw('DATE_FORMAT(`sfp_reservation_rooms`.`start_time`,"%Y-%m-%d") NOT IN ('.$holidays.')');
        $room_reservations = $room_reservations_sql->get();

        //Add the allDay false value
        foreach ( $room_reservations as $r ):
            $r->allDay = false;
            if($r->type == '1'){
                $r->borderColor = '#000000';
                $r->textColor = '#000000';
                $r->className = 'externalEvent';
            }

            $r->faculty = User::getNameString(explode(',',$r->faculty));
        endforeach;

        //dd((array)$room_reservations);
        return json_encode($room_reservations);
    }
    public static function _getTimesheetReservationsSegments($start,$end,$room_area ="",$room="") {

      if( $room != "" )
          $room_array = explode(',',$room);

      $start_date = date('Y-m-d',$start);
      $end_date = date('Y-m-d',$end);
      $show_booked_by = [];

      if( Auth::check() /*&& (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() )*/ ){

          $selection_array = [
              'reservations.reservation_desc as title'
              ,'reservation_rooms.room_id as resourceId'//this is the room id
              ,'ccode as color'
              ,'courses.course_title as course'
              ,'rr_type as type' //external or internal
              ,'reservation_rooms.rr_id as isRep'
              ,'rooms.room_name as room'
              ,'rooms.area_id as area'
              ,'reservation_statuses.reservation_status_name as status'
              ,'reservations.start_time as start'
              ,'reservations.created_at as rdate'
              ,'reservations.end_time as end'
              ,'reservation_rooms.reservation_room_id as id'
              ,'reservations.reservation_id as segments'
              ,'users.fname as mby_f'
              ,'users.lname as mby_l'
              ,'reservations.faculty as faculty'
          ];

      }

      //Opted to use fluent approach, simplicity
      $room_reservations_sql = DB::table('reservations')
          ->join('reservation_rooms','reservations.reservation_room_id','=','reservation_rooms.reservation_room_id')
          ->join('courses','courses.course_id','=','reservations.course_id')
          ->join('rooms', 'reservation_rooms.room_id', '=', 'rooms.room_id')
          ->join('users','reservation_rooms.user_id','=','users.user_id')
          ->join('reservation_statuses','reservation_statuses.reservation_status_id','=','reservations.reservation_status_id')
          ->select($selection_array)->where('reservations.start_time','>=',$start_date)
          ->where('reservations.end_time','<=',$end_date);
          Log::alert('reached here atleast');
      if( $room_area != "" )
          $room_reservations_sql->where('reservations.faculty','=',$room_area);
      if( $room != "" )
          $room_reservations_sql->whereIn('rooms.room_id',$room_array);

      $holidays = implode('","',Settings::getHolidayForCalendar());
      $holidays = '"'.$holidays.'"';

      //remove any holiday reservations
      $room_reservations_sql->whereRaw('DATE_FORMAT(`sfp_reservation_rooms`.`start_time`,"%Y-%m-%d") NOT IN ('.$holidays.')');
      $room_reservations = $room_reservations_sql->get();

      //Add the allDay false value
      foreach ( $room_reservations as $r ):
          $r->allDay = false;
          if($r->type == '1'){
              $r->borderColor = '#000000';
              $r->textColor = '#000000';
              $r->className = 'externalEvent';
          }

          $r->faculty = User::getNameString(explode(',',$r->faculty));
      endforeach;

      //dd((array)$room_reservations);
      Log::alert($room_reservations);
      return json_encode($room_reservations);
    }


    /**
     * @param $area
     * @return json
     */
    public static function _getResources($area){

        $res = DB::table('rooms')->
            select (
                array('room_id as id','room_name as title','capacity as max')
            )->where('rooms.area_id','=',$area);
        $result = $res->get();

        return json_encode($result);
    }

    public static function _getReservedFaculty(){
      $faculty = DB::table('users')->select(DB::raw('CONCAT(fname, lname) as fname,user_id as user_id'))
  								->join('reservations','users.user_id','=','reservations.faculty');
      $faculty=$faculty->get();
      $fc;
      foreach($faculty as &$f){
        $fc[$f->user_id]=$f->fname;
      };
      return $fc;
    }

    public static function _getFacultyResources($area){
// SQL: select `user_id` as `id`, `CONCAT(fname,lname)` as `title`, `capacity` as `max` from `sfp_users` where `sfp_users`.`user_id` = 2
        Log::alert($area);
        $res = DB::table('reservations')->
            select (
                array("rooms.room_id as id",'rooms.room_name as title','rooms.capacity as max')
            )->distinct()
            ->join('reservation_rooms','reservations.reservation_room_id','=','reservation_rooms.reservation_room_id')
            ->join('rooms', 'reservation_rooms.room_id', '=', 'rooms.room_id')
            ->where('reservations.faculty','=',$area)
            // ->where('start_time','>=','CURDATE()')
            ;
        $result = $res->get();

        return json_encode($result);
    }

    /**
     * @param $area
     * @return json
     */
    public static function _getResourcesFromArray($area){

        $area_array = explode(",",$area);

        $res = DB::table('rooms')->
        select (
            array('room_id as id','room_name as title','capacity as max')
        )->whereIn('rooms.area_id',$area_array);
        $result = $res->get();

        return json_encode($result);
    }

    /**
     * @param $start
     * @param $end
     * @return string
     */
    public static function _getReservationSegments($start,$end) {

        $start_date = date('Y-m-d',$start);
        $end_date = date('Y-m-d',$end);
        $segments=DB::table('reservations')
        ->join('reservations','reservations.reservation_room_id','=','reservation_rooms.reservation_room_id')
        ->where('reservation_rooms.start_time','>=',$start_date)
                          ->where('reservation_rooms.end_time','<=',$end_date)
                          ->get();
                          foreach ($segments as $value) {
                            $value->allDay=false;
                          }

                          return json_encode($segments);

      }

    /**
     *
     * @param type $area
     * @return type
     */
    public static function _getRooms($area = "",$format="json") {

        if ($area != "") {
            $rooms = Room::select(
                            array(
                                'room_id as id',
                                'room_name as title',
                    ))->where('area_id', '=', $area)->get()->toArray();
        } else {
            $rooms = Room::select(
                            array(
                                'room_id as id',
                                'room_name as title',
                                'capacity as cap'
                    ))->get()->toArray();
        }
        if( $format == "json" )
            return json_encode($rooms);
        else
            return $rooms;
    }

    public static function _getFaculty($area = "",$format="json") {


    }

}
