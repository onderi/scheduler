<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

class Mails extends \Eloquent {

    protected $table='mail_tmpls';
    protected $primaryKey='mail_tmpl_id';
    protected $fillable = ['name',
        'message',
        'description'];

    /**
     * @param $input
     * @return \Illuminate\Validation\Validator
     */
    public static function validate($input){
        $nice=[
            'name'=>'Name',
            'description'=>'Description',
            'message'=>'Message'
        ];

        $rules=[
            'name'=>'required|min:3',
            'description'=>'required|min:10',
            'message'=>'required'
        ];

        $validator=Validator::make($input,$rules);
        $validator->setAttributeNames($nice);
        return $validator;

    }

    /**
     * @param $user_id
     * @param $room_id
     * @param $reservation_id
     * @param $group_id
     * @param $course_id
     * @return array
     */
    public function data($user_id,$room_id,$reservation_id,$group_id,$course_id){
        $user=User::find($user_id);
        $room=Room::find($room_id);
        $reservation=ReservationRoom::find($reservation_id);
        //dd(ReservationRoom::find($reservation_id));
        $course=Course::find($course_id);
        $ref='Reference Number';//$reservation->reservation_room_id.'/'.$reservation->room->room_name.'/'.$reservation->dept_id.'';

        $rooms=array(''=>'Please select one')+Room::lists('room_name','room_id');
        //		$groups=array(''=>'Please select one')+Group::lists('group_name','group_id');
        $courses=array(''=>'Please select one')+Course::lists('course_title','course_id');
        $statuses=array(''=>'Please select one')+ReservationStatus::lists('reservation_status_name','reservation_status_id');

        return array(
            'user'=>$user,
            'room'=>$room,
            'auser' => Auth::user(),
            'reservation'=>$reservation,
            'course'=>$course,
            'rooms'=>$rooms,
            'courses'=>$courses,
            'statuses'=>$statuses,
            'ref'=>$ref
        );
    }

    public function data_add($reserved_for,$rooms,$start_time,$end_time){

        $user=User::find($reserved_for);

        $room_names = [];
        //Create insert array
        foreach($rooms as $room) {
            $r_name = Room::findOrFail($room)->room_name;
            array_push($room_names,$r_name);
        }

        return array(
            'user'=>$user,
            'rooms'=>implode(',',$room_names),
            'start_time' => $start_time,
            'end_time' => $end_time
        );
    }

    /**
     * @param $user_id
     * @param $room_id
     * @param $reservation_id
     * @param $group_id
     * @param $course_id
     * @param $old_reservation_room
     * @param $old_reservation_start
     * @param $old_reservation_end
     * @param $old_reservation_id
     * @return array
     */
    public function data_edit($user_id,$room_id,$reservation_id,$group_id,$course_id,$old_reservation_room,$old_reservation_start,$old_reservation_end,$old_reservation_name){
        $user=User::find($user_id);
        $room=Room::find($room_id);
        $old_room = Room::find($old_reservation_room);
        $reservation=ReservationRoom::find($reservation_id);

        //dd(ReservationRoom::find($reservation_id));
        $course=Course::find($course_id);
        $ref='Reference Number';//$reservation->reservation_room_id.'/'.$reservation->room->room_name.'/'.$reservation->dept_id.'';

        $rooms=array(''=>'Please select one')+Room::lists('room_name','room_id');
        $courses=array(''=>'Please select one')+Course::lists('course_title','course_id');
        $statuses=array(''=>'Please select one')+ReservationStatus::lists('reservation_status_name','reservation_status_id');

        return array(
            'user'=>$user,
            'auser'=>Auth::user(),
            'room'=>$room,
            'reservation'=>$reservation,
            'course'=>$course,
            'rooms'=>$rooms,
            'courses'=>$courses,
            'statuses'=>$statuses,
            'ref'=>$ref,
            'old_reservation_name' => $old_reservation_name,
            'old_room' => $old_room,
            'old_reservation_start' => $old_reservation_start,
            'old_reservation_end' => $old_reservation_end
        );
    }

    /**
     * @param $mail
     * @param $recipient
     * @param $data
     * @param $subject
     * @param $cc
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send_mail($mail,$recipient,$data,$subject,$cc){
        try{

            $user=User::find($recipient);
            $to=$user->email;
            Mail::queue(['html'=>'mail_temps/reservationConfirmation'],$data,function($message)use($data,$to,$subject,$cc){
                $message->to($to)->subject($subject);
                $message->from('sfplanner@strathmore.edu','SBS Faculty Planner');
                if(isset($cc)){
                    $message->cc($cc);
                }

            });
        }


        catch(Exception $exc){

            return Redirect::back()->withErrors('Mailing Error');
        }
    }

    /**
     * @param $mail_id
     * @return \Illuminate\View\View
     */
    public function render($mail_id){
        $mail=Mails::find($mail_id);

        return View::make('mail_temps/general',compact('mail'));

    }

}
