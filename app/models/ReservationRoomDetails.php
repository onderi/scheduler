<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

class ReservationRoomDetails extends \Eloquent
{
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $table = 'reservations';
    protected $primaryKey = 'reservation_id';
    protected $fillable = [
        'user_id',
        'ref_number',
        'course_id',
        'category',
        'start_time',
        'end_time',
        'desc',
        'reservation_status_id',
        'faculty',
        'hours','reservation_room_id','created_by','created_date','last_modified'];

    private $mail;

    public function __construct()
    {
        $this->mail = new Mails();
    }

    public static function validate($input)
    {
        $nicenames = array(
            'rr' => 'Room Reservation ID',
            'user_id' => 'User',
            'ref' => 'Reference Number',
            'courses' => 'Course',
            'ccategory' => 'Course Category',
            'start' => 'Start Time',
            'end' => 'Completion Time',
            'desc' => 'Description',
            'status' => 'Status',
            'faculty' => 'Faculty name');

        $rules = array(
            'rr' => 'required',
            'user_id' => 'required|numeric',
            'ref' => 'required',
            'courses' => 'required',
            'ccategory' => 'required',
            'start' => 'required',
            'end' => 'required',
            'desc' => 'required',
            'faculty' => 'required',
            'status' => 'required');

        $validator = Validator::make($input, $rules);
        $validator->setAttributeNames($nicenames);
        return $validator;
    }

    public function room_reservation()
    {
        return $this->belongsTo('ReservationRoom', 'reservation_room_id');
    }

    public function user()
    {
        return $this->belongsTo('User', 'created_by');
    }

    public function department()
    {
        return $this->belongsTo('Department');
    }

    public function course()
    {
        return $this->belongsTo('Course');
    }


    public function status()
    {
        return $this->belongsTo('ReservationStatus', 'reservation_status_id');
    }

    /**
     * @param $subject
     * @param $recipient
     * @param $name
     * @param $start
     * @param $end
     * @param $keys
     */
    public function invite($subject, $recipient, $name, $start, $end, $keys)
    {


        $data = $this->mail->data($keys[0], $keys[1], $keys[2], $keys[3], $keys[4]);

        $data = array('name' => $name, 'start' => $start, 'end' => $end) + $data;

        Mail::queue('mail_temps/invite', $data, function ($message) use ($subject, $recipient, $data, $start, $end, $name) {

            $message->to($recipient)->subject('Session Invite - ' . $subject);

            //$message->from('sbs_scheduler@strathmore.edu','SBS Scheduler');

        });

    }

    /**
     * Get the error string conflicts of a certain faculty
     * @param $reservation
     * @return string
     */
    public static function getDetails($reservation,$faculty)
    {

        //get the faculty name
        $names = [];
        $i = 0;

        foreach( $faculty as $f ){
            $names[$i] = User::getUserById($f);
            $i++;
        }

        $name_str = implode(" and ",$names);

        //get the course
        $course = Course::find($reservation->course_id)->course_title;

        $auxillary_verb = (count($names) == 2 ) ? "are " : 'is ';

        return "<p>$name_str ".$auxillary_verb." teaching in $course within the selected time<p>";


    }

}