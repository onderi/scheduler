<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of Areas
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Area extends Eloquent {

    protected $table = "areas";
    protected $primaryKey = "area_id";
    protected $fillable = array('area_name', 
      'area_desc',
      'building_id', 
      'default_duration', 
      'resolution', 
      'start_interval', 
      'end_interval', 
      );

    /**
     * Validate Input Data
     * @param type $input
     * @return type
     */ 
    public static function validate($input) {
        $niceNames = array(
            'name' => 'Name',
            'description' => 'Description',
            'building_id'=>'Building',
            'default_duration' => 'Default Duration',
            'start_interval' => 'Start Interval',
            'end_interval' => 'End Interval',
            'resolution' => 'Resolution',
            //'privacy' => 'Privacy'
        );

        $rules =  array(
            'name' => 'Required|alpha_spaces',
            'description' => 'alpha_spaces',
            'building_id'=>'required',
            'default_duration' => 'Required',
            'start_interval' => 'Required|max:5',
            'end_interval' => 'Required|max:5',
            'resolution' => 'Required',
            //'privacy' => 'Required'
        );

        $validator = Validator::make($input, $rules);
        $validator->setAttributeNames($niceNames);
        return $validator;
    }

    /**
     * Has zero,one or many Areas
     * @return type
     */
    public function rooms() {
        return $this->hasMany('Room');
    }
    public function building(){
      return $this->belongsTo('Building');
    }
    
}
