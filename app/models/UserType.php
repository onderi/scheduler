<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */


/**
 * Description of FacultyTypes
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */

class UserType extends \Eloquent
{

    protected $table='user_type';
    protected $primaryKey = 'id';
    protected $fillable = ['name','hours','desc'];


    public function users(){
        $this->belongsTo('users');
    }


}