<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of User
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
use Zizaco\Entrust\HasRole;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface
{
    use HasRole;

    use \Venturecraft\Revisionable\RevisionableTrait;
    protected $revisionEnabled = true;
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    protected $fillable = array('fname', 'lname', 'username', 'email', 'password', 'salt', 'phone', 'last_login', 'status_id', 'notification_status', 'gender');
    public $timestamps = true;

    /**
     * Validate Input Data
     * @param type $input
     * @return Validator
     */
    public static function validate($input, $edit = false)
    {
        //dd($input);
        $niceNames = array(
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'username' => 'Username',
            'email' => 'Email Address',
            'phone' => 'Phone',
            'status' => 'Status',
            'department' => 'Department',
            'notification' => 'Notification',
            'gender' => 'Gender',
            'roles' => 'Roles'
        );
        if ($edit) {
            $username = 'Required|AlphaNum|unique:users,username,' . (int)$input["user_id"] . ',user_id';
            $email = 'Required|Email|Unique:users,email,' . (int)$input["user_id"] . ',user_id';
        } else {
            $username = 'Required|AlphaNum|unique:users,username';
            $email = 'Required|Email|Unique:users,email';
        }
        $rules = array(
            'fname' => 'Required|Alpha',
            'lname' => 'Required|alpha_spaces',
            'username' => $username,
            'email' => $email,
            'phone' => 'numeric',
            'status' => 'Required',
            'department' => 'Required',
            'notification' => 'Required',
            'gender' => 'Required',
            'roles' => 'Required'
        );
        $validator = Validator::make($input, $rules);
        // dd($validator);
        $validator->setAttributeNames($niceNames);
        return $validator;
    }

    /*
     * Inverse relation i.e. Status belongs to user
     */
    public function status()
    {
        return $this->hasOne('Status', 'status_id', 'status_id');
    }

    /**
     * A user can belong to many departments
     * @return type
     */
    public function departments()
    {
        return $this->belongsToMany('Department', 'user_depts', 'user_id', 'dept_id');
    }

    public function roles()
    {
        return $this->belongsToMany('Role', 'user_roles', 'user_id', 'role_id');
    }

    /**
     * A User makes many reservations
     * @return type
     */
    public function room_reservations()
    {
        return $this->hasMany('ReservationRoom');
    }

    public function reservation_details()
    {
        return $this->hasMany('ReservationRoomDetails');
    }

    public function type()
    {
        return $this->hasOne('UserType', 'id', 'user_type');
    }

    /**
     * @return array
     */
    public static function getUserDropdown(){
        return User::select(DB::raw('CONCAT(fname," ",lname) AS name,user_id'))->where('status_id','=','1')->orderBy('name','asc')->lists('name','user_id');
    }

    /**
     * Get name by Id
     * @param type $id
     * @return String
     */
    public static function getUserById($id)
    {
        return User::select(DB::raw('CONCAT(fname," ",lname) AS name'))->where('user_id', '=', $id)->first()->name;
    }

    /**
     * @param $uname
     * @return mixed
     */
    public static function getUserByUsername($uname)
    {
        return User::select('user_id')->where('username', '=', $uname)->first()->user_id;
    }


    public function getAuthIdentifier()
    {
        return $this->user_id;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function getRememberTokenName()
    {
        return 'return_token';
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getReminderEmail()
    {
        return $this->email;
    }

    public function isSysAdmin()
    {
        foreach ($this->roles as $role) {
            if ($role->name == "System Administrator") {
                return true;
            }
        }
        return false;

    }

    public function isBookingAdmin()
    {
        foreach ($this->roles as $role) {
            if ($role->name == "Booking Administrator") {
                return true;
            }
        }
        return false;

    }


    public function isSystemPlanningAdmin(){

        foreach ($this->roles as $role) {
            if ($role->name == "System Planning Administrator") {
                return true;
            }
        }
        return false;

    }

    public function isProgramManager()
    {
        foreach ($this->roles as $role) {
            if ($role->name == 'Department Program Manager') {
                return true;
            }
        }
        return false;

    }

    public function hasDept($against)
    {
        foreach ($this->departments() as $dept) {
            if ($dept->dept_id == $against) {
                return $against;
            }
        }
        return false;

    }

    /**
     * Get faculty
     * @return array|static[]
     */

    public static function getFaculty()
    {

        $faculty = DB::table('users')
            ->select(DB::raw('user_id,CONCAT(fname," ",lname) as faculty_name'))
            ->join('user_type', 'users.user_type', '=', 'user_type.id')
            ->where('name', '!=', 'Administrative')->lists('faculty_name', 'user_id');

        return $faculty;
    }

    /**
     * Get a string of names from faculty id's
     * @param $faculty_values
     * @return string
     */
    public static function getNameString($faculty_values){

        if ( $faculty_values[0] != "0" )
            $users = User::select(DB::raw('CONCAT(fname," ",lname) as faculty_names'))->whereIn('user_id',$faculty_values)->lists('faculty_names');
        else
            $users = ["No Faculty assigned"];

        return implode($users,' and ');

    }

    /**
     * @param $str
     * @return string
     */
    public static function getSubscribers($str){

        if( strpos($str,':') !== false ){

            $str_array = explode(':',$str);

            $role = $str_array[0];
            $users = $str_array[1];

            $user_names = ($users == "0") ? "All Users" : User::getNameString($users);

            $role_name = ($role == "0") ? "ALL" : Role::find($role)->first()->name;

            return $role_name.'( '.$user_names.')';
        }else{
            return ( $str == "0" ) ? "All Users" :  User::getNameString($str);
        }

    }

}
