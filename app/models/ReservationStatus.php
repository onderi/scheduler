<?php
/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

class ReservationStatus extends \Eloquent {
	protected $table='reservation_statuses';
	protected $primaryKey='reservation_status_id';
	protected $fillable = [];

	public function reservation(){
		return $this->hasMany('ReservationRoomDetails');
	}
}