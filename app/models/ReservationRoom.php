<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */


/**
 * Description of ReservationRoom
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class ReservationRoom extends Eloquent
{

    //Monitor the CRUD fellaz..
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $table = "reservation_rooms";
    protected $primaryKey = "reservation_room_id";
    protected $fillable = array('start_time',
        'end_time',
        'user_id',
        'dept_id',
        'reservation_name',
        'reservation_desc',
        'rr_id',
        'room_id',
        'created_for',
        'approval', 'rtype');
    private $mail;

    public function __construct()
    {
        $this->mail = new Mails();
    }

    /**
     * Validate the input
     * @param type $input
     * @return type
     */

    public static function validate($input, $allDay, $repeat_type, $days,$commit_type)
    {

        $nicenames = array(
            'start' => 'Start',
            'end' => 'End',
            'name' => 'Name',
            'description' => 'Description',
            'department' => 'Department',
            'rtype' => 'Repeat Type',
            'room' => 'Room',
            'repeat_end' => 'Repeat End',
            'rep_day' => 'Repeat Day'
        );

        //Handle all day stuff
        $rule_date = "";
        $rule_time = "";
        if ($allDay != '1') {
            $rule_date = "required|date_time";
            $rule_time = "required|e_time";
        }

        //Handle Repeat type
        $r_rule = "";
        if ($repeat_type != 'NONE' && $repeat_type != '' && $commit_type == 'add') {
            $r_rule = "required|Date";
        }

        $req_days = '';
        //Handle repeat day selection
        if ($days) {
            $req_days = "required";
        }

        $rules = array(
            'start' => $rule_date,
            'end' => $rule_time,
            'name' => 'required',
            'description' => 'required',
            'department' => 'required',
            'rtype' => 'required',
            'room' => 'required|room',
            'repeat_end' => $r_rule,
            'rep_day' => $req_days
        );

        $validator = Validator::make($input, $rules);
        $validator->setAttributeNames($nicenames);
        return $validator;
    }

    /**
     * A reservation belongs to a department
     * @return type
     */
    public function department()
    {
        return $this->belongsTo('Department', 'dept_id');
    }

    /**
     * A reservation belongs to / is made for a room.
     * @return type
     */
    public function room()
    {
        return $this->belongsTo('Room', 'room_id', 'room_id');
    }

    /**
     * A reservation is made by a user.
     * @return type
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * A reservation may have a repeat or not
     * @return type
     */
    public function repeat()
    {
        return $this->belongsTo('Repeat', 'repeat_id', 'repeat_id');
    }

    /**
     * A reservation may
     * @return type
     */
    public function rr_repeat()
    {
        return $this->belongsTo('ReservationRoomRepeat', 'rr_id');
    }

    /**
     * Get the area id from room
     * @param type $room_id
     * @return type
     */
    public static function _getAreaId($room_id)
    {

        if (empty($room_id) || !isset($room_id) || $room_id == 'undefined') {
            return Room::select('area_id')->first()->area_id;
        }

        return Room::select('area_id')->where('room_id', '=', $room_id)->first()->area_id;
    }

    /**
     * @param string $key
     * @return array|mixed
     */
    public static function getReservationTypes($key = ''){

        $reservation_type = [

            'room' => 'Room bookings',
            'course' => 'Course bookings',
            'timesheet' =>'Faculty bookings'

        ];

        return ( $key == "" ) ? $reservation_type : $reservation_type[$key];

    }

    /**
     *
     * @param type $room_area
     * @return Array
     */
    public static function getRoomAreaDefaults($room_area)
    {

        return Area::select('start_interval', 'end_interval')
            ->where('area_id', '=', $room_area)
            ->first()
            ->toArray();
    }


    /**
     * Create a single entry reservation
     * @param $start_dt
     * @param $end_dt
     * @param $r_name
     * @param $r_desc
     * @param $r_rooms
     * @param $r_dept
     * @param $r_for
     * @param $rr_type
     * @return bool|String
     */
    public function createSingleReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $r_for, $rr_type)
    {
        $loggedInUser = Auth::user()->user_id;
        //check for overlaps in all the rooms
        $has_no_overlap = $this->hasNoOverlap($r_rooms, $start_dt, $end_dt);

        //If a boolean is returned i.e. true, then there is no overlap
        if (is_bool($has_no_overlap) == true) {

            //Create insert array
            foreach ($r_rooms as $room) {
                //do save
                //Single instance
                $reservation = new ReservationRoom();
                $reservation->start_time = $start_dt;
                $reservation->end_time = $end_dt;
                $reservation->user_id = $loggedInUser;
                $reservation->reservation_name = $r_name;
                $reservation->reservation_desc = $r_desc;
                $reservation->dept_id = $r_dept;
                $reservation->room_id = $room;
                $reservation->created_for = $r_for;

                //set all the reservations to approved since the booking are made by the administrators
                $reservation->approval = 'Approved';

                if (is_null($rr_type)) {
                    $reservation->rr_type = 0;

                } else{
                    $reservation->rr_type = $rr_type;
                }

                if ($reservation->save()) {

                    if (is_null($r_for) || $r_for == "") {
                        $recipient = Auth::user()->user_id;
                    } else {
                        $recipient = $r_for;
                    }

                }

            }//for loop end
            return true;
        } else {
            return $has_no_overlap;
        }
    }

    public function createRepeatReservation($start_dt, $end_dt, $r_name, $r_desc, $r_rooms, $r_dept, $repeat_arr, $r_for, $rr_type)
    {

        $loggedInUser = Auth::user()->user_id;
        //Get the end time
        $end_ = explode(" ", $end_dt);
        $end_time = $end_[1];

        //Get the start time
        $start_ = explode(" ", $start_dt);
        $start_time = $start_[1];

        $repeat_type = $repeat_arr['repeat_type'];

        $r_days = $repeat_arr['repeat_days'];

        //Generate the repeating dates based on the repeat array - PHP DateInterval
        $start_date = new DateTime($start_dt);
        $interval = new DateInterval('P' . $repeat_arr['repeat_times'] . $repeat_type[0]);
        $end = new DateTime($repeat_arr['repeat_end']);

        //$period = new DatePeriod($start_date, $interval, $end);
        $date_days = $this->getDateFromDay($start_dt, $r_days);

        if (count($r_days) > 1 && $repeat_type == 'WEEKLY') {


            //Check individual date and break when false, all we need is one occurence
            $error_code = false;

            foreach ($date_days as $start) {

                $start_date = new DateTime($start);

                //things remain the same
                $period = new DatePeriod($start_date, $interval, $end);


                //check for overlaps in all the rooms and all the time periods
                $has_no_overlap = $this->hasNoOverlap($r_rooms, $start_dt, $end_time, $period);

                if (is_bool($has_no_overlap) != true) {
                    $error_code = true;
                    break;
                }

            }
            //dd($has_no_overlap);
            if ($error_code) {
                return $has_no_overlap;
            }


            foreach ($r_rooms as $room) {

                $repeat = new ReservationRoomRepeat();
                $repeat->start_time = $start_dt;
                $repeat->end_time = $end_dt;
                $repeat->n_times = $repeat_arr['repeat_times'];
                $repeat->repeat_id = Repeat::getIdByname($repeat_arr['repeat_type']);
                $repeat->opt = implode(',', $repeat_arr['repeat_days']);
                $repeat->room_id = $room;
                $repeat->repeat_end_date = $repeat_arr['repeat_end'];
                $repeat->save();

                $inserted_rr_id = $repeat->reservation_rr_id;

                foreach ($date_days as $start) {

                    $start_dte = new DateTime($start);

                    $interval2 = new DateInterval('P' . $repeat_arr['repeat_times'] . 'W');
                    $end_dte = new DateTime($repeat_arr['repeat_end']);

                    $period = new DatePeriod($start_dte, $interval2, $end_dte);

                    foreach ($period as $time) {
                        $start_time = $time->format('Y-m-d H:i:s');
                        $end = explode(" ", $start_time);
                        $end_dt = $end[0] . " " . $end_time;

                        $reservation = new ReservationRoom();
                        $reservation->start_time = $start_time;
                        $reservation->end_time = $end_dt;
                        $reservation->user_id = $loggedInUser;
                        $reservation->reservation_name = $r_name;
                        $reservation->reservation_desc = $r_desc;
                        $reservation->dept_id = $r_dept;
                        $reservation->room_id = $room;
                        $reservation->created_for = $r_for;
                        $reservation->rr_id = $inserted_rr_id;

                        //set all the reservations to approved since the booking are made by the administrators
                        $reservation->approval = 'Approved';

                        if (is_null($rr_type))
                            $reservation->rr_type = 0;
                        else
                            $reservation->rr_type = $rr_type;


                        if ($reservation->save()) {

                        }


                    }
                    if (is_null($r_for) || $r_for == "") {
                        $recipient = Auth::user()->user_id;
                    } else {
                        $recipient = $r_for;
                    }
                }

            }


        } else {
            //things remain the same
            $period = new DatePeriod($start_date, $interval, $end);


            //check for overlaps in all the rooms and all the time periods
            $has_no_overlap = $this->hasNoOverlap($r_rooms, $start_dt, $end_time, $period);
            if (is_bool($has_no_overlap) != true) {
                //break; //causing issues in booking
                return $has_no_overlap;
            } else {
                foreach ($r_rooms as $room) {
                    //Add Repeat first
                    $repeat = new ReservationRoomRepeat();

                    $repeat->start_time = $start_dt;
                    $repeat->end_time = $end_dt;
                    $repeat->n_times = $repeat_arr['repeat_times'];
                    $repeat->repeat_id = Repeat::getIdByname($repeat_arr['repeat_type']);
                    $repeat->opt = "";
                    $repeat->room_id = $room;
                    $repeat->repeat_end_date = $repeat_arr['repeat_end'];
                    $repeat->save();

                    $inserted_rr_id = $repeat->reservation_rr_id;

                    foreach ($period as $time) {

                        $start = $time->format('Y-m-d H:i:s');
                        $end = explode(" ", $start);
                        $end_dt = $end[0] . " " . $end_time;

                        $reservation = new ReservationRoom();
                        $reservation->start_time = $start;
                        $reservation->end_time = $end_dt;
                        $reservation->user_id = $loggedInUser;
                        $reservation->reservation_name = $r_name;
                        $reservation->reservation_desc = $r_desc;
                        $reservation->dept_id = $r_dept;
                        $reservation->room_id = $room;
                        $reservation->created_for = $r_for;
                        $reservation->rr_id=$inserted_rr_id;
                        if (is_null($rr_type))
                            $reservation->rr_type = 0;
                        else
                            $reservation->rr_type = $rr_type;

                        if ($reservation->save()) {

                        }
                    }

                    if (is_null($r_for) || $r_for == "") {
                        $recipient = Auth::user()->user_id;
                    } else {
                        $recipient = $r_for;
                    }

                }
                return true;
            }


            //Return the error if present else add them
        }
    }

    /**
     * Check whether or not the reservation in question is overlapping with any other for the same room
     * @param type $rooms
     * @param type $start
     * @param type $end
     * @param type $period
     * @return boolean|String String if overlaps, true if otherwise
     * @example checkOverlap([4],'2015-01-06 19:00:00' , '2015-01-06 20:00:00',DatePeriodObject);
     */
    public function hasNoOverlap($rooms, $start, $end, $period = "")
    {

        if ($period != "") {

            //Select all with repeat reservations for the room that have conflicting begin and end time irrespective of the dates
            $room_ids = implode(',', $rooms);

            $Errors = "";
            foreach ($period as $date):

                $single_start_date = $date->format('Y-m-d H:i');
                $single_end_date = $date->format('Y-m-d ') . $end;

                $hasOverlap = ReservationRoom::
                select('*')
                    ->whereRaw("room_id IN ($room_ids)")
                    ->whereRaw("start_time < '$single_end_date' AND end_time > '$single_start_date'")
                    ->get();

                if (count($hasOverlap) > 0) {
                    //there is an overlap
                    //Get all the overlapping stuff
                    foreach ($hasOverlap as $single) {
                        //Get the collision details
                        $Errors .= $this->getReservationDetailsStr($single);
                    }
                }

            endforeach;


            if ($Errors != "") {

                return "<b>One or more conflict(s) encountered: </b><br/>" . $Errors;
            } else {

                return true;
            }
        } else {

            //Select all with repeat reservations for the room that have conflicting begin and end time irrespective of the dates
            $room_ids = implode(',', $rooms);

            $hasOverlap = ReservationRoom::
            select('*')
                ->whereRaw("room_id IN ($room_ids)")
                ->whereRaw("start_time < '$end' AND end_time > '$start'")
                ->get();

            if (count($hasOverlap) > 0) {
                //there is an overlap
                //Get all the overlapping stuff
                $Errors = "";
                foreach ($hasOverlap as $single) {
                    //Get the collision details
                    $Errors .= $this->getReservationDetailsStr($single);
                }
                return "<b>One or more conflict(s) encountered: </b><br/>" . $Errors;
            } else {
                return true;
            }
        }
    }

    /**
     * Make sure the edited room does not overlap with another event-on edit event
     * @param $room
     * @param $start
     * @param $end
     * @param $r_id
     */
    public function hasNoOverlapEdit($rooms,$start,$end,$r_id){

        //Select all with repeat reservations for the room that have conflicting begin and end time irrespective of the dates
        $room_ids = implode(',', $rooms);

        $hasOverlap = ReservationRoom::
        select('*')
            ->whereRaw("room_id IN ($room_ids)")
            ->whereRaw("start_time < '$end' AND end_time > '$start'")
            ->whereRaw("reservation_room_id != $r_id")
            ->get();

        if (count($hasOverlap) > 0) {
            //there is an overlap
            //Get all the overlapping stuff
            $Errors = "";
            foreach ($hasOverlap as $single) {
                //Get the collision details
                $Errors .= $this->getReservationDetailsStr($single);
            }
            return "<b>One or more conflict(s) encountered: </b><br/>" . $Errors;
        } else {
            return true;
        }

    }

    /**
     * Return the conflicting booking
     * @param type $reservation
     * @return String
     */
    public function getReservationDetailsStr($reservation)
    {

        //name,start and end, room,booked by
        $st_dt = $reservation->start_time;
        $start_time = date('l jS F Y H:i', strtotime($st_dt));
        $end_time = date('l jS F Y H:i', strtotime($reservation->end_time));
        $user = User::getUserById($reservation->user_id);
        $room = Room::getRNameById($reservation->room_id);

        $date_link = date('Y-m-d', strtotime($st_dt));

        return "$room is booked by $user between <a class='errorlink' href='/cal/$date_link'>$start_time and $end_time</a><br/>";
    }

    /**
     * @author jberanek <MBRS>
     *
     * Returns a list of the repeating entrys
     *
     * $time     - The start time
     * $enddate  - When the repeat ends
     * $rep_type - What type of repeat is it
     * $rep_opt  - The repeat entrys
     * $max_ittr - After going through this many entrys assume an error has occured
     * $_initial_weeknumber - Save initial week number for use in 'monthly repeat same week number' case
     *
     * Returns:
     *   empty     - The entry does not repeat
     *   an array  - This is a list of start times of each of the repeat entrys
     */
    function getRepeatEntryList($time, $enddate, $rep_type, $rep_opt, $max_ittr, $rep_num_weeks)
    {

        $sec = date("s", $time);
        $min = date("i", $time);
        $hour = date("G", $time);
        $day = date("d", $time);
        $month = date("m", $time);
        $year = date("Y", $time);

        $_initial_weeknumber = (int)(($day - 1) / 7) + 1;
        $week_num = 0;

        $start_day = date('w', mktime($hour, $min, $sec, $month, $day, $year));
        $cur_day = $start_day;

        $entrys = "";
        for ($i = 0; $i < $max_ittr; $i++) {

            $time = mktime($hour, $min, $sec, $month, $day, $year);
            if ($time > $enddate) {
                break;
            }

            $entrys[$i] = $time;

            switch ($rep_type) {
                // Daily repeat
                case 1:
                    $day += 1;
                    break;

                // Weekly repeat
                case 2:
                    $j = $cur_day = date("w", $entrys[$i]);
                    // Skip over days of the week which are not enabled:
                    while (($j = ($j + 1) % 7) != $cur_day && !$rep_opt[$j]) {
                        $day += 1;
                    }

                    $day += 1;
                    break;

                // Monthly repeat
                case 3:
                    $month += 1;
                    break;

                // Yearly repeat
                case 4:
                    $year += 1;
                    break;

                // Monthly repeat on same week number and day of week
                case 5:
                    $day += same_day_next_month($time);
                    break;

                // n Weekly repeat
                case 6:
                    // Loop until we hit the end time
                    while ($time <= $enddate) {
                        $day++;
                        $cur_day = ($cur_day + 1) % 7;

                        if (($cur_day % 7) == $start_day) {
                            $week_num++;
                        }

                        if (($week_num % $rep_num_weeks == 0) &&
                            ($rep_opt[$cur_day] == 1)
                        ) {
                            break;
                        }
                    }

                    break;

                // Unknown repeat option
                default:
                    return;
            }
        }

        return $entrys;
    }

    function getDateFromDay($start, $r_days)
    {

        $start_date = date("d-m-Y", strtotime($start));

        // Assuming $date is in format DD-MM-YYYY
        list($day, $month, $year) = explode("-", $start_date);

        // Get the weekday of the given date
        $wkday = date('l', mktime('0', '0', '0', $month, $day, $year));

        switch ($wkday) {
            case 'Monday':
                $numDaysToMon = 0;
                break;
            case 'Tuesday':
                $numDaysToMon = 1;
                break;
            case 'Wednesday':
                $numDaysToMon = 2;
                break;
            case 'Thursday':
                $numDaysToMon = 3;
                break;
            case 'Friday':
                $numDaysToMon = 4;
                break;
            case 'Saturday':
                $numDaysToMon = 5;
                break;
            case 'Sunday':
                $numDaysToMon = 6;
                break;
        }

        // Timestamp of the monday for that week
        $monday = mktime('0', '0', '0', $month, $day - $numDaysToMon, $year);

        $seconds_in_a_day = 86400;

        //Static array of weekdays
        $weekdays = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");

        // Get date for 7 days from Monday (inclusive)
        for ($i = 0; $i < 7; $i++) {
            $dates[$weekdays[$i]] = date('Y-m-d', $monday + ($seconds_in_a_day * $i));
        }

        //Remove days that we do not need - if they exist.
        if ($r_days != NULL)
            $d = array_intersect_key($dates, array_flip($r_days));
        else
            $d = $dates;

        return $d;

    }

    public function deleteReservation($id)
    {


    }

    public function deleteReservationAjax($id)
    {

    }

    /**
     * @param $repeat_reservation_id
     * @return array
     */
    public static function getRoomIds($repeat_reservation_id){

        if( $repeat_reservation_id == 0  ):
            return [];
        else:
            $ids = ReservationRoom::where('rr_id','=',$repeat_reservation_id)->lists('reservation_room_id');
            return $ids;
        endif;

    }

}
