<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of Events
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Events extends Eloquent
{

    protected $table = 'events';
    protected $primaryKey = 'id';
    //What can be filled
    protected $fillable = array('name', 'description','enabled');

    const REMINDER_ROOM_RESERVATION = 'Room reservation reminder';
    const REMINDER_RESERVATION = 'Reservation reminder';
    const REMINDER_FACULTY = 'Faculty reminder';

    /**
     * @param string $key
     * @return array|mixed
     */
    public static function getReminderIntervals($key = '')
    {
        $reminder_intervals = ['2d' => '2 Days Before',
            '3d' => '3 Days Before',
            '5d' => '5 Days Before',
            '6d' => '6 Days Before',
            '7d' => '7 Days Before',
            '14d' => '14 Days Before',
            '21d' => '21 Days Before',
            '28d' => '28 Days Before',
            'weekly' => 'Weekly',
            'monthly' => 'Monthly'
        ];

        if ( $key == '' )
            return $reminder_intervals;
        else
            return $reminder_intervals[$key];

    }
    /**
     * @param $name
     * @param $enabled
     * @return bool
     */
    public static function updateStatus($name,$enabled){
        $sql = "UPDATE sfp_events SET enabled='$enabled' WHERE name='$name'";
        DB::statement($sql);
        return true;
    }

    /**
     * @param $events
     * @return bool
     */
    public static function updateStatuses($events){

        //disable all
        $sql = "UPDATE sfp_events SET enabled='0'";
        DB::statement($sql);

        //enable selected
        foreach ( $events as $e ){
            $sql = "UPDATE sfp_events SET enabled='1' WHERE `id`=$e";
            DB::statement($sql);
        }

        return true;
    }

    /**
     * @param $name
     * @return mixed
     */
    public static function getStatus($name){
        return Events::where('name','=',$name)->first()->enabled;

    }


}