<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */


/**
 * Description of Building
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Department extends Eloquent{
    protected $table = 'depts';
    protected $primaryKey = 'dept_id';
    //What can be filled
    protected $fillable = array('dept_name', 'dept_abbrev', 'dept_desc');
    /**
     * Validate Input Data
     * @param type $input
     * @return type
     */
    public static function validate($input){
        $niceNames = array(
             'name' => 'Name',
             'abbreviation' => 'Abbreviation',
             'description' => 'Description'
         );
        $rules = array(
           'name' => 'Required|alpha_spaces',
           'abbreviation' => 'Required|Alpha|Between:2,7',
           'description' => 'alpha_spaces',
        );
        
        $validator = Validator::make($input, $rules);
        $validator->setAttributeNames($niceNames);
        return $validator;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany('User', 'user_depts','dept_id','user_id');
    }

    /**
     * A Department has many room reservations
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function room_reservations(){
        return $this->hasMany('ReservationRoom','dept_id','dept_name');
    }

    /**
     * @param $name
     * @return mixed
     */
    public static function getDepartmentByName($name){
        return Department::select('dept_id')->where('dept_abbrev','=',$name)->first()->dept_id;
    }
}
