<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of Settings
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Settings extends Eloquent
{

    protected $table = 'settings';
    protected $primaryKey = 'id';
    //What can be filled
    protected $fillable = array('name', 'value');

    /**
     * @param $holidays
     * @return array
     */
    public static function getCurrentHolidays($holidays){

        $holidays_arr = explode(',',$holidays);
        $holidays_clean_array = [];

        foreach($holidays_arr as $h){
            $recurrent = strpos($h,'&r');

            if ( $recurrent ){
                $holiday = date('F jS, Y',strtotime(substr($h,0,$recurrent).'-'.date('Y'))) .'%r';
            }else{

                $holiday = date('F jS, Y',strtotime(substr($h,0,strpos($h,'&s')))).'%s';
            }

            array_push($holidays_clean_array,$holiday);
        }

        return $holidays_clean_array;

    }

    /**
     * @param $holidays
     * @return string
     */
    public static function getHolidayDBString($holidays){

        $holiday_clean_array = [];
        foreach($holidays as $h){

            $hl = explode('%',$h);
            if ( $hl[1] == "s" ){
                $hl_str = date('Y-m-d',strtotime($hl[0])).'&s';
            }

            if ( $hl[1] == "r" ){
                $hl_str = date('d-m',strtotime($hl[0])).'&r';
            }

            array_push($holiday_clean_array,$hl_str);

        }

        return implode($holiday_clean_array,',');

    }

    /**
     * @return array
     */
    public static function getHolidayForCalendar(){
        $holidays = Settings::getVal('holidays');
        $holidays_arr = explode(',',$holidays);

        $holiday_clean_array = [];

        foreach($holidays_arr as $h){

            $hl = explode('&',$h);

            if ( $hl[1] == "s" ){
                $hl_str = date('Y-m-d',strtotime($hl[0]));
            }

            if ( $hl[1] == "r" ){
                $hl_str = date('Y-m-d',strtotime($hl[0].'-'.date('Y'))).'';
            }

            array_push($holiday_clean_array,$hl_str);

        }


        return $holiday_clean_array;
    }

    /**
     * @param $name
     * @param $value
     * @return bool
     */
    public static function updateVal($name,$value){
        $sql = "UPDATE sfp_settings SET value='$value' WHERE name='$name'";
        DB::statement($sql);
        return true;
    }

    /**
     * @param $name
     * @return mixed
     */
    public static function getVal($name){
        return Settings::where('name','=',$name)->first()->value;
    }
}
