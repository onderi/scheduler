<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of Subscription
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Subscription extends Eloquent
{

    protected $table = 'subscriptions';
    protected $primaryKey = 'id';
    //What can be filled
    protected $fillable = array('subscriber', 'event_id','subscribed_to','recurring','interval');

    const TYPE_EVENT_SUBSCRIPTION = 0;
    const TYPE_REMINDER_SUBSCRIPTION = 1;

    /**
     * @return string
     */
    public static function addEventSubscription($subscriber,$subscribed_to){

        Subscription::create([
            'event_id' => $subscribed_to,
            'subscriber' => $subscriber,
            'subscribed_to' => $subscribed_to
        ]);

        return '1';

    }

}