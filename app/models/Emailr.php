<?php
/*
  Class to send emails - queue mode
*/
class Emailr {

    public static function queue($tmpl,$new_stat, $data, $to ,$subject,$to_for) {
        Queue::push(function($job) use ($data, $to ,$subject,$to_for,$tmpl,$new_stat) {
            Emailr::send($tmpl,$new_stat, $data, $to ,$subject,$to_for);
        });
        Log::info("Mail [$subject] QUEUED to $to");
    }

    public static function send($tmpl,$new_stat, $data, $to ,$subject,$to_for) {

        Mail::send(['html' => $tmpl], $data, function ($message) use ($data, $to ,$subject,$to_for,$new_stat) {

            if( $new_stat ){

              $message->to($to_for)->cc($to)->subject($subject);

            }else{
              $message->to($to)->subject($subject);

              //if there is an email
              if(isset($to_for) && $to_for != null ):
                  $message->cc($to_for);
              endif;

            }

            $message->from('sbs_scheduler@strathmore.edu', 'SBS Scheduler');
        });

        Log::info("Mail [$subject] SENT to $to");

    }

}
