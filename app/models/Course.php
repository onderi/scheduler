<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */


/**
 * Description of Course
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Course extends Eloquent{

    protected $table = "courses";
    protected $primaryKey = "course_id";
    protected $fillable = array('course_id','course_code','course_title','course_desc','category','startdate','sortorder');

    /**
     * A course belongs to one course category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ccategory(){
        
        return $this->belongsTo('CourseCategory','category','id');
        
    }

    /**
     * Synchronize courses
     * @param bool $command
     */
    public static function SynchronizeCourses($command = false){

        if ( $command )
            echo "Course Category Sync Started".PHP_EOL;

        $courses = DB::connection('elmysql')->table('course')
            ->select(DB::raw('*'))
            ->where('visible','=','1')
            ->where('category','!=','0')
            ->get();


        foreach( $courses as $course ){

            $db_course = Course::find($course->id);

            if( $db_course == null ){

                if ( $command )
                    echo ':..::..:: Inserting '.$course->fullname.PHP_EOL;

                Course::create([
                    'course_id' => $course->id,
                    'course_title' => $course->fullname,
                    'course_code' => $course->shortname,
                    'course_desc' => $course->summary,
                    'category' => $course->category,
                    'sortorder' => $course->sortorder,
                    'startdate' => $course->startdate
                ]);

            }else{

                if( $command )
                    echo ":..::..:: Updating ".$course->fullname.PHP_EOL;

                $db_course->course_id = $course->id;
                $db_course->course_title = $course->fullname;
                $db_course->course_code = $course->shortname;
                $db_course->course_desc = $course->summary;
                $db_course->category = $course->category;
                $db_course->sortorder = $course->sortorder;
                $db_course->startdate = $course->startdate;

                $db_course->save();
            }

        }

        if ( $command )
            echo PHP_EOL.'::..::-- Course Sync Completed --::..::';
        else
            return true;

    }
}
