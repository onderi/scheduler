<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of ReservationRoomRepeat
 *
 * @author kinyua
 */
class ReservationRoomRepeat extends Eloquent {

    //Monitor the CRUD fellaz..
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $table = "reservation_room_repeats";
    protected $primaryKey = "reservation_rr_id";
    protected $fillable = array(
    'start_time',
      'end_time',
      'repeat_id',
      'n_times',
      'opt',
      'room_id',
       'repeat_end_date'
       );

    public function rr_repeats() {
        return $this->belongsTo('ReservationRoom');
    }

}
