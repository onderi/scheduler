<?php
/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of Status
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Status extends Eloquent {
    protected $table = "statuses";
    protected $primaryKey = 'status_id';

    /*
     * 1 to 1 Relationship
     */
    public function user(){
        return $this->belongsTo('User');
    }
}