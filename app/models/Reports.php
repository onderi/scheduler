<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

class Reports extends \Eloquent {
	protected $fillable = [];
	public  static $RESERVATION_TITLE_MEMBERS = array('CEM','MBA',"IHM","MPPM","CPM","EHMP","WILLS","PMD","SMLP","OMP","AMP","Meeting","DLC","CPM","LeHHO","Bio Entrepreneurship","GEE","NMLP","MHB","CPPC","MSH","HCM","CIPIT","BBK","CAP","TED","WELL","EXAM","SEDC","SDRC");

	/**
	 * @return bool|string
	 * @desc One month ago
	 */
	public static function getDefaultStart(){
		return date("Y-m-d H:i", strtotime( date( "Y-m-d H:i", strtotime( date("Y-m-d H:i") ) ) . "-1 month" ) );
	}

	/**
	 * @return bool|string
	 * @desc Today's date
	 */
	public static function getDefaultEnd(){
		return date("Y-m-d H:i");
	}

	/**
	 * Get the reservation data in a formatted manner
	 *  - [
	 * 		{
	 * 		title : abc,
	 * 		value : 2,
	 * 		data : [//for other data types
	 * 			{title:db,value:3}
	 * 			]
	 * 		}
	 * 	  ]
	 * @param $rdata
	 * @return array
	 */
	public static function _getReserveData($rdata,$report_type,$utilisationUsed){
		$reservation_data_unclean = explode(',', $rdata);
		$result = array();

		switch ($report_type){
			case "# Room Bookings":
				$allowedNames = Reports::_buildArrayAllowedNames();
				$reservations = $reservation_data_unclean;

				$matchedItems = 0;
				$allowedNamesCount = count($allowedNames);
				$reservationsCount = count($reservations);
				$counter = 0;

				foreach ( $allowedNames as $allowed ) {
					++$counter;
					foreach ( $reservations as $r ) {

						//mba-hcm hack
						$rname_ = preg_replace("/MBA HCM/","/HCM/",$r);

						if ( strpos( $rname_, $allowed ) !== false) {
							$matchedItems++;
							if( isset($result[$allowed]) )
								$result[$allowed] = array("title" => $allowed,"value" => $result[$allowed]['value'] + 1);
							else
								$result[$allowed] = array("title" => $allowed,"value" => 1);

						}
						//last item - set other
						if ( $counter == $allowedNamesCount ){
							$diff = $reservationsCount - $matchedItems;

							if ( $diff > 0 )
								$result['Other'] = array("title" => 'Other',"value" => $diff);
						}

					}

				}

				break;

			case "# Hours":
				$allowedNames = Reports::_buildArrayAllowedNames();
				$reservations = $reservation_data_unclean;

				$utilFractionTime = 0;
				$allowedNamesCount = count($allowedNames);

				$counter = 0;

				foreach ( $allowedNames as $allowed ) {

					++$counter;
					foreach ( $reservations as $r ) {

						$r_array = explode("----",$r);
						$rname = $r_array[0];

						if( isset($r_array[1]) )
							$r_util_value = $r_array[1];
						else
							$r_util_value = 0;

						//mba-hcm hack
						$rname_ = preg_replace("/MBA HCM/","/HCM/",$rname);

						if ( strpos( $rname_, $allowed ) !== false) {

							$utilFractionTime += $r_util_value;
							if( isset($result[$allowed]) )
								$result[$allowed] = array("title" => $allowed,"value" => $result[$allowed]['value'] + $r_util_value);
							else
								$result[$allowed] = array("title" => $allowed,"value" => $r_util_value);
						}

						//last item - set other
						if ( $counter == $allowedNamesCount ){
							$diff = $utilisationUsed - $utilFractionTime;

							if ( $diff > 0 )
								$result['Other'] = array("title" => 'Other',"value" => $diff);
						}

					}

				}

				break;

			case "Reservation type":

				$allowedNames = Reports::_buildArrayAllowedNames();
				$reservations = $reservation_data_unclean;

				$matchedItems = 0;
				$allowedNamesCount = count($allowedNames);
				$reservationsCount = count($reservations);
				$counter = 0;

				foreach ( $allowedNames as $allowed ) {
					++$counter;
					foreach ( $reservations as $r ) {

						//mba-hcm hack
						$rname_ = preg_replace("/MBA HCM/","/HCM/",$r);

						if ( strpos( $rname_, $allowed ) !== false) {
							//echo $rname_.'----'.$allowed.'<br/>';
							$matchedItems++;
							if( isset($result[$allowed]) )
								$result[$allowed] = array("title" => $allowed,"value" => $result[$allowed]['value'] + 1);
							else
								$result[$allowed] = array("title" => $allowed,"value" => 1);
						}

						//last item - set other
						if ( $counter == $allowedNamesCount ){
							$diff = $reservationsCount - $matchedItems;

							if ( $diff > 0 )
								$result['Other'] = array("title" => 'Other',"value" => $diff);
						}

					}

				}

				break;
		}


//dd();
		return array_values($result);
	}

	/**
	 * @param $startd
	 * @param $endd
	 * @return mixed
	 */
	public static function calculateFullUtilization($startd,$endd){
		$start = new DateTime($startd);
		$end = new DateTime($endd);

		// otherwise the  end date is excluded (bug?)
		$end->modify('+1 day');

		$interval = $end->diff($start);

		// total days
		$days = $interval->days;

		// create an iterateable period of date (P1D equates to 1 day)
		$period = new DatePeriod($start, new DateInterval('P1D'), $end);

		// best stored as array, so you can add more than one
		$holidays = array('01-01','01-06','01-07','20-08','12-12','25-12','26-12');

		foreach($period as $dt) {
			$curr = $dt->format('D');

			// for the updated question
			if (in_array($dt->format('m-d'), $holidays)) {
				$days--;
			}

			// substract if Sunday
			if ($curr == 'Sun') {
				$days--;
			}
		}

		$total_utilization = $days;

		return $total_utilization;
	}

	/**
	 * Get the list of allowed group members for the second level categorization of room usage
	 * @return array
	 */
	public static function _buildArrayAllowedNames(){

		$departments = Department::lists('dept_abbrev');
		$members = array_merge($departments,Reports::$RESERVATION_TITLE_MEMBERS);
		$allowed  = array_unique($members);
		return $allowed;

	}

	/**
	 * @param $room_ids
	 * @param $report_type
	 * @param $start
	 * @param $end
	 * Get the ChartData
	 */
	public static function getUsageData($room_ids,$report_type,$start,$end,$retun_type = "json"){

		$start_date = date('Y-m-d H:i',strtotime($start));
		$end_date = date('Y-m-d H:i',strtotime($end));
		$total_utilization = Reports::calculateFullUtilization($start_date,$end_date);
		switch ($report_type){
			case  "# Room Bookings":
				$rreservations = DB::table('reservation_rooms')->select(DB::raw('room_name as title,count(*) as value,GROUP_CONCAT(reservation_name) as reservations'))
					->join('rooms','rooms.room_id','=','reservation_rooms.room_id')
					->whereIn('rooms.room_id',$room_ids)
					->where('reservation_rooms.start_time','<=',$end_date)
					->where('reservation_rooms.end_time','>=',$start_date)
					->groupBy('rooms.room_name')->get();

				$dataCount = count($rreservations);
				$final_array = [];

				for( $j=0; $j<$dataCount; $j++ ) {
					$data = [];
					$rr = $rreservations[$j];

					$final_array[$j]['title'] = $rr->title;
					$totalUtilizationUsed = $rr->value;
					$final_array[$j]['value'] = $totalUtilizationUsed;

					$data = Reports::_getReserveData($rr->reservations,"# Room Bookings",$totalUtilizationUsed);

					$final_array[$j]['data'] = $data;
				}

				return json_encode($final_array);

				break;
			case  "# Hours":
				$rreservations = DB::table('reservation_rooms')->select(DB::raw('room_name as title,SUM( TIMESTAMPDIFF(HOUR,start_time,end_time) / 11 ) as value,GROUP_CONCAT(reservation_name,"----",TIMESTAMPDIFF(HOUR,start_time,end_time) / 11) as reservations'))
					->join('rooms','rooms.room_id','=','reservation_rooms.room_id')
					->whereIn('rooms.room_id',$room_ids)
					->where('reservation_rooms.start_time','<=',$end_date)
					->where('reservation_rooms.end_time','>=',$start_date)
					->groupBy('rooms.room_name')->get();

				$dataCount = count($rreservations);
				$final_array = [];

				for( $j=0; $j<$dataCount; $j++ ) {
					$data = [];
					$rr = $rreservations[$j];

					$final_array[$j]['title'] = $rr->title;
					$totalUtilizationUsed = $rr->value;
					$final_array[$j]['value'] = $totalUtilizationUsed;

					$data = Reports::_getReserveData($rr->reservations,"# Hours",$totalUtilizationUsed);

					$final_array[$j]['data'] = $data;
				}

				return json_encode($final_array);

				break;

			case  "Reservation type":

				$rreservations = DB::table('reservation_rooms')->select(DB::raw('IF(rr_type = 1 ,"External","Internal")as title,count(*) as value,GROUP_CONCAT(reservation_name) as reservations'))
					->join('rooms','rooms.room_id','=','reservation_rooms.room_id')
					->whereIn('rooms.room_id',$room_ids)
					->where('reservation_rooms.start_time','<=',$end_date)
					->where('reservation_rooms.end_time','>=',$start_date)
					->groupBy('reservation_rooms.rr_type')->get();

				$dataCount = count($rreservations);
				$final_array = [];

				for( $j=0; $j<$dataCount; $j++ ) {
					$data = [];
					$rr = $rreservations[$j];

					$final_array[$j]['title'] = $rr->title;
					$totalUtilizationUsed = $rr->value;
					$final_array[$j]['value'] = $totalUtilizationUsed;

					$data = Reports::_getReserveData($rr->reservations,"Reservation type",$totalUtilizationUsed);

					$final_array[$j]['data'] = $data;
				}

				return json_encode($final_array);
				break;

			default :
				//do nothing.
				break;
		}

	}

}