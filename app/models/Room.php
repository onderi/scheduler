<?php

/*
  |--------------------------------------------------------------------------
  | All rights reserved - The Strathmore Business School
  |--------------------------------------------------------------------------
  |
  | The SBS Scheduler
  |
 */

/**
 * Description of Room
 *
 * @author Kinyua Wallace <kinyuawallace at gmail.com>
 */
class Room extends Eloquent{
    
    protected $table = "rooms";
    protected $primaryKey = 'room_id';
    
    protected $fillable = array('room_name', 'capacity','area_id','ccode');
    
     /**
     * Validate Input Data
     * @param type $input
     * @return type
     */
    public static function validate($input){
         $niceNames = array(
             'name' => 'Name',
             'capacity' => 'Capacity',
             'ccode'=>'Color Code',
             'area' => 'Room Area'
         );
        
        $rules = array(
           'name' => 'Required|alpha_spaces',
           'capacity' => 'Required|integer|max:200',
           
           'area' => 'Required'
        );
        
        $validator = Validator::make($input, $rules);
        $validator->setAttributeNames($niceNames);
        return $validator;
    }
    
    /**
     * Belongs to building
     * @return type
     */
    public function building(){
        return $this->belongsTo('Building');
    }
   /**
    * Belongs to Many One or Zero Areas
    * @return type
    */
    public function area(){
        return $this->belongsTo('Area');
    }
    /**
     * A Room has many reservations made
     * @return type
     */
    public function room_reservations(){
        return $this->hasMany('ReservationRoom','room_id','room_id');
    }
    /**
     * Get the room name by id
     * @param type $room_id
     * @return String
     */
    public static function getRNameById($room_id){
        return Room::select('room_name')->where('room_id','=',$room_id)->first()->room_name;
    }

    /**
     * @param $name
     * @return array
     */
    public static function getRoomByName($name){
        $room_id = Room::select('room_id')->where('room_name','=',$name)->first()->room_id;
        return [$room_id];
    }


    /**
     * Get the room as a concatenated string
     * @param $area
     * @return string
     */
    public static function _getRoomsString($area){
        $rooms_array = Calendar::_getRooms($area,"array");

        $room_id = "";

        $room_count = count($rooms_array);

        for ( $i = 0; $i < $room_count;$i++ ) {
            if ( $i == $room_count - 1 ){
                $room_id .= $rooms_array[$i]['id'];
            }else{
                $room_id .= $rooms_array[$i]['id'] . ',';
            }
        }

        return $room_id;
    }


    /**
     * Get the room as a concatenated string
     * @param $area
     * @return array
     */
    public static function _getRoomsArray($area){
        $rooms_array = Calendar::_getRooms($area,"array");

        $room_ids = [];

        $room_count = count($rooms_array);

        for ( $i = 0; $i < $room_count;$i++ ) {
            $room_ids[$i] = $rooms_array[$i]['id'];
        }

        return $room_ids;
    }

}
