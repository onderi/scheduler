<?php

class LintCommand extends \Illuminate\Console\Command {
  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'command:lint';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'PHP code linter.';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function fire()
  {
    $this->check_php(dirname(dirname(dirname(__FILE__))));
  }

  /**
   * Checks syntax of PHP scripts.
   * @param $directory The directory of files to recursively process.
   * @return void
   */
  private function check_php($directory)
  {
    $dir = opendir($directory);
    while (false !== ($file = readdir($dir))) {
      if (in_array($file, array('.', '..', 'public', 'vendor')) || substr($file, 0, 1) == '.') {
        continue;
      }

      $filename = $directory . '/' . $file;
      if (!is_dir($filename) && strtolower(substr($file, -4)) == ".php") {
        $result = shell_exec('php -l ' . $filename);
        if (strpos($result, 'No syntax errors detected in ' . $filename) === false) {
          echo $result;
          exit(2);
        }
      }

      if (is_dir($filename)) {
        $this->check_php($filename);
      }
    }
  }
}
?>