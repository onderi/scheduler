<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ElearningSyncCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'elearning:sync';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Retrieve e-learning course data';

    /**
     * Create a new command instance.
     * ElearningSyncCommand constructor.
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

	    //Fetch the course categories and update the database accordingly
	    ElearningSyncCommand::SyncCourseCategories();

	    //Fetch the courses and update the database accordingly
	    ElearningSyncCommand::SyncCourses();

	}

    /**
     * Synchronize course categories
     */
	public static function SyncCourseCategories(){

	   CourseCategory::SynchronizeCourseCategories(true);

    }

    /**
     * Synchronize Courses
     */
    public static function SyncCourses(){

       Course::SynchronizeCourses(true);

    }


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
