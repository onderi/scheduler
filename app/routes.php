<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

//Authentication
Route::get('login', ['uses' => 'UserController@login']);
Route::post('user/login', ['uses' => 'UserController@do_login']);
Route::get('user/forgot_password', ['uses' => 'UserController@forgot_password']);
Route::post('user/forgot_password', ['uses' => 'UserController@do_forgot_password']);
Route::get('user/reset_password/{token}', ['uses' => 'UserController@reset_password']);
Route::post('user/reset_password', ['uses' => 'UserController@do_reset_password']);

//Calendar
Route::get('calendar/getAll', ['uses' => 'CalendarController@getAllReservations', 'as' => 'calendar/getAll']);
Route::get('calendar/getTimesheets', ['uses' => 'CalendarController@getAllTimesheets', 'as' => 'calendar/getTimesheets']);
Route::get('calendar/getAllCr', ['uses' => 'CalendarController@getAllCourseReservations', 'as' => 'calendar/getAllCr']);
Route::get('calendar/getSome/{room_area}', ['uses' => 'CalendarController@getAllReservations', 'as' => 'calendar/getSome']);
Route::get('calendar/getRoomAreas', ['uses' => 'CalendarController@getAllRoomAreas', 'as' => 'calendar/getRoomAreas']);
Route::get('calendar/getRooms/{id}', ['uses' => 'CalendarController@getAllRooms', 'as' => 'calendar/getRooms']);
Route::get('calendar/getResources', ['uses' => 'CalendarController@getResources', 'as' => 'calendar/getResources']);
Route::get('calendar/getFacultyResources', ['uses' => 'CalendarController@getFacultyResources', 'as' => 'calendar/getFacultyResources']);
Route::get('calendar/getResourcesFromArray', ['uses' => 'CalendarController@getResourcesFromArray', 'as' => 'calendar/getResourcesFromArray']);
Route::post('calendar/deleteReserve', ['uses' => 'CalendarController@deleteCalendarReservation', 'as' => 'calendar/deleteReserve']);


//timetable
Route::get('reports',['uses'=>'ReportsController@index','as'=>'reports']);
Route::post('report/generate',['uses'=>'ReportsController@generate','as'=>'report/generate']);
Route::get('report/timetable/{course_id}/{start}/{end}',['uses'=>'ReportsController@timetable_pdf','as'=>'report/timetable'])->where('course_id','[0-9]+');

//General User routes
Route::group(array('before'=>'auth'),function(){
    Route::get('user/logout', ['uses' => 'UserController@logout']);
    /*Routes for normal user - security! */
    Route::get('user/{id}/u_edit', ['uses' => 'UserController@edit_user', 'as' => 'user/u_edit'])->where('id', '[0-9]+');
    Route::post('user/u_update/{id}', ['uses' => 'UserController@user_update', 'as' => 'user/u_update'])->where('id', '[0-9]+');

});

Route::group(array('before'=>array('auth')),function(){
    //Default route
    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);
    Route::get('home', ['uses' => 'HomeController@index', 'as' => 'home']);

});


//System Admin Routes
Route::group(array('before'=>array('auth|sysAdmin')),function(){


//Timeline
Route::get("tmln", "UserController@tmln");

// User routes
Route::get('users', ['uses' => 'UserController@index', 'as' => 'users']);
Route::get('user/add', ['uses' => 'UserController@add', 'as' => 'user/add']);
Route::post('user/save', ['uses' => 'UserController@store']);
Route::get('user/{id}/edit', ['uses' => 'UserController@edit', 'as' => 'user/edit'])->where('id', '[0-9]+');
Route::post('user/update/{id}', ['uses' => 'UserController@update', 'as' => ''])->where('id', '[0-9]+');
Route::post('user/delete/{id}', ['uses' => 'UserController@destroy'])->where('id', '[0-9]+');
Route::post('user/status', ['uses' => 'UserController@status']);
Route::post('user/roles', ['uses' => 'UserController@assigned_roles']);
Route::post('role/getUsers', ['uses' => 'RoleController@getUsers']);


//Departments
Route::get('departments', ['uses' => 'DepartmentController@index', 'as' => 'departments']);
Route::get('department/add', ['uses' => 'DepartmentController@create', 'as' => 'department/add']);
Route::post('department/save', 'DepartmentController@store');
Route::get('department/{id}/edit', ['uses' => 'DepartmentController@edit', 'as' => 'department/edit'])->where('id', '[0-9]+');
Route::post('department/update/{id}', 'DepartmentController@update')->where('id', '[0-9]+');
Route::post('department/delete/{id}', 'DepartmentController@destroy')->where('id', '[0-9]+');

//Buildings
Route::get('buildings', ['uses' => 'BuildingController@index', 'as' => 'buildings']);
Route::get('building/add', ['uses' => 'BuildingController@create', 'as' => 'building/add']);
Route::post('building/save', 'BuildingController@store');
Route::get('building/{id}/edit', ['uses' => 'BuildingController@edit', 'as' => 'building/edit'])->where('id', '[0-9]+');
Route::post('building/update/{id}', 'BuildingController@update')->where('id', '[0-9]+');
Route::post('building/delete/{id}', 'BuildingController@destroy')->where('id', '[0-9]+');

//Rooms
Route::get('rooms', ['uses' => 'RoomController@index', 'as' => 'rooms']);
Route::get('room/add', ['uses' => 'RoomController@create', 'as' => 'room/add']);
Route::post('room/save', 'RoomController@store');
Route::get('room/{id}/edit', ['uses' => 'RoomController@edit', 'as' => 'room/edit'])->where('id', '[0-9]+');
Route::post('room/update/{id}', 'RoomController@update')->where('id', '[0-9]+');
Route::post('room/delete/{id}', 'RoomController@destroy')->where('id', '[0-9]+');

//Room Areas
Route::get('areas', ['uses' => 'AreaController@index', 'as' => 'areas']);
Route::get('area/add', ['uses' => 'AreaController@create', 'as' => 'area/add']);
Route::post('area/save', 'AreaController@store');
Route::get('area/{id}/edit', ['uses' => 'AreaController@edit', 'as' => 'area/edit'])->where('id', '[0-9]+');
Route::post('area/update/{id}', 'AreaController@update')->where('id', '[0-9]+');
Route::post('area/delete/{id}', 'AreaController@destroy')->where('id', '[0-9]+');


//Course Categories
Route::get('ccategories', ['uses' => 'CourseCategoryController@index', 'as' => 'ccategories']);
Route::get('ccategory/sync', ['uses' => 'CourseCategoryController@SynchronizeCourseCategories', 'as' => 'ccategory/sync']);
Route::post('ccategory/getCourse','CourseCategoryController@getCategoryCourses');
Route::post('ccategory/getCourseSelect','CourseCategoryController@getCategoryCoursesSelect');


//Courses
Route::get('courses', ['uses' => 'CourseController@index', 'as' => 'courses']);
Route::get('course/sync', ['uses' => 'CourseController@SynchronizeCourses', 'as' => 'course/sync']);


//Roles
Route::get('roles', ['uses' => 'RoleController@index', 'as' => 'roles']);
Route::get('role/add', ['uses' => 'RoleController@create', 'as' => 'role/add']);
Route::post('role/save', 'RoleController@store');
Route::get('role/{id}/edit', array('before' => 'role', 'uses' => 'RoleController@edit', 'as' => 'role/edit'))->where('id', '[0-9]+');
Route::post('role/update/{id}', array('before' => 'role', 'uses' => 'RoleController@update'))->where('id', '[0-9]+');
Route::post('role/delete/{id}', array('before' => 'role', 'uses' => 'RoleController@destroy'))->where('id', '[0-9]+');

//Mail Templates
Route::get('mails',['uses'=>'MailController@index','as'=>'mails']);
Route::get('mails/create',['uses'=>'MailController@create','as'=>'mails/create']);
Route::post('mails/store','MailController@store');
Route::get('mails/{id}/edit',['uses'=>'MailController@edit','as'=>'mails/edit'])->where('id','[0-9]+');
Route::post('mails/update/{id}','MailController@update')->where('id','[0-9]+');
Route::post('mails/delete/{id}','MailController@destroy')->where('id','[0-9]+');

// Faculty Planner Bookings
Route::get('lecturer/booking', ['uses' => 'LecturerController@booking', 'as' => 'lecturer/booking']);
Route::get('lecturer/booking/confirm/{reservation_id}', ['uses' => 'LecturerController@confirm_booking', 'as' => 'lecturer/booking/confirm']);
Route::get('lecturer/booking/reject/{reservation_id}', ['uses' => 'LecturerController@reject_booking', 'as' => 'lecturer/booking/reject']);
Route::post('lecturer/book_confirm', 'LecturerController@book_confirm');
Route::get('lecturer/confirmed_booking', ['uses' => 'LecturerController@confirmed_booking', 'as' => 'lecturer/confirmed_booking']);

// Faculty Planner Lecturer Details
Route::get('lecturer/lecturers', ['uses' => 'LecturerController@lecturers', 'as' => 'lecturer/lecturers']);
Route::get('lecturer/lecturers/add_hours/{user_id}', ['uses' => 'LecturerController@add_hours', 'as' => 'lecturer/lecturers/add_hours']);
Route::post('lecturer/lecturers/update_lec_hours/{user_id}', ['uses' => 'LecturerController@update_lec_hours', 'as' => 'lecturer/update_lec_hours']);

Route::get('lecturer/book', ['uses' => 'LecturerController@book', 'as' => 'lecturer/book']);
Route::get('lecturer/booking_filter', ['uses' => 'LecturerController@booking_filter', 'as' => 'lecturer/booking_filter']);

// Faculty Planner Materials
Route::get('lecturer/materials',['uses'=>'LecturerController@materials','as'=>'lecturer/materials']);
Route::get('lecturer/add_materials', ['uses' => 'LecturerController@add_materials', 'as' => 'lecturer/add_materials']);
Route::post('lecturer/save_materials', ['uses' => 'LecturerController@save_materials', 'as' => 'lecturer/save_materials']);
Route::post('lecturer/materials', 'LecturerController@materials_request');
// End of Faculty Planner

Route::get('lecturer/timesheets',['uses'=>'LecturerController@timesheets','as'=>'lecturer/timesheets']);
Route::post('lecturer/refresh_timesheets', ['uses'=>'LecturerController@refresh_timesheets', 'as'=>'lecturer/refresh_timesheets']);
Route::get('lecturer/viewtimesheets/{id}', ['uses' => 'LecturerController@viewtimesheets', 'as' => 'lecturer/viewtimesheets']);

//workloads Routes
Route::get('lecturer/workloads',['uses'=>'LecturerController@workloads','as'=>'lecturer/workloads']);
Route::post('lecturer/refresh_workloads', ['uses'=>'LecturerController@refresh_workloads', 'as'=>'lecturer/refresh_workloads']);
Route::get('lecturer/viewworkloads/{id}', ['uses' => 'LecturerController@viewworkloads', 'as' => 'lecturer/viewworkloads']);

// Lecturer Sending Email
Route::get('send_email','LecturerController@email');


//Announcements
Route::get('announcements',['uses'=>'AnnouncementController@index','as'=>'announcements']);
Route::get('announcement/create',['uses'=>'AnnouncementController@create','as'=>'announcement/create']);
Route::post('announcement/store','AnnouncementController@store');
Route::get('announcement/{id}/edit',['uses'=>'AnnouncementController@edit','as'=>'announcement/edit'])->where('id','[0-9]+');
Route::post('announcement/update/{id}','AnnouncementController@update')->where('id','[0-9]+');
Route::post('announcement/delete/{id}','AnnouncementController@destroy')->where('id','[0-9]+');

//Route::get('initImport',['uses' => 'ImportController@_initImport','as' => 'init.import']);
Route::get('cdata',['uses' => 'ImportController@_cImport','as' => 'cdata.import']);

//Settings
Route::get('settings',['uses'=>'SettingsController@index','as'=>'settings']);
Route::get('setting/add_reminder_subscriber',['uses'=>'SettingsController@add_reminder','as'=>'setting.add_reminder_subscriber']);
Route::post('setting/store','SettingsController@store');
Route::post('setting/event_subscription/save','SettingsController@addEventSubscription');
Route::post('setting/reminder_subscription/save','SettingsController@addReminderSubscription');


});

//Route required by non-admins when booking a new reservation
Route::post('area/getRooms', ['uses' => 'AreaController@getRoomsInArea', 'as' => 'area/getRooms']);

//Booking admin routes
Route::group(array('before'=>array('auth|bookingAdmin')),function(){
    Route::get('reservation/approval',['uses'=>'ReservationApprovalController@index', 'as'=>'reservation/approval']);
    Route::get('api/r_approvals', ['as'=>'api.r_approvals', 'uses'=>'ReservationApprovalController@getDatatable']);

    Route::get('reservation/approval/{id}/edit',['uses'=>'ReservationApprovalController@edit','as'=>'reservation/approval/edit'])->where('id','[0-9]+');
    Route::post('reservation/approval/update/{id}','ReservationApprovalController@update')->where('id','[0-9]+');
    //faculty
    Route::get('faculty/details/create/{id}',['uses'=>'ReservationDetailsController@createLec','as'=>'faculty/details/create'])->where('id','[0-9]+');
    //Reservation Details (modal)
    Route::get('reservation/details/create/{id}',['uses'=>'ReservationDetailsController@create','as'=>'reservation/details/create'])->where('id','[0-9]+');
    Route::post('reservation/details/store','ReservationDetailsController@store');
    Route::get('reservation/details/{id}/edit',['uses'=>'ReservationDetailsController@edit','as'=>'reservation/details/edit'])->where('id','[0-9]+');
    Route::post('reservation/details/update','ReservationDetailsController@update');
    Route::post('reservation/details/delete','ReservationDetailsController@destroy');

        //Reservation details form
        Route::get('reservation/details/create_frm/{id}',['uses'=>'ReservationDetailsController@create_frm','as'=>'reservation/details/create_frm'])->where('id','[0-9]+');
        Route::post('reservation/details/store_frm','ReservationDetailsController@store_frm');
        Route::get('reservation/details/{id}/edit_frm',['uses'=>'ReservationDetailsController@edit_frm','as'=>'reservation/details/edit_frm'])->where('id','[0-9]+');
        Route::post('reservation/details/update_frm','ReservationDetailsController@update_frm');
        Route::post('reservation/details/delete_frm','ReservationDetailsController@destroy_frm');


    //Reservation
    Route::get('reservation/room', ['uses' => 'ReservationController@rr_index', 'as' => 'reservation/room']);
    Route::get('api/r_rooms', ['as'=>'api.r_rooms', 'uses'=>'ReservationController@getDatatable']);

    Route::get('reservation/room/add/{room_id}/{start}/{end}', ['uses' => 'ReservationController@rr_create', 'as' => 'reservation/room/add']);
    Route::post('reservation/room/save', 'ReservationController@rr_store');
    Route::get('reservation/room/{id}/edit', ['uses' => 'ReservationController@rr_edit', 'as' => 'reservation/room/edit'])->where('id', '[0-9]+');
    Route::post('reservation/room/update/{id}', 'ReservationController@rr_update')->where('id', '[0-9]+');
    Route::post('reservation/room/delete/{id}', 'ReservationController@rr_destroy')->where('id', '[0-9]+');
    Route::post('reservation/room/delete_aj', 'ReservationController@delete_ajax');

    //Reports
    Route::get('reports/room/util/',['uses'=>'ReportsController@room_util','as'=>'reports/room/util']);
    Route::get('report/refreshData',['uses'=>'ReportsController@refetchChartData','as'=>'report/refreshData']);

});

Route::group(array('before'=>array('auth|programManager')),function(){

    //Check for conflicts
    Route::post('reservation_details/conflict', ['uses' => 'ReservationDetailsController@hasConflict', 'as' => 'reservation_details/conflict']);


});

App::missing(function($exception) {
     return Redirect::to('/');
});



Route::get('/test', function()
{


    $data = [
        'senderName' => 'ABC Widget', // Company name
        'reminder' => 'You’re receiving this because you’re an awesome ABC Widgets customer or subscribed via <a href="http://www.abcwidgets.com/" style="color: #a6a6a6">our site</a>',
        'unsubscribe' => null,
        'address' => '87 Street Avenue, California, USA',
        'name' =>'ddf',
        'twitter' => 'http://www.facebook.com/abcwidgets',
        'facebook' => 'http://twitter.com/abcwidgets',
        'flickr' => 'http://www.flickr.com/photos/abcwidgets'
    ];

    Mail::queue('lecturer.email', $data, function($message)
    {
        $message->to('wmuchiri@strathmore.edu', 'Kennedy Kinoti')->subject('Welcome!');
    });

});
