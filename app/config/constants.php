<?php

/*
|--------------------------------------------------------------------------
| All rights reserved - The Strathmore Business School 
|--------------------------------------------------------------------------
|
| The SBS Faculty Planner  
|
*/
/**
 * Returns all the application constant
 * -- Database names with similar names to models (includes those without models) ->use with fluent
 * -- 
 */
return array(
    /*--- Database tables ---*/
    'Announcement'  => 'announcements',
    'ReservationRoom' => 'reservation_room',
    'Area' => 'areas',
    'Building' => 'buildings',
    'Course' => 'courses',
    'Department' => 'department',
    'DeptSubjectArea' => 'depts_subject_areas',
    'Group' => 'groups',
    'MailTemplate' => 'mail_tmpls',
    'Permission' => 'permissions',
    'Remider' => 'reminders',
    'Reservation' => 'reservations',
    'ReservationReminder' => 'reservation_reminder',
    'ReservationRepeat' => 'reservation_repeats',
    'ReservationRoom' => 'reservation_rooms',
    'ReservationRoomRepeat' => 'reservation_room_repeats',
    'Status' => 'reservation_statuses',
    'ReservationUser' => 'reservation_users',
    'Role' => 'roles',
    'RolePermission' => 'role_permissions',
    'Room' => 'rooms',
    'Status' => 'statuses',
    'SubjectArea' => 'subject_areas',
    'User' => 'users',
    'UserRole' => 'user_roles',
    /*---  ---*/
    
    
);