<?php return array(
    //NB: If you want to disable minification in a particular view, put {{-- skipmin --}} somewher in the view
    // Turn on/off minification
    'enabled' => true,

    // If you are using a javascript framework that conflicts
    // with Blade's tags, you can change them here
    'blade' => array(
        'contentTags' => array('{{', '}}'),
        'escapedContentTags' => array('{{{', '}}}')
    )

);
