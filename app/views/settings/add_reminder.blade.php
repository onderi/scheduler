<style type="text/css">
    .dropdown-menu>li>a {
        display: block;
        padding: 3px 40px;
        clear: both;
    }

    .multiselect {
        text-align: left;
    }
    .multiselect b.caret {
        position: absolute;
        top: 14px;
        right: 8px;
    }
    .multiselect-group {
        font-weight: bold;
        text-decoration: underline;
    }
</style>



<div class="box box-warning">
    <div class="box-header">
        <!-- <h3 class="box-title">User Form</h3> !-->
    </div>

    <form id="role" name="add_reservation_subscription" method="post" action="#" role="form">
        {{ Form::token(); }}

        <div class="box-body">

            <div id="error_block">

            </div>

            <div id="roles_div" class='col-md-12 form-group'>
                <label>Role</label>
                {{Form::select('roles',['' => 'Please select'] + $roles,'',array('class'=>'form-control','id' => 'roles'))}}
            </div>

            <div id="users_div" class='col-md-12 form-group'>
                <label>User(s)</label>
                {{Form::select('users',[],'',array('class'=>'form-control','id' => 'users'))}}
            </div>

            <div class="col-md-12 form-group">
                {{Form::label('reminder','Reminder')}}
                {{Form::select('reminder',['' => 'Please select'] + $events,'',array('class'=>'form-control','id' => 'reminder','required' => 'required'))}}
            </div>

            <div id="rooms_div" style="display: none;" class='col-md-12 form-group'>
                <label>Rooms</label>
                {{Form::select('rooms',$rooms,'',array('class'=>'form-control','id' => 'rooms','multiple' => 'multiple'))}}
            </div>

            <div id="faculty_div" style="display: none;" class='col-md-12 form-group'>
                <label>Faculty</label>
                {{Form::select('faculty',$faculty,'',array('class'=>'form-control','id' => 'faculty'))}}
            </div>

            <div class='col-md-12 form-group'>
                <label>Reminder Interval</label>
                {{Form::select('reminder_interval',$reminder_interval,'',array('class'=>'form-control','id' => 'reminder_interval','required' => 'required'))}}
            </div>


        </div><!-- /.box-body -->



    </form>
</div>

<script type="text/javascript">

    var room_reservation_reminder = "{{Events::REMINDER_ROOM_RESERVATION}}";
    var faculty_reminder = "{{Events::REMINDER_FACULTY}}";
    $('#faculty').select2({
        theme : 'classic',
        width : '100%'
    });

    $('#rooms').select2({
        theme : 'classic',
        width : '100%'
    });

    $('#reminder').select2({
        theme : 'classic',
        width : '100%'
    });


    $('#reminder_interval').select2({
        theme : 'classic',
        width : '100%'
    });

    $('#reminder').change(function(){

        var selected = $('#reminder :selected').text();

        if( selected == room_reservation_reminder){

            $('#faculty_div').slideUp();
            $('#faculty').removeAttr('required');

            $('#rooms_div').slideDown();
            $('#rooms').attr('required','required');

        } else if ( selected == faculty_reminder ){

            $('#rooms_div').slideUp();
            $('#rooms').removeAttr('required');

            $('#faculty_div').slideDown();
            $('#faculty').attr('required','required');


        }else{
            $('#rooms_div').slideUp();
            $('#faculty_div').slideUp();
            $('#rooms').removeAttr('required');
            $('#faculty').removeAttr('required');

        }

    });

    $('#roles').change(function(){

        var selected_role = $(this).val();
        $.ajax({
           url : 'role/getUsers',
           method : 'post',
           data: {'role_id' : selected_role },
           success : function(data){
               var $users_select = $('#users');
               $users_select.empty();
               $users_select.append(data);
           }
        });

    });

</script>