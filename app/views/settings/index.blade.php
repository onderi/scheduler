@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Settings
@endsection


<!-- Custom CSS !-->
@section('style')
    {{ HTML::style('js/plugins/noty/animate.css')}}
    {{ HTML::style('js/plugins/smalot-datetimepicker/bootstrap-datetimepicker.min.css')}}
    {{ HTML::style('js/plugins/select2/select2.css')}}
    {{ HTML::style('js/plugins/ladda/ladda-themeless.min.css')}}

    <style type="text/css">
    .modal-dialog{
        width:500px !important;
    }
</style>
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Application Settings
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    {{Form::open(array('method'=>'POST','action'=>'SettingsController@store'))}}

        <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
            <li class="active"><a href="#general" data-toggle="tab" aria-expanded="true"> <i class="fa fa-gear" aria-hidden="true"></i> General</a></li>
            <li class=""><a href="#calendar" data-toggle="tab" aria-expanded="false"> <i class="fa fa-calendar"></i> Calendar</a></li>
            <li class=""><a href="#notification" data-toggle="tab" aria-expanded="false"> <i class="fa fa-clock-o"></i> Notification</a></li>

            <li class="pull-left header"><i class="fa fa-cogs"></i> </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="general">

                <div class="box box-warning">
                    <div class="box-header">
                        <!-- <h3 class="box-title">User Form</h3> !-->
                    </div>

                    <div class="box-body">

                        <div class="col-md-6 form-group">
                            {{Form::label('company_name','Company Name')}}
                            {{Form::text('company_name',$company_name,array('class'=>'form-control','required'=>'required','placeholder'=>'Please enter Company Name'))}}
                        </div>

                        <div class="col-md-6 form-group">
                            {{Form::label('company_website','Company Website')}}
                            {{Form::text('company_website',$company_website,array('class'=>'form-control','required'=>'required','placeholder'=>'Please enter Company Website'))}}
                        </div>

                        <div class="col-md-6 form-group">
                            {{Form::label('application_name','Application Name')}}
                            {{Form::text('application_name',$application_name,array('class'=>'form-control','required'=>'required','placeholder'=>'Please enter Application Name'))}}
                        </div>

                        <div class="col-md-6 form-group">
                            {{Form::label('email','Email Name')}}
                            {{Form::text('email',$email,array('class'=>'form-control','required'=>'required','placeholder'=>'Please enter Email Name'))}}
                        </div>

                    </div>

                </div>

            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="calendar">

                    <div class="box box-warning">
                        <div class="box-header">
                            <!-- <h3 class="box-title">User Form</h3> !-->
                        </div>

                        <div class="box-body">

                            <div class="col-md-6 form-group">
                                {{Form::label('holiday','Add Holiday')}}
                                {{Form::text('holiday','',array('class'=>'form-control','placeholder'=>'Please enter Holidays (the format is Day-Month-Year)','id' => 'holiday_input','onkeydown' => 'return false','autocomplete' => 'off'))}}
                            </div>
                            <div class="col-md-6">

                               <table class="table table-sm table-bordered table-striped table-hover" id="holidays_tbl">

                                   <thead class="thead-inverse">
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Recurring</th>
                                    <th>Actions</th>
                                   </thead>
                                   <tbody class="">
                                       <?php $counter = 1; ?>
                                       @foreach($holidays as $h)
                                           <?php
                                           $hj = explode('%',$h);
                                           ?>
                                           <tr>
                                               <td>{{$counter}}</td>
                                               <td>{{$hj[0]}}</td>
                                               <td>
                                                   @if ( $hj[1] == "r" )
                                                       <input class='recurring_chk' type="checkbox" value="1" checked />
                                                   @else
                                                       <input class='recurring_chk' type="checkbox" value="1" />
                                                   @endif
                                               </td>
                                               <td>
                                                   <a data-rel="tooltip" title="Remove Holiday" class="remove_holiday" href="#"> <i class="fa fa-times" ></i> </a>
                                               </td>
                                               <input type='hidden' class="value_holder" value='{{$h}}' name='input_holidays[]'/>
                                           </tr>
                                           <?php $counter++; ?>
                                       @endforeach
                                   </tbody>
                               </table>
                            </div>
                            <div class="col-md-6 form-group">
                                {{Form::label('calendar_start','Calendar Start')}}
                                {{Form::text('calendar_start',$calendar_start,array('class'=>'form-control','placeholder'=>'Calendar Start Interval (format 00:00)','id' => 'calendar_start','onkeydown' => 'return false','autocomplete' => 'off'))}}
                            </div>
                            <div class="col-md-6 form-group">
                                {{Form::label('calendar_end','Calendar Start')}}
                                {{Form::text('calendar_end',$calendar_end,array('class'=>'form-control','placeholder'=>'Calendar Start Interval (format 00:00)','id' => 'calendar_end','onkeydown' => 'return false','autocomplete' => 'off'))}}
                            </div>


                        </div>


                    </div>


            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="notification">

                <div class="box box-warning">
                    <div class="box-header">
                        <!-- <h3 class="box-title">User Form</h3> !-->
                    </div>

                    <div class="box-body">

                        <h4> <span style="border-bottom: solid 2px;border-bottom-color: #f39c12;">Notifications</span></h4>

                        <div class="col-md-6 form-group">
                            {{Form::label('notification_enabled','Notification Enabled')}}
                            {{Form::select('notification_enabled',$notification_enabled,Settings::getVal('notifications_enabled'),array('class'=>'form-control','required'=>'required'))}}
                        </div>

                        <div class="col-md-6 form-group">
                            {{Form::label('notification_events','Notification Events & Reminders')}}
                            <select name="notification_events[]" id="notification_events" class="form-control" multiple="multiple" required>
                                @foreach($events as $ev)
                                    <option {{($ev->enabled == "1") ? "selected" : ""}} value="{{$ev->id}}">{{$ev->description}}</option>
                                @endforeach
                            </select>
                        </div>

                        <h4> <span style="border-bottom: solid 2px;border-bottom-color: #f39c12;">Event Subscriptions</span></h4>


                        <div class="col-md-12">
                            <button style="float: left;margin-bottom:13px;" class="btn btn-primary btn-outline" id="add_event_subscription"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Event Subscription</button>
                            <button style="float: left;margin-bottom:13px;" class="btn btn-warning btn-outline" id="add_reminder_subscription"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Reminder Subscription</button>

                            <table class="table table-sm table-bordered table-striped table-hover" id="subscription_tbl">

                                <thead class="thead-inverse">
                                    <th>#</th>
                                    <th>Subscriber</th>
                                    <th>Subscribed To</th>
                                    <th>Recurring</th>
                                    <th>Interval</th>
                                </thead>
                                <tbody>
                                    @if( count($event_subscribers) > 0 )
                                        @foreach($event_subscribers as $e)
                                            <tr>
                                                <td>{{$e->id}}</td>
                                                <td>
                                                    {{  User::getSubscribers($e->subscriber)}}
                                                </td>
                                                <td>
                                                    {{ Events::find($e->event_id)->description }}
                                                    {{ (Events::find($e->event_id)->type == Subscription::TYPE_EVENT_SUBSCRIPTION) ? ' <em>(Event)</em>' : ''}}

                                                    {{(Events::find($e->event_id)->type== Subscription::TYPE_REMINDER_SUBSCRIPTION) ? ' <em>(Reminder)</em>' : ''}}

                                                </td>
                                                <td>
                                                    {{ ($e->interval == "weekly" || $e->interval == "monthly") ? 'Yes' : 'No'  }}
                                                </td>
                                                <td>
                                                    {{ ($e->interval == "0") ? "On Event" : Events::getReminderIntervals($e->interval)  }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3" align="center">No subscribers found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>

            </div>

            <div style="padding-left : 24px;">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>  Save Changes</button>
            </div>

        </div>
        <!-- /.tab-content -->
        </div>
    {{Form::close()}}
</div>

<?php  $event_options = '';$user_options = "<option value='0'>All Users</option>"; ?>
@foreach($notification_event as $nt )
    @if( $nt->type == Subscription::TYPE_EVENT_SUBSCRIPTION )
     <?php $event_options .= "<option value='$nt->id'>$nt->description</option>"; ?>
    @endif

@endforeach

@foreach($users as $key => $value )
    <?php $user_options .= "<option value='$key'>$value</option>"; ?>
@endforeach



@endsection

<!-- Custom Scripts !-->
@section('scripts')
   {{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
   {{ HTML::script('js/plugins/smalot-datetimepicker/bootstrap-datetimepicker.min.js')}}
   {{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}
   {{ HTML::script('js/plugins/select2/select2.js')}}
   {{ HTML::script('js/plugins/spin/spin.min.js')}}
   {{ HTML::script('js/plugins/ladda/ladda.min.js')}}

   <script type="text/javascript">

       var room_reservation_reminder_str = "{{Events::REMINDER_ROOM_RESERVATION}}";
       var faculty_reminder_str = "{{Events::REMINDER_FACULTY}}";

       $(function() {

        $('#notification_events').select2({
            theme : 'classic',
            multiple : true,
            tags : false,
            width : '100%'
        });

        $('.remove_holiday').click(function(){
            $(this).closest('tr').remove();
        });

        $('#add_reminder_subscription').click(function (ev) {
               ev.preventDefault();

                var add_url_link = "{{URL::to('setting/add_reminder_subscriber')}}";

               BootstrapDialog.show({
                   title: 'Add Reminder Subscription',
                   message: function(dialog_edit_rs) {
                       var $message = $('<div></div>');
                       var pageToLoadEd = dialog_edit_rs.getData('pageToLoadEd');
                       $message.load(pageToLoadEd);
                       return $message;
                   },
                   data: {
                       'pageToLoadEd': add_url_link
                   },

                   buttons: [
                       {
                           label: '<span class="ladda-label"> Save </span>',
                           icon : 'fa fa-save',
                           cssClass: 'btn-primary ladda-button',
                           id : 'btn_save_subscriptions_rem',
                           action: function(dialog) {
                               var l_edit_btn = Ladda.create( document.querySelector('#btn_save_subscriptions_rem') );

                               var reminder_val = $('#reminder').val();
                               var reminder_txt = $('#reminder :selected').text();
                               var rooms_val = $('#rooms').val();
                               var faculty_val = $('#faculty').val();
                               var reminder_interval = $('#reminder_interval').val();
                               var user_val = $('#users').val();
                               var roles_val = $('#roles').val();



                               if ( reminder_txt == room_reservation_reminder_str && rooms_val == null ){

                                       //append to the div
                                       var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                       //remove the type - its the last element
                                       error_div += "<li>Please select rooms!</li></ul></div>";
                                       $('#error_block').html(error_div);

                               }else if( roles_val == "" || roles_val == null ) {

                                   //append to the div
                                   var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                   //remove the type - its the last element
                                   error_div += "<li>Please select role!</li></ul></div>";
                                   $('#error_block').html(error_div);

                               }else if(reminder_val == "" || reminder_val == null){
                                   //append to the div
                                   var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                   //remove the type - its the last element
                                   error_div += "<li>Please select reminder!</li></ul></div>";
                                   $('#error_block').html(error_div);
                               }else{

                                   l_edit_btn.start();

                                   $.ajax({
                                       type : 'post',
                                       url : 'setting/reminder_subscription/save',
                                       dataType : 'json',
                                       data : {'reminder' : reminder_val,'roles' : roles_val,'rooms' : rooms_val,'faculty' : faculty_val,'reminder_interval' : reminder_interval,'user' : user_val },
                                       success : function (data_add_new){
                                           console.log(data_add_new);
                                           var json_object = data_add_new;
                                           var msg_len = Object.keys(data_add_new).length;
                                           var msg_type = json_object['type'];

                                           if(msg_type === "error" && msg_len > 1){
                                               //append to the div
                                               var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                               //remove the type - its the last element
                                               var msg_len_correct = msg_len - 1;
                                               for(var jd=0;jd<msg_len_correct;jd++){
                                                   error_div += '<li>'+json_object[jd]+'</li>';
                                               }
                                               error_div += "</ul></div>";
                                               $('#error_block').html(error_div);
                                           }

                                           if( msg_type === "notice" ) {
                                               BootstrapDialog.show({
                                                   type: BootstrapDialog.TYPE_SUCCESS,
                                                   title: 'Success',
                                                   message: 'Reminder Added Successfully.',
                                                   closable: true,
                                                   cssClass: 'small-dialog',
                                                   buttons: [{
                                                       icon: 'fa fa-cog fa-spin',
                                                       label: ' Ok',
                                                       action: function (dialog_success_edt) {
                                                           dialog_success_edt.close();
                                                           l_edit_btn.stop();
                                                           dialog.close();

                                                           location.reload();
                                                       }
                                                   }]

                                               });
                                           }


                                       }

                                   });

                               }

                           }
                       },

                       {
                           label: 'Cancel',
                           icon : 'fa fa-crosshairs',
                           cssClass: 'btn-danger',
                           action: function(dialog) {
                               dialog.close();
                           }
                       },

                   ]
               });
           });

        $('#add_event_subscription').click(function (ev){
            ev.preventDefault();

            //build dialog
            var $message = $("<div class='form-group'></div>");
            //events

            var event_options = "<?php echo $event_options; ?>";
            var user_options = "<?php echo $user_options; ?>";

            var ev_field = "<div class='form-group'><label>Event</label><select id='ev_field_dialog' class='form-control'>"+event_options+"</select></div>";
            var user_field = "<div class='form-group'><label>User</label><select id='user_field_dialog' class='form-control'>"+user_options+"</select></div>";
            $message.append(ev_field);
            $message.append(user_field);

            BootstrapDialog.show({
                title: 'Add Event Subscription',
                message: $message,
                buttons: [
                    {
                        label: '<span class="ladda-label"> Save </span>',
                        icon : 'fa fa-save',
                        cssClass: 'btn-primary ladda-button',
                        hotkey: 13, // Enter.
                        id : 'btn_save_subscriptions',
                        action: function(dialog) {
                            var l_edit_btn = Ladda.create( document.querySelector('#btn_save_subscriptions') );
                            var event_val = $('#ev_field_dialog').val();
                            var user_val = $('#user_field_dialog').val();

                            l_edit_btn.start();

                            $.ajax({
                               type : 'post',
                               url : 'setting/event_subscription/save',
                               data : {'event' : event_val,'user' : user_val},
                               success : function (data){
                                   l_edit_btn.stop();
                                   dialog.close();
                                   location.reload();
                               }

                            });

                        }
                    },

                    {
                        label: 'Cancel',
                        icon : 'fa fa-crosshairs',
                        cssClass: 'btn-danger',
                        action: function(dialog) {
                            dialog.close();
                        }
                    },

                ]
            });

        });

        $('.recurring_chk').change(function(){
            var $input_holder = $(this).closest('tr').find('.value_holder');
            var input_value = $input_holder.val();
            var new_value = "";
            if ( $(this).is(':checked') ){
                //replace %s with %r
                new_value = input_value.replace('%s',"%r");
            }else{
                //replace %r with %s
                new_value = input_value.replace('%r',"%s");
            }
            //set the new value
            $input_holder.val(new_value);
        });

        $('#holiday_input').change(function () {

            var selected_date = $(this).val();
            var selected_date_moment = moment(selected_date).format('MMMM Do, YYYY');
            //append the value to the list
            var html = "<tr><td>New</td><td>" + selected_date_moment + "</td> <td><input class='recurring_chk' type='checkbox' value='1' /></td>" + '  <td><a class="remove_holiday" data-rel="tooltip" title="Remove Holiday" href="#"> <i class="fa fa-times" ></i> </a></td></tr>' + "<input type='hidden' value='"+selected_date+"%s' name='input_holidays[]'/>";
            $('#holidays_tbl').append(html);

            $(this).val('');
        });


        $('#holiday_input').datetimepicker({
            format: 'yyyy-mm-dd',
            startView: 3,
            autoclose: true,
            minuteStep: 60,
            minView : 2
        });

        $('#calendar_start').datetimepicker({
            format: 'hh:ii',
            startView: 0,
            autoclose: true,
            minuteStep: 30,
            minView : 0,
            maxView : 1
        });

        $('#calendar_end').datetimepicker({
            format: 'hh:ii',
            startView: 0,
            autoclose: true,
            minuteStep: 30,
            minView : 0,
            maxView : 1
        });

        @if ( Session::get('notice') )

            noty({
                text: "Success! {{ Session::get('notice') }}",
                theme: "relax",
                dismissQueue: true,
                progressBar : true,
                timeout     : 3000,
                layout : 'topRight',
                type: 'success',
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight', // Animate.css class names
                    easing: 'swing', // unavailable - no need
                    speed: 500 // unavailable - no need
                }
            });

        @endif

        @if ( Session::get('error') )
           noty({
                text: "Error, {{ Session::get('error')}}",
                theme: "relax",
                dismissQueue: true,
                progressBar : true,
                timeout     : 10000,
                layout : 'topRight',
                type: 'error',
                animation: {
                    open: 'animated swing', // Animate.css class names
                    close: 'animated tada', // Animate.css class names
                    easing: 'swing',
                    speed: 500 // opening & closing animation speed
                }
             });
        @endif


    });
</script>

@endsection

