@extends('layouts.master')

        <!-- Page Title !-->
@section('title')
    | Course Categories
    @endsection


            <!-- Custom CSS !-->
@section('style')
    <style type="text/css">
        small{
            margin-left: 2px;
        }
        .cat_list li{
            padding-left: .6em;
            background-position: 0 50%;
            margin: .4em 0;
        }

        .cat_list ul {
            padding-left: 2em;
        }
        mark {
            background: orange;
            color: inherit;
            padding: 0;
        }

    </style>
    <!-- CSS Datatables !-->
    {{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
    {{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
    {{ HTML::style('css/datatables/TableTools.css')}}
    {{ HTML::style('css/qtip/jquery.qtip-new.min.css')}}
    {{ HTML::style('js/plugins/noty/animate.css')}}
    <style type="text/css">
        .modal-dialog{
            width:300px !important;
        }
    </style>

@endsection

            <!-- Content Heading !-->
@section('content_head')
    <h1>
        Course Categories
    </h1>
    @endsection


            <!-- Breadcrumbs !-->
    @section('breadcrumb')
    {{ Breadcrumbs::render() }}
    @endsection


            <!-- Page Content !-->
@section('page_content')
    <div class="col-md-12">
        <div class="box box-warning">
            <div class="box-header">

            </div>
            <div class="box-body">

                    <div class="btn-group" style="float: right;">

                        <button class="btn btn-warning faa-parent animated-hover openall" > <i class="fa fa-expand faa-shake"> </i> Expand All</button>

                        <button class="btn btn-danger faa-parent animated-hover closeall" > <i class="fa fa-compress faa-shake"> </i> Collapse All</button>

                        @if( Auth::user()->isSysAdmin() || Auth::user()->isSystemPlanningAdmin())
                            <button class="btn btn-success faa-parent animated-hover" id="sync-categories" > <i class="fa fa-refresh faa-spin"> </i> Sync Categories</button>
                        @endif

                    </div>

                    <input placeholder="Type to search" name="search" class="form-control" style="width: 200px;">
                <br/>
                <div id="accordion" class="categories_accordion">
                    {{$ccategories}}
                </div>


        </div>
            <div class="box-footer">

            </div>
        </div>

    </div>


    @endsection

            <!-- Custom Scripts !-->
    @section('scripts')
            <!-- JS Datatables !-->
    {{ HTML::script('js/jquery-ui-1.10.3.js')}}
    {{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
    {{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
    {{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
    {{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
    {{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
    {{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
    {{ HTML::script('js/plugins/highlight/jquery.mark.min.js')}}
    {{ HTML::script('js/plugins/spin/spin.min.js')}}
    {{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}
    {{HTML::script('js/plugins/qtip/jquery.qtip-new.min.js')}}


    <script type="text/javascript">
        var spinOpts = {
            lines: 13 // The number of lines to draw
            , length: 28 // The length of each line
            , width: 14 // The line thickness
            , radius: 42 // The radius of the inner circle
            , scale: 1 // Scales overall size of the spinner
            , corners: 1 // Corner roundness (0..1)
            , color: '#000' // #rgb or #rrggbb or array of colors
            , opacity: 0.25 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1 // Rounds per second
            , trail: 60 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 2e9 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '50%' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: false // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
        };
        var target = document.getElementById('accordion');
        var spinner = new Spinner(spinOpts);

        $(function() {

            $('select,input').addClass('form-control input-sm');
            $('.alert').fadeOut(5000);


        });


        $(document).ready(function (){



            var $input = $("input[name='search']");
            var $context = $("div.categories_accordion");
            $input.on("input", function() {
                var term = $(this).val();
                $context.show().unmark();
                if (term) {
                    $context.mark(term, {
                        done: function() {
                            $context.not(":has(mark)").hide();
                        }
                    });
                }
            });

            $('.closeall').click(function(){
                spinner.spin(target);

                $('.panel-collapse.in')
                    .collapse('hide');

                spinner.stop();
            });
            $('.openall').click(function(){
                spinner.spin(target);

                $('.panel-collapse:not(".in")')
                    .collapse('show');

                spinner.stop();
            });


            $('#sync-categories').click(function(event){


                $.ajax({

                    url : 'ccategory/sync',
                    type : 'get',
                    beforeSend : function (){
                        spinner.spin(target);
                    },
                    success : function (data){
                        if ( data == "success" ){

                            noty({
                                text: "Success! Course Categories Synced",
                                theme: "relax",
                                layout : 'topRight',
                                dismissQueue: true,
                                progressBar : true,
                                timeout     : 3000,
                                type: 'success',
                                animation: {
                                    open: 'animated bounceInRight', // Animate.css class names
                                    close: 'animated bounceOutRight', // Animate.css class names
                                    easing: 'swing', // unavailable - no need
                                    speed: 500 // unavailable - no need
                                }
                            });

                            spinner.stop();

                            //reload page
                            location.reload();

                        }else if(data == "error-p"){

                            noty({
                                text: "Error Occured! You do not have rights.",
                                theme: "relax",
                                layout : 'topRight',
                                type: 'error',
                                animation: {
                                    open: 'animated bounceInRight', // Animate.css class names
                                    close: 'animated bounceOutRight', // Animate.css class names
                                    easing: 'swing', // unavailable - no need
                                    speed: 500 // unavailable - no need
                                }
                            });

                            spinner.stop();


                        }else{

                            noty({
                                text: "Error Occured! Please try again",
                                theme: "relax",
                                layout : 'topRight',
                                dismissQueue: true,
                                progressBar : true,
                                timeout     : 10000,
                                type: 'error',
                                animation: {
                                    open: 'animated bounceInRight', // Animate.css class names
                                    close: 'animated bounceOutRight', // Animate.css class names
                                    easing: 'swing', // unavailable - no need
                                    speed: 500 // unavailable - no need
                                }
                            });

                            spinner.stop();

                        }

                    }

                });

            });


            $('.course_present').each(function (e){

                var cat_id = $(this).attr('id');

                $(this).qtip({
                    content: {
                        text: function(event, api) {
                            $.ajax({
                                url : 'ccategory/getCourse',
                                data : {'id' : cat_id},
                                type : 'POST',
                                dataType : 'json'
                            })
                                .then(function(data) {
                                    // Set the tooltip content upon successful retrieval
                                    api.set('content.title',data.title);
                                    api.set('content.text', data.body);
                                }, function(xhr, status, error) {
                                    // Upon failure... set the tooltip content to error
                                    api.set('content.text', status + ': ' + error);
                                });

                            return 'Loading...'; // Set some initial text
                        }
                    },
                    position: {
                        viewport: $(window)
                    },
                    style: 'qtip-cluetip qtip-rounded'
                });
            });

            @if ( Session::get('notice') )

            noty({
                text: "Success! {{ Session::get('notice') }}",
                theme: "relax",
                dismissQueue: true,
                progressBar : true,
                timeout     : 3000,
                layout : 'topRight',
                type: 'success',
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight', // Animate.css class names
                    easing: 'swing', // unavailable - no need
                    speed: 500 // unavailable - no need
                }
            });

            @endif

            @if ( Session::get('error') )
               noty({
                text: "Error, {{ Session::get('error')}}",
                theme: "relax",
                dismissQueue: true,
                progressBar : true,
                timeout     : 10000,
                layout : 'topRight',
                type: 'error',
                animation: {
                    open: 'animated swing', // Animate.css class names
                    close: 'animated tada', // Animate.css class names
                    easing: 'swing',
                    speed: 500 // opening & closing animation speed
                }
            });
            @endif

        });
    </script>

@endsection

