@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Courses
@endsection


<!-- Custom CSS !-->
@section('style')
<style type="text/css">
    small{
        margin-left: 2px;
    }
</style>
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
{{ HTML::style('js/plugins/noty/animate.css')}}
<style type="text/css">
    .modal-dialog{
        width:300px !important;
    }
</style>
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Courses
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">

        </div>
        <div class="box-body">

            <table class="table table-bordered table-striped dataTable" id="courses">
                <thead>
                <th>Course Category</th>
                <th>Name</th>
                <th>Code</th>
                <th>Description</th>
                <th>Start Date</th>
                </thead>
                <tbody>
                    @if( count($courses) > 0 )
                    @foreach($courses as $course)
                    <tr>
                        <td>{{$course->ccategory->name}}</td>
                        <td><span class='pull-left'>{{$course->course_title}}</span> </td>
                        <td>{{$course->course_code}}</td>
                        <td style="width : 30%;">{{$course->course_desc}}</td>
                        <td>{{date("jS F, Y",$course->startdate)}}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>

            </table>

        </div>
        <div class="box-footer">

        </div>
    </div>

</div>


@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
{{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
{{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
{{ HTML::script('js/plugins/spin/spin.min.js')}}
{{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}
<script type="text/javascript">
    var spinOpts = {
        lines: 13 // The number of lines to draw
        , length: 28 // The length of each line
        , width: 14 // The line thickness
        , radius: 42 // The radius of the inner circle
        , scale: 1 // Scales overall size of the spinner
        , corners: 1 // Corner roundness (0..1)
        , color: '#000' // #rgb or #rrggbb or array of colors
        , opacity: 0.25 // Opacity of the lines
        , rotate: 0 // The rotation offset
        , direction: 1 // 1: clockwise, -1: counterclockwise
        , speed: 1 // Rounds per second
        , trail: 60 // Afterglow percentage
        , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
        , zIndex: 2e9 // The z-index (defaults to 2000000000)
        , className: 'spinner' // The CSS class to assign to the spinner
        , top: '50%' // Top position relative to parent
        , left: '50%' // Left position relative to parent
        , shadow: false // Whether to render a shadow
        , hwaccel: false // Whether to use hardware acceleration
        , position: 'absolute' // Element positioning
    };
    var target = document.getElementById('courses');
    var spinner = new Spinner(spinOpts);

    $(function() {

        $('#courses').dataTable({
            "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "oLanguage": {
                "sLengthMenu": "_MENU_ Course per page"
            },
            "oTableTools": {
                "aButtons": [
                  @if( Auth::user()->isSysAdmin() || Auth::user()->isSystemPlanningAdmin())
                    {
                        "sExtends": "gotoURL",
                        "sButtonText": '<button id="sync-courses" class="btn btn-success btn-sm faa-parent animated-hover"><i class="fa fa-refresh faa-spin"></i>  Sync Courses</button>',
                        "sGoToURL": "#"
                    },
                  @endif
                    {
                        "sExtends": "pdf",
                        "sFileName": "Course.pdf",
                        "sPdfOrientation": "landscape",
                        "sPdfMessage": "SBS Planner Course",
                        "sButtonText": "Save to PDF"
                    },
                    {
                        "sExtends": "csv",
                        "sFileName": "Course.csv",
                        "sButtonText": "Save to CSV"
                    },
                    {
                        "sExtends": "xls",
                        "sFileName": "Course.xls",
                        "sButtonText": "Save to Excel"
                    }
                ]
            }
        });
        $('select,input').addClass('form-control input-sm');
        $('.alert').fadeOut(5000);

        $('#sync-courses').click(function(event){


            $.ajax({

                url : 'course/sync',
                type : 'get',
                beforeSend : function (){
                    spinner.spin(target);
                },
                success : function (data){
                    if ( data == "success" ){

                        noty({
                            text: "Success! Courses Synced",
                            theme: "relax",
                            layout : 'topRight',
                            type: 'success',
                            dismissQueue: true,
                            progressBar : true,
                            timeout     : 3000,
                            animation: {
                                open: 'animated bounceInRight', // Animate.css class names
                                close: 'animated bounceOutRight', // Animate.css class names
                                easing: 'swing', // unavailable - no need
                                speed: 500 // unavailable - no need
                            }
                        });

                        spinner.stop();

                        //reload page
                        location.reload();

                    }else if(data == "error-p"){

                        noty({
                            text: "Error Occured! You do not have rights.",
                            theme: "relax",
                            layout : 'topRight',
                            dismissQueue: true,
                            progressBar : true,
                            timeout     : 10000,
                            type: 'error',
                            animation: {
                                open: 'animated bounceInRight', // Animate.css class names
                                close: 'animated bounceOutRight', // Animate.css class names
                                easing: 'swing', // unavailable - no need
                                speed: 500 // unavailable - no need
                            }
                        });

                        spinner.stop();


                    }else{

                        noty({
                            text: "Error Occured! Please try again",
                            theme: "relax",
                            layout : 'topRight',
                            type: 'error',
                            dismissQueue: true,
                            progressBar : true,
                            timeout     : 10000,
                            animation: {
                                open: 'animated bounceInRight', // Animate.css class names
                                close: 'animated bounceOutRight', // Animate.css class names
                                easing: 'swing', // unavailable - no need
                                speed: 500 // unavailable - no need
                            }
                        });

                        spinner.stop();

                    }


                }

            });

        });

        @if ( Session::get('notice') )

           noty({
            text: "Success! {{ Session::get('notice') }}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 2000,
            layout : 'topRight',
            type: 'success',
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight', // Animate.css class names
                easing: 'swing', // unavailable - no need
                speed: 500 // unavailable - no need
            }
        });

        @endif

        @if ( Session::get('error') )
           noty({
            text: "Error, {{ Session::get('error')}}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 10000,
            layout : 'topRight',
            type: 'error',
            animation: {
                open: 'animated swing', // Animate.css class names
                close: 'animated tada', // Animate.css class names
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            }
        });
        @endif

    });
</script>

@endsection

