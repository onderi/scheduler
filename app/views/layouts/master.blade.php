<!DOCTYPE html>
<html>
<head>
    <title>
        {{Settings::getVal('application_name')}} @yield('title')
    </title>
    <link rel="shortcut icon" href="{{ URL::to('img/logo.png') }}">

    <style type="text/css">

        .no-js #loader{ display: none;}
        .js #loader{
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url("{{URL::to('img/Preloader_2.gif')}}") center no-repeat #fff;
        }
    </style>


    <!-- bootstrap 3.0.2 -->
    {{ HTML::style('css/bootstrap.min.css')}}
    <!-- Font Awesome -->
    {{HTML::style('css/font-awesome.min.css')}}
    <!-- Ionicons -->
    {{ HTML::style('css/ionicons.min.css')}}
    <!-- Animated Font Awesome Add-on -->
    {{ HTML::style('css/font-awesome-animation.min.css')}}
    <!-- Theme style -->
    {{ HTML::style('css/AdminLTE.css')}}
    {{ HTML::style('css/skins/skin-blue.css')}}
    {{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.min.css')}}
    @yield('style')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.0.2 -->
    {{ HTML::script('js/jquery.min.js')}}
    <!-- Modernizer for the pre-loader -->
    {{ HTML::script('js/modernizr.min.js')}}
    <!-- Store Js Local DB Storage -->
    {{ HTML::script('js/plugins/store/store.min.js') }}

    <script type="text/javascript">
        $(window).load(function () {
            $(".se-pre-con").fadeOut("slow");
            {{-- Init params --}}
            store.set('isload','1');
            store.set('last_area','2');
            store.set('selected_rooms','undefined');
            store.set('filter_changed','0');
        });

        //remove resources when you close the browser window
        window.onbeforeunload = function() {
            {{-- UNSET Resources --}}
            //store.clear();
            store.remove('isload');
            store.remove('last_area');
            store.remove('selected_rooms');
            store.remove('filter_changed');
            store.remove('view');
        };
    </script>

</head>

<body  class="hold-transition skin-blue sidebar-mini sidebar-collapse">

<div class="se-pre-con"></div>
<!-- header logo: style can be found in header.less -->
<header class="main-header">

    <a href="{{URL::to('/')}}" class="logo">
        <span class="logo-lg">{{Settings::getVal('application_name')}}</span>
        <span class="logo-mini"><b>S</b>BS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <!--
        <div>
            <ul class="nav navbar-nav">
                <li><a class="faa-parent animated-hover modal-feature" href="#"> <i class="fa fa-table faa-flash"></i> Generate timetable</a></li>
            </ul>
        </div>
        -->

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                @if(Auth::check())
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                                <span class="hidden-xs">
                                    {{Auth::user()->fname.' '.Auth::user()->lname}}
                                    <i class="caret"></i>
                                </span>
                    </a>

                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                                @if(Auth::user()->gender=='Male')
                                    <img src="{{URL::to('img/avatar3.png')}}" class="img-circle" alt="User Image" />
                                @else
                                    <img src="{{URL::to('img/black_woman.jpg')}}" class="img-circle" alt="User Image" />
                                @endif
                                <p>
                                    {{Auth::user()->fname.' '.Auth::user()->lname}}
                                    <small>Member since {{date('M Y',strtotime(Auth::user()->created_at))}}</small>
                                </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="{{URL::to('user/'.Auth::user()->user_id.'/u_edit')}}" class="btn btn-default btn-flat faa-parent animated-hover"> <i class="fa fa-pencil faa-wrench"></i> Edit Profile</a>
                            </div>
                        </li>

                    </ul>
                </li>
                @endif
                <li>
                    @if(Auth::check())
                        <a class="faa-parent animated-hover" href="{{action('UserController@logout')}}"> <i class="fa fa-sign-out faa-horizontal"></i> Sign Out</a>
                    @else
                        <a class="faa-parent animated-hover" href="{{action('UserController@login')}}"> <i class="fa fa-sign-in faa-horizontal"></i> Sign In</a>
                    @endif
                </li>
            </ul>


        </div>
    </nav>

</header>

<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="height: auto;">
            <ul class="sidebar-menu">
                <li class="{{ Ekko::isActiveURL('/') }}">
                    <a class="faa-parent animated-hover" href="{{URL::to('/')}}">
                        <i class="fa fa-dashboard faa-vertical"></i> <span> Dashboard</span>
                    </a>
                </li>
                @if(Auth::check())

                            @if(Auth::user()->isSysAdmin()||Auth::user()->isBookingAdmin())
                                <li class="{{ Ekko::areActiveRoutes(['announcements','announcement/*']) }}"><a class="faa-parent animated-hover" href="{{ URL::to('announcements')}}"><i class="fa fa-comment faa-vertical" ></i> <span>Announcements</span></a></li>
                            @endif
                            @if(Auth::user()->isSysAdmin())
                                <li class="{{ Ekko::areActiveRoutes(['roles','role/*']) }}"><a class="faa-parent animated-hover" href="{{ URL::to('roles')}}">
                                    <i class="fa fa-key faa-horizontal "></i>
                                    <span>Roles</span></a>
                                </li>
                                <li class="{{ Ekko::areActiveRoutes(['users','user/*']) }}"><a class="faa-parent animated-hover" href="{{ URL::to('users')}}">
                                    <i class="fa fa-users faa-vertical"></i>
                                    <span>Users</span></a>
                                </li>
                                <li class="header">Rooms</li>
                                <li class="{{ Ekko::areActiveRoutes(['buildings','building/*']) }}">
                                    <a class="faa-parent animated-hover" href="{{ URL::to('buildings')}}" >
                                        <i class="fa fa-building-o faa-vertical"></i>
                                        <span>Buildings</span>
                                    </a>
                                </li>
                                <li class="{{ Ekko::areActiveRoutes(['areas','area/*']) }}">
                                    <a class="faa-parent animated-hover" href="{{ URL::to('areas')}}" >
                                        <i class="fa fa-square faa-vertical"></i>
                                        <span>Room Areas</span>
                                    </a>
                                </li>
                                <li class="{{ Ekko::areActiveRoutes(['rooms','room/*']) }}">
                                    <a class="faa-parent animated-hover" href="{{ URL::to('rooms')}}" >
                                        <i class="fa fa-square-o faa-vertical"></i>
                                        <span>Rooms</span>
                                    </a>
                                </li>
                                <li class="header">Courses</li>
                                <li class="{{ Ekko::areActiveRoutes(['departments','department/*']) }}">
                                    <a class="faa-parent animated-hover" href="{{ URL::to('departments')}}">
                                        <i class="fa fa-suitcase faa-vertical"></i>
                                        <span>Departments</span>
                                    </a>
                                </li>
                                <li class="{{ Ekko::areActiveRoutes(['ccategories','category/*']) }}">
                                    <a class="faa-parent animated-hover" href="{{ URL::to('ccategories')}}">
                                        <i class="fa fa-briefcase faa-vertical"></i>
                                        <span>Course Categories</span>
                                    </a>
                                </li>
                                <li class="{{ Ekko::areActiveRoutes(['courses','course/*']) }}">
                                    <a class="faa-parent animated-hover" href="{{ URL::to('courses')}}">
                                        <i class="fa fa-book faa-vertical"></i>
                                        <span>Courses</span>
                                    </a>
                                </li>
                            @endif
                            @if(Auth::user()->isBookingAdmin())
                            <li class="header">Reservations</li>
                            <li class="treeview {{ Ekko::areActiveRoutes(['reservation/room','reservation/approval','reservation/approval/*','reservation/room/*','reservation/details/*']) }}">
                                <a class="faa-parent animated-hover" href="{{ URL::to('reservation/room')}}">
                                    <i class="fa fa-clock-o faa-bounce animated-hover"></i>
                                    <span>Reservations</span>
                                    <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu" style="display:none;">
                                    <li class="{{ Ekko::areActiveRoutes(['reservation/approval','reservation/approval/*']) }}">
                                        <a href="{{ URL::to('reservation/approval')}}" style="margin-left: 10px;">
                                            <i class="fa fa-th-list"></i>
                                            <span>Approval</span>
                                        </a>
                                    </li>

                                    <li class="{{ Ekko::areActiveRoutes(['reservation/room','reservation/room/*','reservation/details/*']) }}">
                                        <a href="{{ URL::to('reservation/room')}}" style="margin-left: 10px;">
                                            <i class="active fa fa-check-square-o"></i>
                                            <span>Room Reservations</span>
                                        </a>
                                    </li>
                                </ul>
                                @endif
                                @if(Auth::user()->isSysAdmin())
                                <li class="header">Lecturers</li>
                                <li class="treeview {{ Ekko::areActiveRoutes(['reports/room/util']) }}">
                                    <a href="">
                                        <i class="fa fa-delicious faa-bounce faa-vertical"></i>
                                        <span>Lecturers</span>
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </a>

                                    <ul class="treeview-menu" style="display: none;">

                                        <li class="{{ Ekko::areActiveRoutes(['lecturer/booking']) }}">
                                            <a href="{{ URL::to('lecturer/booking')}}" style="margin-left: 10px;">
                                                <i class="fa fa-bookmark"></i>
                                                <span>Bookings</span>
                                            </a>
                                        </li>

                                        <li class="{{ Ekko::areActiveRoutes(['lecturer/lecturers']) }}">
                                            <a href="{{ URL::to('lecturer/lecturers')}}" style="margin-left: 10px;">
                                                <i class="fa fa-tags"></i>
                                                <span>Lecturers</span>
                                            </a>
                                        </li>

                                        <li class="{{ Ekko::areActiveRoutes(['lecturer/book']) }}">
                                            <a href="{{ URL::to('lecturer/book')}}" style="margin-left: 10px;">
                                                <i class="fa fa-book"></i>
                                                <span>Book a lecturer</span>
                                            </a>
                                        </li>

                                        <li class="{{ Ekko::areActiveRoutes(['lecturer/materials']) }}">
                                            <a href="{{ URL::to('lecturer/materials')}}" style="margin-left: 10px;">
                                                <i class="fa fa-exchange"></i>
                                                <span>Materials</span>
                                            </a>
                                        </li>

                                        <!-- <li class="{{ Ekko::areActiveRoutes(['reports/room/util']) }}">
                                            <a href="{{ URL::to('reports/room/util')}}" style="margin-left: 10px;">
                                                <i class="fa fa-pencil"></i>
                                                <span>Exams</span>
                                            </a>
                                        </li> -->

                                        <!-- <li class="{{ Ekko::areActiveRoutes(['lecturer/timesheets']) }}">
                                            <a href="{{ URL::to('lecturer/timesheets')}}" style="margin-left: 10px;">
                                                <i class="fa fa-calendar-times-o"></i>
                                                <span>Timesheets</span>
                                            </a>
                                        </li> -->

                                        <li class="{{ Ekko::areActiveRoutes(['lecturer/workloads']) }}">
                                            <a href="{{ URL::to('lecturer/workloads')}}" style="margin-left: 10px;">
                                                <i class="fa fa-calendar-times-o"></i>
                                                <span>Workloads</span>
                                            </a>
                                        </li>
                                    </ul>

                                </li>
                                @endif
                            @if(Auth::user()->isBookingAdmin())
                                <li class="header">Reports</li>
                                <li class="treeview {{ Ekko::areActiveRoutes(['reports/room/util']) }}">
                                    <a class="faa-parent animated-hover" href="#">
                                        <i class="fa fa-bar-chart faa-bounce animated-hover"></i>
                                        <span>Reports</span>
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </a>

                                    <ul class="treeview-menu" style="display:none;">
                                        <li class="{{ Ekko::areActiveRoutes(['reports/room/util']) }}">
                                            <a href="{{ URL::to('reports/room/util')}}" style="margin-left: 10px;">
                                                <i class="fa fa-pie-chart"></i>
                                                <span>Room Utilization</span>
                                            </a>
                                        </li>

                                        @if( Auth::user()->isSystemPlanningAdmin() || Auth::user()->isSysAdmin() )

                                        <li>
                                            <a href="#" style="margin-left: 10px;">
                                                <i class="fa fa-line-chart"></i>
                                                <span>Faculty Analysis</span>
                                            </a>
                                        </li>

                                        @endif
                                    </ul>
                                </li>

                            @endif;


                            @if(Auth::user()->isSysAdmin())

                                <li class="header">Settings</li>
                                <li class="{{ Ekko::areActiveRoutes(['settings']) }}">
                                    <a class="faa-parent animated-hover" href="{{ URL::to('settings')}}">
                                        <i class="fa fa-cogs faa-vertical" ></i>
                                        <span>Settings</span>
                                    </a>
                                </li>

                            @endif

                    @endif
                </ul>

            </section>


        </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('content_head')
            @yield('breadcrumb')
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                @yield('page_content')
            </div>
        </section>
    </aside>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; 2015-{{date('Y')}} <a href="{{Settings::getVal('company_website')}}">{{Settings::getVal('company_name')}}</a>.</strong> All rights
        reserved.
    </footer>
</div>


<!-- Modal Dialog -->
<div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Permanently</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure about this ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="confirm">Delete</button>
            </div>
        </div>
    </div>
</div>

</body>
<!-- Bootstrap -->
{{ HTML::script('js/bootstrap.min.js')}}
        <!-- AdminLTE App -->
{{ HTML::script('js/AdminLTE/app.js')}}
{{HTML::script('js/plugins/bootstrap-dialog/bootstrap-dialog.min.js')}}
@yield('scripts')
</html>
