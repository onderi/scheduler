@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Edit Room  
@endsection


<!-- Custom CSS !-->
@section('style')

@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Room
    <small>Edit</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')

<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>

        <form id="room" name="room" method="post" action="{{URL::to('room/update/'.$room->room_id);}}" role="form">
            {{ Form::token(); }}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="col-lg-6 form-group">
                    {{Form::label('name','Name')}}
                    {{Form::text('name',$room->room_name,array('class'=>'form-control','placeholder'=>'Please enter the Room Name','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('capacity','Capacity')}}
                    {{Form::text('capacity',$room->capacity,array('class'=>'form-control','placeholder'=>'Please enter the Room Capacity','required'=>'required'))}}
                </div>
                <!--<div class="col-lg-6 form-group">
                    {{Form::label('building','Building')}}
                    {{Form::select('building',$buildings,$room->area->building->building_id,array('class'=>'form-control','required'=>'required'))}}
                </div>-->
                <div class="col-lg-6 form-group">
                    {{Form::label('area','Room Area')}}
                    {{Form::select('area',$areas,$room->area->area_id,array('class'=>'form-control','required'=>'required'))}}
                </div>
            </div><!-- /.box-body -->
            <div  class="box-footer">
                <div style="padding: 13px;" class="form-group"> 
                    <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i>  Save Changes</button>
                </div>
            </div>
        </form>

    </div>  



</div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
    <script>
        $(document).ready(function(){

            {{--//Expand the menu :D--}}
             $('body').removeClass('sidebar-collapse');
        });
    </script>
@endsection 