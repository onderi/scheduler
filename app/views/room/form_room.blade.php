@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| New Room  
@endsection


<!-- Custom CSS !-->
@section('style')
{{ HTML::style('css/colorpicker/bootstrap-colorpicker.min.css') }}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Room
    <small>Add New</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')

<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>

        <form id="role" name="room" method="post" action="{{URL::to('room/save');}}" role="form">
            {{ Form::token(); }}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif


                <div class="col-lg-6 form-group">
                    {{Form::label('name','Name')}}
                    {{Form::text('name','',array('class'=>'form-control','placeholder'=>'Please enter the Room Name','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('capacity','Capacity')}}
                    {{Form::text('capacity','',array('class'=>'form-control','placeholder'=>'Please enter the Room Capacity','required'=>'required'))}}
                </div>
                
                <div class="col-lg-6 form-group">
                    {{Form::label('area','Room Area')}}
                    {{Form::select('area',$areas,'',array('class'=>'form-control','required'=>'required'))}}
                </div>
                 <div class="col-lg-6 form-group">
                    {{Form::label('ccode','Color Code')}}
                    {{Form::text('ccode','',array('class'=>'form-control','placeholder'=>'Please select color code','required'=>'required'))}}
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div style="padding: 13px;" class="form-group">
                    <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i>  Submit</button>
                </div>
            </div>
        </form>

    </div>  



</div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js') }}
{{ HTML::script('js/plugins/colorpicker/bootstrap-colorpicker.min.js') }}

<script type="text/javascript">
    
    $(function(){
        
        $('#ccode').colorpicker();
        
    });

    $(document).ready(function(){

        {{--//Expand the menu :D--}}
         $('body').removeClass('sidebar-collapse');
    });
    
</script>
@endsection 