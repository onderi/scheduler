@extends('layouts.master')

@section('title')
| Create Mail Template
@endsection

@section('style')
<style type="text/css">
    .dropdown-menu>li>a {
        display: block;
        padding: 3px 40px;
        clear: both;
    }

    .multiselect {
        text-align: left;
    }
    .multiselect b.caret {
        position: absolute;
        top: 14px;
        right: 8px;
    }
    .multiselect-group {
        font-weight: bold;
        text-decoration: underline;
    }
    
</style>
{{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}
{{ HTML::style('css/bootstrap-multiselect/bootstrap-multiselect.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Message Templates
    <small>Create message templates</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')


<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>
        @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                
                
                @endif
                 <div class="box-body">
                {{Form::open(['method'=>'POST','action'=>'MailController@store'])}}
                <div class="form-group col-md-12">
                	{{Form::label('name','Name')}}
                	{{Form::text('name','',['class'=>'form-control','placeholder'=>'Please input the message name'])}}
                </div>
                <div class="form-group col-md-12">
                {{Form::label('description','Message description')}}
                {{Form::text('description','',['class'=>'form-control','placeholder'=>'Please input a short description of the message'])}}
                </div>
                <div class="form-group col-md-12">
                	{{Form::label('message','Message')}}
                	{{Form::textarea('message','',['class'=>'form-control','placeholder'=>'Message','rows'=>'5'])}}
                </div>
                {{Form::submit('Add Message',['class'=>'btn btn-primary form-control'])}}
                {{Form::close()}}
                </div>
                </div>
                </div>
                @endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}
{{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
{{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}
@endsection

