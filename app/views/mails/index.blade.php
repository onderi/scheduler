@extends('layouts.master')

@section('title')
| Mail Templates
@endsection


@section('style')
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Mail Templates
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection

@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">

        </div>
        <div class="box-body">
            @if ( Session::get('notice') )
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Success!</b> {{{ Session::get('notice') }}}.
            </div>
            @endif
            @if ( Session::get('error') )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Error!</b> {{{ Session::get('error') }}}.
            </div>
            @endif
            <table class="table table-bordered table-striped dataTable" id="templates">
                <thead>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>

                </thead>
                <tbody>
                @foreach($mails as $mail)
                <tr>
                	<td>{{$mail->name}}</td>
                	
                	<td>{{$mail->description}}</td>
                	<td>
                		{{Form::open(['id'=>'delform'.$mail->mail_tmpl_id,'method'=>'post','url'=>'mails/delete/'.$mail->mail_tmpl_id])}}
                		<a data-rel='tooltip' title="Edit {{$mail->name}}" href="{{URL::to('mails/'.$mail->mail_tmpl_id.'/edit')}}">
                		<span class="fa fa-pencil"></span>
                		</a>
                		<a data-rel='tooltip' title="Delete {{$mail->name}}" href="#" class="delete_entry" id="delete{{$mail->mail_tmpl_id}}">
                		<span class="fa fa-trash"></span>
                		</a>
                		{{Form::close()}}
                	</td>
                </tr>
                @endforeach
                </tbody>
                </table>


        </div>
        <div class="box-footer">

        </div>
    </div>

</div>


@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
{{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
{{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
<script type="text/javascript">
    $(function() {
        $('#templates').dataTable({
            "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "oLanguage": {
                "sLengthMenu": "_MENU_ Mail Templates per page"
            },
            "oTableTools": {
                "aButtons": [
                {
                        "sExtends": "gotoURL",
                        "sButtonText": '<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>  Add Mail Template</button>',
                        "sGoToURL": "{{URL::to('mails/create')}}"
                    },
                    {
                        "sExtends": "pdf",
                        "sFileName": "Mail Templates.pdf",
                        "sPdfOrientation": "landscape",
                        "sPdfMessage": "SBS Planner Mail Templates",
                        "sButtonText": "Save to PDF"
                    },
                    {
                        "sExtends": "csv",
                        "sFileName": "Mail Templates.csv",
                        "sButtonText": "Save to CSV"
                    },
                    {
                        "sExtends": "xls",
                        "sFileName": "Mail Templates.xls",
                        "sButtonText": "Save to Excel"
                    }
                ]
            }
        });
        $('select,input').addClass('form-control input-sm');
        $('.alert').fadeOut(12000);
    });
</script>

@endsection