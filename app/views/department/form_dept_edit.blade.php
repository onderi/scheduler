@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Edit Department  
@endsection


<!-- Custom CSS !-->
@section('style')

@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Department
    <small>Edit</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')

<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>

        <form id="department" name="department" method="post" action="{{URL::to('department/update/'.$department->dept_id);}}" role="form">
            {{ Form::token(); }}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

               
                <div class="col-lg-6 form-group">
                    {{Form::label('name','Name')}}
                    {{Form::text('name',$department->dept_name,array('class'=>'form-control','placeholder'=>'Please enter the Department Name','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('abbreviation','Abbreviated Name')}}
                    {{Form::text('abbreviation',$department->dept_abbrev,array('class'=>'form-control','placeholder'=>'Please enter the Depatment short Name','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('description','Description')}}
                    {{Form::text('description',$department->dept_desc,array('class'=>'form-control','placeholder'=>'Please enter the Department Description'))}}
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div style="padding: 13px;" class="form-group">
                    <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i>  Save Changes</button>
                </div>
            </div>
        </form>

    </div>  



</div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')

    <script>
        $(document).ready(function(){

            {{--//Expand the menu :D--}}
             $('body').removeClass('sidebar-collapse');
        });
    </script>
@endsection 