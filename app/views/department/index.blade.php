@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Departments
@endsection


<!-- Custom CSS !-->
@section('style')
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
{{ HTML::style('js/plugins/noty/animate.css')}}

<style type="text/css">
    .modal-dialog{
        width:300px !important;
    }
</style>
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Departments
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">

        </div>
        <div class="box-body">

            <table class="table table-bordered table-striped dataTable" id="departments">
                <thead>
                <th>Name</th>
                <th>Abbreviated Name</th>
                <th>Description</th>
                <th>Actions</th>
                </thead>
                <tbody>
                    @if( count($departments) > 0 )
                    @foreach($departments as $department)
                    <tr>
                        <td>
                            <span class="pull-left"> {{ $department->dept_name}}</span>
                            <?php $deletable = count( $department->users ); ?>
                            @if( count( $department->users ) > 0 )
                                <?php    
                                    $prefix = '';
                                    $list = ""; 
                                 ?>
                                @foreach( $department->users as $u )
                                    <?php
                                        $list .= $prefix.$u->fname.' '.$u->lname;
                                        $prefix = '&#013;'; 
                                    ?>
                                @endforeach
                                <small data-placement="left" title="{{$list}}" class="badge pull-right bg-green"> {{ count( $department->users )}}</small>
                            @else
                                <small data-placement="left" title="No Users" class="badge pull-right bg-gray"> 0</small>
                            @endif
                        </td>
                        <td>{{$department->dept_abbrev}}</td>
                        <td>{{$department->dept_desc}}</td>
                        <td>
                            {{--
                            <a data-rel="tooltip" title="Add New" href="{{URL::to('department/add')}}" >
                            <span class="fa fa-plus-square-o"/>
                            </a>
                            --}}
                            <form id="delform{{$department->dept_id}}" method="post" action="{{URL::to('department/delete/'.$department->dept_id)}}">
                                {{Form::token()}}
                                <a data-rel="tooltip" title="Edit {{$department->dept_abbrev}}" href="{{URL::to('department/'.$department->dept_id.'/edit')}}" >
                                    <span class="fa fa-edit"/>
                                </a>
                                @if( $deletable == 0)
                                <a href='#' class="delete_entry" id="delete{{$department->dept_id}}" data-rel="tooltip" title="Delete {{$department->dept_abbrev}}" >
                                    <span class="fa fa-trash-o"/>
                                </a>
                                @endif
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>

            </table>

        </div>
        <div class="box-footer">

        </div>
    </div>

</div>


@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
{{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
{{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
{{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

<script type="text/javascript">
    $(function() {
        {{--//We want the calendar to be full screen :D--}}
        $('body').addClass('sidebar-collapse');
        {{--//Enable the navbar shower--}}
        $('.navbar-btn').fadeIn(2000);
        $('#departments').dataTable({
            "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "oLanguage": {
                "sLengthMenu": "_MENU_ Departments per page"
            },
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "gotoURL",
                        "sButtonText": '<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>  Add New Department</button>',
                        "sGoToURL": "{{URL::to('department/add')}}"
                    },
                    {
                        "sExtends": "pdf",
                        "sFileName": "Departments.pdf",
                        "sPdfOrientation": "landscape",
                        "sPdfMessage": "SBS Planner System Departments",
                        "sButtonText": "Save to PDF"
                    },
                    {
                        "sExtends": "csv",
                        "sFileName": "Departments.csv",
                        "sButtonText": "Save to CSV"
                    },
                    {
                        "sExtends": "xls",
                        "sFileName": "Departments.xls",
                        "sButtonText": "Save to Excel"
                    }
                ]
            }
        });
        $('select,input').addClass('form-control input-sm');
        $('.alert').fadeOut(5000);

        @if ( Session::get('notice') )

           noty({
            text: "Success! {{ Session::get('notice') }}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 3000,
            layout : 'topRight',
            type: 'success',
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight', // Animate.css class names
                easing: 'swing', // unavailable - no need
                speed: 500 // unavailable - no need
            }
        });

        @endif

        @if ( Session::get('error') )
           noty({
            text: "Error, {{ Session::get('error')}}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 10000,
            layout : 'topRight',
            type: 'error',
            animation: {
                open: 'animated swing', // Animate.css class names
                close: 'animated tada', // Animate.css class names
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            }
        });
        @endif
    });
</script>

@endsection

