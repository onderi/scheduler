@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| [TITLE]
@endsection


<!-- Custom CSS !-->
@section('style')

@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    General Form Elements
    <small>Preview</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
         <div class="box-header">
             <h3 class="box-title">User Form</h3>
        </div>
        <div class="box-body">
            
        </div>
        <div class="box-footer">
            
        </div>
    </div>
    
</div>


@endsection

<!-- Custom Scripts !-->
@section('scripts')
<script type="text/javascript">
    $(function() {
        $('.navbar-btn').fadeIn(2000);
    });
</script>
@endsection