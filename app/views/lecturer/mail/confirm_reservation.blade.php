<p>Hello,</p> 

</p>A booking for (lecturer_fame) (lecturer_lname) has been made and is pending confirmation.</p>

<p>Click on the below link <a href="https://my.sbs.ac.ke/scheduler/public/lecturer/booking/confirm/7"></a> to approve </p>

<p>Click on the link to disapprove <a href="https://my.sbs.ac.ke/scheduler/public/lecturer/booking/reject/7"></a></p>

<p>Do not respond to this email.</p>