<!DOCTYPE html>
<html>
<body>
<table border='0' width='100%'>
<tr>
    <td>
    <table border='0' width='100%'>
    <tr>
      <td width='120px'>
        <h3>SFPlanner</h3>
      </td>

      <td>
        <h3><small>Lecturer Booking Approval</small></h3>
      </td>
      </tr>
      </table>
      </td>
    </tr>
    <tr>
    <td>Dear {{$faculty_fname}} {{$faculty_lname}},</td>
    </tr>
    <tr>
    <td>You have been booked and approved to teach ($program_name) ($date)
    </tr>

    <tr>
    <td>You can segment the session from this
    <a href=''>link</a>
    <br>
    or copy link to your browser http://my.sbs.ac.ke/scheduler/lecturer/booking/confirm/{{$reservation_id}}
    </td>
    <td>
    You may log into the system and attempt to make another reservation or contact the booking administrator
    </td>
    </tr>

</table>
<br>
  <i>You may be required to login</i><br>
  <br>

  <footer>
    <address>
      <small><b>Strathmore Business School</b></small><br>
      <small>Ole Sangale Road, Madaraka Estate</small><br>
      <small>P.O. Box 59857-00200 Nairobi, Kenya</small><br>
      <small><a href="http://www.sbs.ac.ke">www.sbs.ac.ke</a></small>
    </address>
  </footer>
</body>
</html>