<h2>Hi, {{ $lecturer }}</h2>

<p>You have are requested to provide the following materials</p> 

<p><b>{{ $materials }}</b></p>

<p>To facilitate the following Course</p>

<p>{{ $course }}</p>

<p>Kindly ensure you submit the above materials between <i>{{ $start_date }} and latest {{ $end_date }}</i></p>

<p>Do not respond to this email.</p>
