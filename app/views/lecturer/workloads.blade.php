@extends('layouts.master')

<!-- Page Title -->
@section('title')
| Timesheets
@endsection

@section('style')
<!-- Css and datatables -->
{{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
   Timesheets
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection

<!-- Page Content !-->
@section('page_content')

  <!-- Filter view -->
  <div class="col-xs-3">

    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title faa-parent animated-hover"> <i class="fa fa-filter faa-shake"></i> Filter</h3>
      </div>

      <div class="box-body">

        <div id="lec">

          <p style="font-size: 14px;" class="faa-parent animated-hover">
              <i class="fa fa-bookmark text-red faa-burst"></i> <span > Lecturer</span>
          </p>
          <div class=" col-md-12 form-group">
              {{Form::label('faculty','Faculty')}}

              <select class="form-control" name="lecturer" id="lecturer" >
                  <option selected="" disabled="">Kindly Select a Lecturer(s)</option>
                  @foreach($faculty as $f)
                      <option value="{{$f->id}}">{{$f->lec_fname}} {{$f->lec_lname}}</option>
                  @endforeach
              </select>
          </div>
        </div>

        <div id="period">

          <p style="font-size: 14px;" class="faa-parent animated-hover">
              <i class="fa fa-clock-o text-red faa-burst"></i> <span > Period</span>
          </p>
          <div class=" col-md-12 form-group">
              {{Form::label('from','From')}}
              {{Form::text('from', '',array('class'=>'form-control','id' => 'from','placeholder'=>'From','data-date-format' => "YYYY-MM-DD HH:mm"))}}
          </div>

          <div class="col-md-12 form-group">
              {{Form::label('to','To')}}
              {{Form::text('to','',array('class'=>'form-control','id' => 'to','placeholder'=>'To','data-date-format' => "YYYY-MM-DD HH:mm"))}}
          </div>
        </div>

        <div class="col-md-12 form-group">

            <button class="btn-sm btn btn-primary" id="refresh-report" > <i class="fa fa-refresh"></i> Refresh</button>
        </div>

      </div>

    </div>
  </div>
  <div  class="col-xs-9">

    <div class="box box-warning">
      <div class="box-header">
        <h3 class="box-title">Summary</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
@include('lecturer.workloadstable')
      </div>
      <!-- /.box-body -->
      </div>
      <!-- /.box -->
      </div>
      <!-- /.col -->

<!-- /.content-wrapper -->
@endsection

        <!-- Custom Scripts !-->
@section('scripts')
        <!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
{{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}


<script type="text/javascript">
  $('#lecturers').dataTable({
      "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": true,
      "bSort": true,
      "bInfo": false,
      "bAutoWidth": false,
      "oLanguage": {
          "sLengthMenu": "_MENU_ Timesheet Report per page"
      },
      "oTableTools": {
          "aButtons": [
              {
                  "sExtends": "pdf",
                  "sFileName": "Timesheets.pdf",
                  "sPdfOrientation": "landscape",
                  "sPdfMessage": "SBS Planner System Timesheet",
                  "sButtonText": "Save to PDF"
              },
              {
                  "sExtends": "csv",
                  "sFileName": "Timesheets.csv",
                  "sButtonText": "Save to CSV"
              },
              {
                  "sExtends": "xls",
                  "sFileName": "Timesheets.xls",
                  "sButtonText": "Save to Excel"
              }
          ]
      }
  });

  $('#from').datetimepicker({
      pickTime:false,
      minuteStepping: 60,

  });
  $('#to').datetimepicker({
      pickTime:false,
      minuteStepping: 60,

  });

  $("#start").on("dp.change", function (e) {$('.end').data("DateTimePicker").setMinDate(e.date);});
  $("#end").on("dp.change", function (e) {$('.start').data("DateTimePicker").setMaxDate(e.date);});

  $("#refresh-report").click(function (e){
    e.preventDefault();

    var lecturer = $("#lecturer").val();
    var from = $("#from").val();
    var to = $("#to").val();

    if ( lecturer == "" ){
      alert('Please select a lecturer(s)');
      $('#lecturer').focus();

    } else if( from == "" ){
      alert('Please set FROM date');
      $('#from').focus();

    } else if ( to == "" ) {
      alert('Please set To date');
      $('#to').focus();

    } else {

      console.log('haha');

      $.post('/lecturer/refresh_workloads',
      {
        "_token": "{{ csrf_token() }}",
        "lecturer" : lecturer,
         "from" : from,
         "to" : to
       },
        function(data,status,xhr){
          $('#lecturers').html(data);
        }
      );

    }

  });

</script>

@endsection
