@extends('layouts.master')

<!-- Page Title !-->
@section('title')
 | Materials Request 
@endsection


<!-- Custom CSS !-->
@section('style')
 <style type="text/css">
     .dropdown-menu>li>a {
         display: block;
         padding: 3px 40px;
         clear: both;
     }

     .multiselect {
         text-align: left;
     }
     .multiselect b.caret {
         position: absolute;
         top: 14px;
         right: 8px;
     }
     .multiselect-group {
         font-weight: bold;
         text-decoration: underline;
     }
 </style>
 {{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}
 {{ HTML::style('css/bootstrap-multiselect/bootstrap-multiselect.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Teaching Materials and Exams Request
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>


        {{Form::open(['method'=>'POST','action'=>'LecturerController@materials_request'])}}
        <div class="box-body">
            @if ( $errors->count() > 0 )
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                <b>Error!</b>
                <ul>
                    @foreach( $errors->all() as $message )
                    <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
             <div style="padding-left: 15px;" class="form-group">
                <button class="btn btn-warning" id="add_material"><i class="fa fa-plus"></i>  Add New Material</button>
            </div>

            <div class='form-group col-md-6'>
            {{Form::label('text','Course')}}
            <select class="form-control" name="course" required="required">
                <option value="" selected="" disabled="">Kindly Select a Course</option>
                @foreach($course as $c)          
                    <option>{{$c->course_title}}</option>
                @endforeach
            </select>
            </div>
            <div class='form-group col-md-6'>
            {{Form::label('text','Lecturer')}}   
                <select class="form-control" name="lecturer" required="required">
                    <option value="" selected="" disabled="">Kindly Select a Lecturer</option>            
                    @foreach($faculty as $f)   
                        <option>{{$f->fname}} {{$f->lname}}</option>
                    @endforeach
                </select>
            </div>
            <div class='form-group col-md-6'>
             {{Form::label('text','Material Needed')}}
            <select name="materials" id="materials" class="form-control" multiple="multiple" required="required">
                @foreach($materials as $material)   
                <option value="{{$material->name}}">{{$material->name}}</option>
                @endforeach
            </select>
            </div>
            <div class='form-group col-md-6'>
            {{Form::label('Urgency','')}}
            {{Form::select('urgency',$urgency,'',['class'=>'form-control','required'=>'required','placeholder'=>''])}}
            </div>
            <div class='form-group col-md-6'>
            {{Form::label('start_date','Start Date')}}
            {{Form::text('start_date','',['id'=>'start','required'=>'required','class'=>'form-control start','placeholder'=>'Materials Request Start Date'])}}
            </div>
            <div class='form-group col-md-6'>
            {{Form::label('end_date','End Date')}}
            {{Form::text('end_date','',['id'=>'end','required'=>'required','class'=>'form-control end','placeholder'=>'Materials Request Start Date'])}}
            </div>
            <div style="padding-left: 15px;" class="form-group">
                <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i>  Submit</button>
            </div>
        </div>
    {{Form::close()}}
    </div>
</div>
@endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
{{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}
{{ HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}

<script type="text/javascript">
    $(function() {
        $('#materials').multiselect({
            numberDisplayed: 2,
            includeSelectAllOption: true,
            selectAllText: 'Select All'
        });
        /*Width Hack*/
        $('.dropdown-toggle').css({width:'100%'});
        $('.btn-group').css({width:'100%'});
    });

    $('#start').datetimepicker({
        pickTime:false,
        minuteStepping: 60,

    });
    $('#end').datetimepicker({
        pickTime:false,
        minuteStepping: 60,

    });

    $("#add_material").click(function(){
        window.location = "{{URL::to('lecturer/add_materials')}}";
    })
    
</script>
@endsection