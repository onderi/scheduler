@extends('layouts.master')
@section('title')
 | Lecturer Summary
@endsection

<!-- Custom CSS !-->
@section('style')
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}

@endsection

<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection

<!-- Content heading -->
@section('content_head')
<h1>
    Book a lecturer
</h1>
@endsection


@section('page_content')

  <div class="col-xs-12">

    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Summary</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="lecturers-table" class="table table-bordered table-striped dataTable">
          <thead>
            <th>Lecturer Name</th>
            <th>Department</th>
            <th>Allocated Hours</th>
            <th>Available Hours</th>
            <th>Added Hours</th>
            <th>Status</th>
          </thead>
          <tbody>
            @foreach($faculty as $f)

            <tr class="vrows" id="{{url('view',$f->user_id)}}">

              {{Form::open(array('url' => 'edit_lecturer', 'method' => 'POST'))}}

                <td>{{$f->fname}} {{$f->lname}}</td>
                <td>{{$f->dept_abbrev}}</td>
                <td>{{$f->hours}}</td>
                <td>{{$f->hours - $f->duration}}</td>
                <td>{{$f->added_hours}}</td>
                <td>
                  <a href="{{ URL::to('/')}}" data-rel="tooltip" title="Book"><span class="fa fa-book"/></a>
                  &nbsp
                  <a href="{{ URL::to('lecturer/lecturers/add_hours/'.$f->user_id)}}" data-rel="tooltip" title="Add Hours"><span class="fa fa-plus"/></a>

                </td>

              {{Form::close()}}

            </tr>

            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->

</div>
<!-- /.content-wrapper -->
@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}

<script type="text/javascript">
  $(document).ready(function() {
      $('#lecturers-table').dataTable();
  } );
</script>
@endsection

