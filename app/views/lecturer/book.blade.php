@extends('layouts.master')

<!-- Page Title -->
@section('title')
| Book a Lecturer
@endsection

@section('style')
<!-- Css and datatables -->
{{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}
@endsection

<!-- Content heading -->
@section('content_head')
<h1>
    Book a lecturer
</h1>
@endsection

<!-- Breadcrumbs !-->
<!-- 
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Book Lecturer</li>
</ol>
 -->
<!-- Page content -->
@section('page_content')

<div class="col-md-12">
  <!-- general form elements disabled -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Book a Lecturer</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    {{Form::open(['method'=>'POST','action'=>'LecturerController@book_confirm'])}}
        
        <div class="col-md-6">
          <div class="form-group">
          <label>From</label>
            <div class="input-group date date-picker">
              <input type="text" class="form-control" name="from" id="from">
              <span class="input-group-btn">
              <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
              </span>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
          <label>To</label>
            <div class="input-group date date-picker">
              <input type="text" class="form-control" name="to" id="to">
              <span class="input-group-btn">
              <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
              </span>
            </div>
          </div>
        </div>
      
        <!-- select -->
        <div class="form-group">
          <label>Select a Lecturer</label>
          <select class="form-control" id="lecturer" name="lecturer_id">
              <option selected disabled>Choose a lecturer</option>
              @foreach ($faculty as $f)
                <option value="{{$f->user_id}}" >{{$f->fname}} {{$f->lname}}</option>
              @endforeach
            </select>
          </select>
        </div>

        <!-- 
        Hidden fields for email 
        1. Lecturer Names
        2. Lecturer email
        3. Faculty affairs office email
        -->


        <div class="form-group">
          <label>Select a Specialization</label>
          <select class="form-control" name="course_id">
            <option selected="" disabled="">Choose a course</option>
            @foreach ($courses as $c)
              <option value="{{$c->course_id}}">{{$c->course_title}}</option>
            @endforeach
          </select>
        </div>

        <!-- text input -->
        <div class="form-group">
          <label>Available Duration</label> (in hours)
          <input type="text" class="form-control" disabled value="">
        </div>

        <div class="form-group">
          <label>Booking Duration</label> (in hours)
          <input type="text" class="form-control" disabled value="">
        </div>

        <!-- /.box-body -->
        <div class="box-footer">
          <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
          {{ Form::submit('Book', ['class' => 'btn btn-info pull-right']) }}
        </div>
        <!-- /.box-footer -->
      
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
@endsection

@section('scripts')
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
{{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}

<script type="text/javascript">
  $('#from').datetimepicker({
      pickTime:true,

  });
  $('#to').datetimepicker({
      pickTime:true,

  });
</script>
@endsection