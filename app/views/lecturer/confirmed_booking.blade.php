@extends('layouts.master')

<!-- Page Title -->
@section('title')
| Booked Lecturers
@endsection

@section('style')
<!-- Css and datatables -->
<style type="text/css">
    small{
        margin-left: 2px;
    }
</style>
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
{{ HTML::style('js/plugins/noty/animate.css')}}

<style type="text/css">
    .modal-dialog{
        width:300px !important;
    }
</style>
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Lecturer Bookings
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection

<!-- Page Content !-->
@section('page_content')
  
  <div class="col-xs-12">

    <div class="box box-warning">
      <div class="box-header">
        <h3 class="box-title">Summary</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <table class="table table-bordered table-striped dataTable" id="lecturers">
          <thead>
            <th>Lecturer Name</th>
            <th>Category</th>
            <th>Course</th>
            <th>Booked From</th>
            <th>Booked To</th>
            <th>Room</th>
            <th>Options</th>
          </thead>
          <tbody>
           
            @foreach($faculty as $f)

            <tr class="vrows">

              <td>{{$f->fname}} {{$f->lname}}</td>
              <td>{{$f->name}}</td>
              <td>{{$f->course_title}}</td>
              <td>{{$f->start_time}}</td>
              <td>{{$f->end_time}}</td>
              <td>{{$f->ref_number}}</td>
              <td>
                @if ($f->status == 3)
                <a href="" data-rel="tooltip" title="Approved"><span class="fa fa-thumbs-o-up"/></a>
                @elseif ($f->status == 4)
                <a href="{{url('lecturer/booking/confirm/'.$f->reservation_id)}}" data-rel="tooltip" title="Confirm" ><span class="fa fa-check"/></a>
                &nbsp
                <a href="{{url('lecturer/booking/reject/'.$f->reservation_id)}}" data-rel="tooltip" title="Disapprove"><span class="fa fa-close"/></a>
                &nbsp
                @elseif ($f->status == 5)
                <a href="" data-rel="tooltip" title="Rejected"><span class="fa fa-thumbs-o-down"/></a>
                @endif
              </td>
              <input type="hidden" name="id" value="{{ $f->reservation_id }}" >


            </tr>

            @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->

<!-- /.content-wrapper -->
@endsection

        <!-- Custom Scripts !-->
@section('scripts')
        <!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
{{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
{{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
{{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}


<script type="text/javascript">
  $(function() {
      $('#lecturers').dataTable({
          "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": false,
          "bAutoWidth": false,
          "oLanguage": {
              "sLengthMenu": "_MENU_ Bookings per page"
          },
          "oTableTools": {
              "aButtons": [
                  {
                      "sExtends": "pdf",
                      "sFileName": "General Bookings.pdf",
                      "sPdfOrientation": "landscape",
                      "sPdfMessage": "SBS Planner System Bookings",
                      "sButtonText": "Save to PDF"
                  },
                  {
                      "sExtends": "csv",
                      "sFileName": "Bookings.csv",
                      "sButtonText": "Save to CSV"
                  },
                  {
                      "sExtends": "xls",
                      "sFileName": "Bookings.xls",
                      "sButtonText": "Save to Excel"
                  }
              ]
          }
      });
  });
</script>
@endsection
