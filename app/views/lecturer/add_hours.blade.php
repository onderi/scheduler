@extends('layouts.master')

<!-- Page Title -->
@section('title')
| Book a Lecturer
@endsection

@section('style')
<!-- Css and datatables -->
{{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}
@endsection

<!-- Content heading -->
@section('content_head')
<h1>
    Book a lecturer
</h1>
@endsection

<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection

<!-- Page content -->
@section('page_content')

<div class="col-md-12">
  <!-- general form elements disabled -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Book a Lecturer</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    {{Form::open(['method'=>'POST','action'=>'LecturerController@update_lec_hours'])}}
     
        <!-- select -->
        <div class="form-group">
          {{Form::label('text', 'Lecturer Name')}}
          <select class="form-control" id="lecturer_name" name="lecturer_name">
              @foreach($faculty as $f)   
                  <option selected="">{{$f->fname}} {{$f->lname}}</option>
              @endforeach
          </select>
        </div>

        <div class="form-group">
          {{Form::label('text', 'Number of Hours to Add')}}
          {{Form::text('no_hours','',['class'=>'form-control','required'=>'required','placeholder'=>''])}}
        </div>

        <div class="form-group" hidden="">
          <select class="form-control" id="lecturer" name="lecturer_id">
              @foreach($faculty as $f)   
                  <option selected="">{{$f->user_id}}</option>
              @endforeach
          </select>
        </div>

        <!-- /.box-body -->
        <div class="box-footer">
          <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
          {{ Form::submit('Add', ['class' => 'btn btn-info pull-right']) }}
        </div>
        <!-- /.box-footer -->
      
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
@endsection

@section('scripts')
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
{{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}

@endsection