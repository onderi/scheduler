<table id="lecturers" class="table table-bordered table-striped dataTable">
  <thead>
    <th>Lecturer Name</th>

    <th>Duration</th>

    <th>Options</th>
  </thead>
  <tbody>

    @foreach($faculty as $f)

    <tr class="vrows">

      {{Form::open(array('url' => 'edit_lecturer', 'method' => 'POST'))}}

        <td>{{$f->lec_fname}} {{$f->lec_lname}}</td>

        <td>{{$f->duration}}</td>
        <td>
          <a href="{{URL::to('lecturer/viewworkloads/'.$f->id)}}" data-rel="tooltip" title="View"><span class="fa fa-eye"/></a>
          &nbsp
          <a href="" data-rel="tooltip" title="Edit"><span class="fa fa-edit"/></a>
          </td>
        <input type="hidden" name="id" value="" >

      {{Form::close()}}

    </tr>

    @endforeach

  </tbody>
  <tfoot>
    <tr>
      <td>Total</td>
      <!-- total duration -->
      @foreach($total as $t)
      <th>{{$t->dir}}</th>
      @endforeach

      <th>Options</th>
    </tr>

  </tfoot>
</table>
