@extends('layouts.master')
@section('title')
 | Workloads Detail
@endsection

<!-- Custom CSS !-->
@section('style')
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}

@endsection

<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection

<!-- Content heading -->
@section('content_head')
<h1>
    Workloads Detail
</h1>
@endsection


@section('page_content')

  <div class="col-xs-12">

    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Summary</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="lecturers-table" class="table table-bordered table-striped dataTable">
          <thead>
            <th>Lecturer Name</th>
            <th>Course</th>
            <th>Cartegory</th>
            <th>Duration</th>
            <th>Room</th>
            <th>Created By</th>

          </thead>
          <tbody>

            @foreach($faculty as $f)
            <tr>
              <td>{{$f->fname}} {{$f->lname}}</td>
              <td>{{$f->course_title}}</td>
              <td>{{$f->name}}</td>
              <td>{{$f->duration}}</td>
              <td>{{$f->ref_number}}</td>
              <td>{{$f->created_fname}} {{$f->created_lname}}</td>
              
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->

</div>
<!-- /.content-wrapper -->
@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}

<script type="text/javascript">

  $('#lecturers-table').dataTable({
      "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": true,
      "bSort": true,
      "bInfo": false,
      "bAutoWidth": false,
      "oLanguage": {
          "sLengthMenu": "_MENU_ Individual Timesheet Report per page"
      },
      "oTableTools": {
          "aButtons": [
              {
                  "sExtends": "pdf",
                  "sFileName": "TImesheets.pdf",
                  "sPdfOrientation": "landscape",
                  "sPdfMessage": "SBS Planner System Timesheet",
                  "sButtonText": "Save to PDF"
              },
              {
                  "sExtends": "csv",
                  "sFileName": "Timesheets.csv",
                  "sButtonText": "Save to CSV"
              },
              {
                  "sExtends": "xls",
                  "sFileName": "Timesheets.xls",
                  "sButtonText": "Save to Excel"
              }
          ]
      }
  });
</script>


@endsection
