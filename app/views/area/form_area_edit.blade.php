@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Edit Room Area  
@endsection


<!-- Custom CSS !-->
@section('style')
{{HTML::style('css/timepicker/bootstrap-timepicker.min.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Room Area
    <small>Edit</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')

<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>

        <form id="area" name="area" method="post" action="{{URL::to('area/update/'.$area->area_id);}}" role="form">
            {{ Form::token(); }}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="col-lg-6 form-group">
                    {{Form::label('name','Name')}}
                    {{Form::text('name',$area->area_name,array('class'=>'form-control','placeholder'=>'Please enter the Room Area Name','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('description','Description')}}
                    {{Form::text('description',$area->area_desc,array('class'=>'form-control','placeholder'=>'Please enter the Room Area Description'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('building','Building')}}
                    {{Form::select('building_id',$buildings,$selected,array('class'=>'form-control','placeholder'=>'Please select a building the area is located','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('default_duration','Default Duration')}}
                    {{Form::text('default_duration',$area->default_duration / 60,array('class'=>'form-control','placeholder'=>'Please enter the default duration','title'=>'in Minutes','data-rel'=>'tooltip','data-placement'=>'top','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('resolution','Resolution')}}
                    {{Form::text('resolution',$area->resolution / 60,array('class'=>'form-control','placeholder'=>'Please enter the resolution','title'=>'in Minutes','data-rel'=>'tooltip','data-placement'=>'top','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('start_interval','Start Interval')}}
                    <div class="bootstrap-timepicker">
                        {{Form::text('start_interval',$area->start_interval,array('class'=>'form-control','placeholder'=>'Please enter the start interval','required'=>'required'))}}
                    </div>
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('end_interval','End Interval')}}
                    <div class="bootstrap-timepicker">
                        {{Form::text('end_interval',$area->end_interval,array('class'=>'form-control','placeholder'=>'Please enter the end interval','required'=>'required'))}}
                    </div>
               </div>
                {{--
                <div class="col-lg-6 form-group">
                    {{Form::label('privacy','Private Booking')}}
                    {{Form::select('privacy',array('0'=>'Disabled','1'=>'Enabled'),$area->private_enabled,array('class'=>'form-control'))}}
                </div>
                --}}
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div style="padding-left: 15px;" class="form-group"> 
                    <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i>  Save</button>
                </div>
            </div>
        </form>

    </div>  



</div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/plugins/timepicker/bootstrap-timepicker.min.js') }}
<script type="text/javascript">
    $(document).ready(function(){

        {{--//Expand the menu :D--}}
         $('body').removeClass('sidebar-collapse');
    });
    //Start Interval
    $('#start_interval').timepicker({
        minuteStep: 30,
        showInputs: false,
        defaultTime: '06:30',
        disableFocus: true,
        showMeridian:false
    });
    
    //end interval time picker
     $('#end_interval').timepicker({
        minuteStep: 30,
        showInputs: false,
        defaultTime: '21:00',
        disableFocus: true,
        showMeridian:false
    });
</script>
@endsection 