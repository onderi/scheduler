@extends('layouts.master')

<!-- Page Title !-->
@section('title')
    | Home
    @endsection

            <!-- Custom CSS !-->
@section('style')
    {{ HTML::style('js/plugins/fullcalendar/fc/lib/fullcalendar.min.css' )}}
    {{ HTML::style('js/plugins/fullcalendar/fc/scheduler.min.css' )}}
    {{ HTML::style('js/plugins/fullcalendar/fc/sbs-calendar.css' )}}
    {{ HTML::style('css/jQueryUI/jquery-ui-1.10.3.custom.min.css') }}
    {{ HTML::style('js/plugins/smalot-datetimepicker/bootstrap-datetimepicker.min.css')}}

    {{ HTML::style('css/bootstrap-multiselect/bootstrap-multiselect.css')}}
    {{ HTML::style('css/qtip/jquery.qtip.min.css')}}
    {{ HTML::style('css/bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}
    {{ HTML::style('js/plugins/ladda/ladda-themeless.min.css')}}
    {{ HTML::style('js/plugins/noty/animate.css')}}
    {{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}
    {{ HTML::style('css/bootstrap-multiselect/bootstrap-multiselect.css')}}
    {{ HTML::style('js/plugins/select2/select2.css')}}
    {{ HTML::style('js/plugins/bootstrap-slider/slider.css')}}


    <style>
        .max_height{
            height:40px;
        }
        .view_more{
            float:right;
            color: rgb(219,104,28);
            cursor:pointer;
        }

        .add-reserv-dialog .modal-dialog{
            width:300px !important;
        }

        .small-dialog  .modal-dialog{
            width:300px !important;
        }

        .edit-reserve-dialog .modal-dialog{
            width: 600px !important;
        }

        .reserve-modal-onClickEvent .modal-dialog{
            width:400px !important;
        }

        .add_new_segment .modal-dialog{
            width: 65% !important;
        }
        .edit_new_segment .modal-dialog{
            width: 65% !important;
        }
    </style>
    @endsection

<!-- Content Heading !-->
@section('content_head')
    <h1>
        Home
    </h1>
    @endsection

            <!-- Breadcrumbs !-->
@section('breadcrumb')
    {{ Breadcrumbs::render() }}
    @endsection

            <!-- Page Content !-->
@section('page_content')
    @if(count($announcements)>0)
        <div class="col-md-12">
        <div class="box box-primary">
            <h4><b>Announcements</b></h4>
            <marquee>
                @foreach($announcements as $announce)
                    <b>{{$priority[$announce->priority]}}</b>: {{$announce->text}}
                @endforeach
            </marquee>
        </div>
        </div>
    @endif

    <div id="filt" class="col-md-2">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title faa-parent animated-hover"> <i class="fa fa-filter faa-shake"></i> Filters</h4>
            </div>
            <div class="box-body">

                @if( Auth::check() )

                    <p style="font-size: 14px;" class="faa-parent animated-hover">
                        <i class="fa fa-tag text-red faa-burst"></i> <span > Reservation Type</span>
                    </p>

                    {{Form::select('reservation_type',$reservation_type,$default_rtype,array('class'=>'form-control','id'=>'reservation_type','title' => ''))}}

                            <br/>
                @endif

                <p style="font-size: 14px;" class="faa-parent animated-hover">
                    <i class="fa fa-object-group text-red faa-burst"></i> <span id="filter-title"> Areas</span>
                </p>

                <form id="filter-category">
                    {{Form::select('filter',$areas,$default_area,array('class'=>'form-control','id'=>'filter','title' => ''))}}
                </form>
                <br/>
                {{-- The Checkboxes --}}
                <div id="rooms">

                    <div id="inner-rooms">
                        <p style="font-size: 14px;" class="faa-parent animated-hover">
                            <i class="fa fa-university text-red faa-burst"></i> <span > Rooms</span>
                        </p>
                        @foreach( $rooms_array as $room )
                        <div class='checkbox checkbox-primary'><input rtitle="<?php echo $room['title']; ?>" value="<?php echo $room['id']; ?>" id='checkbox<?php echo $room['id']; ?>' class='styled rooms_filter' type='checkbox' name="rooms_chk"  checked='checked'><label for='checkbox<?php echo $room['id']; ?>'> <?php echo $room['title']; ?></label></div>
                        @endforeach
                    </div>

                </div>

            </div><!-- /.box-body -->
        </div><!-- /. box -->
    </div>

    <div id="cont" class="col-md-10">

        <div class="box box-primary">

            <div class="box-body no-padding">
                <div id="calendar"></div>


                @if( Auth::check() )
                    <div  style="display:none;" id="reservation_calendar"></div>

                @endif

                <div  style="display:none;" id="timesheet_calendar"></div>

            </div><!-- /.box-body -->
        </div><!-- /. box -->
    </div>

    @endsection

            <!-- Custom Scripts !-->
@section('scripts')
    {{HTML::script('js/plugins/fullcalendar/fc/lib/moment.min.js')}}
    {{HTML::script('js/plugins/fullcalendar/fc/lib/fullcalendar.min.js')}}
    {{HTML::script('js/plugins/fullcalendar/fc/scheduler.js')}}
    {{HTML::script('js/jquery-ui-1.10.3.min.js')}}
    {{HTML::script('js/plugins/fullcalendar/fullCalendarExt.js')}}
    {{HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}

    {{HTML::script('js/plugins/qtip/jquery.qtip.min.js')}}
    {{HTML::script('js/plugins/fullcalendar/fc/sbs-calendar.js')}}
    {{HTML::script('js/plugins/spin/spin.min.js')}}
    {{HTML::script('js/plugins/ladda/ladda.min.js')}}
    {{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

    {{ HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}
    {{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
    {{ HTML::script('js/plugins/smalot-datetimepicker/bootstrap-datetimepicker.min.js')}}

    {{ HTML::script('js/plugins/bootstrap-slider/bootstrap-slider.js')}}
    {{ HTML::script('js/plugins/select2/select2.js')}}


    <script type="text/javascript">

        var holidays = <?php echo $holidays; ?>;
        /* Change the variable both on mousemove and click - whichever fires first */
        if( store.get('isload') == '1' ){
            //change
            $('body').on({
                mousemove : function(){
                    store.set('isload','0');
                }
            },'.wrapper');
        }

        if( store.get('isload') == '1' ){
            //change
            $('body').on({
                click : function(){
                    store.set('isload','0');
                }
            },'.wrapper');
        }

        $('body').click(function(){
            store.set('isload','0');
        });

        /**
         * Get the current resources callback
         * @param resources
         */
        function getCurrentResources(c_area){

            $.ajax({
                url: 'calendar/getResources',
                        type: 'GET',
                        data: {
                    area:  c_area
                },
                success: function (data) {
                },
                errors: function (error) {
                }
            });

        }



        {{-- Because your select element is dynamic (It is added to the DOM after the initial page load, or to be specific; after the initial event handler attachment) you will need to delegate your change event handler --}}
        $(document).on('change','.rooms_filter',function (){
            var room_id = $(this).val();
            var room_name = $(this).attr('rtitle');
            var selected_reservation_type = $('#reservation_type').val();

            if( $(this).is(':checked') ){
                {{--Add Resource--}}
                if ( selected_reservation_type == "room" ){
                    $('#calendar').fullCalendar('addResource',{id : room_id,title:room_name});

                }

                if ( selected_reservation_type == "course" ){
                    $('#reservation_calendar').fullCalendar('addResource',{id : room_id,title:room_name});
                }

                if ( selected_reservation_type == "timesheet" ){
                  $('#filter').html('{{Form::select("filter",$facultymembers,$default_area,array("class"=>"form-control","id"=>"filter","title" => ""))}}');
                    $('#timesheet_calendar').fullCalendar('addResource',{id : room_id,title:room_name});
                }

            }else {
                {{-- Remove Resource --}}
                if ( selected_reservation_type == "room" ){
                    $('#calendar').fullCalendar('removeResource', room_id);
                }

                if ( selected_reservation_type == "course" ){
                    $('#reservation_calendar').fullCalendar('removeResource', room_id);
                }

                if ( selected_reservation_type == "timesheet" ){
                  $('#filter-title').text('Lecturers');
                  $('#filter').html('{{Form::select("filter",$areas,$default_area,array("class"=>"form-control","id"=>"filter","title" => ""))}}');
                    $('#timesheet_calendar').fullCalendar('removeResource', room_id);
                }

            }
            var arrRooms = [];
            $("input:checkbox[name=rooms_chk]:checked").each(function(){
                 arrRooms.push($(this).val());
            });
            store.set('selected_rooms',arrRooms);

           ;

            if ( selected_reservation_type == "room" ){
                //refetch Events
                $('#calendar').fullCalendar('refetchEvents');
            }

            if ( selected_reservation_type == "course" ){
                //refetch Events
                $('#reservation_calendar').fullCalendar('refetchEvents');
            }

            if ( selected_reservation_type == "timesheet" ){
                //refetch Events
                $('#filter-title').text('Areas');
                $('#filter').html('{{Form::select("filter",$facultymembers,$default_area,array("class"=>"form-control","id"=>"filter","title" => ""))}}');
                $('#timesheet_calendar').fullCalendar('refetchEvents');

            }


        });

        $(document).ready(function () {

            var edit_url = "{{URL::to('reservation/room')}}";
            var add_segment = "{{URL::to('reservation/details/create')}}";
            var edit_segment = "{{URL::to('reservation/details')}}";
            var save_segment = "{{URL::to('reservation/details/store')}}";
            //our spinner & target
            var spinner;
            var target = document.getElementById('calendar');
            //options for the spinner
            var opts = {
                lines: 13, // The number of lines to draw
                length: 17, // The length of each line
                width: 8, // The line thickness
                radius: 21, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 58, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#3c8dbc', // #rgb or #rrggbb or array of colors
                speed: 0.9, // Rounds per second
                trail: 100, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: '50%', // Top position relative to parent
                left: '50%', // Left position relative to parent
                scale: 2,
                opacity: 0
            };


            $('#reservation_type').change(function(){

                var selected_type = $(this).val();

                if ( selected_type == 'room' ){
                    //$('#reservation_calendar').fullCalendar('destroy');
                    $("#reservation_calendar").css({"display": "none"});
                    $("#timesheet_calendar").css({"display": "none"});
                    $('#filter-title').text('Areas');
                    $('#filter').html('{{Form::select("filter",$areas,$default_area,array("class"=>"form-control","id"=>"filter","title" => ""))}}');
                    $("#calendar").css({"display": "block"});
                    $("#calendar").fullCalendar('render');
                    $('#calendar').fullCalendar( 'refetchResources' );
                    $('#calendar').fullCalendar('refetchEvents');

                }

                if ( selected_type == 'course' ){

                    $("#calendar").css({"display": "none"});
                    $("#timesheet_calendar").css({"display": "none"});
                    $('#filter-title').text('Areas');
                    $('#filter').html('{{Form::select("filter",$areas,$default_area,array("class"=>"form-control","id"=>"filter","title" => ""))}}');
                    $("#reservation_calendar").css({"display": "block"});
                    $("#reservation_calendar").fullCalendar('render');
                    $('#reservation_calendar').fullCalendar( 'refetchResources' );
                    $('#reservation_calendar').fullCalendar('refetchEvents');


                }

                if ( selected_type == 'timesheet' ){

                    $("#calendar").css({"display": "none"});
                    $("#reservation_calendar").css({"display": "none"});
                    $('#filter-title').text('Lecturers');
                    $('#filter').html('{{Form::select("filter",$facultymembers,$default_area,array("class"=>"form-control","id"=>"filter","title" => ""))}}');
                    $("#timesheet_calendar").css({"display": "block"});
                    $("#timesheet_calendar").fullCalendar('render');
                    // filterstuff();
                    $('#timesheet_calendar').fullCalendar( 'refetchResources' );
                    $('#timesheet_calendar').fullCalendar('refetchEvents');
                    var add_segment = "{{URL::to('faculty/details/create')}}";


                }


            });

            $('#filter').on('change',function(){
                {{-- unset the rooms --}}
                store.set('selected_rooms','undefined');
                {{-- Enable filter change --}}
                store.set('filter_changed','1');
                var area_id = $(this).val();
                {{-- //close it to avoid the 'hanging look' --}}
                $(this).blur();

                var selected_reservation_type = $('#reservation_type').val();


                if (area_id !== "") {

                    if ( selected_reservation_type == "room" ){
                        $('#calendar').fullCalendar( 'refetchResources' );
                        //Events are changed for all views
                        $('#calendar').fullCalendar( 'refetchEvents' );
                    }

                    if ( selected_reservation_type == "course" ){
                        $('#reservation_calendar').fullCalendar( 'refetchResources' );
                        //Events are changed for all views
                        $('#reservation_calendar').fullCalendar( 'refetchEvents' );
                    }

                    if ( selected_reservation_type == "timesheet" ){
                        $('#timesheet_calendar').fullCalendar( 'refetchResources' );
                        //Events are changed for all views
                        $('#timesheet_calendar').fullCalendar( 'refetchEvents' );
                    }

                }
             });
             function filterstuff() {
               {{-- unset the rooms --}}
               store.set('selected_rooms','undefined');
               {{-- Enable filter change --}}
               store.set('filter_changed','1');
               var area_id = $(this).val();
               {{-- //close it to avoid the 'hanging look' --}}
               $(this).blur();



             }

            var newReservationLink = "<?php echo URL::to('reservation/room/add'); ?>";
            var saveReservationLink = "<?php echo URL::to('reservation/room/save') ?>";
            var loginLink = "<?php echo URL::to('login') ?>";

            {{-- Room reservations calendar --}}
            $('#calendar').fullCalendar({
                schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',//Open source moments
                allDaySlot: false, // true, false
                minTime: "{{Settings::getVal('calendar_start')}}", {{--//limits changed to use moment.js--}}
                maxTime: "{{Settings::getVal('calendar_end')}}", {{--//limits changed to use moment.js--}}
                slotEventOverlap: true,
                eventOverlap: false,
                {{-- Commented to allow DayClick Event on month selectOverlap: false, --}}
                eventLimit: true, {{--// allow "more" link when too many events--}}
                theme : false,
                icons : {
                    prev: 'left-single-arrow',
                    next: 'right-single-arrow',
                    prevYear: 'left-double-arrow',
                    nextYear: 'right-double-arrow'
                },
                customButtons: {
                    // goto: {
                    //     // text: 'Go to date',
                    //     // click: function() {
                    //     //     $('.fc-goto-button').datetimepicker('show');
                    //     // }
                    // }
                },
                header: {
                    left: 'prevYear,prev,next,nextYear today,goto',
                    center: 'title',
                    right: 'agendaDay,agendaSevenDay,month'
                },
                views: {
                    agendaSevenDay: {
                        type: 'agenda',
                        duration: {days: 7},
                        {{--// views that are more than a day will NOT do this behavior by default
                        // so, we need to explicitly enable it--}}
                        groupByResource: true
                    },
                    agendaDay: {
                        titleFormat: 'ddd, Do MMM YYYY'
                    }
                },
                buttonText: {
                    today: 'Today',
                    month: 'Month',
                    agendaSevenDay: 'Week',
                    day: 'Day'
                },
                displayEventTime: false, {{--//Avoid showing the event time on top of the event.--}}
                prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
                next: "<span class='fc-text-arrow'>&rsaquo;</span>",
                prevYear: "<span class='fc-text-arrow'>&laquo;</span>",
                nextYear: "<span class='fc-text-arrow'>&raquo;</span>",
                timezone: 'local',
                axisFormat: 'HH:mm', {{--//Axis Time-format - 24 hours is 2px less hence more 'economical'--}}
                defaultView: 'agendaDay',
                slotMinutes: "00:30:00",
                selectable: true,
                resources:function (callback){
                    var selected_area = $('#filter').val();
                    $.ajax({
                        url : 'calendar/getResources?area='+selected_area,
                        cache : false,
                        success:function (data_area){
                            var list_of_resources = [];
                            var inner_rooms_div = "<p style='font-size: 14px;'  class='faa-parent animated-hover'> <i class='fa fa-university text-red faa-burst'></i> <span > Rooms</span> </p><div id='inner-rooms'>";
                            for(var b = 0; b < data_area.length;b++){
                                 var item = data_area[b];
                                 list_of_resources.push(item);
                                inner_rooms_div += "<div class='checkbox checkbox-primary'><input name='rooms_chk' id='checkbox"+b+"' rtitle='"+item.title+"' value='"+item.id+"' class='rooms_filter styled' type='checkbox' checked=''><label for='checkbox"+b+"'> "+item.title+"</label></div>";
                            }
                            {{-- Update the rooms --}}
                            inner_rooms_div += "</div>";
                            updateRoomsFilter(inner_rooms_div);
                            {{-- Pass them to calendar--}}
                             callback(list_of_resources);
                        },
                        type:'GET',
                        dataType:'json'
                    });
                },{{--//updated to a json feed to allow auto refetch on refetchResources--}}
                lazyFetching: true, {{--//fetch only what you see--}}
                {{--Use eventSources for faster loading.. :D--}}
                events: {
                        url: 'calendar/getAll',
                        type: 'GET',
                        data: function (){
                            return {
                                room_area: $('#filter').val()+"_"+store.get('selected_rooms')
                            };
                        }
                    }
                ,
                {{--
                 Loading Div
                  - Give feednack to the user when loaading resources
                  - Fired for events and resource change
                 --}}
                loading: function (isloading, view) {
                    {{--//load except the first instance--}}
                    if (store.get('isload') == '0' && view.name != 'agendaDay') {
                        if (isloading) {
                            spinner = new Spinner(opts).spin(target);
                        } else {
                            {{--//stop only if it is triggered--}}
                            if (typeof spinner !== "undefined")
                                spinner.stop();
                        }
                    }
                    {{-- @TODO Resolve ->NB: The loader will be displayed if the individual does not click anywhere on the page ikimaliza since the variable is changed onClick --}}
                    if (store.get('isload') == '1' && view.name == 'agendaDay') {
                        {{-- //@Hack - Prevent it from loading on the first page load view to avoid loading both loaders --}}
                        if (isloading) {
                            spinner = new Spinner(opts).spin(target);
                        } else {
                            {{--//stop only if it is triggered--}}
                            if (typeof spinner !== "undefined")
                                spinner.stop();
                        }
                    }
                    {{-- When the dropdown is selected before anyother click i.e interest is to close it --}}
                    if (store.get('isload') == '0' && view.name == 'agendaDay') {
                        {{--//stop only if it has been triggered--}}
                        if (typeof spinner !== "undefined")
                                spinner.stop();
                    }
                },

                {{--
                  EventRender
                  @param event
                  @param element
                  @param view
                 --}}
                eventRender: function (event, element, view) {
                    {{--//Attach Tooltip - for description--}}
                    var status = event.status;
                    var view_name = view.name;
                    var title_date = event.start.format('Do MMM');
                    var stat  = "";
                    var type = event.type;
                    var type_text = 'Internal';

                    if( type == "1" ){
                        type_text = "External";
                    }
                    //set the view
                    store.set('view',view_name);

                    if (  view_name === "resourceDay" )
                        stat = status;
                    else
                        stat = status + " for " + event.title;

                    {{--//Show descriptive text except for month i.e the eventClick is preferred--}}
                    if( view_name !== "month" ) {
                        var my_title = event.room;
                        {{-- If view is weekly - add date to title of tooltip --}}
                        if( view_name === "agendaSevenDay" )
                            my_title = my_title + ' - ' + title_date + ' (' + type_text + ')';
                        else
                            my_title = my_title + ' (' + type_text + ')';

                                    element.qtip({
                            content: {
                                title: my_title,
                                text: stat
                            },
                            style: 'qtip-cluetip qtip-rounded ',
                            position: {
                                target: "mouse",
                                my: 'bottom left'
                            }
                        });
                    }
                },
                {{--
                  Event Click - Bootstrap Modal showing details
                  @param event
                  @param element
                  @param view
                 --}}
                eventClick: function (event, element, view) {

                    var ev_date = event.start.format('MMMM Do YYYY');
                    var ev_reserve_date =  moment(event.rdate).format("Do MMM YYYY");
                    var room_reservation_id_ = event.id;
                    var ev_segments = event.segments;

                    if ( ev_segments == null ){

                        var segment_action =  {
                            label: 'Add Segment(s)',
                            icon: 'fa fa-plus-circle',
                            cssClass: 'btn-normal',
                            action: function (dialogRef) {
                                dialogRef.close();
                                //open edit dialog
                                add_segment_url = add_segment +'/'+ room_reservation_id_;
                                BootstrapDialog.show({
                                    cssClass : 'add_new_segment',
                                    title: 'Add Segments',
                                    closeByBackdrop: false,
                                    closeByKeyboard: false,
                                    id : 'add_new_segment_diag',
                                    message: function(dialog_new) {
                                        var $message = $('<div></div>');
                                        var pageToLoad = dialog_new.getData('pageToLoad');
                                        $message.load(pageToLoad);
                                        return $message;
                                    },
                                    data: {
                                        'pageToLoad': add_segment_url
                                    },
                                    buttons: [{
                                        label: '<span class="ladda-label"> Save </span>',
                                        cssClass: 'btn-primary ladda-button',
                                        icon : 'fa fa-save',
                                        id : 'save_reserve_btn',
                                        action: function(dialog_new) {
                                            var new_l_save = Ladda.create( document.querySelector('#save_reserve_btn') );

                                            //arrays
                                            var a_start = $('input[name="start[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_end = $('input[name="end[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_desc = $('textarea[name="desc[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_courses = $('select[name="courses[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            var a_ccategory = $('select[name="ccategory[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();


                                            var a_status =  $('select[name="status[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_faculty = $('select[name="faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            var a_first_faculty = $('input[name="first_faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_second_faculty = $('input[name="second_faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            //single values
                                            var created_by = $('input[name="user_id"]').val();
                                            var rr_id = $('input[name="rr"]').val();
                                            var rr_name = $('input[name="rr_name"]').val();
                                            var owner = $('input[name="owner"]').val();
                                            var ref_str = $('input[name="ref"]').val();
                                            var a_t_al = '0';

                                            if( $("#a_t_all").is(':checked') == true ){
                                                a_t_al = "1";
                                            }

                                            $.ajax({
                                                url:save_segment,
                                                beforeSend : function(x,settings){
                                                    new_l_save.start();
                                                },
                                                type : 'POST',
                                                data: {applytoall:a_t_al,start:a_start,end:a_end,desc:a_desc,courses:a_courses,ccategory: a_ccategory,status:a_status,faculty:a_faculty,first_faculty:a_first_faculty,second_faculty:a_second_faculty,user_id:created_by,rr:rr_id,rr_name:rr_name,owner:owner,ref:ref_str},
                                                success:function(data_add_new){
                                                    var json_object = JSON.parse( data_add_new );
                                                    var msg_len = Object.keys(json_object).length;
                                                    var msg_type = json_object['type'];


                                                    if(msg_type === "error" && msg_len > 1){
                                                        //append to the div
                                                        var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                        //remove the type - its the last element
                                                        var msg_len_correct = msg_len - 1;
                                                        for(var jd=0;jd<msg_len_correct;jd++){
                                                            error_div += '<li>'+json_object[jd]+'</li>';
                                                        }
                                                        error_div += "</ul></div>";
                                                        $('#error_block').html(error_div);
                                                    }

                                                    if( msg_type === "notice" ) {
                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS,
                                                            title: 'Success',
                                                            message: 'Segment(s) Added Successfully.',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function (dialog_success) {
                                                                    {{--- Add the event ---}}
                                                                    $('#calendar').fullCalendar('refetchEvents');
                                                                    dialog_success.close();
                                                                    dialog_new.close();
                                                                }
                                                            }]

                                                        });



                                                    }

                                                    new_l_save.stop();



                                                },
                                                error:function(error){
                                                    console.log('error->'+error);
                                                }
                                            });



                                        }
                                    }, {
                                        label: 'Cancel',
                                        cssClass: 'btn-danger',
                                        icon : 'fa fa-crosshairs',
                                        action: function(dialog_new) {
                                            dialog_new.close();
                                        }
                                    }]
                                });


                            }
                        };


                    }else{

                        var segment_action =  {
                            label: 'Edit Segment(s)',
                            icon: 'fa fa-plus-circle',
                            cssClass: 'btn-normal',
                            action: function (dialogRef) {
                                dialogRef.close();
                                //open edit dialog
                                edit_segment_url = edit_segment +'/'+ room_reservation_id_+'/edit';
                                BootstrapDialog.show({
                                    cssClass : 'edit_new_segment',
                                    title: 'Edit Segment(s)',
                                    id : 'edit_new_segment_diag',
                                    message: function(dialog_new) {
                                        var $message = $('<div></div>');
                                        var pageToLoad = dialog_new.getData('pageToLoad');
                                        $message.load(pageToLoad);
                                        return $message;
                                    },
                                    data: {
                                        'pageToLoad': edit_segment_url
                                    },
                                    buttons: [{
                                        label: '<span class="ladda-label"> Save </span>',
                                        cssClass: 'btn-primary ladda-button',
                                        icon : 'fa fa-save',
                                        id : 'edit_reserve_btn',
                                        action: function(dialog_new) {
                                            var new_l_save = Ladda.create( document.querySelector('#edit_reserve_btn') );

                                            //arrays
                                            var a_start = $('input[name="start[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_end = $('input[name="end[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_desc = $('textarea[name="desc[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_courses = $('select[name="courses[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            var a_ccategory = $('select[name="ccategory[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();


                                            var a_status =  $('select[name="status[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_faculty = $('select[name="faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            var a_first_faculty = $('input[name="first_faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_second_faculty = $('input[name="second_faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            //single values
                                            var created_by = $('input[name="user_id"]').val();
                                            var rr_id = $('input[name="rr"]').val();
                                            var rr_name = $('input[name="rr_name"]').val();
                                            var owner = $('input[name="owner"]').val();
                                            var ref_str = $('input[name="ref"]').val();
                                            var a_t_al = '0';

                                            if( $("#a_t_all").is(':checked') == true ){
                                                a_t_al = "1";
                                            }

                                            $.ajax({
                                                url:'reservation/details/update',
                                                beforeSend : function(x,settings){
                                                    new_l_save.start();
                                                },
                                                type : 'POST',
                                                data: {applytoall:a_t_al,start:a_start,end:a_end,desc:a_desc,courses:a_courses,ccategory: a_ccategory,status:a_status,faculty:a_faculty,first_faculty:a_first_faculty,second_faculty:a_second_faculty,user_id:created_by,rr:rr_id,rr_name:rr_name,owner:owner,ref:ref_str},
                                                success:function(data_add_new){
                                                    var json_object = JSON.parse( data_add_new );
                                                    var msg_len = Object.keys(json_object).length;
                                                    var msg_type = json_object['type'];


                                                    if(msg_type === "error" && msg_len > 1){
                                                        //append to the div
                                                        var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                        //remove the type - its the last element
                                                        var msg_len_correct = msg_len - 1;
                                                        for(var jd=0;jd<msg_len_correct;jd++){
                                                            error_div += '<li>'+json_object[jd]+'</li>';
                                                        }
                                                        error_div += "</ul></div>";
                                                        $('#error_block').html(error_div);
                                                    }

                                                    if( msg_type === "notice" ) {
                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS,
                                                            title: 'Success',
                                                            message: 'Segment(s) Edited Successfully.',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function (dialog_success) {
                                                                    {{--- Add the event ---}}
                                                                    $('#calendar').fullCalendar('refetchEvents');
                                                                    dialog_success.close();
                                                                    dialog_new.close();
                                                                }
                                                            }]

                                                        });



                                                    }

                                                    new_l_save.stop();



                                                },
                                                error:function(error){
                                                    console.log('error->'+error);
                                                }
                                            });

                                        }
                                    }, {
                                        label: 'Cancel',
                                        cssClass: 'btn-danger',
                                        icon : 'fa fa-crosshairs',
                                        action: function(dialog_new) {
                                            dialog_new.close();
                                        }
                                    }]
                                });


                            }
                        };

                    }

                    //console.log(ev_reserve_date);
                    var type = event.type;
                    var type_text = 'Internal';

                    if( type == "1" ){
                        type_text = "External";
                    }
                    var start_time = "?";
                    if ( event.start !== null )
                        start_time = event.start.format('HH:mm a');

                    var end_time = "?";
                    if( event.end !== null )
                        end_time = event.end.format('HH:mm a');

                    var color = event.color;
                    var status = "";
                    if (event.status === "Pending Approval") {
                        status = "<span style='color:orangered;'>" + event.status + "</span>";
                    } else {
                        status = event.status;
                    }

                    var reserved_by_who = "";

                    @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )

                            reserved_by_who = "<li>Booked By : "+ event.mby_f + " " + event.mby_l +"</li>";

                    @endif


                    var msg = "<ul><li> Reserve Date : "+ev_reserve_date+"</li><li>Date : " + ev_date + " </li>" + "<li> Duration :" + start_time + " to " + end_time + " <li> Status :" + status + "</li><li>Type : "+type_text+reserved_by_who+"</ul>";

                    var button_arr = [{

                        label: 'Close',
                        icon: 'fa fa-times',
                        cssClass: 'btn-primary',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }

                    }];

                    @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )

                        button_arr = [];

                        var edit_r_reservation = {
                            label: '<span class="ladda-label"> Edit</span>',
                            icon: 'fa fa-pencil',
                            cssClass: 'btn-success ladda-button',
                            id : 'edit_reserve_btn',
                            action: function (dialogRef) {
                                dialogRef.close();
                                var edit_url_link = edit_url + '/' + event.id+'/edit';
                                var editReservationLink = "<?php echo URL::to('reservation/room/update') ?>"+'/'+event.id;

                                BootstrapDialog.show({
                                    title: 'Edit Reservation',
                                    cssClass: "edit-reserve-dialog",
                                    message: function(dialog_edit_rs) {
                                        var $message = $('<div></div>');
                                        var pageToLoadEd = dialog_edit_rs.getData('pageToLoadEd');
                                        $message.load(pageToLoadEd);
                                        return $message;
                                    },
                                    data: {
                                        'pageToLoadEd': edit_url_link
                                    },
                                    buttons: [{
                                        label: '<span class="ladda-label"> Save Changes </span>',
                                        cssClass: 'btn-primary ladda-button',
                                        icon : 'fa fa-save',
                                        id : 'edt_save_reserve_btn',
                                        action: function(dialog_edt_save) {
                                            var l_edit_btn = Ladda.create( document.querySelector('#edt_save_reserve_btn') );
                                            var r_name =  $('#name').val();
                                            var r_start = $('#start').val();
                                            var r_end =   $('#end').val();
                                            var r_desc = $('#description').val();
                                            var r_dept = $('#department').val();
                                            var r_type = 'NONE';
                                            var r_room = $('#room').val();
                                            var r_repeat_end = '';
                                            var r_user = $('#user').val();

                                            var selected_days = [];

                                            var r_area = $('#area').val();
                                            var rep_times_val = '';

                                            //remember it is a checkbox..
                                            var all_day = '0';
                                            if( $('#allday').is(':checked') ){
                                                all_day = $('#allday').val();
                                            }

                                            var ext = '0';
                                            if($('#external').is(':checked')){
                                                ext = $('#external').val();
                                            }

                                            var repeat = '0';
                                            if( $('#repeat').is(':checked') ){
                                                repeat = $('#repeat').val();
                                            }

                                            $.ajax({
                                                url:editReservationLink,
                                                beforeSend : function(x,settings){
                                                    l_edit_btn.start();
                                                },
                                                data:{user:r_user,rep_day:selected_days,rep_times_input:rep_times_val,area:r_area,repeat:repeat,rr_type:ext,allday:all_day,name:r_name,start:r_start,end:r_end,description:r_desc,department:r_dept,rtype:r_type,room:r_room,repeat_end:r_repeat_end},
                                                type : 'POST',
                                                success:function(data_add_new){
                                                    var json_object = JSON.parse( data_add_new );
                                                    var msg_len = Object.keys(json_object).length;
                                                    var msg_type = json_object['type'];


                                                    if(msg_type === "error" && msg_len > 1){
                                                        //append to the div
                                                        var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                        //remove the type - its the last element
                                                        var msg_len_correct = msg_len - 1;
                                                        for(var jd=0;jd<msg_len_correct;jd++){
                                                            error_div += '<li>'+json_object[jd]+'</li>';
                                                        }
                                                        error_div += "</ul></div>";
                                                        $('#error_block').html(error_div);
                                                    }

                                                    if( msg_type === "notice" ) {
                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS,
                                                            title: 'Success',
                                                            message: 'Reservation Edited Successfully.',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function (dialog_success_edt) {
                                                                    dialog_success_edt.close();
                                                                    dialog_edt_save.close();
                                                                }
                                                            }]

                                                        });
                                                        {{--- Add the event ---}}
                                                        $('#calendar').fullCalendar('refetchEvents');
                                                    }
                                                    l_edit_btn.stop();
                                                },
                                                error:function(error){
                                                    console.log('error->'+error);
                                                }
                                            });


                                        }
                                    }, {
                                        label: 'Cancel',
                                        cssClass: 'btn-danger',
                                        icon : 'fa fa-crosshairs',
                                        action: function(dialog_ed) {
                                            dialog_ed.close();
                                        }
                                    }]
                                });


                            }
                        };
                        var delete_r_reservation = {
                            label: 'Delete',
                            id: 'outer_button',
                            icon: 'fa fa-trash',
                            cssClass: 'btn-danger',
                            action: function (dialogRef) {
                                var l_outer = Ladda.create( document.querySelector('#outer_button') );
                                l_outer.start();
                                {{-- Tooltip with Yes or No --}}
                                BootstrapDialog.show({
                                    type: BootstrapDialog.TYPE_INFO,
                                    title: 'Confirm Delete',
                                    message: 'Are you sure?',
                                    closable: false,
                                    cssClass: 'small-dialog ',
                                    buttons: [{
                                        id : 'delete-btn',
                                        icon: 'fa fa-trash',
                                        label: '<span class="ladda-label"> Yes </span>',
                                        cssClass: 'btn-danger ladda-button',
                                        action: function(dialog) {
                                            var new_l = Ladda.create( document.querySelector('#delete-btn') );
                                            {{-- In Yes block run ajax to delete the event --}}
                                             $.ajax({
                                                url : 'calendar/deleteReserve',
                                                data : {ud:event.id},
                                                type: 'post',
                                                beforeSend : function(x,settings){
                                                    new_l.start();
                                                },
                                                success : function (data){
                                                    if( data === '1'){

                                                        $('#calendar').fullCalendar('removeEvents', event.id);

                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS,
                                                            title: 'Success',
                                                            message: 'Reservation Deleted Successfully.',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function(dialog_delete) {
                                                                    dialog_delete.close();
                                                                }
                                                            }]

                                                        });


                                                    }else if(data==='0'){
                                                        {{--Error --}}
                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_DANGER,
                                                            title: 'Error',
                                                            message: 'Unable to delete Reservation, Integrity constraints enforced ',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function(dialog_delete) {
                                                                    dialog_delete.close();
                                                                }
                                                            }]
                                                        });

                                                    }
                                                    {{--stop the spinners--}}
                                                    new_l.stop();
                                                    l_outer.stop();
                                                    {{--close the dialogs--}}
                                                    dialogRef.close();
                                                }
                                            });
                                            dialog.close();
                                        }
                                    },
                                        {
                                            icon: 'fa fa-times',
                                            label: ' No',
                                            cssClass: 'btn-primary',
                                            action: function(dialog) {
                                                dialog.close();
                                                dialogRef.close();
                                            }
                                        }
                                    ]

                                });



                                {{-- : when all done refetch resources --}}

                                ///dialogRef.close();
                            }
                        };
                        var close_r_reservation = {
                            label: 'Close',
                            icon: 'fa fa-times',
                            cssClass: 'btn-primary',
                            action: function (dialogRef) {
                                dialogRef.close();
                            }
                        };

                        button_arr.push(segment_action);

                        button_arr.push(edit_r_reservation);
                        button_arr.push(delete_r_reservation);
                        button_arr.push(close_r_reservation);


                    @endif
                    var diag = new BootstrapDialog({
                        size: BootstrapDialog.SIZE_SMALL,
                        title: event.title,
                        message: msg,
                        closable: true,
                        cssClass : 'reserve-modal-onClickEvent',
                        autospin: true,
                        buttons: button_arr
                    });
                    diag.realize();
                    diag.getModalHeader().css('background-color', color);
                    diag.open();
                },
                dayClick : function(date,jsEvent,view,resource){
                    {{-- GO TO Resource View for clicked date --}}
                    if( view.name === 'month' ) {
                        $('#calendar').fullCalendar('changeView', 'agendaDay');
                        $('#calendar').fullCalendar('gotoDate', date);
                    }
                },
                {{-- Select function - Trigger create new reservation / Alert error on past date --}}
                select: function (start, end, event, view, resource) {
                    var check = start.format('YYYY-MM-DD HH');
                    var today = moment().format('YYYY-MM-DD HH');

                    var clean_start = start.format('YYYY-MM-DD|HH:mm');
                    var clean_end = end.format('YYYY-MM-DD|HH:mm');

                    var resource_id, addReservationLink;
                    //get the current view
                    var pview_current = view.name;

                    var start_date = start.format('YYYY-MM-DD');
                    var is_holiday = false;

                    $.each(holidays,function(key,value){
                        if ( start_date == value )
                            is_holiday = true;
                    });
                    {{--No holidays--}}
                    if ( is_holiday === false ){

                        {{-- No Past Dates/Times --}}
                        if (event && ( pview_current === "agendaDay" || pview_current === "agendaSevenDay")) {

                            if (check < today) {
                                var message = '<b style="text-align: center;">Reservations forbidden for past date and/or time.</b> <br/>';

                                BootstrapDialog.show({
                                    title: 'Error!',
                                    message: message,
                                    closable: true,
                                    type: BootstrapDialog.TYPE_DANGER,
                                    cssClass: "small-dialog"
                                });

                            } else {
                                resource_id = resource.id;
                                addReservationLink = newReservationLink + '/' + resource_id + '/' + clean_start + '/' + clean_end;

                                var diag = new BootstrapDialog({
                                    type: BootstrapDialog.TYPE_INFO,
                                    title: "Confirm Action : New Reservation?",
                                    closable: true,
                                    cssClass: "add-reserv-dialog",
                                    buttons: [{
                                        label: 'Yes',
                                        cssClass: 'btn btn-primary',
                                        icon: 'fa fa-check',
                                        action: function(dialog) {
                                            @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )
                                                dialog.close();

                                            BootstrapDialog.show({
                                                title: 'Add New Reservation',
                                                message: function(dialog_new) {
                                                    var $message = $('<div></div>');
                                                    var pageToLoad = dialog_new.getData('pageToLoad');
                                                    $message.load(pageToLoad);
                                                    return $message;
                                                },
                                                data: {
                                                    'pageToLoad': addReservationLink
                                                },
                                                buttons: [{
                                                    label: '<span class="ladda-label"> Save </span>',
                                                    cssClass: 'btn-primary ladda-button',
                                                    icon : 'fa fa-save',
                                                    id : 'save_reserve_btn',
                                                    action: function(dialog_new) {
                                                        var new_l_save = Ladda.create( document.querySelector('#save_reserve_btn') );

                                                        var r_name =  $('#name').val();
                                                        var r_start = $('#start').val();
                                                        var r_end =   $('#end').val();
                                                        var r_desc = $('#description').val();
                                                        var r_dept = $('#department').val();
                                                        var r_type = $('#rtype').val();
                                                        var r_room = $('#room').val();
                                                        var r_repeat_end = $('#repeat_end').val();
                                                        var r_for = $('#user').val();

                                                        var selected_days = [];
                                                        $.each($("input[name^=rep_day]:checked"), function() {
                                                            selected_days.push($(this).val());
                                                            // or you can do something to the actual checked checkboxes by working directly with  'this'
                                                            // something like $(this).hide() (only something useful, probably) :P
                                                        });

                                                        var r_area = $('#area').val();
                                                        var rep_times_val = $('#rep_times_input').val();

                                                        //remember it is a checkbox..
                                                        var all_day = '0';
                                                        if( $('#allday').is(':checked') ){
                                                            all_day = $('#allday').val();
                                                        }

                                                        var ext = '0';
                                                        if($('#external').is(':checked')){
                                                            ext = $('#external').val();
                                                        }

                                                        var repeat = '0';
                                                        if( $('#repeat').is(':checked') ){
                                                            repeat = $('#repeat').val();
                                                        }

                                                        $.ajax({
                                                            url:saveReservationLink,
                                                            beforeSend : function(x,settings){

                                                                new_l_save.start();
                                                            },
                                                            data:{user:r_for,rep_day:selected_days,rep_times_input:rep_times_val,area:r_area,repeat:repeat,rr_type:ext,allday:all_day,name:r_name,start:r_start,end:r_end,description:r_desc,department:r_dept,rtype:r_type,room:r_room,repeat_end:r_repeat_end},
                                                            type : 'POST',
                                                            success:function(data_add_new){
                                                                var json_object = JSON.parse( data_add_new );
                                                                var msg_len = Object.keys(json_object).length;
                                                                var msg_type = json_object['type'];


                                                                if(msg_type === "error" && msg_len > 1){
                                                                    //append to the div
                                                                    var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                                    //remove the type - its the last element
                                                                    var msg_len_correct = msg_len - 1;
                                                                    for(var jd=0;jd<msg_len_correct;jd++){
                                                                        error_div += '<li>'+json_object[jd]+'</li>';
                                                                    }
                                                                    error_div += "</ul></div>";
                                                                    $('#error_block').html(error_div);
                                                                }

                                                                if( msg_type === "notice" ) {
                                                                    BootstrapDialog.show({
                                                                        type: BootstrapDialog.TYPE_SUCCESS,
                                                                        title: 'Success',
                                                                        message: 'Reservation Added Successfully.',
                                                                        closable: true,
                                                                        cssClass: 'small-dialog',
                                                                        buttons: [{
                                                                            icon: 'fa fa-cog fa-spin',
                                                                            label: ' Ok',
                                                                            action: function (dialog_success) {
                                                                                {{--- Add the event ---}}
                                                                                $('#calendar').fullCalendar('refetchEvents');
                                                                                dialog_success.close();
                                                                                dialog_new.close();
                                                                            }
                                                                        }]

                                                                    });



                                                                }

                                                                new_l_save.stop();



                                                            },
                                                            error:function(error){
                                                                console.log('error->'+error);
                                                            }
                                                        });


                                                    }
                                                }, {
                                                    label: 'Cancel',
                                                    cssClass: 'btn-danger',
                                                    icon : 'fa fa-crosshairs',
                                                    action: function(dialog_new) {
                                                        dialog.close();
                                                        dialog_new.close();
                                                    }
                                                }]
                                            });
                                            @else
                                                window.location = loginLink;
                                            @endif
                                        }
                                    }, {
                                        label: 'No',
                                        cssClass: 'btn btn-danger',
                                        icon: 'fa fa-close',
                                        action: function(dialog) {
                                            dialog.close();
                                        }
                                    }]
                                });

                                diag.realize();
                                diag.getModalBody().hide();
                                diag.getModalBody().remove();
                                diag.open();
                            }
                        }

                    }else{
                        var message = '<b style="text-align: center;">Reservations forbidden for non-working days.</b> <br/>';

                        BootstrapDialog.show({
                            title: 'Error!',
                            message: message,
                            closable: true,
                            type: BootstrapDialog.TYPE_DANGER,
                            cssClass: "small-dialog"
                        });
                    }
                }
            });

            {{--Course calendar --}}
            $('#reservation_calendar').fullCalendar({
                schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',//Open source moments
                allDaySlot: false, // true, false
                minTime: "{{Settings::getVal('calendar_start')}}", {{--//limits changed to use moment.js--}}
                maxTime: "{{Settings::getVal('calendar_end')}}", {{--//limits changed to use moment.js--}}
                slotEventOverlap: true,
                eventOverlap: false,
                {{-- Commented to allow DayClick Event on month selectOverlap: false, --}}
                eventLimit: true, {{--// allow "more" link when too many events--}}
                theme : false,
                icons : {
                    prev: 'left-single-arrow',
                    next: 'right-single-arrow',
                    prevYear: 'left-double-arrow',
                    nextYear: 'right-double-arrow'
                },
                customButtons: {
                    // goto: {
                    //     // text: 'Go to date',
                    //     // click: function() {
                    //     //     $('.fc-goto-button').datetimepicker('show');
                    //     // }
                    // }
                },
                header: {
                    left: 'prevYear,prev,next,nextYear today,goto',
                    center: 'title',
                    right: 'agendaDay,agendaSevenDay,month'
                },
                views: {
                    agendaSevenDay: {
                        type: 'agenda',
                        duration: {days: 7},
                        {{--// views that are more than a day will NOT do this behavior by default
                        // so, we need to explicitly enable it--}}
                        groupByResource: true
                    },
                    agendaDay: {
                        titleFormat: 'ddd, Do MMM YYYY'
                    }
                },
                buttonText: {
                    today: 'Today',
                    month: 'Month',
                    agendaSevenDay: 'Week',
                    day: 'Day'
                },
                displayEventTime: false, {{--//Avoid showing the event time on top of the event.--}}
                prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
                next: "<span class='fc-text-arrow'>&rsaquo;</span>",
                prevYear: "<span class='fc-text-arrow'>&laquo;</span>",
                nextYear: "<span class='fc-text-arrow'>&raquo;</span>",
                timezone: 'local',
                axisFormat: 'HH:mm', {{--//Axis Time-format - 24 hours is 2px less hence more 'economical'--}}
                defaultView: 'agendaDay',
                slotMinutes: "00:30:00",
                selectable: true,
                resources:function (callback){
                    var selected_area = $('#filter').val();
                    $.ajax({
                        url : 'calendar/getResources?area='+selected_area,
                        cache : false,
                        success:function (data_area){
                            var list_of_resources = [];
                            var inner_rooms_div = "<p style='font-size: 14px;'  class='faa-parent animated-hover'> <i class='fa fa-university text-red faa-burst'></i> <span > Rooms</span> </p><div id='inner-rooms'>";
                            for(var b = 0; b < data_area.length;b++){
                                var item = data_area[b];
                                list_of_resources.push(item);
                                inner_rooms_div += "<div class='checkbox checkbox-primary'><input name='rooms_chk' id='checkbox"+b+"' rtitle='"+item.title+"' value='"+item.id+"' class='rooms_filter styled' type='checkbox' checked=''><label for='checkbox"+b+"'> "+item.title+"</label></div>";
                            }
                            {{-- Update the rooms --}}
                                inner_rooms_div += "</div>";
                            updateRoomsFilter(inner_rooms_div);
                            {{-- Pass them to calendar--}}
                             callback(list_of_resources);
                        },
                        type:'GET',
                        dataType:'json'
                    });
                },{{--//updated to a json feed to allow auto refetch on refetchResources--}}
                lazyFetching: true, {{--//fetch only what you see--}}
                        {{--Use eventSources for faster loading.. :D--}}
                events: {
                    url: 'calendar/getAllCr',
                    type: 'GET',
                    data: function (){
                        return {
                            room_area: $('#filter').val()+"_"+store.get('selected_rooms')
                        };
                    }
                }
                ,
                {{--
                 Loading Div
                  - Give feednack to the user when loaading resources
                  - Fired for events and resource change
                 --}}
                loading: function (isloading, view) {
                    {{--//load except the first instance--}}
                    if (store.get('isload') == '0' && view.name != 'agendaDay') {
                        if (isloading) {
                            spinner = new Spinner(opts).spin(target);
                        } else {
                            {{--//stop only if it is triggered--}}
                            if (typeof spinner !== "undefined")
                                spinner.stop();
                        }
                    }
                    {{-- @TODO Resolve ->NB: The loader will be displayed if the individual does not click anywhere on the page ikimaliza since the variable is changed onClick --}}
                    if (store.get('isload') == '1' && view.name == 'agendaDay') {
                        {{-- //@Hack - Prevent it from loading on the first page load view to avoid loading both loaders --}}
                        if (isloading) {
                            spinner = new Spinner(opts).spin(target);
                        } else {
                            {{--//stop only if it is triggered--}}
                            if (typeof spinner !== "undefined")
                                spinner.stop();
                        }
                    }
                    {{-- When the dropdown is selected before anyother click i.e interest is to close it --}}
                    if (store.get('isload') == '0' && view.name == 'agendaDay') {
                        {{--//stop only if it has been triggered--}}
                        if (typeof spinner !== "undefined")
                            spinner.stop();
                    }
                },

                {{--
                  EventRender
                  @param event
                  @param element
                  @param view
                 --}}
                eventRender: function (event, element, view) {
                            {{--//Attach Tooltip - for description--}}
                    var status = event.status;
                    var view_name = view.name;
                    var title_date = event.start.format('Do MMM');
                    var stat  = "";
                    var type = event.type;
                    var type_text = 'Internal';
                    var course = event.course;
                    var faculty = event.faculty;

                    if( type == "1" ){
                        type_text = "External";
                    }
                    //set the view
                    store.set('view',view_name);

                    if (  view_name === "resourceDay" )
                        stat = status;
                    else
                        stat = status + " for " + event.title;

                    {{--//Show descriptive text except for month i.e the eventClick is preferred--}}
                    if( view_name !== "month" ) {
                        var my_title = course;
                        {{-- If view is weekly - add date to title of tooltip --}}
                        if( view_name === "agendaSevenDay" )
                            my_title = my_title + ' - ' + title_date + ' (' + faculty + ')';
                        else
                            my_title = my_title + ' (' + faculty + ')';

                        element.qtip({
                            content: {
                                title: my_title,
                                text: stat
                            },
                            style: 'qtip-cluetip qtip-rounded ',
                            position: {
                                target: "mouse",
                                my: 'bottom left'
                            }
                        });
                    }
                },
                {{--
                  Event Click - Bootstrap Modal showing details
                  @param event
                  @param element
                  @param view
                 --}}
                eventClick: function (event, element, view) {

                    var ev_date = event.start.format('MMMM Do YYYY');
                    var ev_reserve_date =  moment(event.rdate).format("Do MMM YYYY");



                    //console.log(ev_reserve_date);
                    var type = event.type;
                    var type_text = 'Internal';

                    var course = event.course;
                    var faculty = event.faculty;

                    if( type == "1" ){
                        type_text = "External";
                    }
                    var start_time = "?";
                    if ( event.start !== null )
                        start_time = event.start.format('HH:mm a');

                    var end_time = "?";
                    if( event.end !== null )
                        end_time = event.end.format('HH:mm a');

                    var color = event.color;
                    var status = "";
                    if (event.status === "Pending Approval") {
                        status = "<span style='color:orangered;'>" + event.status + "</span>";
                    } else {
                        status = event.status;
                    }

                    var reserved_by_who = "";

                    @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )

                        reserved_by_who = "<li>Booked By : "+ event.mby_f + " " + event.mby_l +"</li>";

                            @endif


                    var msg = "<ul><li>Course : " + course + " </li><li>Lecturer(s) : " + faculty + " </li><li> Reserve Date : "+ev_reserve_date+"</li><li>Date : " + ev_date + " </li>" + "<li> Duration :" + start_time + " to " + end_time + " <li> Status :" + status + "</li><li>Type : "+type_text+reserved_by_who+"</ul>";

                    var button_arr = [{

                        label: 'Close',
                        icon: 'fa fa-times',
                        cssClass: 'btn-primary',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }

                    }];

                    @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )
                        button_arr = [
                        {
                            label: '<span class="ladda-label"> Edit</span>',
                            icon: 'fa fa-pencil',
                            cssClass: 'btn-success ladda-button',
                            id : 'edit_reserve_btn',
                            action: function (dialogRef) {
                                dialogRef.close();
                                var edit_url_link = edit_url + '/' + event.id+'/edit';
                                var editReservationLink = "<?php echo URL::to('reservation/room/update') ?>"+'/'+event.id;

                                BootstrapDialog.show({
                                    title: 'Edit Reservation',
                                    cssClass: "edit-reserve-dialog",
                                    message: function(dialog_edit_rs) {
                                        var $message = $('<div></div>');
                                        var pageToLoadEd = dialog_edit_rs.getData('pageToLoadEd');
                                        $message.load(pageToLoadEd);
                                        return $message;
                                    },
                                    data: {
                                        'pageToLoadEd': edit_url_link
                                    },
                                    buttons: [{
                                        label: '<span class="ladda-label"> Save Changes </span>',
                                        cssClass: 'btn-primary ladda-button',
                                        icon : 'fa fa-save',
                                        id : 'edt_save_reserve_btn',
                                        action: function(dialog_edt_save) {
                                            var l_edit_btn = Ladda.create( document.querySelector('#edt_save_reserve_btn') );
                                            var r_name =  $('#name').val();
                                            var r_start = $('#start').val();
                                            var r_end =   $('#end').val();
                                            var r_desc = $('#description').val();
                                            var r_dept = $('#department').val();
                                            var r_type = 'NONE';
                                            var r_room = $('#room').val();
                                            var r_repeat_end = '';
                                            var r_user = $('#user').val();

                                            var selected_days = [];

                                            var r_area = $('#area').val();
                                            var rep_times_val = '';

                                            //remember it is a checkbox..
                                            var all_day = '0';
                                            if( $('#allday').is(':checked') ){
                                                all_day = $('#allday').val();
                                            }

                                            var ext = '0';
                                            if($('#external').is(':checked')){
                                                ext = $('#external').val();
                                            }

                                            var repeat = '0';
                                            if( $('#repeat').is(':checked') ){
                                                repeat = $('#repeat').val();
                                            }

                                            $.ajax({
                                                url:editReservationLink,
                                                beforeSend : function(x,settings){
                                                    l_edit_btn.start();
                                                },
                                                data:{user:r_user,rep_day:selected_days,rep_times_input:rep_times_val,area:r_area,repeat:repeat,rr_type:ext,allday:all_day,name:r_name,start:r_start,end:r_end,description:r_desc,department:r_dept,rtype:r_type,room:r_room,repeat_end:r_repeat_end},
                                                type : 'POST',
                                                success:function(data_add_new){
                                                    var json_object = JSON.parse( data_add_new );
                                                    var msg_len = Object.keys(json_object).length;
                                                    var msg_type = json_object['type'];


                                                    if(msg_type === "error" && msg_len > 1){
                                                        //append to the div
                                                        var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                        //remove the type - its the last element
                                                        var msg_len_correct = msg_len - 1;
                                                        for(var jd=0;jd<msg_len_correct;jd++){
                                                            error_div += '<li>'+json_object[jd]+'</li>';
                                                        }
                                                        error_div += "</ul></div>";
                                                        $('#error_block').html(error_div);
                                                    }

                                                    if( msg_type === "notice" ) {
                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS,
                                                            title: 'Success',
                                                            message: 'Reservation Edited Successfully.',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function (dialog_success_edt) {
                                                                    dialog_success_edt.close();
                                                                    dialog_edt_save.close();
                                                                }
                                                            }]

                                                        });
                                                        {{--- Add the event ---}}
                                                        $('#calendar').fullCalendar('refetchEvents');
                                                    }
                                                    l_edit_btn.stop();
                                                },
                                                error:function(error){
                                                    console.log('error->'+error);
                                                }
                                            });


                                        }
                                    }, {
                                        label: 'Cancel',
                                        cssClass: 'btn-danger',
                                        icon : 'fa fa-crosshairs',
                                        action: function(dialog_ed) {
                                            dialog_ed.close();
                                        }
                                    }]
                                });


                            }
                        },
                        {
                            label: 'Delete',
                            id: 'outer_button',
                            icon: 'fa fa-trash',
                            cssClass: 'btn-danger',
                            action: function (dialogRef) {
                                var l_outer = Ladda.create( document.querySelector('#outer_button') );
                                l_outer.start();
                                {{-- Tooltip with Yes or No --}}
                                BootstrapDialog.show({
                                    type: BootstrapDialog.TYPE_INFO,
                                    title: 'Confirm Delete',
                                    message: 'Are you sure?',
                                    closable: false,
                                    cssClass: 'small-dialog ',
                                    buttons: [{
                                        id : 'delete-btn',
                                        icon: 'fa fa-trash',
                                        label: '<span class="ladda-label"> Yes </span>',
                                        cssClass: 'btn-danger ladda-button',
                                        action: function(dialog) {
                                            var new_l = Ladda.create( document.querySelector('#delete-btn') );
                                            {{-- In Yes block run ajax to delete the event --}}
                                             $.ajax({
                                                url : 'calendar/deleteReserve',
                                                data : {ud:event.id},
                                                type: 'post',
                                                beforeSend : function(x,settings){
                                                    new_l.start();
                                                },
                                                success : function (data){
                                                    if( data === '1'){

                                                        $('#calendar').fullCalendar('removeEvents', event.id);

                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS,
                                                            title: 'Success',
                                                            message: 'Reservation Deleted Successfully.',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function(dialog_delete) {
                                                                    dialog_delete.close();
                                                                }
                                                            }]

                                                        });


                                                    }else{
                                                        {{--Error --}}
                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_DANGER,
                                                            title: 'Error',
                                                            message: 'Unable to delete Reservation, Integrity constraints enforced ',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function(dialog_delete) {
                                                                    dialog_delete.close();
                                                                }
                                                            }]
                                                        });

                                                    }
                                                    {{--stop the spinners--}}
                                                    new_l.stop();
                                                    l_outer.stop();
                                                    {{--close the dialogs--}}
                                                    dialogRef.close();
                                                }
                                            });
                                            dialog.close();
                                        }
                                    },
                                        {
                                            icon: 'fa fa-times',
                                            label: ' No',
                                            cssClass: 'btn-primary',
                                            action: function(dialog) {
                                                dialog.close();
                                                dialogRef.close();
                                            }
                                        }
                                    ]

                                });



                                {{-- : when all done refetch resources --}}

                                ///dialogRef.close();
                            }
                        },
                        {
                            label: 'Close',
                            icon: 'fa fa-times',
                            cssClass: 'btn-primary',
                            action: function (dialogRef) {
                                dialogRef.close();
                            }
                        }
                    ];
                            @endif
                    var diag = new BootstrapDialog({
                            size: BootstrapDialog.SIZE_SMALL,
                            title: event.title,
                            message: msg,
                            closable: true,
                            cssClass : 'reserve-modal-onClickEvent',
                            autospin: true,
                            buttons: button_arr
                        });
                    diag.realize();
                    diag.getModalHeader().css('background-color', color);
                    diag.open();
                },
                dayClick : function(date,jsEvent,view,resource){
                    {{-- GO TO Resource View for clicked date --}}
                    if( view.name === 'month' ) {
                        $('#calendar').fullCalendar('changeView', 'agendaDay');
                        $('#calendar').fullCalendar('gotoDate', date);
                    }
                },
            });
            {{-- revamp timesheet calendar --}}
            $('#timesheet_calendar').fullCalendar({
                schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',//Open source moments
                allDaySlot: false, // true, false
                minTime: "{{Settings::getVal('calendar_start')}}", {{--//limits changed to use moment.js--}}
                maxTime: "{{Settings::getVal('calendar_end')}}", {{--//limits changed to use moment.js--}}
                slotEventOverlap: true,
                eventOverlap: false,
                {{-- Commented to allow DayClick Event on month selectOverlap: false, --}}
                eventLimit: true, {{--// allow "more" link when too many events--}}
                theme : false,
                icons : {
                    prev: 'left-single-arrow',
                    next: 'right-single-arrow',
                    prevYear: 'left-double-arrow',
                    nextYear: 'right-double-arrow'
                },
                customButtons: {
                    // goto: {
                    //     // text: 'Go to date',
                    //     // click: function() {
                    //     //     $('.fc-goto-button').datetimepicker('show');
                    //     // }
                    // }
                },
                header: {
                    left: 'prevYear,prev,next,nextYear today,goto',
                    center: 'title',
                    right: 'agendaDay,agendaSevenDay,month'
                },
                views: {
                    agendaSevenDay: {
                        type: 'agenda',
                        duration: {days: 7},
                        {{--// views that are more than a day will NOT do this behavior by default
                        // so, we need to explicitly enable it--}}
                        groupByResource: true
                    },
                    agendaDay: {
                        titleFormat: 'ddd, Do MMM YYYY'
                    }
                },
                buttonText: {
                    today: 'Today',
                    month: 'Month',
                    agendaSevenDay: 'Week',
                    day: 'Day'
                },
                displayEventTime: false, {{--//Avoid showing the event time on top of the event.--}}
                prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
                next: "<span class='fc-text-arrow'>&rsaquo;</span>",
                prevYear: "<span class='fc-text-arrow'>&laquo;</span>",
                nextYear: "<span class='fc-text-arrow'>&raquo;</span>",
                timezone: 'local',
                axisFormat: 'HH:mm', {{--//Axis Time-format - 24 hours is 2px less hence more 'economical'--}}
                defaultView: 'agendaDay',
                slotMinutes: "00:30:00",
                selectable: true,
                resources:function (callback){
                    var selected_area = $('#filter').val();
                    $.ajax({
                        url : 'calendar/getFacultyResources?area='+selected_area,
                        cache : false,
                        success:function (data_area){
                            var list_of_resources = [];
                            var inner_rooms_div = "<p style='font-size: 14px;'  class='faa-parent animated-hover'> <i class='fa fa-university text-red faa-burst'></i> <span > Rooms</span> </p><div id='inner-rooms'>";
                            for(var b = 0; b < data_area.length;b++){
                                 var item = data_area[b];
                                 list_of_resources.push(item);
                                inner_rooms_div += "<div class='checkbox checkbox-primary'><input name='rooms_chk' id='checkbox"+b+"' rtitle='"+item.title+"' value='"+item.id+"' class='rooms_filter styled' type='checkbox' checked=''><label for='checkbox"+b+"'> "+item.title+"</label></div>";
                            }
                            {{-- Update the rooms --}}
                            inner_rooms_div += "</div>";
                            updateRoomsFilter(inner_rooms_div);

                            {{-- Pass them to calendar--}}
                             callback(list_of_resources);
                        },
                        type:'GET',
                        dataType:'json'
                    });
                },{{--//updated to a json feed to allow auto refetch on refetchResources--}}
                lazyFetching: true, {{--//fetch only what you see--}}
                {{--Use eventSources for faster loading.. :D--}}
                events: {
                        url: 'calendar/getTimesheets',
                        type: 'GET',
                        data: function (){
                            return {
                                room_area: $('#filter').val()+"_"+store.get('selected_rooms')
                            };
                        }
                    }
                ,
                {{--
                 Loading Div
                  - Give feednack to the user when loaading resources
                  - Fired for events and resource change
                 --}}
                loading: function (isloading, view) {
                    {{--//load except the first instance--}}
                    if (store.get('isload') == '0' && view.name != 'agendaDay') {
                        if (isloading) {
                            spinner = new Spinner(opts).spin(target);
                        } else {
                            {{--//stop only if it is triggered--}}
                            if (typeof spinner !== "undefined")
                                spinner.stop();
                        }
                    }
                    {{-- @TODO Resolve ->NB: The loader will be displayed if the individual does not click anywhere on the page ikimaliza since the variable is changed onClick --}}
                    if (store.get('isload') == '1' && view.name == 'agendaDay') {
                        {{-- //@Hack - Prevent it from loading on the first page load view to avoid loading both loaders --}}
                        if (isloading) {
                            spinner = new Spinner(opts).spin(target);
                        } else {
                            {{--//stop only if it is triggered--}}
                            if (typeof spinner !== "undefined")
                                spinner.stop();
                        }
                    }
                    {{-- When the dropdown is selected before anyother click i.e interest is to close it --}}
                    if (store.get('isload') == '0' && view.name == 'agendaDay') {
                        {{--//stop only if it has been triggered--}}
                        if (typeof spinner !== "undefined")
                                spinner.stop();
                    }
                },

                {{--
                  EventRender
                  @param event
                  @param element
                  @param view
                 --}}
                eventRender: function (event, element, view) {
                    {{--//Attach Tooltip - for description--}}
                    var status = event.status;
                    var view_name = view.name;
                    var title_date = event.start.format('Do MMM');
                    var stat  = "";
                    var type = event.type;
                    var type_text = 'Internal';

                    if( type == "1" ){
                        type_text = "External";
                    }
                    //set the view
                    store.set('view',view_name);

                    if (  view_name === "resourceDay" )
                        stat = status;
                    else
                        stat = status + " for " + event.title;

                    {{--//Show descriptive text except for month i.e the eventClick is preferred--}}
                    if( view_name !== "month" ) {
                        var my_title = event.room;
                        {{-- If view is weekly - add date to title of tooltip --}}
                        if( view_name === "agendaSevenDay" )
                            my_title = my_title + ' - ' + title_date + ' (' + type_text + ')';
                        else
                            my_title = my_title + ' (' + type_text + ')';

                                    element.qtip({
                            content: {
                                title: my_title,
                                text: stat
                            },
                            style: 'qtip-cluetip qtip-rounded ',
                            position: {
                                target: "mouse",
                                my: 'bottom left'
                            }
                        });
                    }
                },
                {{--
                  Event Click - Bootstrap Modal showing details
                  @param event
                  @param element
                  @param view
                 --}}
                 eventClick: function (event, element, view) {

                     var ev_date = event.start.format('MMMM Do YYYY');
                     var ev_reserve_date =  moment(event.rdate).format("Do MMM YYYY");
                     var room_reservation_id_ = event.id;
                     var ev_segments = event.segments;

                     if ( ev_segments == null ){

                         var segment_action =  {
                             label: 'Add Segment(s)',
                             icon: 'fa fa-plus-circle',
                             cssClass: 'btn-normal',
                             action: function (dialogRef) {
                                 dialogRef.close();
                                 //open edit dialog
                                 add_segment_url = add_segment +'/'+ room_reservation_id_;
                                 BootstrapDialog.show({
                                     cssClass : 'add_new_segment',
                                     title: 'Add Segments',
                                     closeByBackdrop: false,
                                     closeByKeyboard: false,
                                     id : 'add_new_segment_diag',
                                     message: function(dialog_new) {
                                         var $message = $('<div></div>');
                                         var pageToLoad = dialog_new.getData('pageToLoad');
                                         $message.load(pageToLoad);
                                         return $message;
                                     },
                                     data: {
                                         'pageToLoad': add_segment_url
                                     },
                                     buttons: [{
                                         label: '<span class="ladda-label"> Save </span>',
                                         cssClass: 'btn-primary ladda-button',
                                         icon : 'fa fa-save',
                                         id : 'save_reserve_btn',
                                         action: function(dialog_new) {
                                             var new_l_save = Ladda.create( document.querySelector('#save_reserve_btn') );

                                             //arrays
                                             var a_start = $('input[name="start[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_end = $('input[name="end[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_desc = $('textarea[name="desc[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_courses = $('select[name="courses[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();

                                             var a_ccategory = $('select[name="ccategory[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();


                                             var a_status =  $('select[name="status[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_faculty = $('select[name="faculty[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();

                                             var a_first_faculty = $('input[name="first_faculty[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_second_faculty = $('input[name="second_faculty[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();

                                             //single values
                                             var created_by = $('input[name="user_id"]').val();
                                             var rr_id = $('input[name="rr"]').val();
                                             var rr_name = $('input[name="rr_name"]').val();
                                             var owner = $('input[name="owner"]').val();
                                             var ref_str = $('input[name="ref"]').val();
                                             var a_t_al = '0';

                                             if( $("#a_t_all").is(':checked') == true ){
                                                 a_t_al = "1";
                                             }

                                             $.ajax({
                                                 url:save_segment,
                                                 beforeSend : function(x,settings){
                                                     new_l_save.start();
                                                 },
                                                 type : 'POST',
                                                 data: {applytoall:a_t_al,start:a_start,end:a_end,desc:a_desc,courses:a_courses,ccategory: a_ccategory,status:a_status,faculty:a_faculty,first_faculty:a_first_faculty,second_faculty:a_second_faculty,user_id:created_by,rr:rr_id,rr_name:rr_name,owner:owner,ref:ref_str},
                                                 success:function(data_add_new){
                                                     var json_object = JSON.parse( data_add_new );
                                                     var msg_len = Object.keys(json_object).length;
                                                     var msg_type = json_object['type'];


                                                     if(msg_type === "error" && msg_len > 1){
                                                         //append to the div
                                                         var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                         //remove the type - its the last element
                                                         var msg_len_correct = msg_len - 1;
                                                         for(var jd=0;jd<msg_len_correct;jd++){
                                                             error_div += '<li>'+json_object[jd]+'</li>';
                                                         }
                                                         error_div += "</ul></div>";
                                                         $('#error_block').html(error_div);
                                                     }

                                                     if( msg_type === "notice" ) {
                                                         BootstrapDialog.show({
                                                             type: BootstrapDialog.TYPE_SUCCESS,
                                                             title: 'Success',
                                                             message: 'Segment(s) Added Successfully.',
                                                             closable: true,
                                                             cssClass: 'small-dialog',
                                                             buttons: [{
                                                                 icon: 'fa fa-cog fa-spin',
                                                                 label: ' Ok',
                                                                 action: function (dialog_success) {
                                                                     {{--- Add the event ---}}
                                                                     $('#timesheet_calendar').fullCalendar( 'refetchResources' );
                                                                     $('#timesheet_calendar').fullCalendar('refetchEvents');
                                                                     //$('#calendar').fullCalendar('refetchEvents');
                                                                     dialog_success.close();
                                                                     dialog_new.close();
                                                                 }
                                                             }]

                                                         });



                                                     }

                                                     new_l_save.stop();



                                                 },
                                                 error:function(error){
                                                     console.log('error->'+error);
                                                 }
                                             });



                                         }
                                     }, {
                                         label: 'Cancel',
                                         cssClass: 'btn-danger',
                                         icon : 'fa fa-crosshairs',
                                         action: function(dialog_new) {
                                             dialog_new.close();
                                         }
                                     }]
                                 });


                             }
                         };


                     }else{

                         var segment_action =  {
                             label: 'Edit Segment(s)',
                             icon: 'fa fa-plus-circle',
                             cssClass: 'btn-normal',
                             action: function (dialogRef) {
                                 dialogRef.close();
                                 //open edit dialog
                                 edit_segment_url = edit_segment +'/'+ room_reservation_id_+'/edit';
                                 BootstrapDialog.show({
                                     cssClass : 'edit_new_segment',
                                     title: 'Edit Segment(s)',
                                     id : 'edit_new_segment_diag',
                                     message: function(dialog_new) {
                                         var $message = $('<div></div>');
                                         var pageToLoad = dialog_new.getData('pageToLoad');
                                         $message.load(pageToLoad);
                                         return $message;
                                     },
                                     data: {
                                         'pageToLoad': edit_segment_url
                                     },
                                     buttons: [{
                                         label: '<span class="ladda-label"> Save </span>',
                                         cssClass: 'btn-primary ladda-button',
                                         icon : 'fa fa-save',
                                         id : 'edit_reserve_btn',
                                         action: function(dialog_new) {
                                             var new_l_save = Ladda.create( document.querySelector('#edit_reserve_btn') );

                                             //arrays
                                             var a_start = $('input[name="start[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_end = $('input[name="end[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_desc = $('textarea[name="desc[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_courses = $('select[name="courses[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();

                                             var a_ccategory = $('select[name="ccategory[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();


                                             var a_status =  $('select[name="status[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_faculty = $('select[name="faculty[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();

                                             var a_first_faculty = $('input[name="first_faculty[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();
                                             var a_second_faculty = $('input[name="second_faculty[]"]').map(function() {
                                                 return $(this).val() ? $(this).val() : null;
                                             }).get();

                                             //single values
                                             var created_by = $('input[name="user_id"]').val();
                                             var rr_id = $('input[name="rr"]').val();
                                             var rr_name = $('input[name="rr_name"]').val();
                                             var owner = $('input[name="owner"]').val();
                                             var ref_str = $('input[name="ref"]').val();
                                             var a_t_al = '0';
                                             if( $("#a_t_all").is(':checked') == true ){
                                                 a_t_al = "1";
                                             }

                                             $.ajax({
                                                 url:'reservation/details/update',
                                                 beforeSend : function(x,settings){
                                                     new_l_save.start();
                                                 },
                                                 type : 'POST',
                                                 data: {applytoall:a_t_al,start:a_start,end:a_end,desc:a_desc,courses:a_courses,ccategory: a_ccategory,status:a_status,faculty:a_faculty,first_faculty:a_first_faculty,second_faculty:a_second_faculty,user_id:created_by,rr:rr_id,rr_name:rr_name,owner:owner,ref:ref_str},
                                                 success:function(data_add_new){
                                                     var json_object = JSON.parse( data_add_new );
                                                     var msg_len = Object.keys(json_object).length;
                                                     var msg_type = json_object['type'];


                                                     if(msg_type === "error" && msg_len > 1){
                                                         //append to the div
                                                         var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                         //remove the type - its the last element
                                                         var msg_len_correct = msg_len - 1;
                                                         for(var jd=0;jd<msg_len_correct;jd++){
                                                             error_div += '<li>'+json_object[jd]+'</li>';
                                                         }
                                                         error_div += "</ul></div>";
                                                         $('#error_block').html(error_div);
                                                     }

                                                     if( msg_type === "notice" ) {
                                                         BootstrapDialog.show({
                                                             type: BootstrapDialog.TYPE_SUCCESS,
                                                             title: 'Success',
                                                             message: 'Segment(s) Edited Successfully.',
                                                             closable: true,
                                                             cssClass: 'small-dialog',
                                                             buttons: [{
                                                                 icon: 'fa fa-cog fa-spin',
                                                                 label: ' Ok',
                                                                 action: function (dialog_success) {
                                                                     {{--- Add the event ---}}
                                                                     $('#timesheet_calendar').fullCalendar( 'refetchResources' );
                                                                     $('#timesheet_calendar').fullCalendar('refetchEvents');
                                                                     //$('#calendar').fullCalendar('refetchEvents');
                                                                     dialog_success.close();
                                                                     dialog_new.close();
                                                                 }
                                                             }]

                                                         });



                                                     }

                                                     new_l_save.stop();



                                                 },
                                                 error:function(error){
                                                     console.log('error->'+error);
                                                 }
                                             });

                                         }
                                     }, {
                                         label: 'Cancel',
                                         cssClass: 'btn-danger',
                                         icon : 'fa fa-crosshairs',
                                         action: function(dialog_new) {
                                             dialog_new.close();
                                         }
                                     }]
                                 });


                             }
                         };

                     }

                     //console.log(ev_reserve_date);
                     var type = event.type;
                     var type_text = 'Internal';

                     if( type == "1" ){
                         type_text = "External";
                     }
                     var start_time = "?";
                     if ( event.start !== null )
                         start_time = event.start.format('HH:mm a');

                     var end_time = "?";
                     if( event.end !== null )
                         end_time = event.end.format('HH:mm a');

                     var color = event.color;
                     var status = "";
                     if (event.status === "Pending Approval") {
                         status = "<span style='color:orangered;'>" + event.status + "</span>";
                     } else {
                         status = event.status;
                     }

                     var reserved_by_who = "";

                     @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )

                             reserved_by_who = "<li>Booked By : "+ event.mby_f + " " + event.mby_l +"</li>";

                     @endif


                     var msg = "<ul><li> Reserve Date : "+ev_reserve_date+"</li><li>Date : " + ev_date + " </li>" + "<li> Duration :" + start_time + " to " + end_time + " <li> Status :" + status + "</li><li>Type : "+type_text+reserved_by_who+"</ul>";

                     var button_arr = [{

                         label: 'Close',
                         icon: 'fa fa-times',
                         cssClass: 'btn-primary',
                         action: function (dialogRef) {
                             dialogRef.close();
                         }

                     }];

                     @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )

                         button_arr = [];


                         var delete_r_reservation = {
                             label: 'Delete',
                             id: 'outer_button',
                             icon: 'fa fa-trash',
                             cssClass: 'btn-danger',
                             action: function (dialogRef) {
                                 var l_outer = Ladda.create( document.querySelector('#outer_button') );
                                 l_outer.start();
                                 {{-- Tooltip with Yes or No --}}
                                 BootstrapDialog.show({
                                     type: BootstrapDialog.TYPE_INFO,
                                     title: 'Confirm Delete',
                                     message: 'Are you sure?',
                                     closable: false,
                                     cssClass: 'small-dialog ',
                                     buttons: [{
                                         id : 'delete-btn',
                                         icon: 'fa fa-trash',
                                         label: '<span class="ladda-label"> Yes </span>',
                                         cssClass: 'btn-danger ladda-button',
                                         action: function(dialog) {
                                             var new_l = Ladda.create( document.querySelector('#delete-btn') );
                                             {{-- In Yes block run ajax to delete the event --}}
                                              $.ajax({
                                                url : 'reservation/details/delete',
                                                data : {rr_id:event.segments},
                                                 type: 'post',
                                                 beforeSend : function(x,settings){
                                                     new_l.start();
                                                 },
                                                 success : function (data){
                                                     if( data === '1'){

                                                         $('#timesheet_calendar').fullCalendar('removeEvents', event.id);

                                                         BootstrapDialog.show({
                                                             type: BootstrapDialog.TYPE_SUCCESS,
                                                             title: 'Success',
                                                             closable: true,
                                                             message: 'Reservation Deleted Successfully.',
                                                             cssClass: 'small-dialog',
                                                             buttons: [{
                                                                 icon: 'fa fa-cog fa-spin',
                                                                 label: ' Ok',
                                                                 action: function(dialog_delete) {
                                                                     dialog_delete.close();
                                                                 }
                                                             }]

                                                         });


                                                     }else{
                                                         {{--Error --}}
                                                         BootstrapDialog.show({
                                                             type: BootstrapDialog.TYPE_DANGER,
                                                             title: 'Error',
                                                             message: 'Unable to delete Reservation, Integrity constraints enforced ',
                                                             closable: true,
                                                             cssClass: 'small-dialog',
                                                             buttons: [{
                                                                 icon: 'fa fa-cog fa-spin',
                                                                 label: ' Ok',
                                                                 action: function(dialog_delete) {
                                                                     dialog_delete.close();
                                                                 }
                                                             }]
                                                         });

                                                     }
                                                     {{--stop the spinners--}}
                                                     new_l.stop();
                                                     l_outer.stop();
                                                     {{--close the dialogs--}}
                                                     dialogRef.close();
                                                 }
                                             });
                                             dialog.close();
                                         }
                                     },
                                         {
                                             icon: 'fa fa-times',
                                             label: ' No',
                                             cssClass: 'btn-primary',
                                             action: function(dialog) {
                                                 dialog.close();
                                                 dialogRef.close();
                                             }
                                         }
                                     ]

                                 });



                                 {{-- : when all done refetch resources --}}

                                 ///dialogRef.close();
                             }
                         };
                         var close_r_reservation = {
                             label: 'Close',
                             icon: 'fa fa-times',
                             cssClass: 'btn-primary',
                             action: function (dialogRef) {
                                 dialogRef.close();
                             }
                         };

                         button_arr.push(segment_action);
                         button_arr.push(delete_r_reservation);
                         button_arr.push(close_r_reservation);


                     @endif
                     var diag = new BootstrapDialog({
                         size: BootstrapDialog.SIZE_SMALL,
                         title: event.title,
                         message: msg,
                         closable: true,
                         cssClass : 'reserve-modal-onClickEvent',
                         autospin: true,
                         buttons: button_arr
                     });
                     diag.realize();
                     diag.getModalHeader().css('background-color', color);
                     diag.open();
                 },
                dayClick : function(date,jsEvent,view,resource){
                    {{-- GO TO Resource View for clicked date --}}
                    if( view.name === 'month' ) {
                        $('#calendar').fullCalendar('changeView', 'agendaDay');
                        $('#calendar').fullCalendar('gotoDate', date);
                    }
                },
                {{-- Select function - Trigger create new reservation / Alert error on past date --}}
                select: function (start, end, event, view, resource) {
                    var check = start.format('YYYY-MM-DD HH');
                    var today = moment().format('YYYY-MM-DD HH');

                    var clean_start = start.format('YYYY-MM-DD|HH:mm');
                    var clean_end = end.format('YYYY-MM-DD|HH:mm');

                    var resource_id, addReservationLink;
                    //get the current view
                    var pview_current = view.name;

                    var start_date = start.format('YYYY-MM-DD');
                    var is_holiday = false;

                    $.each(holidays,function(key,value){
                        if ( start_date == value )
                            is_holiday = true;
                    });
                    {{--No holidays--}}
                    if ( is_holiday === false ){

                        {{-- No Past Dates/Times --}}
                        if (event && ( pview_current === "agendaDay" || pview_current === "agendaSevenDay")) {

                            if (check < today) {
                                var message = '<b style="text-align: center;">Reservations forbidden for past date and/or time.</b> <br/>';

                                BootstrapDialog.show({
                                    title: 'Error!',
                                    message: message,
                                    closable: true,
                                    type: BootstrapDialog.TYPE_DANGER,
                                    cssClass: "small-dialog"
                                });

                            } else {
                                resource_id = resource.id;
                                addReservationLink = newReservationLink + '/' + resource_id + '/' + clean_start + '/' + clean_end;

                                var diag = new BootstrapDialog({
                                    type: BootstrapDialog.TYPE_INFO,
                                    title: "Confirm Action : New Reservation?",
                                    closable: true,
                                    cssClass: "add-reserv-dialog",
                                    buttons: [{
                                        label: 'Yes',
                                        cssClass: 'btn btn-primary',
                                        icon: 'fa fa-check',
                                        action: function(dialog) {
                                            @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )
                                                dialog.close();

                                            BootstrapDialog.show({
                                                title: 'Add New Reservation',
                                                message: function(dialog_new) {
                                                    var $message = $('<div></div>');
                                                    var pageToLoad = dialog_new.getData('pageToLoad');
                                                    $message.load(pageToLoad);
                                                    return $message;
                                                },
                                                data: {
                                                    'pageToLoad': addReservationLink
                                                },
                                                buttons: [{
                                                    label: '<span class="ladda-label"> Save </span>',
                                                    cssClass: 'btn-primary ladda-button',
                                                    icon : 'fa fa-save',
                                                    id : 'save_reserve_btn',
                                                    action: function(dialog_new) {
                                                        var new_l_save = Ladda.create( document.querySelector('#save_reserve_btn') );

                                                        var r_name =  $('#name').val();
                                                        var r_start = $('#start').val();
                                                        var r_end =   $('#end').val();
                                                        var r_desc = $('#description').val();
                                                        var r_dept = $('#department').val();
                                                        var r_type = $('#rtype').val();
                                                        var r_room = $('#room').val();
                                                        var r_repeat_end = $('#repeat_end').val();
                                                        var r_for = $('#user').val();

                                                        var selected_days = [];
                                                        $.each($("input[name^=rep_day]:checked"), function() {
                                                            selected_days.push($(this).val());
                                                            // or you can do something to the actual checked checkboxes by working directly with  'this'
                                                            // something like $(this).hide() (only something useful, probably) :P
                                                        });

                                                        var r_area = $('#area').val();
                                                        var rep_times_val = $('#rep_times_input').val();

                                                        //remember it is a checkbox..
                                                        var all_day = '0';
                                                        if( $('#allday').is(':checked') ){
                                                            all_day = $('#allday').val();
                                                        }

                                                        var ext = '0';
                                                        if($('#external').is(':checked')){
                                                            ext = $('#external').val();
                                                        }

                                                        var repeat = '0';
                                                        if( $('#repeat').is(':checked') ){
                                                            repeat = $('#repeat').val();
                                                        }

                                                        $.ajax({
                                                            url:saveReservationLink,
                                                            beforeSend : function(x,settings){

                                                                new_l_save.start();
                                                            },
                                                            data:{user:r_for,rep_day:selected_days,rep_times_input:rep_times_val,area:r_area,repeat:repeat,rr_type:ext,allday:all_day,name:r_name,start:r_start,end:r_end,description:r_desc,department:r_dept,rtype:r_type,room:r_room,repeat_end:r_repeat_end},
                                                            type : 'POST',
                                                            success:function(data_add_new){
                                                                var json_object = JSON.parse( data_add_new );
                                                                var msg_len = Object.keys(json_object).length;
                                                                var msg_type = json_object['type'];


                                                                if(msg_type === "error" && msg_len > 1){
                                                                    //append to the div
                                                                    var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                                    //remove the type - its the last element
                                                                    var msg_len_correct = msg_len - 1;
                                                                    for(var jd=0;jd<msg_len_correct;jd++){
                                                                        error_div += '<li>'+json_object[jd]+'</li>';
                                                                    }
                                                                    error_div += "</ul></div>";
                                                                    $('#error_block').html(error_div);
                                                                }

                                                                if( msg_type === "notice" ) {
                                                                    BootstrapDialog.show({
                                                                        type: BootstrapDialog.TYPE_SUCCESS,
                                                                        title: 'Success',
                                                                        message: 'Reservation Added Successfully.',
                                                                        closable: true,
                                                                        cssClass: 'small-dialog',
                                                                        buttons: [{
                                                                            icon: 'fa fa-cog fa-spin',
                                                                            label: ' Ok',
                                                                            action: function (dialog_success) {
                                                                                {{--- Add the event ---}}
                                                                                $('#calendar').fullCalendar('refetchEvents');
                                                                                dialog_success.close();
                                                                                dialog_new.close();
                                                                            }
                                                                        }]

                                                                    });



                                                                }

                                                                new_l_save.stop();



                                                            },
                                                            error:function(error){
                                                                console.log('error->'+error);
                                                            }
                                                        });


                                                    }
                                                }, {
                                                    label: 'Cancel',
                                                    cssClass: 'btn-danger',
                                                    icon : 'fa fa-crosshairs',
                                                    action: function(dialog_new) {
                                                        dialog.close();
                                                        dialog_new.close();
                                                    }
                                                }]
                                            });
                                            @else
                                                window.location = loginLink;
                                            @endif
                                        }
                                    }, {
                                        label: 'No',
                                        cssClass: 'btn btn-danger',
                                        icon: 'fa fa-close',
                                        action: function(dialog) {
                                            dialog.close();
                                        }
                                    }]
                                });

                                diag.realize();
                                diag.getModalBody().hide();
                                diag.getModalBody().remove();
                                diag.open();
                            }
                        }

                    }else{
                        var message = '<b style="text-align: center;">Reservations forbidden for non-working days.</b> <br/>';

                        BootstrapDialog.show({
                            title: 'Error!',
                            message: message,
                            closable: true,
                            type: BootstrapDialog.TYPE_DANGER,
                            cssClass: "small-dialog"
                        });
                    }
                }
            });
            {{-- Timesheet calendar --}}
            $('#timesheet1_calendar').fullCalendar({
                schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',//Open source moments
                allDaySlot: false, // true, false
                minTime: "{{Settings::getVal('calendar_start')}}", {{--//limits changed to use moment.js--}}
                maxTime: "{{Settings::getVal('calendar_end')}}", {{--//limits changed to use moment.js--}}
                slotEventOverlap: true,
                eventOverlap: false,
                {{-- Commented to allow DayClick Event on month selectOverlap: false, --}}
                eventLimit: true, {{--// allow "more" link when too many events
                --}}
                theme : false,
                icons : {
                    prev: 'left-single-arrow',
                    next: 'right-single-arrow',
                    prevYear: 'left-double-arrow',
                    nextYear: 'right-double-arrow'
                },
                customButtons: {
                    // goto: {
                    //     // text: 'Go to date',
                    //     // click: function() {
                    //     //     $('.fc-goto-button').datetimepicker('show');
                    //     // }
                    // }
                },
                header: {
                    left: 'prevYear,prev,next,nextYear today,goto',
                    center: 'title',
                    right: 'agendaDay,agendaSevenDay,month'
                },
                views: {
                    agendaSevenDay: {
                        type: 'agenda',
                        duration: {days: 7},
                        {{--// views that are more than a day will NOT do this behavior by default
                        // so, we need to explicitly enable it--}}
                        groupByResource: true
                    },
                    agendaDay: {
                        titleFormat: 'ddd, Do MMM YYYY'
                    }
                },
                buttonText: {
                    today: 'Today',
                    month: 'Month',
                    agendaSevenDay: 'Week',
                    day: 'Day'
                },
                displayEventTime: false, {{--//Avoid showing the event time on top of the event.--}}
                prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
                next: "<span class='fc-text-arrow'>&rsaquo;</span>",
                prevYear: "<span class='fc-text-arrow'>&laquo;</span>",
                nextYear: "<span class='fc-text-arrow'>&raquo;</span>",
                timezone: 'local',
                axisFormat: 'HH:mm', {{--//Axis Time-format - 24 hours is 2px less hence more 'economical'--}}
                defaultView: 'agendaDay',
                slotMinutes: "00:30:00",
                selectable: true,
                resources:function (callback){
                    var selected_area = $('#filter').val();
                    $.ajax({
                        url : 'calendar/getFacultyResources?area='+selected_area,
                        cache : false,
                        success:function (data_area){
                            var list_of_resources = [];
                            var inner_rooms_div = "<p style='font-size: 14px;'  class='faa-parent animated-hover'> <i class='fa fa-university text-red faa-burst'></i> <span > Rooms</span> </p><div id='inner-rooms'>";
                            for(var b = 0; b < data_area.length;b++){
                                 var item = data_area[b];
                                 list_of_resources.push(item);
                                inner_rooms_div += "<div class='checkbox checkbox-primary'><input name='rooms_chk' id='checkbox"+b+"' rtitle='"+item.title+"' value='"+item.id+"' class='rooms_filter styled' type='checkbox' checked=''><label for='checkbox"+b+"'> "+item.title+"</label></div>";
                            }
                            {{-- Update the rooms --}}
                            inner_rooms_div += "</div>";
                            updateRoomsFilter(inner_rooms_div);

                            {{-- Pass them to calendar--}}
                             callback(list_of_resources);
                        },
                        type:'GET',
                        dataType:'json'
                    });
                },{{--//updated to a json feed to allow auto refetch on refetchResources--}}
                lazyFetching: true, {{--//fetch only what you see--}}
                {{--Use eventSources for faster loading.. :D--}}
                events: {
                        url: 'calendar/getTimesheets',
                        type: 'GET',
                        data: function (){
                            return {
                                room_area: $('#filter').val()+"_"+store.get('selected_rooms')
                            };
                        }
                    }
                ,
                {{--
                 Loading Div
                  - Give feednack to the user when loaading resources
                  - Fired for events and resource change
                 --}}
                loading: function (isloading, view) {

                  target = document.getElementById('timesheet_calendar');
                    {{--//load except the first instance--}}
                    if (store.get('isload') == '0' && view.name != 'agendaDay') {
                        if (isloading) {
                            spinner = new Spinner(opts).spin(target);
                        } else {
                            {{--//stop only if it is triggered
                              --}}
                            if (typeof spinner !== "undefined")
                                spinner.stop();
                        }
                    }
                    {{-- @TODO Resolve ->NB: The loader will be displayed if the individual does not click anywhere on the page ikimaliza since the variable is changed onClick --}}
                    if (store.get('isload') == '1' && view.name == 'agendaDay') {
                        {{-- //@Hack - Prevent it from loading on the first page load view to avoid loading both loaders --}}
                        if (isloading) {
                            spinner = new Spinner(opts).spin(target);
                        } else {
                            {{--//stop only if it is triggered
                              --}}
                            if (typeof spinner !== "undefined")
                                spinner.stop();
                        }
                    }
                    {{-- When the dropdown is selected before anyother click i.e interest is to close it --}}
                    if (store.get('isload') == '0' && view.name == 'agendaDay') {
                        {{--//stop only if it has been triggered
                          --}}
                        if (typeof spinner !== "undefined")
                                spinner.stop();
                    }
                },

                {{--
                  EventRender
                  @param event
                  @param element
                  @param view
                 --}}
                eventRender: function (event, element, view) {
                    {{--//Attach Tooltip - for description--}}
                    var status = event.status;
                    var view_name = view.name;
                    var title_date = event.start.format('Do MMM');
                    var stat  = "";
                    var type = event.type;
                    var type_text = 'Internal';

                    if( type == "1" ){
                        type_text = "External";
                    }
                    //set the view
                    store.set('view',view_name);

                    if (  view_name === "resourceDay" )
                        stat = status;
                    else
                        stat = status + " for " + event.title;

                    {{--//Show descriptive text except for month i.e the eventClick is preferred--}}
                    if( view_name !== "month" ) {
                        var my_title = event.room;
                        {{-- If view is weekly - add date to title of tooltip --}}
                        if( view_name === "agendaSevenDay" )
                            my_title = my_title + ' - ' + title_date + ' (' + type_text + ')';
                        else
                            my_title = my_title + ' (' + type_text + ')';

                                    element.qtip({
                            content: {
                                title: my_title,
                                text: stat
                            },
                            style: 'qtip-cluetip qtip-rounded ',
                            position: {
                                target: "mouse",
                                my: 'bottom left'
                            }
                        });
                    }
                },
                {{--
                  Event Click - Bootstrap Modal showing details
                  @param event
                  @param element
                  @param view
                 --}}
                eventClick: function (event, element, view) {

                    var ev_date = event.start.format('MMMM Do YYYY');
                    var ev_reserve_date =  moment(event.rdate).format("Do MMM YYYY");
                    var room_reservation_id_ = event.id;
                    var ev_segments = event.segments;

                    if ( ev_segments == null ){

                        var segment_action =  {
                            label: 'Add Segment(s)',
                            icon: 'fa fa-plus-circle',
                            cssClass: 'btn-normal',
                            action: function (dialogRef) {
                                dialogRef.close();
                                //open edit dialog
                                add_segment_url = add_segment +'/'+ room_reservation_id_;
                                BootstrapDialog.show({
                                    cssClass : 'add_new_segment',
                                    title: 'Add Segments',
                                    closeByBackdrop: false,
                                    closeByKeyboard: false,
                                    id : 'add_new_segment_diag',
                                    message: function(dialog_new) {
                                        var $message = $('<div></div>');
                                        var pageToLoad = dialog_new.getData('pageToLoad');
                                        $message.load(pageToLoad);
                                        return $message;
                                    },
                                    data: {
                                        'pageToLoad': add_segment_url
                                    },
                                    buttons: [{
                                        label: '<span class="ladda-label"> Save </span>',
                                        cssClass: 'btn-primary ladda-button',
                                        icon : 'fa fa-save',
                                        id : 'save_reserve_btn',
                                        action: function(dialog_new) {
                                            var new_l_save = Ladda.create( document.querySelector('#save_reserve_btn') );

                                            //arrays
                                            var a_start = $('input[name="start[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_end = $('input[name="end[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_desc = $('textarea[name="desc[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_courses = $('select[name="courses[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            var a_ccategory = $('select[name="ccategory[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();


                                            var a_status =  $('select[name="status[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_faculty = $('select[name="faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            var a_first_faculty = $('input[name="first_faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_second_faculty = $('input[name="second_faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            //single values
                                            var created_by = $('input[name="user_id"]').val();
                                            var rr_id = $('input[name="rr"]').val();
                                            var rr_name = $('input[name="rr_name"]').val();
                                            var owner = $('input[name="owner"]').val();
                                            var ref_str = $('input[name="ref"]').val();
                                            var a_t_al = '0';

                                            if( $("#a_t_all").is(':checked') == true ){
                                                a_t_al = "1";
                                            }

                                            $.ajax({
                                                url:save_segment,
                                                beforeSend : function(x,settings){
                                                    new_l_save.start();
                                                },
                                                type : 'POST',
                                                data: {applytoall:a_t_al,start:a_start,end:a_end,desc:a_desc,courses:a_courses,ccategory: a_ccategory,status:a_status,faculty:a_faculty,first_faculty:a_first_faculty,second_faculty:a_second_faculty,user_id:created_by,rr:rr_id,rr_name:rr_name,owner:owner,ref:ref_str},
                                                success:function(data_add_new){
                                                    var json_object = JSON.parse( data_add_new );
                                                    var msg_len = Object.keys(json_object).length;
                                                    var msg_type = json_object['type'];


                                                    if(msg_type === "error" && msg_len > 1){
                                                        //append to the div
                                                        var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                        //remove the type - its the last element
                                                        var msg_len_correct = msg_len - 1;
                                                        for(var jd=0;jd<msg_len_correct;jd++){
                                                            error_div += '<li>'+json_object[jd]+'</li>';
                                                        }
                                                        error_div += "</ul></div>";
                                                        $('#error_block').html(error_div);
                                                    }

                                                    if( msg_type === "notice" ) {
                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS,
                                                            title: 'Success',
                                                            message: 'Segment(s) Added Successfully.',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function (dialog_success) {
                                                                    {{--- Add the event ---}}
                                                                    $('#calendar').fullCalendar('refetchEvents');
                                                                    dialog_success.close();
                                                                    dialog_new.close();
                                                                }
                                                            }]

                                                        });



                                                    }

                                                    new_l_save.stop();



                                                },
                                                error:function(error){
                                                    console.log('error->'+error);
                                                }
                                            });



                                        }
                                    }, {
                                        label: 'Cancel',
                                        cssClass: 'btn-danger',
                                        icon : 'fa fa-crosshairs',
                                        action: function(dialog_new) {
                                            dialog_new.close();
                                        }
                                    }]
                                });


                            }
                        };


                    }else{

                        var segment_action =  {
                            label: 'Edit Segment(s)',
                            icon: 'fa fa-plus-circle',
                            cssClass: 'btn-normal',
                            action: function (dialogRef) {
                                dialogRef.close();
                                //open edit dialog
                                edit_segment_url = edit_segment +'/'+ room_reservation_id_+'/edit';
                                BootstrapDialog.show({
                                    cssClass : 'edit_new_segment',
                                    title: 'Edit Segment(s)',
                                    id : 'edit_new_segment_diag',
                                    message: function(dialog_new) {
                                        var $message = $('<div></div>');
                                        var pageToLoad = dialog_new.getData('pageToLoad');
                                        $message.load(pageToLoad);
                                        return $message;
                                    },
                                    data: {
                                        'pageToLoad': edit_segment_url
                                    },
                                    buttons: [{
                                        label: '<span class="ladda-label"> Save </span>',
                                        cssClass: 'btn-primary ladda-button',
                                        icon : 'fa fa-save',
                                        id : 'edit_reserve_btn',
                                        action: function(dialog_new) {
                                            var new_l_save = Ladda.create( document.querySelector('#edit_reserve_btn') );

                                            //arrays
                                            var a_start = $('input[name="start[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_end = $('input[name="end[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_desc = $('textarea[name="desc[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_courses = $('select[name="courses[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            var a_ccategory = $('select[name="ccategory[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();


                                            var a_status =  $('select[name="status[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_faculty = $('select[name="faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            var a_first_faculty = $('input[name="first_faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();
                                            var a_second_faculty = $('input[name="second_faculty[]"]').map(function() {
                                                return $(this).val() ? $(this).val() : null;
                                            }).get();

                                            //single values
                                            var created_by = $('input[name="user_id"]').val();
                                            var rr_id = $('input[name="rr"]').val();
                                            var rr_name = $('input[name="rr_name"]').val();
                                            var owner = $('input[name="owner"]').val();
                                            var ref_str = $('input[name="ref"]').val();


                                        }
                                    }, {
                                        label: 'Cancel',
                                        cssClass: 'btn-danger',
                                        icon : 'fa fa-crosshairs',
                                        action: function(dialog_new) {
                                            dialog_new.close();
                                        }
                                    }]
                                });


                            }
                        };

                    }

                    //console.log(ev_reserve_date);
                    var type = event.type;
                    var type_text = 'Internal';

                    if( type == "1" ){
                        type_text = "External";
                    }
                    var start_time = "?";
                    if ( event.start !== null )
                        start_time = event.start.format('HH:mm a');

                    var end_time = "?";
                    if( event.end !== null )
                        end_time = event.end.format('HH:mm a');

                    var color = event.color;
                    var status = "";
                    if (event.status === "Pending Approval") {
                        status = "<span style='color:orangered;'>" + event.status + "</span>";
                    } else {
                        status = event.status;
                    }

                    var reserved_by_who = "";

                    @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )

                            reserved_by_who = "<li>Booked By : "+ event.mby_f + " " + event.mby_l +"</li>";

                    @endif


                    var msg = "<ul><li> Reserve Date : "+ev_reserve_date+"</li><li>Date : " + ev_date + " </li>" + "<li> Duration :" + start_time + " to " + end_time + " <li> Status :" + status + "</li><li>Type : "+type_text+reserved_by_who+"</ul>";

                    var button_arr = [{

                        label: 'Close',
                        icon: 'fa fa-times',
                        cssClass: 'btn-primary',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }

                    }];

                    @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )

                        button_arr = [];

                        var edit_r_reservation = {
                            label: '<span class="ladda-label"> Edit</span>',
                            icon: 'fa fa-pencil',
                            cssClass: 'btn-success ladda-button',
                            id : 'edit_reserve_btn',
                            action: function (dialogRef) {
                                dialogRef.close();
                                var edit_url_link = edit_url + '/' + event.id+'/edit';
                                var editReservationLink = "<?php echo URL::to('reservation/room/update') ?>"+'/'+event.id;

                                BootstrapDialog.show({
                                    title: 'Edit Reservation',
                                    cssClass: "edit-reserve-dialog",
                                    message: function(dialog_edit_rs) {
                                        var $message = $('<div></div>');
                                        var pageToLoadEd = dialog_edit_rs.getData('pageToLoadEd');
                                        $message.load(pageToLoadEd);
                                        return $message;
                                    },
                                    data: {
                                        'pageToLoadEd': edit_url_link
                                    },
                                    buttons: [{
                                        label: '<span class="ladda-label"> Save Changes </span>',
                                        cssClass: 'btn-primary ladda-button',
                                        icon : 'fa fa-save',
                                        id : 'edt_save_reserve_btn',
                                        action: function(dialog_edt_save) {
                                            var l_edit_btn = Ladda.create( document.querySelector('#edt_save_reserve_btn') );
                                            var r_name =  $('#name').val();
                                            var r_start = $('#start').val();
                                            var r_end =   $('#end').val();
                                            var r_desc = $('#description').val();
                                            var r_dept = $('#department').val();
                                            var r_type = 'NONE';
                                            var r_room = $('#room').val();
                                            var r_repeat_end = '';
                                            var r_user = $('#user').val();

                                            var selected_days = [];

                                            var r_area = $('#area').val();
                                            var rep_times_val = '';

                                            //remember it is a checkbox..
                                            var all_day = '0';
                                            if( $('#allday').is(':checked') ){
                                                all_day = $('#allday').val();
                                            }

                                            var ext = '0';
                                            if($('#external').is(':checked')){
                                                ext = $('#external').val();
                                            }

                                            var repeat = '0';
                                            if( $('#repeat').is(':checked') ){
                                                repeat = $('#repeat').val();
                                            }

                                            $.ajax({
                                                url:editReservationLink,
                                                beforeSend : function(x,settings){
                                                    l_edit_btn.start();
                                                },
                                                data:{user:r_user,rep_day:selected_days,rep_times_input:rep_times_val,area:r_area,repeat:repeat,rr_type:ext,allday:all_day,name:r_name,start:r_start,end:r_end,description:r_desc,department:r_dept,rtype:r_type,room:r_room,repeat_end:r_repeat_end},
                                                type : 'POST',
                                                success:function(data_add_new){
                                                    var json_object = JSON.parse( data_add_new );
                                                    var msg_len = Object.keys(json_object).length;
                                                    var msg_type = json_object['type'];


                                                    if(msg_type === "error" && msg_len > 1){
                                                        //append to the div
                                                        var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                        //remove the type - its the last element
                                                        var msg_len_correct = msg_len - 1;
                                                        for(var jd=0;jd<msg_len_correct;jd++){
                                                            error_div += '<li>'+json_object[jd]+'</li>';
                                                        }
                                                        error_div += "</ul></div>";
                                                        $('#error_block').html(error_div);
                                                    }

                                                    if( msg_type === "notice" ) {
                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS,
                                                            title: 'Success',
                                                            message: 'Reservation Edited Successfully.',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function (dialog_success_edt) {
                                                                    dialog_success_edt.close();
                                                                    dialog_edt_save.close();
                                                                }
                                                            }]

                                                        });
                                                        {{--- Add the event ---}}
                                                        $('#calendar').fullCalendar('refetchEvents');
                                                    }
                                                    l_edit_btn.stop();
                                                },
                                                error:function(error){
                                                    console.log('error->'+error);
                                                }
                                            });


                                        }
                                    }, {
                                        label: 'Cancel',
                                        cssClass: 'btn-danger',
                                        icon : 'fa fa-crosshairs',
                                        action: function(dialog_ed) {
                                            dialog_ed.close();
                                        }
                                    }]
                                });


                            }
                        };
                        var delete_r_reservation = {
                            label: 'Delete',
                            id: 'outer_button',
                            icon: 'fa fa-trash',
                            cssClass: 'btn-danger',
                            action: function (dialogRef) {
                                var l_outer = Ladda.create( document.querySelector('#outer_button') );
                                l_outer.start();
                                {{-- Tooltip with Yes or No --}}
                                BootstrapDialog.show({
                                    type: BootstrapDialog.TYPE_INFO,
                                    title: 'Confirm Delete',
                                    message: 'Are you sure?',
                                    closable: false,
                                    cssClass: 'small-dialog ',
                                    buttons: [{
                                        id : 'delete-btn',
                                        icon: 'fa fa-trash',
                                        label: '<span class="ladda-label"> Yes </span>',
                                        cssClass: 'btn-danger ladda-button',
                                        action: function(dialog) {
                                            var new_l = Ladda.create( document.querySelector('#delete-btn') );
                                            {{-- In Yes block run ajax to delete the event --}}
                                             $.ajax({
                                                url : 'reservation/details/delete',
                                                data : {rr_id:event.id},
                                                type: 'post',
                                                beforeSend : function(x,settings){
                                                    new_l.start();
                                                },
                                                success : function (data){
                                                    if( data === '1'){

                                                        $('#calendar').fullCalendar('removeEvents', event.id);

                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS,
                                                            title: 'Success',
                                                            message: 'Reservation Deleted Successfully.',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function(dialog_delete) {
                                                                    dialog_delete.close();
                                                                }
                                                            }]

                                                        });


                                                    }else{
                                                        {{--Error --}}
                                                        BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_DANGER,
                                                            title: 'Error',
                                                            message: 'Unable to delete Reservation, Integrity constraints enforced ',
                                                            closable: true,
                                                            cssClass: 'small-dialog',
                                                            buttons: [{
                                                                icon: 'fa fa-cog fa-spin',
                                                                label: ' Ok',
                                                                action: function(dialog_delete) {
                                                                    dialog_delete.close();
                                                                }
                                                            }]
                                                        });

                                                    }
                                                    {{--stop the spinners--}}
                                                    new_l.stop();
                                                    l_outer.stop();
                                                    {{--close the dialogs--}}
                                                    dialogRef.close();
                                                }
                                            });
                                            dialog.close();
                                        }
                                    },
                                        {
                                            icon: 'fa fa-times',
                                            label: ' No',
                                            cssClass: 'btn-primary',
                                            action: function(dialog) {
                                                dialog.close();
                                                dialogRef.close();
                                            }
                                        }
                                    ]

                                });



                                {{-- : when all done refetch resources --}}

                                ///dialogRef.close();
                            }
                        };
                        var close_r_reservation = {
                            label: 'Close',
                            icon: 'fa fa-times',
                            cssClass: 'btn-primary',
                            action: function (dialogRef) {
                                dialogRef.close();
                            }
                        };

                        button_arr.push(segment_action);

                        button_arr.push(edit_r_reservation);
                        button_arr.push(delete_r_reservation);
                        button_arr.push(close_r_reservation);


                    @endif
                    var diag = new BootstrapDialog({
                        size: BootstrapDialog.SIZE_SMALL,
                        title: event.title,
                        message: msg,
                        closable: true,
                        cssClass : 'reserve-modal-onClickEvent',
                        autospin: true,
                        buttons: button_arr
                    });
                    diag.realize();
                    diag.getModalHeader().css('background-color', color);
                    diag.open();
                },
                dayClick : function(date,jsEvent,view,resource){
                    {{-- GO TO Resource View for clicked date --}}
                    if( view.name === 'month' ) {
                        $('#calendar').fullCalendar('changeView', 'agendaDay');
                        $('#calendar').fullCalendar('gotoDate', date);
                    }
                },
                {{-- Select function - Trigger create new reservation / Alert error on past date --}}
                select: function (start, end, event, view, resource) {
                    var check = start.format('YYYY-MM-DD HH');
                    var today = moment().format('YYYY-MM-DD HH');

                    var clean_start = start.format('YYYY-MM-DD|HH:mm');
                    var clean_end = end.format('YYYY-MM-DD|HH:mm');

                    var resource_id, addReservationLink;
                    //get the current view
                    var pview_current = view.name;

                    var start_date = start.format('YYYY-MM-DD');
                    var is_holiday = false;

                    $.each(holidays,function(key,value){
                        if ( start_date == value )
                            is_holiday = true;
                    });
                    {{--No holidays--}}
                    if ( is_holiday === false ){

                        {{-- No Past Dates/Times --}}
                        if (event && ( pview_current === "agendaDay" || pview_current === "agendaSevenDay")) {

                            if (check < today) {
                                var message = '<b style="text-align: center;">Reservations forbidden for past date and/or time.</b> <br/>';

                                BootstrapDialog.show({
                                    title: 'Error!',
                                    message: message,
                                    closable: true,
                                    type: BootstrapDialog.TYPE_DANGER,
                                    cssClass: "small-dialog"
                                });

                            } else {
                                resource_id = resource.id;
                                addReservationLink = newReservationLink + '/' + resource_id + '/' + clean_start + '/' + clean_end;

                                var diag = new BootstrapDialog({
                                    type: BootstrapDialog.TYPE_INFO,
                                    title: "Confirm Action : New Reservation?",
                                    closable: true,
                                    cssClass: "add-reserv-dialog",
                                    buttons: [{
                                        label: 'Yes',
                                        cssClass: 'btn btn-primary',
                                        icon: 'fa fa-check',
                                        action: function(dialog) {
                                            @if( Auth::check() && (Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() ) )
                                                dialog.close();

                                            BootstrapDialog.show({
                                                title: 'Add New Reservation',
                                                message: function(dialog_new) {
                                                    var $message = $('<div></div>');
                                                    var pageToLoad = dialog_new.getData('pageToLoad');
                                                    $message.load(pageToLoad);
                                                    return $message;
                                                },
                                                data: {
                                                    'pageToLoad': addReservationLink
                                                },
                                                buttons: [{
                                                    label: '<span class="ladda-label"> Save </span>',
                                                    cssClass: 'btn-primary ladda-button',
                                                    icon : 'fa fa-save',
                                                    id : 'save_reserve_btn',
                                                    action: function(dialog_new) {
                                                        var new_l_save = Ladda.create( document.querySelector('#save_reserve_btn') );

                                                        var r_name =  $('#name').val();
                                                        var r_start = $('#start').val();
                                                        var r_end =   $('#end').val();
                                                        var r_desc = $('#description').val();
                                                        var r_dept = $('#department').val();
                                                        var r_type = $('#rtype').val();
                                                        var r_room = $('#room').val();
                                                        var r_repeat_end = $('#repeat_end').val();
                                                        var r_for = $('#user').val();

                                                        var selected_days = [];
                                                        $.each($("input[name^=rep_day]:checked"), function() {
                                                            selected_days.push($(this).val());
                                                            // or you can do something to the actual checked checkboxes by working directly with  'this'
                                                            // something like $(this).hide() (only something useful, probably) :P
                                                        });

                                                        var r_area = $('#area').val();
                                                        var rep_times_val = $('#rep_times_input').val();

                                                        //remember it is a checkbox..
                                                        var all_day = '0';
                                                        if( $('#allday').is(':checked') ){
                                                            all_day = $('#allday').val();
                                                        }

                                                        var ext = '0';
                                                        if($('#external').is(':checked')){
                                                            ext = $('#external').val();
                                                        }

                                                        var repeat = '0';
                                                        if( $('#repeat').is(':checked') ){
                                                            repeat = $('#repeat').val();
                                                        }

                                                        $.ajax({
                                                            url:saveReservationLink,
                                                            beforeSend : function(x,settings){

                                                                new_l_save.start();
                                                            },
                                                            data:{user:r_for,rep_day:selected_days,rep_times_input:rep_times_val,area:r_area,repeat:repeat,rr_type:ext,allday:all_day,name:r_name,start:r_start,end:r_end,description:r_desc,department:r_dept,rtype:r_type,room:r_room,repeat_end:r_repeat_end},
                                                            type : 'POST',
                                                            success:function(data_add_new){
                                                                var json_object = JSON.parse( data_add_new );
                                                                var msg_len = Object.keys(json_object).length;
                                                                var msg_type = json_object['type'];


                                                                if(msg_type === "error" && msg_len > 1){
                                                                    //append to the div
                                                                    var error_div = '<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Error!</b><ul>';
                                                                    //remove the type - its the last element
                                                                    var msg_len_correct = msg_len - 1;
                                                                    for(var jd=0;jd<msg_len_correct;jd++){
                                                                        error_div += '<li>'+json_object[jd]+'</li>';
                                                                    }
                                                                    error_div += "</ul></div>";
                                                                    $('#error_block').html(error_div);
                                                                }

                                                                if( msg_type === "notice" ) {
                                                                    BootstrapDialog.show({
                                                                        type: BootstrapDialog.TYPE_SUCCESS,
                                                                        title: 'Success',
                                                                        message: 'Reservation Added Successfully.',
                                                                        closable: true,
                                                                        cssClass: 'small-dialog',
                                                                        buttons: [{
                                                                            icon: 'fa fa-cog fa-spin',
                                                                            label: ' Ok',
                                                                            action: function (dialog_success) {
                                                                                {{--- Add the event ---}}
                                                                                $('#calendar').fullCalendar('refetchEvents');
                                                                                dialog_success.close();
                                                                                dialog_new.close();
                                                                            }
                                                                        }]

                                                                    });



                                                                }

                                                                new_l_save.stop();



                                                            },
                                                            error:function(error){
                                                                console.log('error->'+error);
                                                            }
                                                        });


                                                    }
                                                }, {
                                                    label: 'Cancel',
                                                    cssClass: 'btn-danger',
                                                    icon : 'fa fa-crosshairs',
                                                    action: function(dialog_new) {
                                                        dialog.close();
                                                        dialog_new.close();
                                                    }
                                                }]
                                            });
                                            @else
                                                window.location = loginLink;
                                            @endif
                                        }
                                    }, {
                                        label: 'No',
                                        cssClass: 'btn btn-danger',
                                        icon: 'fa fa-close',
                                        action: function(dialog) {
                                            dialog.close();
                                        }
                                    }]
                                });

                                diag.realize();
                                diag.getModalBody().hide();
                                diag.getModalBody().remove();
                                diag.open();
                            }
                        }

                    }else{
                        var message = '<b style="text-align: center;">Reservations forbidden for non-working days.</b> <br/>';

                        BootstrapDialog.show({
                            title: 'Error!',
                            message: message,
                            closable: true,
                            type: BootstrapDialog.TYPE_DANGER,
                            cssClass: "small-dialog"
                        });
                    }
                }
            });


            // $('.fc-goto-button').datetimepicker({
            //     showClose : true,
            //     pickTime: false,
            //     viewMode : 'months'
            // });

            $('.fc-goto-button').on('dp.hide',function(date_selected){
                var date_to_use = date_selected.date.format('YYYY-MM-DD');
                var selected_reservation_type = $('#reservation_type').val();

                if ( selected_reservation_type == "room" ){
                    $('#calendar').fullCalendar( 'gotoDate', date_to_use );
                }

                if ( selected_reservation_type == "course" ){
                    $('#reservation_calendar').fullCalendar( 'gotoDate', date_to_use );
                }
            });

            @if ( Session::get('notice') )
            noty({
                text: "Success! {{ Session::get('notice') }}",
                theme: "relax",
                layout : 'topRight',
                type: 'success',
                dismissQueue: true,
                progressBar : true,
                timeout     : 3000,
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight', // Animate.css class names
                    easing: 'swing', // unavailable - no need
                    speed: 500 // unavailable - no need
                }
            });
            @endif
        });







    </script>


@endsection
