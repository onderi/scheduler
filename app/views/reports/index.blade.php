@extends('layouts.master')

@section('title')
| Reports and Timetables
@endsection


@section('style')
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
{{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}
{{ HTML::style('js/plugins/noty/animate.css')}}

@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Reports and Timetables
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection

@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">

        </div>
        <div class="box-body">
            <b class="text-red">* For legacy bookings please use 'DEFAULT' as the program</b>
          
				{{Form::open(['method'=>'POST','action'=>'ReportsController@generate'])}}
				<div class="form-group col-md-12">
				{{Form::label('program','Program')}}
				{{Form::select('program',$programs,'',['class'=>'form-control'])}}
				</div>
				<div class="form-group col-md-6">
				{{Form::label('start_limit','Start Limit')}}
				{{Form::text('start_limit','',['class'=>'form-control start_limit','placeholder'=>'Start date limit','required'=>'required'])}}
				</div>
				<div class="form-group col-md-6">
				{{Form::label('end_limit','End Limit')}}
				{{Form::text('end_limit','',['class'=>'form-control end_limit','placeholder'=>'End date limit','required'=>'required'])}}
				</div>
				
				{{Form::submit('Generate Program Timetable',['class'=>'btn btn-primary form-control'])}}
				
				{{Form::close()}}

        </div>
        <div class="box-footer">

        </div>
    </div>

</div>


@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
{{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
{{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
{{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
{{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}
{{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

<script type="text/javascript">
$('.start_limit').datetimepicker({
    pickTime:false,
    //minDate:moment().subtract('days',1)

});
$('.end_limit').datetimepicker({
    pickTime:false,
    //minDate:moment().subtract('days',1)

});
$(".start_limit").on("dp.change", function (e) {$('.end_limit').data("DateTimePicker").setMinDate(e.date);});
$(".end_limit").on("dp.change", function (e) {$('.start_limit').data("DateTimePicker").setMaxDate(e.date);});

$(document).ready(function(){
    @if ( Session::get('notice') )

           noty({
        text: "Success! {{ Session::get('notice') }}",
        theme: "relax",
        dismissQueue: true,
        progressBar : true,
        timeout     : 3000,
        layout : 'topRight',
        type: 'success',
        animation: {
            open: 'animated bounceInRight', // Animate.css class names
            close: 'animated bounceOutRight', // Animate.css class names
            easing: 'swing', // unavailable - no need
            speed: 500 // unavailable - no need
        }
    });

    @endif

    @if ( Session::get('error') )
       noty({
        text: "Error, {{ Session::get('error')}}",
        theme: "relax",
        dismissQueue: true,
        progressBar : true,
        timeout     : 10000,
        layout : 'topRight',
        type: 'error',
        animation: {
            open: 'animated swing', // Animate.css class names
            close: 'animated tada', // Animate.css class names
            easing: 'swing',
            speed: 500 // opening & closing animation speed
        }
    });
    @endif
});
</script>

@endsection