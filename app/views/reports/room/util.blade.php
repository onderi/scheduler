@extends('layouts.master')

@section('title')
    | Room Utilization Report
    @endsection


    @section('style')
            <!-- CSS Datatables !-->
    {{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}
    {{ HTML::style('css/bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}
    {{ HTML::style('js/plugins/ladda/ladda-themeless.min.css')}}
    {{ HTML::style('js/plugins/noty/animate.css')}}
    {{ HTML::style('js/plugins/amcharts/export/export.css')}}
    <style type="text/css">

        #chartdiv {
            width	: 100%;
            height: 545px;
            margin: 0 auto;
            font-size: 14px;
            padding: 10px;
        }

        .amcharts-chart-div a {color:lightgrey !important;font-size:8px !important;}

    </style>
    @endsection

            <!-- Content Heading !-->
@section('content_head')
    <h1>
        Room Utilization Report
    </h1>
    @endsection


            <!-- Breadcrumbs !-->
@section('breadcrumb')
    {{ Breadcrumbs::render() }}
@endsection

@section('page_content')
    <div id="filt" class="col-md-2">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title faa-parent animated-hover"> <i class="fa fa-filter faa-shake"></i> Filters</h4>
            </div>
            <div class="box-body">

                <p style="font-size: 14px;" class="faa-parent animated-hover">
                    <i class="fa fa-eye text-red faa-burst"></i> <span > Type</span>
                </p>


                {{Form::select('report-type',$report_type,$default_type,array('class'=>'form-control','id'=>'report-type','title' => ''))}}

                <br/>

                <p style="font-size: 14px;" class="faa-parent animated-hover">
                    <i class="fa fa-object-group text-red faa-burst"></i> <span > Areas</span>
                </p>

                {{Form::select('filter',$areas,$default_area,array('class'=>'form-control','id'=>'filter',"multiple"=>"multiple",'title' => 'Please press Ctrl + click to select multiple areas'))}}

                <br/>
                {{-- The Checkboxes --}}
                <div id="rooms">

                    <div id="inner-rooms">
                        <p style="font-size: 14px;" class="faa-parent animated-hover">
                            <i class="fa fa-university text-red faa-burst"></i> <span > Rooms</span>
                        </p>

                        @foreach( $rooms_array as $room )
                            <div class='checkbox checkbox-primary'><input rtitle="<?php echo $room['title']; ?>" value="<?php echo $room['id']; ?>" id='checkbox<?php echo $room['id']; ?>' class='styled rooms_filter' type='checkbox' name="rooms_chk"  checked='checked'><label for='checkbox<?php echo $room['id']; ?>'> <?php echo $room['title']; ?></label></div>
                        @endforeach
                    </div>

                </div>

                <div id="period">
                    <p style="font-size: 14px;" class="faa-parent animated-hover">
                        <i class="fa fa-clock-o text-red faa-burst"></i> <span > Period</span>
                    </p>
                    <div class=" col-md-12 form-group">
                        {{Form::label('from','From')}}
                        {{Form::text('from',$start_date,array('class'=>'form-control','id' => 'from','placeholder'=>'From','data-date-format' => "YYYY-MM-DD HH:mm"))}}
                    </div>

                    <div class="col-md-12 form-group">
                        {{Form::label('to','To')}}
                        {{Form::text('to',$end_date,array('class'=>'form-control','id' => 'to','placeholder'=>'To','data-date-format' => "YYYY-MM-DD HH:mm"))}}
                    </div>
                </div>

                <div class="col-md-12">
                    <button class="btn-sm btn btn-primary" id="refresh-report" > <i class="fa fa-refresh"></i> Refresh report</button>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /. box -->
    </div>


    <div  class="col-md-10">
        <div class="box box-primary">
            <div class="box-header">

                <h4 style="color:#a20002;" class="box-title faa-parent animated-hover"> <i class="fa fa-pie-chart faa-shake"></i> <span id="chart-title-custom"> <?php echo $default_type;?></span></h4>
                @if ( Session::get('notice') )
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Success!</b> {{{ Session::get('notice') }}}.
                    </div>
                @endif
                @if ( Session::get('error') )
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Error!</b> {{{ Session::get('error') }}}.
                    </div>
                @endif
            </div>
            <div class="box-body">
                <div id="chartdiv"></div>

                <div id="legenddiv" style="border: 2px dotted #3c8dbc; margin: 5px 0 5px 0;position: relative;"></div>
            </div>
            <div class="box-footer">

            </div>
        </div>

    </div>

    @endsection

            <!-- Custom Scripts !-->
    @section('scripts')
            <!-- JS Datatables !-->
    {{ HTML::script('js/jquery-ui-1.10.3.js')}}
    {{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
    {{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}
    {{ HTML::script('js/plugins/amcharts/amcharts.js')}}
    {{ HTML::script('js/plugins/amcharts/pie.js')}}
    {{ HTML::script('js/plugins/amcharts/export/export.min.js')}}
    {{ HTML::script('js/plugins/amcharts/themes/light.js')}}
    {{HTML::script('js/plugins/fullcalendar/fc/sbs-calendar.js')}}
    {{HTML::script('js/plugins/spin/spin.min.js')}}

    <script type="text/javascript">

        var chartData = <?php echo $chartdata; ?>;
        var default_report_type = "<?php echo $default_type; ?>";
        setRooms("<?php echo $room_ids; ?>");

        //our spinner & target
        var spinner;
        var target = document.getElementById('chartdiv');
        //options for the spinner
        var opts = {
            lines: 13, // The number of lines to draw
            length: 15, // The length of each line
            width: 8, // The line thickness
            radius: 15, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 58, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#3c8dbc', // #rgb or #rrggbb or array of colors
            speed: 0.9, // Rounds per second
            trail: 100, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent
            left: '50%', // Left position relative to parent
            scale: 2,
            opacity: 0
        };

        // create pie chart
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "pie",
            "theme": "light",
            "autoResize" : true,
            "outlineAlpha": 0.4,
            "depth3D": 20,
            "angle": 50,
            "dataProvider": chartData,
            "valueField": "value",
            "titleField": "title",
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "labelText": "[[title]]: [[percents]]%",
            "pullOutOnlyOne": true,
            "titles": [{
                "text": "Room Utilization "
            }],
            "allLabels": [],
            "export": {
                "enabled": true
            }

        });

        chart.marginTop = 0;
        chart.marginBottom = 0;
        chart.marginLeft = 5;
        chart.marginRight = 5;
        chart.pullOutRadius = 0;
        var legend = new AmCharts.AmLegend({

        });
        chart.addLegend(legend,"legenddiv");


        // initialize step array
        chart.drillLevels = [{
            "title": "Room Utilization",
            "data": chartData
        }];




        // add slice click handler
        chart.addListener("clickSlice", function (event) {

            // get chart object
            var chart = event.chart;

            // check if drill-down data is avaliable
            if (event.dataItem.dataContext.data !== undefined) {

                // save for back button
                chart.drillLevels.push(event.dataItem.dataContext);

                // replace data
                chart.dataProvider = event.dataItem.dataContext.data;

                // replace title
                chart.titles[0].text = event.dataItem.dataContext.title;

                // add back link
                // let's add a label to go back to yearly data
                event.chart.addLabel(
                        0, 25,
                        "< Go back",
                        undefined,
                        undefined,
                        undefined,
                        undefined,
                        undefined,
                        undefined,
                        'javascript:drillUp();');

                // take in data and animate
                chart.validateData();
                chart.animateAgain();
            }
        });


        function drillUp() {

            // get level
            chart.drillLevels.pop();
            var level = chart.drillLevels[chart.drillLevels.length - 1];

            // replace data
            chart.dataProvider = level.data;

            // replace title
            chart.titles[0].text = level.title;

            // remove labels
            if (chart.drillLevels.length === 1)
                chart.clearLabels();

            // take in data and animate
            chart.validateData();
            chart.animateAgain();
        }

        $(document).ready(function () {
            //set the initial rooms
            //store.set("rooms_selected_for_report");

            {{--//We want the calendar to be full screen :D--}}
            $('body').addClass('sidebar-collapse');
            {{--//Enable the navbar shower--}}
            $('.navbar-btn').fadeIn(2000);

            //Date Time Picker
            $('#to').datetimepicker({
                minuteStepping: 60,
                pick12HourFormat: false,
            });

            $('#from').datetimepicker({
                minuteStepping: 60,
                pick12HourFormat: false,
            });
            /**
             * On Change listener dropdown
             */
            $('#filter').on('change',function(){

                var area_id = $(this).val();

                console.log(area_id);
                {{-- //close it to avoid the 'hanging look' --}}
                $(this).blur();

                if (area_id !== "0") {

                    $.ajax({
                        url : '../../calendar/getResourcesFromArray?area='+area_id,
                        cache : false,
                        success:function (data_area){
                            var list_of_resources = [];
                            var inner_rooms_div = "<p style='font-size: 14px;'  class='faa-parent animated-hover'> <i class='fa fa-university text-red faa-burst'></i> <span > Rooms</span> </p><div id='inner-rooms'>";
                            for(var b = 0; b < data_area.length;b++){
                                var item = data_area[b];
                                list_of_resources.push(item.id);
                                inner_rooms_div += "<div class='checkbox checkbox-primary'><input name='rooms_chk' id='checkbox"+b+"' rtitle='"+item.title+"' value='"+item.id+"' class='rooms_filter styled' type='checkbox' checked=''><label for='checkbox"+b+"'> "+item.title+"</label></div>";
                            }
                            {{-- Update the rooms --}}
                                    inner_rooms_div += "</div>";
                            updateRoomsFilterReport(inner_rooms_div);
                            setRooms(list_of_resources.join());
                            {{-- Pass them to calendar--}}
                        },
                        type:'GET',
                        dataType:'json'
                    });

                }else{
                    var list_of_resources = [{
                        id : '0',
                        title : 'All'
                    }];
                    var inner_rooms_div = "<p style='font-size: 14px;'  class='faa-parent animated-hover'> <i class='fa fa-university text-red faa-burst'></i> <span > Rooms</span> </p><div id='inner-rooms'>";
                    inner_rooms_div += "<div class='checkbox checkbox-primary'><input name='rooms_chk' id='checkbox1' rtitle='All' value='0' class='rooms_filter styled' type='checkbox' checked=''><label for='checkbox1'> All</label></div>";
                    inner_rooms_div += "</div>";
                    updateRoomsFilterReport(inner_rooms_div);
                    setRooms("0");//All rooms
                }
            });

            /**
             * OnCheckbox change filter
             */
            $(document).on('change','.rooms_filter',function (){

                var arrRooms = [];

                $("input:checkbox[name=rooms_chk]:checked").each(function(){
                    arrRooms.push($(this).val());
                });

                if( arrRooms.length == 0 ){
                    setRooms('0');
                }else{
                    setRooms(arrRooms.join());
                }

            });

            $("#refresh-report").click(function (e){
                e.preventDefault();

                var from = $("#from").val();
                var to = $("#to").val();

                if( from == ""  ){
                    alert('Please set from date');
                    $('#from').focus();
                }else if( to == ""){
                    alert('Please set to date');
                    $('#to').focus();
                }else{
                    //heavy lifting
                    var report_type = $("#report-type").val();
                    var rooms = getRooms();

                    $.ajax({
                        url : '../../report/refreshData',
                        cache : false,
                        data : {"report_type" : report_type,"from": from,"to":to,"rooms":rooms},
                        beforeSend: function(){
                            spinner = new Spinner(opts).spin(target);
                        },
                        success:function (chartData){
                            if( chartData != "0" ){
                                // initialize step array
                                chart.drillLevels = [{
                                    "title": "Room Utilization",
                                    "data": chartData
                                }];

                                chart.dataProvider = chartData;
                                chart.validateData();

                                chart.marginTop = 0;
                                chart.marginBottom = 0;
                                chart.marginLeft = 5;
                                chart.marginRight = 5;
                                chart.pullOutRadius = 0;


                                $("#chart-title-custom").text(report_type);


                            }
                        },
                        complete : function (){
                            if (typeof spinner !== "undefined")
                                spinner.stop();
                        },
                        type:'GET',
                        dataType:'json'
                    });
                }
            })

        });

    </script>

@endsection