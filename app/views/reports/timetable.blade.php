<title><?=$program->course_title;?></title>
<div id="printable">
    <div style="text-align: center;width: 100%;">
        <h3><?=$program->course_title;?></h3>
        <h6><?=$program->course_desc;?></h6>
        <p><?=$date_period->start->format('Y/m/d')."-".$date_period->end->modify('-1 days')->format('Y/m/d')?></p>
    </div>

    <table border="1" style="font-size: 0.8em;height: 60%">
        <tr>
            <?php
            $cnt=iterator_count($date_period);
            foreach($date_period as $date){
            $count=0;
            foreach($sessions as $session){
                $end_limit=$date->format('Y-m-d'.' 22:00');
                $start_limit=$date->format('Y-m-d'.' 00:00');
                if($session->end_time<$end_limit&&$session->start_time>$start_limit&&$session->course_id==$program->course_id){
                    $start=new datetime($session->start_time);
                    $end=new datetime($session->end_time);
                    $count++;
                }
            }
            if($count>0){
            $wid=100/$cnt;
            ?>
            <td style="background: #fcd3a1; width:<?=$wid?>%">
                <b><?=$date->format('Y-m-d')?></b>
            </td>

            <?php
            }
            }
            echo "</tr><tr>";
            foreach($date_period as $date){
            ?>

            <td style="vertical-align: top">

                <table border='1' >
                    <?php
                    foreach($sessions as $session){
                        $end_limit=$date->format('Y-m-d'.' 22:00');
                        $start_limit=$date->format('Y-m-d'.' 00:00');
                        if($session->end_time<$end_limit&&$session->start_time>$start_limit&&$session->course_id==$program->course_id){
                            $start=new datetime($session->start_time);
                            $end=new datetime($session->end_time);
                            echo "<tr ><td>";
                            echo "<b>".$start->format('H:i')."-".$end->format('H:i')."</b><br>";
                            echo $session->reservation_desc;
                            echo "</td></tr>";
                        }
                    }

                    ?>
                </table>
            </td>

            <?php

            }
            ?>

        </tr>
    </table>
    <style>
        @media print {
            html, body {
                height: 99%;
            }
        }
    </style>

</div>
