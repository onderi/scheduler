@extends('layouts.master')

@section('title')
    | Generated Timetable
    @endsection


    @section('style')
            <!-- CSS Datatables !-->
    {{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
    {{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
    {{ HTML::style('css/datatables/TableTools.css')}}
    {{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}
    @endsection

            <!-- Content Heading !-->
@section('content_head')
    <h1>
        Reports and Timetables
    </h1>
    @endsection


            <!-- Breadcrumbs !-->
@section('breadcrumb')
    {{ Breadcrumbs::render() }}
@endsection

@section('page_content')
    <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
       'version':'1','packages':['timeline']}]}"></script>

    <div class="col-md-12">
        <div class="box box-warning">
            <div class="box-header">

            </div>
            <div class="box-body">
                <?php
                $iteration = 0;?>
                @if($program->course_id==1)
                    @foreach($date_period as $date)
                        <h3><?=$date->format('Y-m-d')?></h3>
                        <div id="table_<?=$iteration;?>"></div>
                        <script>

                            google.setOnLoadCallback(drawChart);
                            function drawChart() {
                                var container = document.getElementById("table_<?=$iteration?>");
                                var chart = new google.visualization.Timeline(container);
                                var dataTable = new google.visualization.DataTable();

                                dataTable.addColumn({type: 'string', id: 'Room_name'});
                                dataTable.addColumn({type: 'string', id: 'Name'});
                                dataTable.addColumn({type: 'date', id: 'Start'});
                                dataTable.addColumn({type: 'date', id: 'End'});
                                dataTable.addRows([
                                    <?php

                                    $count[$iteration]=0;
                                    foreach($sessions as $session){
                                    $end_limit=$date->format('Y-m-d'.' 22:00');
                                    $start_limit=$date->format('Y-m-d'.' 00:00');
                                    if($session->end_time<$end_limit&&$session->start_time>$start_limit&&$session->course_id==$program->course_id){
                                    $ref=explode('/',$session->ref_number);
                                    $session_start=new datetime($session->start_time);
                                    $session_end=new datetime($session->end_time);
                                    $m=$session_start->format('m')-1;

                                    $event="['".$ref[1]."','".$session->reservation_desc."',new Date(".$session_start->format('Y').",".$m.",".$session_start->format('d').",".$session_start->format('H').",".$session_start->format('i').",".$session_start->format('s')."),new Date(".$session_end->format('Y').",".$m.",".$session_end->format('d').",".$session_end->format('H').",".$session_end->format('i').",".$session_end->format('s').")],";

                                    echo $event;
                                    $count[$iteration]++;
                                    }
                                    }
                                    ?>
                                    ]);

                                var rowHeight = 45;
                                var chartHeight = rowHeight * (dataTable.getNumberOfRows() + 1);
                                var options = {
                                    timeline: {colorByRowLabel: true},
                                    height: chartHeight,
                                };
                                chart.draw(dataTable, options);
                            }
                        </script>
                        <?php
                        $iteration++;
                        ?>

                    @endforeach
                @else
                    <h3><?=$program->course_title;?></h3>
                    <div id="table_"></div>
                    <script>

                        google.setOnLoadCallback(drawChart);
                        function drawChart() {
                            var container = document.getElementById("table_");
                            var chart = new google.visualization.Timeline(container);
                            var dataTable = new google.visualization.DataTable();

                            dataTable.addColumn({type: 'string', id: 'Session date'});
                            dataTable.addColumn({type: 'string', id: 'Description'});
                            dataTable.addColumn({type: 'date', id: 'Start'});
                            dataTable.addColumn({type: 'date', id: 'End'});
                            dataTable.addRows([
                                <?php
                                $default_date=new datetime();
            foreach($sessions as $session){
            $ref=explode('/',$session->ref_number);
            $session_start=new datetime($session->start_time);
            $session_end=new datetime($session->end_time);
            $m=$default_date->format('m')-1;
            $event="['".$session_start->format('Y-m-d')."','".addslashes($session->reservation_desc."-".$ref[1])."',new Date(".$default_date->format('Y').",".$m.",".$default_date->format('d').",".$session_start->format('H').",".$session_start->format('i').",".$session_start->format('s')."),new Date(".$default_date->format('Y').",".$m.",".$default_date->format('d').",".$session_end->format('H').",".$session_end->format('i').",".$session_end->format('s').")],";
            echo $event;

            }


            ?>


                        ]);

                            var rowHeight = 45;
                            var chartHeight = rowHeight * (7);
                            var options = {
                                timeline: {colorByRowLabel: true},
                                height: chartHeight,
                            };
                            chart.draw(dataTable, options);
                        }
                    </script>
                    <a class="btn btn-primary" href="{{URL::to('report/timetable/'.$program->course_id.'/'.$start_date_limit->format('Y-m-d').'/'.$end_date_limit->format('Y-m-d'))}}">Generate Timetable</a>

                    {{--<div id="printable">--}}
                        {{--<div>--}}
                        {{--<h3><?=$program->course_title;?></h3>--}}
                           {{--<h6><?=$program->course_desc;?></h6>--}}
                        {{--</div>--}}
                        {{--<table border="1"  style="height: 100%;width: 100%;font-size:small">--}}
                            {{--<tr>--}}
                                    {{--<?php--}}
                                        {{--foreach($date_period as $date){--}}
                                            {{--?>--}}
                                        {{--<td>--}}
                                            {{--<table style="height: 100%;" border="1">--}}
                                                {{--<tbody>--}}
                                                {{--<?php--}}
                                                        {{--foreach($sessions as $session){--}}
                                                            {{--$end_limit=$date->format('Y-m-d'.' 22:00');--}}
                                                            {{--$start_limit=$date->format('Y-m-d'.' 00:00');--}}
                                                            {{--if($session->end_time<$end_limit&&$session->start_time>$start_limit&&$session->course_id==$program->course_id){--}}
                                                                {{--$start=new datetime($session->start_time);--}}
                                                                {{--$end=new datetime($session->end_time);--}}
                                                                {{--echo "<tr>";--}}
                                                                {{--echo "<td>";--}}
                                                                {{--echo "<b>".$start->format('H:i')."-".$end->format('H:i')."</b><br>";--}}
                                                                {{--echo $session->reservation_desc;--}}
                                                                {{--echo "</td>";--}}
                                                                {{--echo "</tr>";--}}
                                                            {{--}--}}

                                                        {{--}--}}
                                                {{--?>--}}
                                                {{--</tbody>--}}
                                            {{--</table>--}}
                                        {{--</td>--}}
                                    {{--<?php--}}
                                        {{--}--}}
                                    {{--?>--}}

                            {{--</tr>--}}
                        {{--</table>--}}
                    {{--</div>--}}


                @endif


            </div>
            <div class="box-footer">


            </div>
        </div>

    </div>


    @endsection

            <!-- Custom Scripts !-->
    @section('scripts')
            <!-- JS Datatables !-->
    {{ HTML::script('js/jquery-ui-1.10.3.js')}}
    {{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
    {{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
    {{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
    {{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
    {{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
    {{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}

    {{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
    {{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.3.3/jQuery.print.min.js')}}

    <script type="text/javascript">
        function printMe() {
            $("#printable").print({
                GlobalStyles:true,
                rejectWindow:true

            });
        }

        $(document).ready(function(){
            @if ( Session::get('notice') )

                   noty({
                    text: "Success! {{ Session::get('notice') }}",
                    theme: "relax",
                    dismissQueue: true,
                    progressBar : true,
                    timeout     : 3000,
                    layout : 'topRight',
                    type: 'success',
                    animation: {
                        open: 'animated bounceInRight', // Animate.css class names
                        close: 'animated bounceOutRight', // Animate.css class names
                        easing: 'swing', // unavailable - no need
                        speed: 500 // unavailable - no need
                    }
            });
            @endif

            @if ( Session::get('error') )
               noty({
                text: "Error, {{ Session::get('error')}}",
                theme: "relax",
                dismissQueue: true,
                progressBar : true,
                timeout     : 10000,
                layout : 'topRight',
                type: 'error',
                animation: {
                    open: 'animated swing', // Animate.css class names
                    close: 'animated tada', // Animate.css class names
                    easing: 'swing',
                    speed: 500 // opening & closing animation speed
                }
            });
            @endif
        });

    </script>

@endsection
