<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SBS Faculty Planner | Log in</title>
        {{-- Tell the browser to be responsive to screen width --}}
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- bootstrap 3.0.2 -->
        {{ HTML::style('css/bootstrap.min.css')}}
        <!--Skylo -->
        {{ HTML::style('css/skylo/skylo.css')}}
        <!-- Ionicons -->
        {{ HTML::style('css/ionicons.min.css')}}
        <!-- Font Awesome  -->
        {{ HTML::style('css/font-awesome.min.css')}}
        <!-- Animated Font Awesome Add-on -->
        {{ HTML::style('css/font-awesome-animation.min.css')}}
        <!-- Theme style -->
        {{ HTML::style('css/AdminLTE.css')}}

        <link rel="shortcut icon" href="{{ URL::to('img/favicon.ico') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="hold-transition login-page">
    <div class="login-box">

        <div class="login-box-body">
            <div class="login-logo">
                <a href="#">
                    <img style="width: 25%;" src="{{ URL::to('img/logo.png')  }}" alt="logo" />
                </a>
            </div><!-- /.login-logo -->
            <p class="login-box-msg" style="font-size:18px;font-family: Kaushan Script;">Sign in to start your session..</p>
            <form action="{{ URL::to('/user/login') }}" method="post">
                <input type="hidden" name="_token" value="{{ Session::getToken() }}">
                <div class="form-group has-feedback faa-parent animated-hover">
                    <input type="text" name="username" class="form-control" placeholder="Enter your username...">
                    <i class="fa fa-envelope faa-bounce  form-control-feedback">
                    </i>
                </div>

                <div class="form-group has-feedback faa-parent animated-hover">
                    <input type="password" class="form-control" placeholder="Enter your password..." type="password" name="password">
                    <span class="fa fa-lock faa-bounce form-control-feedback"></span>
                </div>

                {{-- The Button Row --}}
                <div class="row">
                    <div class="col-xs-5">
                        <button type="submit" class="btn btn-primary btn-block btn-flat faa-parent animated-hover"> <i class="fa fa-sign-in faa-horizontal"></i>  Sign In</button>
                    </div><!-- /.col -->
                </div>
            </form>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <!-- jQuery 2.0.2 -->
    {{ HTML::script('js/jquery.min.js')}}
    <!-- Bootstrap 3.3.5 -->
    {{HTML::script('js/bootstrap.min.js')}}
    <!-- Skylo -->
    {{ HTML::script('js/plugins/skylo/skylo.js')}}
    <!-- Noty -->
    {{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

    <script type="text/javascript">
        //Default options
        $.skylo({
            state: 'info',
            inchSpeed: 200,
            initialBurst: 5,
            flat: false
        });

        // ------------------------------
        // Progress Bar on State Change
        // ------------------------------
        window.onbeforeunload = function () {
            $.skylo('start');
            $.skylo('set', 50);
        };

        $.skylo('start');
        $.skylo('set', 50);

        $(window).load(function () {
            $.skylo('end');
        });

        $(document).ready(function(){

            @if ( Session::get('notice') )

          noty({
                text: "Success! {{ Session::get('notice') }}",
                theme: "relax",
                dismissQueue: true,
                progressBar: true,
                timeout: 3000,
                layout: 'topRight',
                type: 'success',
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight', // Animate.css class names
                    easing: 'swing', // unavailable - no need
                    speed: 500 // unavailable - no need
                }
            });

            @endif

            @if ( Session::get('error') )
               noty({
                text: "Error, {{ Session::get('error')}}",
                theme: "relax",
                dismissQueue: true,
                progressBar: true,
                timeout: 10000,
                layout: 'topRight',
                type: 'error',
                animation: {
                    open: 'animated swing', // Animate.css class names
                    close: 'animated tada', // Animate.css class names
                    easing: 'swing',
                    speed: 500 // opening & closing animation speed
                }
            });
            @endif



        });

    </script>

    </body>
</html>
