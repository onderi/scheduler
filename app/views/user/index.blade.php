@extends('layouts.master')

<!-- Page Title !-->
@section('title')
    | Users
@endsection


<!-- Custom CSS !-->
@section('style')
    <!-- CSS Datatables !-->
    {{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
    {{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
    {{ HTML::style('css/datatables/TableTools.css')}}
    {{ HTML::style('js/plugins/noty/animate.css')}}

    <style type="text/css">
        .modal-dialog {
            width: 300px !important;
        }
    </style>
@endsection

<!-- Content Heading !-->
@section('content_head')
    <h1>
        Users
    </h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
    {{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')
    <div class="col-md-12">
        <div class="box box-warning">
            <div class="box-header">

            </div>
            <div class="box-body">

                <table class="table table-bordered table-striped dataTable" id="users">
                    <thead>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Department</th>
                    <th>Email</th>
                    <th>Actions</th>
                    </thead>
                    <tbody>
                    @if( count($users) > 0 )
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{$user->fname .' '.$user->lname}}
                                    @if( $user->type->name == "Administrative" )
                                        <small data-placement="left" title="{{$user->type->name}}"
                                               class="badge pull-right bg-aqua"> {{ $user->type->name }}</small>
                                    @else
                                        <small data-placement="left" title="{{$user->type->name}}"
                                               class="badge pull-right bg-green"> {{ $user->type->name }}</small>
                                    @endif
                                </td>
                                <td>{{$user->username}}</td>
                                <td>
                                    @if( count( $user->departments ) > 0 )
                                        <?php
                                        $prefix = '';
                                        $list = "";
                                        ?>
                                        @foreach( $user->departments as $d )
                                            <?php
                                            $list .= $prefix . $d->dept_abbrev;
                                            $prefix = ', ';
                                            ?>
                                        @endforeach
                                        {{ //Multiple department Suppport
                                            $list
                                         }}
                                    @else
                                        {{ 'None'  }}
                                    @endif


                                </td>
                                <td>{{$user->email}}</td>
                                <td style="width: 20%">
                                    {{--
                                    <a data-rel="tooltip" title="Add New" href="{{URL::to('user/add')}}" >
                                    <span class="fa fa-plus-square-o"/>
                                    </a>
                                    --}}
                                    <form name="delform_user" id="delform{{$user->user_id}}" method="post"
                                          action="{{URL::to('user/delete/'.$user->user_id)}}">
                                        {{Form::token()}}
                                        <a data-rel="tooltip" title="Edit {{$user->fname .' '.$user->lname}}"
                                           href="{{URL::to('user/'.$user->user_id.'/edit')}}">
                                            <span class="fa fa-edit"/>
                                        </a>
                                        <a href='#' class="delete_entry" id="delete_user{{$user->user_id}}"
                                           data-rel="tooltip" title="Delete {{$user->fname .' '.$user->lname}}">
                                            <span class="fa fa-trash-o"/>
                                        </a>
                                        <a title="{{$user->fname .' '.$user->lname}} Assigned Role(s)"
                                           data-rel="tooltip" style="cursor:default;"><i data-uid="{{$user->user_id}}"
                                                                                         class="fa fa-thumbs-o-up assigned_role"></i></a>
                                        @if( $user->status->status_name == "Active" )
                                            <a data-rel="tooltip" title="{{$user->fname .' '.$user->lname}} active"
                                               style="cursor: default;"><i class="fa fa-unlock viapost"
                                                                           data-uid="{{$user->user_id}}"
                                                                           data-status="{{'Inactive'}}"></i></a>
                                        @else
                                            <a data-rel="tooltip" title="{{$user->fname .' '.$user->lname}} inactive"
                                               style="cursor: default;"><i class="fa fa-lock viapost"
                                                                           data-uid="{{$user->user_id}}"
                                                                           data-status="{{'Active'}}"></i></a>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
            <div class="box-footer">

            </div>
        </div>

    </div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')
    <!-- JS Datatables !-->
    {{ HTML::script('js/jquery-ui-1.10.3.js')}}
    {{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
    {{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
    {{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
    {{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
    {{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
    {{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
    {{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

    <script type="text/javascript">
        $(function () {

            $('#users').dataTable({
                "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": false,
                "bAutoWidth": false,
                "oLanguage": {
                    "sLengthMenu": "_MENU_ Users per page"
                },
                "oTableTools": {
                    "aButtons": [
                        {
                            "sExtends": "gotoURL",
                            "sButtonText": '<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>  Add New User</button>',
                            "sGoToURL": "{{URL::to('user/add')}}"
                        },
                        {
                            "sExtends": "pdf",
                            "sFileName": "Users.pdf",
                            "sPdfOrientation": "landscape",
                            "sPdfMessage": "SBS Contacts System Users",
                            "sButtonText": "Save to PDF"
                        },
                        {
                            "sExtends": "csv",
                            "sFileName": "SBS-Contacts-Users.csv",
                            "sButtonText": "Save to CSV"
                        },
                        {
                            "sExtends": "xls",
                            "sFileName": "SBS-Contacts-Users.xls",
                            "sButtonText": "Save to Excel"
                        }
                    ]
                }
            });
            $('select,input').addClass('form-control input-sm');
            $('.alert').fadeOut(5000);

            @if ( Session::get('notice') )

           noty({
                text: "Success! {{ Session::get('notice') }}",
                theme: "relax",
                dismissQueue: true,
                progressBar: true,
                timeout: 3000,
                layout: 'topRight',
                type: 'success',
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight', // Animate.css class names
                    easing: 'swing', // unavailable - no need
                    speed: 500 // unavailable - no need
                }
            });

            @endif

            @if ( Session::get('error') )
               noty({
                text: "Error, {{ Session::get('error')}}",
                theme: "relax",
                dismissQueue: true,
                progressBar: true,
                timeout: 10000,
                layout: 'topRight',
                type: 'error',
                animation: {
                    open: 'animated swing', // Animate.css class names
                    close: 'animated tada', // Animate.css class names
                    easing: 'swing',
                    speed: 500 // opening & closing animation speed
                }
            });
            @endif

        });
    </script>
@endsection