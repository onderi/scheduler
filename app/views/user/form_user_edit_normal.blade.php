@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| New User  
@endsection


<!-- Custom CSS !-->
@section('style')

{{ HTML::style('js/plugins/noty/animate.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    User
    <small>Edit</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')

<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>

        <form id="user" method="post" action="{{URL::to('user/u_update/'.$user->user_id)}}" role="form">
            {{Form::token()}}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="col-lg-6 form-group">
                    {{Form::label('fname','First Name')}}
                    {{Form::text('fname',$user->fname,array('class'=>'form-control','placeholder'=>'Please enter your First Name'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('lname','Last Name')}}
                    {{Form::text('lname',$user->lname,array('class'=>'form-control','placeholder'=>'Please enter your Last Name'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('username','Username')}}
                    {{Form::text('username',$user->username,array('class'=>'form-control','disabled'=>'disabled','readonly'=>'readonly','placeholder'=>'Please enter your Username'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('gender','Gender')}}
                    {{Form::select('gender',array('Female'=>'Female','Male'=>'Male'),$user->gender,array('class'=>'form-control'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('email','Email Address')}}
                    {{Form::text('email',$user->email,array('class'=>'form-control','disabled'=>'disabled','readonly'=>'readonly','placeholder'=>'Please enter your Email Address'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('phone','Phone')}}
                    {{Form::text('phone',$user->phone,array('class'=>'form-control','placeholder'=>'Please enter your Phone Number'))}}
                </div>

                <div class="col-lg-6 form-group">
                    {{Form::label('department','Department')}}
                    {{Form::select('department[]',$departments,$current_dept,array('id'=>'department','class'=>'form-control','disabled'=>'disabled'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('notification','Notification')}}
                    {{Form::select('notification',array('1'=>'Enabled','2'=>'Disabled'),$user->notification_status,array('class'=>'form-control'))}}
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div style="padding: 13px;" class="form-group"> 
                    <button type="submit" class="btn btn-success faa-parent animated-hover"><i class="fa fa-thumbs-o-up faa-bounce"></i>  Update Profile</button>
                </div>
            </div>
        </form>

    </div>  



</div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}
<script type="text/javascript">

     $(document).ready(function(){

        {{--//Expand the menu :D--}}
         $('body').removeClass('sidebar-collapse');

         @if ( Session::get('notice') )

          noty({
             text: "Success! {{ Session::get('notice') }}",
             theme: "relax",
             dismissQueue: true,
             progressBar: true,
             timeout: 3000,
             layout: 'topRight',
             type: 'success',
             animation: {
                 open: 'animated bounceInRight', // Animate.css class names
                 close: 'animated bounceOutRight', // Animate.css class names
                 easing: 'swing', // unavailable - no need
                 speed: 500 // unavailable - no need
             }
         });

         @endif

         @if ( Session::get('error') )
            noty({
             text: "Error, {{ Session::get('error')}}",
             theme: "relax",
             dismissQueue: true,
             progressBar: true,
             timeout: 10000,
             layout: 'topRight',
             type: 'error',
             animation: {
                 open: 'animated swing', // Animate.css class names
                 close: 'animated tada', // Animate.css class names
                 easing: 'swing',
                 speed: 500 // opening & closing animation speed
             }
         });
         @endif


     });
</script>

@endsection 