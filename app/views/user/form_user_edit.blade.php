@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| New User  
@endsection


<!-- Custom CSS !-->
@section('style')
<style type="text/css">
    .dropdown-menu>li>a {
        display: block;
        padding: 3px 40px;
        clear: both;
    }

    .multiselect {
        text-align: left;
    }
    .multiselect b.caret {
        position: absolute;
        top: 14px;
        right: 8px;
    }
    .multiselect-group {
        font-weight: bold;
        text-decoration: underline;
    }
</style>
{{ HTML::style('css/bootstrap-multiselect/bootstrap-multiselect.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    User
    <small>Edit</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')

<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>

        <form id="user" method="post" action="{{URL::to('user/update/'.$user->user_id)}}" role="form">
            {{Form::token()}}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="col-lg-6 form-group">
                    {{Form::label('fname','First Name')}}
                    {{Form::text('fname',$user->fname,array('class'=>'form-control','required'=>'required','placeholder'=>'Please enter your First Name'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('lname','Last Name')}}
                    {{Form::text('lname',$user->lname,array('class'=>'form-control','required'=>'required','placeholder'=>'Please enter your Last Name'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('username','Username')}}
                    {{Form::text('username',$user->username,array('class'=>'form-control','required'=>'required','placeholder'=>'Please enter your Username'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('gender','Gender')}}
                    {{Form::select('gender',array('Female'=>'Female','Male'=>'Male'),$user->gender,array('class'=>'form-control','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('email','Email Address')}}
                    {{Form::text('email',$user->email,array('class'=>'form-control','placeholder'=>'Please enter your Email Address','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('phone','Phone')}}
                    {{Form::text('phone',$user->phone,array('class'=>'form-control','placeholder'=>'Please enter your Phone Number'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('status','Status')}}
                    {{Form::select('status',$statuses,$user->status_id,array('class'=>'form-control','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('roles','Roles')}}
                    {{Form::select('roles[]',$roles,$current_role,array('id'=>'roles','class'=>'form-control','multiple'=>'multiple','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('department','Department')}}
                    {{Form::select('department[]',$departments,$current_dept,array('id'=>'department','class'=>'form-control','required'=>'required'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('notification','Notification')}}
                    {{Form::select('notification',array('1'=>'Enabled','2'=>'Disabled'),$user->notification_status,array('class'=>'form-control'))}}
                </div>
                    <div class="col-lg-6 form-group">
                        {{Form::label('user_type','User Type')}}
                        {{Form::select('user_type',$user_type,$user->user_type,array('class'=>'form-control','required'=>'required'))}}
                    </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div style="padding: 13px;" class="form-group"> 
                    <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i>  Save Changes</button>
                </div>
            </div>
        </form>

    </div>  



</div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
<script type="text/javascript">
    $(function() {

        //Department multiselect
        $('#department').multiselect({
            numberDisplayed: 10,
            includeSelectAllOption: true,
            selectAllText: 'Assign All'
        });
        //Roles multiselect
        $('#roles').multiselect({
            numberDisplayed: 2
        });
        
        /*Width Hack*/
        $('.dropdown-toggle').css({width:'100%'});
        $('.btn-group').css({width:'100%'});
        
    });
    //Highlight permissions
    @if($current_dept != '0')
    var values = "{{$current_dept}}";
    $.each(values.split(","), function(i, e) {
        $("#department option[value='" + e + "']").prop("selected", true);
    });
    @endif
    //Highlight permissions
    @if($current_role != '0')
    var values = "{{$current_role}}";
    $.each(values.split(","), function(i, e) {
        $("#roles option[value='" + e + "']").prop("selected", true);
    });
    @endif
     $(document).ready(function(){

        {{--//Expand the menu :D--}}
         $('body').removeClass('sidebar-collapse');
    });
</script>
@endsection 