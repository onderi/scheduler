@extends('layouts.master')

@section('title')
| Room Reservation Approval
@endsection

@section('style')
<style type="text/css">
    .dropdown-menu>li>a {
        display: block;
        padding: 3px 40px;
        clear: both;
    }

    .multiselect {
        text-align: left;
    }
    .multiselect b.caret {
        position: absolute;
        top: 14px;
        right: 8px;
    }
    .multiselect-group {
        font-weight: bold;
        text-decoration: underline;
    }
</style>
{{ HTML::style('css/bootstrap-multiselect/bootstrap-multiselect.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Room Reservation Details
    <small>Define Reservation Details</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')


<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>

        {{Form::open(['method'=>'POST','action'=>['ReservationApprovalController@update',$reservation->reservation_room_id]])}}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                  <div class="form-group col-md-6">
                {{Form::label('name','Program Name:')}}
                {{Form::text('name',$reservation->reservation_name,array('class'=>'form-control','readonly'=>'readonly','disabled'=>'disabled'))}}
                </div>
                <div class="form-group col-md-6">
                {{Form::label('desc','Description:')}}
                {{Form::text('desc ',$reservation->reservation_desc,array('class'=>'form-control','readonly'=>'readonly','disabled'=>'disabled'))}}
                </div>
                @if(is_null($reservation->created_for))
                <div class="form-group col-md-6">
                {{Form::label('user','Created By:')}}
                {{Form::text('user',$reservation->user->username,array('class'=>'form-control','readonly'=>'readonly','disabled'=>'disabled'))}}
                </div>
                @else
                <div class="form-group col-md-6">
                {{Form::label('user','Created For:')}}
                {{Form::text('user',User::find($reservation->created_for)['username'],array('class'=>'form-control','readonly'=>'readonly','disabled'=>'disabled'))}}
                </div>
                @endif
                <div class="form-group col-md-6">
                {{Form::label('dept','Department')}}
                {{Form::text('dept',$reservation->department->dept_name,array('class'=>'form-control','readonly'=>'readonly','disabled'=>'disabled'))}}
                </div>
                <div class="form-group col-md-6">
                {{Form::label('start','Start time')}}
                {{Form::text('start',$reservation->start_time,array('class'=>'form-control','readonly'=>'readonly','disabled'=>'disabled'))}}
                </div>
                <div class="form-group col-md-6">
                {{Form::label('end','End time')}}
                {{Form::text('end',$reservation->end_time,array('class'=>'form-control','readonly'=>'readonly','disabled'=>'disabled'))}}
                </div>
                <div class="form-group col-md-6">
                {{Form::label('room','Room')}}
                {{Form::text('room',$reservation->room->room_name,array('class'=>'form-control','readonly'=>'readonly','disabled'=>'disabled'))}}
                </div>
                <div class="form-group col-md-6">
                {{Form::label('approval','Approval Status')}}
                {{Form::select('approval',$status,$reservation->approval,array('class'=>'form-control'))}}
                </div>


                    <div style="padding: 13px;" class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i>  Update Status</button>
                    </div>
                </div>
                </div>
                {{Form::close()}}
</div>
</div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}
<script>
    $(document).ready(function(){

        {{--//Expand the menu :D--}}
         $('body').removeClass('sidebar-collapse');
    });
</script>
@endsection