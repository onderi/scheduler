@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Room Reservations
@endsection


<!-- Custom CSS !-->
@section('style')
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
{{ HTML::style('js/plugins/noty/animate.css')}}

<style type="text/css">
    .modal-dialog{
        width:300px !important;
    }
</style>
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Room Reservations
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection

<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">

        </div>
        
        <div class="box-body">
                {{ Datatable::table()
                           ->addColumn('Name','Description','Booked by','Booked for','Department','Status','Action')
                           ->setUrl(route('api.r_approvals'))
                           ->setOptions(
                               array(
                               'dom' => "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
                               "sLengthMenu" => "_MENU_ Room Approvals per page",
                               "bLengthChange" => true,
                               'oTableTools' => array(
                                   "sSwfPath" => URL::to('media/swf/copy_csv_xls_pdf.swf'),
                                   "aButtons" => array(
                                       array("sExtends"=>"pdf","sFileName"=>"Room_Reservations_approvals.pdf","sPdfOrientation"=>"landscape","sPdfMessage"=>"SBS Scheduler Room Approvals","sButtonText"=>"Save to PDF"),
                                       array("sExtends"=>"csv","sFileName"=>"Room_Reservations_approvals.csv","sButtonText"=>"Save to CSV"),
                                       array("sExtends"=>"xls","sFileName"=>"Room_Reservations_approvals.xls","sButtonText"=>"Save to Excel"),
                                   )
                               )
                           )
                       )
                           ->render() }}

            </div>
        <div class="box-footer">

        </div>
    </div>

</div>


@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
{{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
{{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
{{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

<script type="text/javascript">
    $(document).ready(function(){
        $('.dataTable').addClass('table-responsive');
        $('.dataTable').addClass('table-striped');
        $('.dataTable').css('width','100%');
        $('.dataTable').attr('role','grid');

        @if ( Session::get('notice') )

               noty({
            text: "Success! {{ Session::get('notice') }}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 3000,
            layout : 'topRight',
            type: 'success',
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight', // Animate.css class names
                easing: 'swing', // unavailable - no need
                speed: 500 // unavailable - no need
            }
        });

        @endif

        @if ( Session::get('error') )
           noty({
            text: "Error, {{ Session::get('error')}}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 10000,
            layout : 'topRight',
            type: 'error',
            animation: {
                open: 'animated swing', // Animate.css class names
                close: 'animated tada', // Animate.css class names
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            }
        });
        @endif
    });

</script>

@endsection
