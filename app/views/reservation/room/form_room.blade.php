<style type="text/css">
    .dropdown-menu>li>a {
        display: block;
        padding: 3px 40px;
        clear: both;
    }

    .multiselect {
        text-align: left;
    }
    .multiselect b.caret {
        position: absolute;
        top: 14px;
        right: 8px;
    }
    .multiselect-group {
        font-weight: bold;
        text-decoration: underline;
    }
</style>


        <div class="box box-warning">
            <div class="box-header">
                <!-- <h3 class="box-title">User Form</h3> !-->
            </div>

            <form id="role" name="room_reservation" method="post" action="#" role="form">
                {{ Form::token(); }}

                <div class="box-body">

                    <div id="error_block">

                    </div>



                    <div class="col-lg-6 form-group">
                        {{Form::label('name','Name')}}
                        {{Form::text('name','',array('class'=>'form-control','placeholder'=>'Please enter the Program Name'))}}
                    </div>
                    <div class="col-lg-6 form-group">
                        {{Form::label('description','Description')}}
                        {{Form::text('description','',array('class'=>'form-control','placeholder'=>'Please enter the Room Reservation Description'))}}
                    </div>
                    <div class="col-lg-6 form-group">
                        {{Form::label('area','Room Area')}}
                        {{Form::select('area',$areas,$area,array('class'=>'form-control'))}}
                    </div>
                    <div class="col-lg-6 form-group">
                        {{Form::label('room','Room')}}
                        <select class="form-control" multiple="multiple" name="room[]" id="room" style="display: none;">
                        </select>
                    </div>
                    <div class="col-lg-6 form-group">
                        {{Form::label('start','Start Date and Time')}}
                        {{Form::text('start',$start_date,array('class'=>'form-control','placeholder'=>'Please select the Start Date & Time','data-date-format' => "YYYY-MM-DD HH:mm"))}}
                    </div>
                    <div class="col-lg-6 form-group">
                        {{Form::label('end','End Time')}}
                        {{Form::text('end',$end_time,array('class'=>'form-control','placeholder'=>'Please select the End Date & Time','data-date-format' => "HH:mm"))}}
                    </div>
                    <div class="col-lg-6 form-group">
                        {{Form::label('department','Department')}}
                        {{Form::select('department',$departments,'',array('class'=>'form-control'))}}
                    </div>
                    @if(Auth::user()->isBookingAdmin())
                        <div class="col-lg-6 form-group">
                            {{Form::label('user','Reservation created for')}}
                            {{Form::select('user',$users,'',array('class'=>'form-control'))}}
                        </div>
                    @endif

                    <div class="col-lg-6 form-group" style="padding-left: 15px;">
                            <input  data-rel="tooltip" type="checkbox" title="Is this booking is an external event?" name="external" id="external" value='1'> External Event
                            <br/><input data-rel="tooltip" title="Defaults to the room area's start and end time" type="checkbox" id="allday" name="allday" value="1"> All Day
                            <br/><input type="checkbox" name="repeat" id="repeat" value='repeat'> Repeat
                    </div>

                    <div  class="repeat_type col-lg-6 form-group" style="display: none;">
                        {{Form::label('rtype','Repeat Type')}}
                        {{Form::select('rtype',$r_type,'',array('class'=>'form-control'))}}
                    </div>
                    <div class="repeat_type col-lg-6 form-group" style="display: none;">
                        {{Form::label('repeat_end','Repeat End Date')}}
                        {{Form::text('repeat_end','',array('class'=>'form-control','placeholder'=>'Please select the Repeat End Date & Time','data-date-format' => "YYYY-MM-DD"))}}
                    </div>

                    <div class="repeat_day col-lg-6 form-group" style="display: none;">
                        {{Form::label('rep_day','Repeat Day')}}
                        <br/>
                        <div style="max-width: 100%;clear:none;">
                            <label class="lbl"><input style="float:left;" class="chkc" name="rep_day[0]" value="Sunday" type="checkbox">Sunday</label>
                            &nbsp;<label class="lbl">&nbsp;<input  style="float:left;" class="chkc" name="rep_day[1]" value="Monday" type="checkbox">Monday</label>
                            &nbsp;<label class="lbl">&nbsp;<input  style="float:left;" class="chkc" name="rep_day[2]" value="Tuesday" type="checkbox">Tuesday</label>
                            &nbsp;<label class="lbl">&nbsp;<input  style="float:left;" class="chkc" name="rep_day[3]" value="Wednesday" type="checkbox">Wednesday</label>
                            &nbsp;<label class="lbl">&nbsp;<input  style="float:left;" class="chkc" name="rep_day[4]" value="Thursday" type="checkbox">Thursday</label>
                            &nbsp;<label class="lbl">&nbsp;<input  style="float:left;" class="chkc" name="rep_day[5]" value="Friday" type="checkbox">Friday</label>
                            &nbsp;<label class="lbl">&nbsp;<input  style="float:left;" class="chkc" name="rep_day[6]" value="Saturday" type="checkbox">Saturday</label>
                        </div>
                    </div>

                    <div class="repeat_times col-lg-6 form-group" style="display: none;" title="Repeat for every n">
                        {{Form::label('rep_times','Repeat Every')}}
                        {{Form::select('rep_times_input',$rep_times,'',array('class'=>'form-control','id'=>'rep_times_input'))}}
                    </div>




                </div><!-- /.box-body -->



            </form>
        </div>


<script type="text/javascript">

    //OnChange Handler
    $('#area').change(function () {
        //Make sure it does not select the room if the user changes the room area.
        setRooms($(this).val(), "0");
    });
    /**
     * Set the rooms from classroom Ajax
     * @param {type} area
     * @returns {undefined}
     */
    function setRooms(area, selected) {
        var area_id = area;
        //get the rooms
        $.ajax({
            url: "{{URL::to('area/getRooms')}}",
            type: 'POST',
            data: {id: area_id}
        }).done(function (data) {
            if (data !== 'none') {
                //Since we return Multiple dataTypes, we need to parse the JSON string, Important or Multiselect will not work
                $('#room').multiselect('dataprovider', JSON.parse(data));
                $('#room').multiselect('rebuild');
                if (selected !== "0"){
                    //Set the selected room
                    $('#room').val(selected);
                    //Refresh the multiselect
                    $("#room").multiselect("refresh");
                }

            } else {
                $('#room').multiselect('dataprovider', [{label: 'Please select', value: ''}]);
            }
        });
    }

    $(function () {
        /** @CodeGems
         * 1 Pet 2:21-25
         * "
         *  This suffering is all part of the work God has given you, Christ, who suffered for you, is your example.
         *  Follow in his steps. He never sinned, never told a lie, never answered back when insulted;
         *  when he suffered he did not threaten to get even;
         *  he left his case in the hands of God who always judges fairly.
         *  He personally carried the load of our sins in his own body when he died on the cross
         *  so that we can be finished with sin and live a good life from now on.
         *  For his wounds have healed ours!
         *  Like sheep you wandered away from God, but now you have returned to your Shepherd,
         *  the guardian of your souls who keeps you safe from all attacks.
         * "
         *
         */

        //check the current date and fire change event :D
        var weekday = "<?php echo $day_number; ?>";
        $("input[name='rep_day[" + weekday + "]']").prop("checked", true).trigger('change');

        //Make sure we load the rooms at the start
        if ($('#area').val() !== '0') {
            var areaId = $('#area').val();
            setRooms(areaId,{{$room}});
        }


        //check the current date and fire change event :D

        //Display the repeat checkbox
        $('#repeat').click(function () {
            if ($(this).is(":checked")) {
                //show the repeat types
                $(".repeat_type").slideDown("slow");

            } else {
                $(".repeat_type").slideUp("fast");
                $('#rtype').val('1');
                if ($(".repeat_day").is(':visible')) {
                    $(".repeat_day").slideUp("fast");
                }

                //Show the number of times
                if ( $('.repeat_times').is(":visible") ){
                    $(".repeat_times").slideUp("fast");
                    updateTitle('none',true);
                }
            }
        });

        //OnChange Handle
        $("#rep_times_input").change( function (){

            var selectedRTimes = $("#rep_times_input option:selected").val();
            //Set the label to this
            updateTitle(selectedRTimes,false);
        });

        //OnChange Handler
        $('#rtype').change(function () {

            var startDate = $('#start').val();
            var endDate = $('#end').val();


            var selected = $("#rtype option:selected").text();

            //None
            if( selected === "NONE" ){
                if ($(".repeat_day").is(':visible')) {
                    $(".repeat_day").slideUp("fast");
                }

                //Show the number of times
                if ( $('.repeat_times').is(":visible") ){
                    $(".repeat_times").slideUp("fast");
                    updateTitle('none',true);
                }

            }

            //Daily
            if( selected === "DAILY" ){
                //Hide the days
                if ($(".repeat_day").is(':visible')) {
                    $(".repeat_day").slideUp("fast");
                }

                //Show the number of times
                if ( $('.repeat_times').is(":hidden") ){
                    $(".repeat_times").slideDown("slow");
                }

                //change the title
                updateTitle("Repeat every 1 Day",true);
            }

            //Weekly booking
            if( selected === "WEEKLY" ){
                if ( $(".repeat_day").is(':hidden') ) {
                    $(".repeat_day").slideDown("slow");
                }

                //Show the number of times
                if ( $('.repeat_times').is(":hidden") ){
                    $(".repeat_times").slideDown("slow");
                }
                //change the title
                updateTitle("Repeat every 1 Week",true);
            }

            //Monthly
            if( selected === "MONTHLY"){
                if ($(".repeat_day").is(':visible')) {
                    $(".repeat_day").slideUp("fast");
                }

                //Show the number of times
                if ( $('.repeat_times').is(":hidden") ){
                    $(".repeat_times").slideDown("slow");
                }

                //change the title
                updateTitle("Repeat every 1 Month",true);
            }

            //Yearly
            if( selected === "YEARLY"){
                if ($(".repeat_day").is(':visible')) {
                    $(".repeat_day").slideUp("fast");
                }

                //Show the number of times
                if ( $('.repeat_times').is(":hidden") ){
                    $(".repeat_times").slideDown("slow");
                }
                //change the title
                updateTitle("Repeat every 1 Year",true);
            }


        });

        //Date Time Picker
        $('#start').datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
            minuteStep: 30,
            startDate: moment().subtract('days', 1),
            autoclose : true,

        });

        //End date
        $('#end').datetimepicker({
            format: 'hh:ii',
            minuteStep: 30,
            autoclose: true
        });

        $('#repeat_end').datetimepicker({
            startDate: moment().subtract('days', 1),
            format: 'yyyy-mm-dd',
            startView: 2,
            minView: 2,
            maxView: 3,
            autoclose: true
        });

        //Room multiselect
        $('#room').multiselect({
            numberDisplayed: 5,
            includeSelectAllOption: true,
            selectAllText: 'Book All'
        });

        /*Width Hack*/
        $('.dropdown-toggle').css({width: '100%'});
        $('.btn-group').css({width: '100%'});


        $("#allday").change(function () {
            //Store the stuff on a local DB
            if ($('#start').val() !== "")
                store.set('start', $('#start').val());

            if ($('#end').val() !== "")
                store.set('end', $('#end').val());
            //If AllDay is Checked
            if (this.checked) {
                $('#start').prop('readonly', true);
                $('#end').val('').prop('readonly', true);
            } else {
                $('#start').val(store.get('start')).prop('readonly', false);
                $('#end').val(store.get('end')).prop('readonly', false);
            }
        });
    });

    function updateTitle(title,whole) {
        //if update the whole title
        if (whole) {

            if (title === 'none') {
                $(".repeat_times").attr('title', '');
                $("label[for='rep_times']").text("Repeat every");
            } else {
                //change the title
                $(".repeat_times").attr('title', title);
                $("label[for='rep_times']").text(title);

                //set repeat_times to 1
                $("#rep_times_input").val("1");
            }

        } else {

            //if only update the number
            var current = $(".repeat_times").attr('title');
            var new_str = current.replace(/\d+/, title);

            if (parseInt(title) > 1) {
                if (new_str.substring(new_str.length, new_str.length - 1) !== 's')
                    new_str = new_str + 's';
            } else {
                new_str = new_str.slice(0, -1);
            }

            //do replace
            $(".repeat_times").attr('title', new_str);
            $("label[for='rep_times']").text(new_str);

        }

    }

</script>