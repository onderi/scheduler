@extends('layouts.master')

        <!-- Page Title !-->
@section('title')
    | Room Reservations
    @endsection


            <!-- Custom CSS !-->
    @section('style')
            <!-- CSS Datatables !-->
    {{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
    {{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
    {{ HTML::style('css/datatables/TableTools.css')}}
    {{ HTML::style('js/plugins/ladda/ladda-themeless.min.css')}}
     {{ HTML::style('js/plugins/noty/animate.css')}}

            <style type="text/css">
                .modal-dialog{
                    width:300px !important;
                }
            </style>
    @endsection

            <!-- Content Heading !-->
@section('content_head')
    <h1>
        Room Reservations
    </h1>
    @endsection


            <!-- Breadcrumbs !-->
    @section('breadcrumb')
    {{ Breadcrumbs::render() }}
    @endsection

            <!-- Page Content !-->
    @section('page_content')
    <div class="col-md-12">
        <div class="box box-warning">
            <div class="box-header">
            </div>

            <div class="box-body">

                    {{ Datatable::table()
                        ->addColumn('Room Name','Reservation Name','Duration','Reserve Date','Booked By','Department','Action')
                        ->setUrl(route('api.r_rooms'))
                        ->setOptions(
                            array(
                            'dom' => "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
                            "sLengthMenu" => "_MENU_ Room Reservations per page",
                            "bLengthChange" => true,
                            'oTableTools' => array(
                                "sSwfPath" => URL::to('media/swf/copy_csv_xls_pdf.swf'),
                                "aButtons" => array(
                                    array("sExtends"=>"pdf","sFileName"=>"Room_Reservations.pdf","sPdfOrientation"=>"landscape","sPdfMessage"=>"SBS Scheduler Room Reservations","sButtonText"=>"Save to PDF"),
                                    array("sExtends"=>"csv","sFileName"=>"Room_Reservations.csv","sButtonText"=>"Save to CSV"),
                                    array("sExtends"=>"xls","sFileName"=>"Room_Reservations.xls","sButtonText"=>"Save to Excel"),
                                )
                            )
                        )
                    )
                        ->render() }}

            </div>
            <div class="box-footer">

            </div>
        </div>

    </div>


    @endsection

    <!-- Custom Scripts !-->
    @section('scripts')
    <!-- JS Datatables !-->
    {{ HTML::script('js/jquery-ui-1.10.3.js')}}
    {{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
    {{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
    {{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
    {{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
    {{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
    {{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
    {{HTML::script('js/plugins/spin/spin.min.js')}}
    {{HTML::script('js/plugins/ladda/ladda.min.js')}}
    {{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

    <script type="text/javascript">
        {{-- Delete function --}}
        $(document).on('click','.delete_rr',function(){
            var d_id = $(this).attr('id');
            //confirmation
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_WARNING,
                title: 'Notification',
                message: 'Are you sure?',
                closable: false,
                cssClass: 'small-dialog',
                buttons: [{
                    label: ' Yes',
                    id: 'delete_rr_button',
                    icon: 'fa fa-trash',
                    cssClass: 'btn-danger',
                    action: function(dialogDel) {
                        var l_r_del = Ladda.create( document.querySelector('#delete_rr_button') );
                        l_r_del.start();
                        //do ajax
                        $.ajax({
                            type : 'post',
                            url : "{{URL::to('reservation/room/delete_aj')}}",
                            data : {d_string:d_id},
                            success:function(data){
                                {{--Remove the row --}}
                                if( data === '1' ) {
                                    BootstrapDialog.show({
                                        type: BootstrapDialog.TYPE_SUCCESS,
                                        title: 'Success',
                                        message: 'Reservation Deleted Successfully.',
                                        closable: true,
                                        cssClass: 'small-dialog',
                                        buttons: [{
                                            icon: 'fa fa-cog fa-spin',
                                            label: ' Ok',
                                            action: function(dialog_delete) {
                                                dialog_delete.close();
                                            }
                                        }]
                                    });

                                    $('#' + d_id).closest('tr').remove();
                                }else{
                                    BootstrapDialog.show({
                                        type: BootstrapDialog.TYPE_DANGER,
                                        title: 'Error',
                                        message: 'Unable to delete Reservation, Integrity constraints enforced ',
                                        closable: true,
                                        cssClass: 'small-dialog',
                                        buttons: [{
                                            icon: 'fa fa-cog fa-spin',
                                            label: ' Ok',
                                            action: function(dialog_delete) {
                                                dialog_delete.close();
                                            }
                                        }]
                                    });
                                }
                            },
                            complete:function(){
                                l_r_del.stop();
                                dialogDel.close();
                            },
                            error: function (error){
                            }
                        });
                    }
                },
                    {
                        label: 'No',
                        icon: 'fa fa-times',
                        cssClass: 'btn-primary',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }
                ]
            });
        });

        $(document).ready(function(){

            $('.dataTable').addClass('table-responsive');
            $('.dataTable').addClass('table-striped');
            $('.dataTable').css('width','100%');
            $('.dataTable').attr('role','grid');

            @if ( Session::get('notice') )

           noty({
                text: "Success! {{ Session::get('notice') }}",
                theme: "relax",
                dismissQueue: true,
                progressBar : true,
                timeout     : 3000,
                layout : 'topRight',
                type: 'success',
                animation: {
                    open: 'animated bounceInRight', // Animate.css class names
                    close: 'animated bounceOutRight', // Animate.css class names
                    easing: 'swing', // unavailable - no need
                    speed: 500 // unavailable - no need
                }
            });

            @endif

            @if ( Session::get('error') )
               noty({
                text: "Error, {{ Session::get('error')}}",
                theme: "relax",
                dismissQueue: true,
                progressBar : true,
                timeout     : 10000,
                layout : 'topRight',
                type: 'error',
                animation: {
                    open: 'animated swing', // Animate.css class names
                    close: 'animated tada', // Animate.css class names
                    easing: 'swing',
                    speed: 500 // opening & closing animation speed
                }
            });
            @endif

        });
    </script>
    @endsection
