<style type="text/css">
    .dropdown-menu > li > a {
        display: block;
        padding: 3px 40px;
        clear: both;
    }
    .multiselect {
        text-align: left;
    }
    .multiselect b.caret {
        position: absolute;
        top: 14px;
        right: 8px;
    }
    .multiselect-group {
        font-weight: bold;
        text-decoration: underline;
    }
</style>
<h4>
    <small>Edit {{$reservation->reservation_name}}</small>
</h4>
<div class="box box-warning" style="height: 100%;">
    <div class="box-header">
        <!-- <h3 class="box-title">User Form</h3> !-->
    </div>
    {{Form::open(['method'=>'POST'])}}
    <div class="box-body">
        <div id="error_block">

        </div>
        <div class="col-lg-6 form-group">
            {{Form::label('name','Name')}}
            {{Form::text('name',$reservation->reservation_name,array('class'=>'form-control','placeholder'=>'Please enter the Program Name'))}}
        </div>
        <div class="col-lg-6 form-group">
            {{Form::label('description','Description')}}
            {{Form::text('description',$reservation->reservation_desc,array('class'=>'form-control','placeholder'=>'Please enter the Room Reservation Description'))}}
        </div>
        <div class="col-lg-6 form-group">
            {{Form::label('area','Room Area')}}
            {{Form::select('area',$areas,$area,array('class'=>'form-control'))}}
        </div>
        <div class="col-lg-6 form-group">
            {{Form::label('room','Room')}}
            {{Form::select('room',$rooms,$room,array('class'=>'form-control'))}}
        </div>
        <div class="col-lg-6 form-group">
            {{Form::label('start','Start Date and Time')}}
            {{Form::text('start',$start_date,array('class'=>'form-control','placeholder'=>'Please select the Start Date & Time','data-date-format' => "YYYY-MM-DD HH:mm"))}}
        </div>
        <div class="col-lg-6 form-group">
            {{Form::label('end','End Time')}}
            {{Form::text('end',$end_time,array('class'=>'form-control','placeholder'=>'Please select the End Date & Time','data-date-format' => "HH:mm"))}}
        </div>
        <div class="col-lg-6 form-group">
            {{Form::label('department','Department')}}
            {{Form::select('department',$departments,$department,array('class'=>'form-control'))}}
        </div>
        @if( Auth::user()->isBookingAdmin() || Auth::user()->isSysAdmin() )
            <div class="col-lg-6 form-group">
                {{Form::label('user','Reservation created for')}}
                {{Form::select('user',$users,$user,array('class'=>'form-control'))}}
            </div>
            <div class="col-lg-6 form-group" style="padding-left: 15px;">
                <input data-rel="tooltip" @if($external == '1')  checked="checked" @endif type="checkbox" title="Is this booking is an external event?" name="external"
                       id="external" value='1'> External Event
                <br/><input data-rel="tooltip" title="Detail not editable" type="checkbox"
                            id="allday" name="allday" value="1"> All Day
                <br/><input disabled type="checkbox" title="Detail not editable" name="repeat" id="repeat" value='repeat'> Repeat
            </div>
        @endif
            {{Form::close()}}
    </div>
</div>
<script type="text/javascript">

    //OnChange Handler
    $('#area').change(function () {
        //Make sure it does not select the room if the user changes the room area.
        setRooms($(this).val(), "0");
    });
    /**
     * Set the rooms from classroom Ajax
     * @param {type} area
     * @returns {undefined}
     */
    function setRooms(area, selected) {
        var area_id = area;
        //get the rooms
        $.ajax({
            url: "{{URL::to('area/getRooms')}}",
            type: 'POST',
            data: {id: area_id}
        }).done(function (data) {
            if (data !== 'none') {
                //Since we return Multiple dataTypes, we need to parse the JSON string, Important or Multiselect will not work
                $('#room').multiselect('dataprovider', JSON.parse(data));
                $('#room').multiselect('rebuild');
                if (selected !== "0") {
                    //Set the selected room
                    $('#room').val(selected);
                    //Refresh the multiselect
                    $("#room").multiselect("refresh");
                }
            } else {
                $('#room').multiselect('dataprovider', [{label: 'Please select', value: ''}]);
            }
        });
    }
    $(function () {
        /*
         * 1 Pet 2:21-25
         * "
         *  This suffering is all part of the work God has given you, Christ, who suffered for you, is your example.
         *  Follow in his steps. He never sinned, never told a lie, never answered back when insulted;
         *  when he suffered he did not threaten to get even;
         *  he left his case in the hands of God who always judges fairly.
         *  He personally carried the load of our sins in his own body when he died on the cross
         *  so that we can be finished with sin and live a good life from now on.
         *  For his wounds have healed ours!
         *  Like sheep you wandered away from God, but now you have returned to your Shepherd,
         *  the guardian of your souls who keeps you safe from all attacks.
         * "
         *
         */
        //Make sure we load the rooms at the start
        if ($('#area').val() !== '0') {
            var areaId = $('#area').val();
            setRooms(areaId, {{$room}});
        }
        //Date Time Picker
        $('#start').datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
            minuteStep: 30,
            startDate: moment().subtract('days', 1),
            autoclose : true,

        });

        //End date
        $('#end').datetimepicker({
            format: 'hh:ii',
            startView: 1,
            minView: 0,
            maxView: 1,
            pickDate: false,
            minuteStep: 30,
            autoclose: true
        });

        //Room multiselect
        $('#room').multiselect({
            numberDisplayed: 5,
            includeSelectAllOption: true,
            selectAllText: 'Book All'
        });
        /*Width Hack*/
        $('.dropdown-toggle').css({width: '100%'});
        $('.btn-group').css({width: '100%'});
        $("#allday").change(function () {
            //Store the stuff on a local DB
            if ($('#start').val() !== "")
                store.set('start', $('#start').val());

            if ($('#end').val() !== "")
                store.set('end', $('#end').val());
            //If AllDay is Checked
            if (this.checked) {
                $('#start').prop('readonly', true);
                $('#end').val('').prop('readonly', true);
            } else {
                $('#start').val(store.get('start')).prop('readonly', false);
                $('#end').val(store.get('end')).prop('readonly', false);
            }
        });
    });
</script>