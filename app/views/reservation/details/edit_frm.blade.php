@extends('layouts.master')

@section('title')
    | Room Reservation Details
@endsection

@section('style')
    <style type="text/css">
        .dropdown-menu > li > a {
            display: block;
            padding: 3px 40px;
            clear: both;
        }

        .multiselect {
            text-align: left;
        }

        .multiselect b.caret {
            position: absolute;
            top: 14px;
            right: 8px;
        }

        .multiselect-group {
            font-weight: bold;
            text-decoration: underline;
        }

        .select2-container .select2-selection--single .select2-selection__rendered {
            overflow: visible !important;
        }

        .modal-dialog{
            width:300px !important;
        }

    </style>
    {{--{{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}--}}
    {{ HTML::style('js/plugins/smalot-datetimepicker/bootstrap-datetimepicker.min.css')}}

    {{ HTML::style('js/plugins/bootstrap-slider/slider.css')}}
    {{ HTML::style('css/bootstrap-multiselect/bootstrap-multiselect.css')}}
    {{ HTML::style('js/plugins/select2/select2.css')}}
    {{ HTML::style('js/plugins/noty/animate.css')}}



@endsection

<!-- Content Heading !-->
@section('content_head')
    <h1>
        Room Reservation Details
        <small>Define Reservation Details</small>
    </h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
    {{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')


    <div class="col-md-12">
        <div class="box box-warning">
            <div class="box-header">

                <div style="padding-left:13px;">
                   {{Form::open(array('method'=>'POST','action'=>'ReservationDetailsController@destroy','id' => 'reset_details_frm'))}}
                        <button id="reset_details" class="btn btn-danger faa-parent animated-hover">
                            <i class="fa fa-crosshairs faa-burst"></i> Reset
                        </button>
                        {{Form::hidden('rr_id',$reservation)}}
                    {{Form::close()}}

                </div>

            </div>

            {{Form::open(array('method'=>'POST','action'=>'ReservationDetailsController@update'))}}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Error!</b>
                        <ul>
                            @foreach( $errors->all() as $message )
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>


                @endif

                <div class="form-group col-md-12">
                    {{Form::label('rr','Reservation ')}}
                    {{Form::text('rr_name',$r_name,array('class'=>'form-control','readonly'=>'readonly','required'=>'required'))}}
                    {{Form::hidden('rr',$reservation)}}
                </div>


                <div class="form-group col-md-6">
                    {{Form::label('owner','Owner')}}
                    {{Form::text('owner',$owner,array('class'=>'form-control','readonly'=>'readonly','required'=>'required'))}}
                    {{Form::hidden('user_id',$user_id)}}
                </div>


                <div class="form-group col-md-6">
                    {{Form::label('ref','Reference Number')}}
                    {{Form::text('ref','Ref: '.$ref,array('class'=>'form-control','readonly'=>'readonly','required'=>'required'))}}
                </div>

         <script type="text/javascript">

             //Date Time Picker
             var day = '<?=date('Y-m-d', strtotime($details[0]->start_time))?>';
             var min_date = day + ' ' + '<?=date('H:i', strtotime($details[0]->start_time));?>';
             var max_date = day + ' ' + '<?=date('H:i', strtotime($details[count($details) - 1]->end_time));?>';
             //var split=min_date.split(' ');
             //var end=split[0]+'T'+max_date;
             var minDateNoDiff = new Date(min_date);
             //ensure that there is atleast a difference of 20 minutes between selections
             var minDate = new Date(minDateNoDiff.getTime() + (20 * 60000));
             //ensure that there is atleast a difference of 20 minutes between selections
             var minDateEnd = new Date(minDate.getTime() + (20 * 60000));
             var maxDate = new Date(max_date);

             var month = minDate.getMonth() + 1;
             var def_min = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + ' ' + minDate.getHours() + ':' + minDate.getMinutes();
             var def_min_end = minDateEnd.getFullYear() + '-' + month + '-' + minDateEnd.getDate() + ' ' + minDateEnd.getHours() + ':' + minDateEnd.getMinutes();
             var def_max = maxDate.getFullYear() + '-' + month + '-' + maxDate.getDate() + ' ' + maxDate.getHours() + ':' + maxDate.getMinutes();
             var spinOpts = {
                 lines: 13 // The number of lines to draw
                 , length: 28 // The length of each line
                 , width: 14 // The line thickness
                 , radius: 42 // The radius of the inner circle
                 , scale: 1 // Scales overall size of the spinner
                 , corners: 1 // Corner roundness (0..1)
                 , color: '#000' // #rgb or #rrggbb or array of colors
                 , opacity: 0.25 // Opacity of the lines
                 , rotate: 0 // The rotation offset
                 , direction: 1 // 1: clockwise, -1: counterclockwise
                 , speed: 1 // Rounds per second
                 , trail: 60 // Afterglow percentage
                 , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
                 , zIndex: 2e9 // The z-index (defaults to 2000000000)
                 , className: 'spinner' // The CSS class to assign to the spinner
                 , top: '50%' // Top position relative to parent
                 , left: '50%' // Left position relative to parent
                 , shadow: false // Whether to render a shadow
                 , hwaccel: false // Whether to use hardware acceleration
                 , position: 'absolute' // Element positioning
             };

         </script>
        <div id="segments">
         @for($counter = 0 ; $counter < count($details); $counter++ )
                <?php
                      if($counter == 0){
                           $div_id = 'segments';
                        }else{
                           $div_id = "seg_".$counter;
                        }
                ?>

                        <div id="{{"seg_".$counter}}" class="container-fluid col-md-12 group" style="border:1px solid blue;border-radius:4px;margin-bottom:13px;">
                            @if( $counter >= 1 )
                                <button class='close'  aria-hidden='true' onClick= 'remove_segment({{$counter}});'> <i class="fa fa-close"></i> </button>
                            @endif
                        <?php
                            $category = $details[$counter]->category;
                            $courses = Course::where('category','=',$category)->lists('course_title','course_id');

                        ?>
                        <div class="form-group col-md-12">
                            {{Form::label('ccategory','1. Course Category')}}
                            {{Form::select('ccategory[]',$ccategory,$category,array('class'=>'form-control ccategory','required'=>'required','id' =>  "ccategory_".$counter))}}
                        </div>

                        <div class="form-group col-md-6">
                            {{Form::label('courses','2. Course')}}
                            {{Form::select('courses[]',$courses,$details[$counter]->course_id,array("class"=>"form-control course",'required'=>'required','id' => "courses_".$counter))}}
                        </div>

                        <div class="form-group col-md-6">
                            {{Form::label('start','3. Start Time')}}

                            {{Form::text('start[]',date('Y-m-d H:i',strtotime($details[$counter]->start_time)),array('id' => "seg_".$counter.'_start','required'=>'required','class'=>'form-control start','data-date-format' => "YYYY-MM-DD HH:mm",'onkeydown'=>'return false','autocomplete' => 'off'))}}
                        </div>

                        <div class="form-group col-md-6">
                            {{Form::label('faculty','5. Faculty')}}
                            <?php $faculty_values = explode(",",$details[$counter]->faculty) ?>
                            {{Form::select('faculty[]',$faculty,$faculty_values,array('class'=>'form-control faculty','multiple'=> 'true','required'=>'required','id' => "faculty_".$counter))}}

                        </div>

                        <div class="form-group col-md-6">
                            {{Form::label('end','4. End Time')}}
                            {{Form::text('end[]',date('H:i',strtotime($details[$counter]->end_time)),array('id' => "seg_".$counter.'_end','class'=>'form-control end','data-date-format' => "HH:mm",'required'=>'required','onkeydown'=>'return false','autocomplete' => 'off'))}}
                        </div>

                    @if( count($faculty_values) == 2 )
                         <?php
                                    $hours = explode(',',$details[$counter]->hours);
                          ?>
                        <div id="faculty-times_{{$counter}}" class="col-md-12" >
                            <label id="time-taught_0" class="col-md-12">
                                Kindly define the hours taught by each faculty member i.e. <?php echo User::getNameString($faculty_values); ?>. Note that this is based on the time selected above ( <?php echo array_sum($hours).' minutes' ?>)
                            </label>

                            <div class="col-md-3">
                                <label id="first-faculty-label_{{$counter}}"><small>(in minutes) : </small></label>
                                <input id="first-faculty_{{$counter}}" name="first_faculty[]" class="first_faculty" value="{{$hours[0]}}"/>
                            </div>

                            <div class="col-md-3">
                                <label id="second-faculty-label_{{$counter}}"><small>(in minutes) : </small></label>
                                <input id="second-faculty_{{$counter}}" name="second_faculty[]" class="second_faculty" value="{{$hours[1]}}" />
                            </div>


                        </div>

                        <div class="form-group col-md-6">
                            {{Form::label('desc','Description')}}
                            {{Form::textarea('desc[]',$details[$counter]->reservation_desc,array('class'=>'form-control','placeholder'=>'Reservation Detail Description','maxlength'=>'200','rows'=>'2','required'=>'required'))}}
                        </div>
                    @else
                            <div id="faculty-times_{{$counter}}" class="col-md-12" style="display: none;">
                                <label id="time-taught_0" class="col-md-12">

                                </label>

                                <div class="col-md-3">
                                    <label id="first-faculty-label_{{$counter}}"><small>(in minutes) : </small></label>
                                    <input id="first-faculty_{{$counter}}" name="first_faculty[]" class="first_faculty" />
                                </div>

                                <div class="col-md-3">
                                    <label id="second-faculty-label_{{$counter}}"><small>(in minutes) : </small></label>
                                    <input id="second-faculty_{{$counter}}" name="second_faculty[]" class="second_faculty"  />
                                </div>


                            </div>

                            <div class="form-group col-md-6">
                                {{Form::label('desc','Description')}}
                                {{Form::textarea('desc[]',$details[$counter]->reservation_desc,array('class'=>'form-control','placeholder'=>'Reservation Detail Description','maxlength'=>'200','rows'=>'2','required'=>'required'))}}
                            </div>

                    @endif

                        <div class="form-group col-md-6">
                            {{Form::label('status','Status')}}
                            {{Form::select('status[]',$status,$details[$counter]->reservation_status,array('class'=>'form-control','required'=>'required','id' => "status_".$counter))}}
                        </div>


                    </div>

        @endfor
            </div>

                    <button id="add_segment_btn" type="button" onClick="add_segment();" value="Add segment" class="btn btn-warning btn-outline"
                            style="margin-bottom:13px;float:right;"> <i class="fa fa-plus-circle"></i> Add Segment</button>

                    <div class="col-md-12">
                        <div style="padding-top: 15px;" class="form-group">
                            <button type="submit" class="btn btn-success faa-parent animated-hover"><i class="fa fa-thumbs-o-up faa-bounce"></i>  Save Changes</button>
                        </div>
                    </div>
            </div>
            {{Form::close()}}


        </div>
    </div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')
    {{ HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}
    {{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
    {{ HTML::script('js/plugins/select2/select2.js')}}
    {{ HTML::script('js/plugins/spin/spin.min.js')}}
    {{ HTML::script('js/plugins/bootstrap-slider/bootstrap-slider.js')}}
    {{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}


    {{--{{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}--}}
    {{ HTML::script('js/plugins/smalot-datetimepicker/bootstrap-datetimepicker.min.js')}}

    @for($jcounter = 0 ; $jcounter < count($details); $jcounter++ )
        @if( $jcounter >= 1 )
            <script type="text/javascript">

        var sel = "<?php echo $jcounter - 1; ?>";
        var set_1 = $('#seg_' + sel + '_start').val();
        var split = set_1.split(' ');
        var next_start = split[0] + ' ' + $('#seg_' + sel + '_end').val();
        $('#seg_' + {{$jcounter}} + '_start').val(next_start);

        $('#seg_' + {{$jcounter}} + '_start').datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
            startView: 1,
            minView: 0,
            maxView: 1,
            startDate: def_min,
            endDate: def_max,
            autoclose: true,
            minuteStep: 10

        });
        $('#seg_' + {{$jcounter}} + '_end').datetimepicker({
            format: 'hh:ii',
            startView: 1,
            minView: 0,
            maxView: 1,
            startDate: def_min_end,
            endDate: def_max,
            autoclose: true,
            minuteStep: 10
        });
        $('#seg_' + {{$jcounter}} + '_start').datetimepicker().on('show', function (ev) {
            var last_end = $('#seg_' + sel + '_end').val();
            var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + last_end;
            $('#seg_' + {{$jcounter}} + '_start').datetimepicker('setStartDate', new_start);
        });
        $('#seg_' + {{$jcounter}} + '_end').datetimepicker().on('show', function (ev) {
            var last_end = $('#seg_' + sel + '_end').val();
            var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + last_end;
            $('#seg_' + {{$jcounter}} + '_end').datetimepicker('setStartDate', new_start);
        });
        $('#seg_' + {{$counter}} + '_start').datetimepicker().on('changeDate', function (ev) {
            var set_1 = $('#seg_' + {{$jcounter}} + '_start').val();
            var split = set_1.split(' ');
            var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + split[1];
            $('#seg_' + {{$jcounter}} + '_end').datetimepicker('setStartDate', new_start);
        });
        $('#seg_' + {{$jcounter}} + '_start').datetimepicker().on('changeDate', function (ev) {
            var set_1 = $('#seg_' + {{$jcounter}} + '_start').val();
            var split = set_1.split(' ');
            var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + split[1];
            $('#seg_' + {{$jcounter}} + '_end').datetimepicker('setStartDate', new_start);
        });
        $('#seg_' + {{$jcounter}} + '_end').datetimepicker().on('changeDate', function (ev) {
            var set_1 = $('#seg_' + get_seg() + '_end').val();
            var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + set_1;
            var next = {{$jcounter}} + 1;
            $('#seg_' + next + '_start').val(new_start);
            $('#seg_' + next + '_start').datetimepicker('setStartDate', new_start);
        });



        $('.courses, .status').select2({
            theme: "classic"
        });

        $('.ccategory').select2({
            theme: "classic"
        });

        $('.faculty').select2({
            theme : 'classic',
            multiple : true,
            tags: false,
            maximumSelectionLength: 2
        });


    </script>

        @endif
    @endfor

    <script type="text/javascript">

        //set the store
        store.set('seg_num',"<?php echo $counter - 1; ?>");

        function set_end(val) {
            val = new Date(val);
            val = val.setMinutes(val.getMinutes + 30);
            var mon = val.getMonth + 1;
            return val.getFullYear() + '-' + mon + '-' + val.getDate() + ' ' + val.getHours() + ':' + val.getMinutes();
        }

        $('#seg_0_start').datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
            startView: 1,
            minView: 0,
            maxView: 1,
            startDate: def_min,
            endDate: def_max,
            autoclose: true,
            minuteStep: 10
        });

        $('#seg_0_end').datetimepicker({
            format: 'hh:ii',
            startView: 1,
            minView: 0,
            maxView: 1,
            startDate: def_min_end,
            endDate: def_max,
            autoclose: true,
            minuteStep: 10
        });

        $('#seg_0_start').datetimepicker().on('changeDate', function (ev) {
            var set_1 = $('#seg_0_start').val();
            var split = set_1.split(' ');
            var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + split[1];
            $('#seg_0_end').datetimepicker('setStartDate', new_start);

        });

        $('#seg_0_end').datetimepicker().on('changeDate', function (ev) {

            var set_1 = $('#seg_0_end').val();
            var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + set_1;
            $('#seg_1_start').val(new_start);
            $('#seg_1_start').datetimepicker('setStartDate', new_start);


        });


        function set_seg(operation) {
            var seg = parseInt(store.get('seg_num'));
            var new_seg ;
            if ( operation == "add" ){
                new_seg = seg + 1;
            }else{
                new_seg = seg - 1;
            }

            store.set('seg_num',new_seg);
        }

        function get_seg(){
            return store.get('seg_num');
        }

        function add_segment() {


            //check the previous segment date is not equal to max
            var selected_value_last = $('#seg_' + get_seg() + '_end').val();

            var set_1 = $('#seg_'+get_seg()+'_start').val();
            var split = set_1.split(' ');

            var last_selected_last = split[0] + ' ' + selected_value_last ;

            var last_selected_with_diff = new Date(new Date(last_selected_last).getTime() + (20 * 60000));

            if ( new Date(last_selected_with_diff).getTime() <=  new Date(max_date).getTime() ){

                set_seg('add');

                var sel = get_seg() - 1;
                var category_selected = $('#ccategory_'+sel).val();
                var segment = "<div id='seg_" + get_seg() + "' class='container-fluid col-md-12 group' style='border:1px solid blue;border-radius:4px;margin-bottom:13px;'><button class='close'  aria-hidden='true' onClick= 'remove_segment(" + get_seg() + ");'><i class='fa fa-close'></i> </button><div class='form-group col-md-12'><label for='ccategory[]'>Course Category</label><select name='ccategory[]' id='ccategory_"+get_seg()+"' class='form-control ccategory' required><?php foreach ($ccategory as $key => $value) {
                        echo '<option value=' . $key . '>' . addslashes($value) . '</option>';
                    } ?></select></div><div class='form-group col-md-6'><label for='course[]'>Course</label><select id='courses_"+get_seg()+"' name='courses[]' class='form-control courses' style='' required><?php foreach ([] as $key => $value) {
                        echo '<option value=' . $key . '>' . addslashes($value) . '</option>';
                    }?></select></div><div class='form-group col-md-6'><label for='start'>Start Time</label><input type='text' autocomplete='off' onkeydown='return false' name='start[]' data-date-format= 'YYYY-MM-DD HH:mm' id='seg_" + get_seg() + "_start' class='form-control' value='<?=date('Y-m-d H:i', strtotime($details[$counter-1]->start_time))?>' required/></div><div class='form-group col-md-6'><label for='faculty'>Faculty</label><select name='faculty[]' id='faculty_"+get_seg()+"'  multiple='' required class='faculty form-control' placeholder='Faculty'><?php foreach ($faculty as $key => $value) {
                        echo '<option value=' . $key . '>' . $value . '</option>';
                    }?></select></div><div class='form-group col-md-6'><label for='end'>End Time</label><input type='text' onkeydown='return false' autocomplete='off' id='seg_" + get_seg() + "_end' name='end[]' data-date-format='HH:mm' class='form-control end' value='<?=date('H:i', strtotime($details[$counter-1]->end_time))?>' required/></div><div id='faculty-times_"+get_seg()+"' class='col-md-12' style='display: none;'><label id='time-taught_"+get_seg()+"' class='col-md-12'></label><div class='col-md-3'><label id='first-faculty-label_"+get_seg()+"'><small>(in minutes) : </small></label><input id='first-faculty_"+get_seg()+"' name='first_faculty[]' class='first_faculty' /></div><div class='col-md-3'><label id='second-faculty-label_"+get_seg()+"'><small>(in minutes) : </small></label><input id='second-faculty_"+get_seg()+"' name='second_faculty[]' class='second_faculty' /></div></div><div class='form-group col-md-6'><label for='desc[]'> Description</label><textarea name='desc[]' placeholder='Reservation Detail Description' maxlength='200' rows='2' class='form-control' required></textarea></div><div class='form-group col-md-6'><label for='status[]'> Status</label><select name='status[]' class='form-control status' required><?php foreach ($status as $key => $value) {
                        echo '<option value=' . $key . '>' . $value . '</option>';
                    }?></select></div></div>";

                jQuery('#segments').append(segment);

                var set_1 = $('#seg_' + sel + '_start').val();
                var split = set_1.split(' ');
                var next_start = split[0] + ' ' + $('#seg_' + sel + '_end').val();
                $('#seg_' + get_seg() + '_start').val(next_start);

                $('#seg_' + get_seg() + '_start').datetimepicker({
                    format: 'yyyy-mm-dd hh:ii',
                    startView: 1,
                    minView: 0,
                    maxView: 1,
                    startDate: def_min,
                    endDate: def_max,
                    autoclose: true,
                    minuteStep: 10

                });
                $('#seg_' + get_seg() + '_end').datetimepicker({
                    format: 'hh:ii',
                    startView: 1,
                    minView: 0,
                    maxView: 1,
                    startDate: def_min,
                    endDate: def_max,
                    autoclose: true,
                    minuteStep: 10
                });
                $('#seg_' + get_seg() + '_start').datetimepicker().on('show', function (ev) {
                    var last_end = $('#seg_' + sel + '_end').val();
                    var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + last_end;
                    $('#seg_' + get_seg() + '_start').datetimepicker('setStartDate', new_start);
                });
                $('#seg_' + get_seg() + '_end').datetimepicker().on('show', function (ev) {
                    var last_end = $('#seg_' + sel + '_end').val();
                    var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + last_end;
                    $('#seg_' + get_seg() + '_end').datetimepicker('setStartDate', new_start);
                });
                $('#seg_' + get_seg() + '_start').datetimepicker().on('changeDate', function (ev) {
                    var set_1 = $('#seg_' + get_seg() + '_start').val();
                    var split = set_1.split(' ');
                    var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + split[1];
                    $('#seg_' + get_seg() + '_end').datetimepicker('setStartDate', new_start);
                });
                $('#seg_' + get_seg() + '_start').datetimepicker().on('changeDate', function (ev) {
                    var set_1 = $('#seg_' + get_seg() + '_start').val();
                    var split = set_1.split(' ');
                    var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + split[1];
                    $('#seg_' + get_seg() + '_end').datetimepicker('setStartDate', new_start);
                });
                $('#seg_' + get_seg() + '_end').datetimepicker().on('changeDate', function (ev) {
                    var set_1 = $('#seg_' + get_seg() + '_end').val();
                    var new_start = minDate.getFullYear() + '-' + month + '-' + minDate.getDate() + " " + set_1;
                    var next = get_seg() + 1;
                    $('#seg_' + next + '_start').val(new_start);
                    $('#seg_' + next + '_start').datetimepicker('setStartDate', new_start);
                });



                $('.courses, .status').select2({
                    theme: "classic"
                });

                $('.ccategory').select2({
                    theme: "classic"
                });

                $('.faculty').select2({
                    theme : 'classic',
                    multiple : true,
                    tags: false,
                    maximumSelectionLength: 2
                });

                $("#ccategory_"+get_seg()).val(category_selected).trigger('change');

            }else{

                noty({
                    text: "Sorry, <p>You cannot add a segment since there is no sufficient time left as defined in this reservation</p>",
                    theme: "relax",
                    dismissQueue: true,
                    progressBar : true,
                    timeout     : 10000,
                    layout : 'center',
                    type: 'error',
                    animation: {
                        open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
                        close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
                        easing: 'swing',
                        speed: 500 // opening & closing animation speed
                    }
                });
            }


        }

        function remove_segment(seg_num) {
            jQuery('#seg_' + seg_num).remove();
            set_seg('subtract');
        }


        $(document).ready(function (){

            $('.course,.status').select2({
                theme: "classic"
            });

            $('.ccategory').select2({
                theme: "classic"
            });

            $('.faculty').select2({
                theme : 'classic',
                multiple : true,
                tags: false,
                maximumSelectionLength: 2
            });

            // With JQuery
            $('#ex1').slider();


            $('#reset_details').click(function(ev){
                ev.preventDefault();
                BootstrapDialog.confirm('Are you sure you reset(delete all) reservation details?', function(result) {
                    if (result) {
                        $('#reset_details_frm').submit();
                    }
                });


            });


        });


        $(document).on( "change", ".ccategory", function() {

            var id_str = $(this).attr('id');
            var id = id_str.split('_')[1];


            var category_id = $(this).val();

            var $course = $('#courses_'+id);


            var target = document.getElementById('courses_'+id);
            var spinner = new Spinner(spinOpts);

            $.ajax({

                url : "{{URL::to('ccategory/getCourseSelect')}}",
                data : {'id' : category_id},
                type : 'POST',
                dataType : 'json',
                beforeSend : function(){
                    //clear contents
                    $course.empty();

                    spinner.spin(target);
                },
                success: function (data) {
                    $.each(data,function(index,dat){
                        // Create the DOM option that is pre-selected by default
                        var option = "<option value='"+dat.id+"' >"+dat.text+"</option>";

                        // Append it to the select
                        $course.append(option);
                    });

                    $course.trigger('change');

                    spinner.stop();

                }

            });

        });

        $(document).on('change','.faculty',function(){

            //check availability first
            var selected_faculty = $(this).val();

            var $faculty = $(this);

            //get the faculty_id
            var faculty_id_str = $(this).attr('id');
            var faculty_id = faculty_id_str.split('_')[1];

            //make sure something is selected.
            if ( selected_faculty != null ){

                var number_of_selected = selected_faculty.length;

                //ensure again that at least one is selected.
                if ( number_of_selected > 0 ){


                    //get the difference in the dates
                    var start_selected =  $('#seg_'+faculty_id+'_start').val();
                    var set_1 = $('#seg_'+faculty_id+'_start').val();
                    var split = set_1.split(' ');
                    var last_selected = split[0] + ' ' + $('#seg_' + faculty_id + '_end').val();

                    var target = document.getElementById('faculty_'+faculty_id);
                    var spinner = new Spinner(spinOpts);

                    $.ajax({

                        url : "{{URL::to('reservation_details/conflict')}}",
                        type : 'POST',
                        data : {faculty : selected_faculty,start : start_selected,end: last_selected},
                        dataType : 'json',
                        beforeSend : function (){
                            spinner.spin(target);
                        },
                        success : function (data){


                            noty({
                                text: data.text,
                                theme: "relax",
                                dismissQueue: true,
                                progressBar : true,
                                timeout     : data.timeout,
                                layout : 'center',
                                type: data.type,
                                animation: {
                                    open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
                                    close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
                                    easing: 'swing',
                                    speed: 500 // opening & closing animation speed
                                }
                            });

                            spinner.stop();


                            if ( data.type == "information" ){
                                //if we select 2 faculty, display the box for adding  hours
                                if( number_of_selected === 2 ){

                                    //get the names
                                    var first_faculty_name = $faculty.select2('data')[0]['text'];
                                    var second_faculty_name = $faculty.select2('data')[1]['text'];

                                    //get the ids
                                    var first_faculty_id = $faculty.select2('data')[0]['id'];
                                    var second_faculty_id = $faculty.select2('data')[1]['id'];

                                    var date_diff = ( new Date(last_selected) - new Date(start_selected) ) / 60000;

                                    var label_text = "Kindly define the hours taught by each faculty member i.e. "+first_faculty_name+" and "+second_faculty_name+". Note that this is based on the time selected above ( <span id='time_selected_"+faculty_id+"'>"+date_diff+" minutes<span> )";


                                    $("#time-taught_"+faculty_id).html(label_text);

                                    $('#first-faculty-label_'+faculty_id).prepend(first_faculty_name+" ");
                                    $("#first-faculty_"+faculty_id).attr('faculty_id',first_faculty_id);

                                    $('#second-faculty-label_'+faculty_id).prepend(second_faculty_name+" ");
                                    $("#second-faculty_"+faculty_id).attr('faculty_id',second_faculty_id);

                                    //display
                                    $('#faculty-times_'+faculty_id).slideDown();


                                }else{

                                    //hide
                                    $('#faculty-times_'+faculty_id).slideUp();

                                    //reset all to default
                                    $("#time-taught_"+faculty_id).text("");
                                    $('#first-faculty-label_'+faculty_id).html("(in minutes) : ");
                                    $('#second-faculty-label_'+faculty_id).html("(in minutes) : ");
                                    $("#first-faculty_"+faculty_id).attr('faculty_id',"");
                                    $("#second-faculty_"+faculty_id).attr('faculty_id',"");


                                }
                            }else{
                                //hide
                                $('#faculty-times_'+faculty_id).slideUp();

                                //reset all to default
                                $("#time-taught_"+faculty_id).text("");
                                $('#first-faculty-label_'+faculty_id).html("(in minutes) : ");
                                $('#second-faculty-label_'+faculty_id).html("(in minutes) : ");
                                $("#first-faculty_"+faculty_id).attr('faculty_id',"");
                                $("#second-faculty_"+faculty_id).attr('faculty_id',"");
                            }


                        }

                    });

                }

            }else{

                //hide
                $('#faculty-times_'+faculty_id).slideUp();

                //reset all to default
                $("#time-taught_"+faculty_id).text("");
                $('#first-faculty-label_'+faculty_id).html("(in minutes) : ");
                $('#second-faculty-label_'+faculty_id).html("(in minutes) : ");
                $("#first-faculty_"+faculty_id).attr('faculty_id',"");
                $("#second-faculty_"+faculty_id).attr('faculty_id',"");



            }

        });

        $(document).on('change','.second_faculty',function(){

            var inputbx_id_str = $(this).attr('id');
            var inputbx_id = inputbx_id_str.split('_')[1];

            var first_faculty = $("#first-faculty_"+inputbx_id).val();
            var second_faculty = $(this).val();

            var total = parseInt(first_faculty) + parseInt(second_faculty) + " Minutes";

            noty({
                text: "Total time taught by the faculty members is "+total,
                theme: "relax",
                dismissQueue: true,
                progressBar : true,
                timeout     : 5000,
                layout : 'center',
                type: 'information',
                animation: {
                    open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
                    close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
                    easing: 'swing',
                    speed: 500 // opening & closing animation speed
                }
            });


        });


        $(document).on('change','.end',function(){


            var end_id_str = $(this).attr('id');
            var end_id = end_id_str.split('_')[1];

            //get the difference in the dates
            var start_selected =  $('#seg_'+end_id+'_start').val();
            var set_1 = $('#seg_'+end_id+'_start').val();
            var split = set_1.split(' ');

            var selected_value_last = $('#seg_' + get_seg() + '_end').val();
            var selected_value_this = $('#seg_' + end_id + '_end').val();

            var last_selected_last = split[0] + ' ' + selected_value_last ;
            var last_selected_this = split[0] + ' ' + selected_value_this ;

            var date_diff = ( new Date(last_selected_this) - new Date(start_selected) ) / 60000;

            $("#time_selected_"+end_id).text(date_diff+" minutes )");

            var last_selected_with_diff = new Date(new Date(last_selected_last).getTime() + (20 * 60000));

            if ( new Date(last_selected_with_diff).getTime() >  new Date(max_date).getTime() ){
                $('#add_segment_icon').removeClass('fa-plus-circle');
                $('#add_segment_icon').addClass('fa-times');
            }else{
                $('#add_segment_icon').removeClass('fa-times');
                $('#add_segment_icon').addClass('fa-plus-circle');
            }

        });
        $(document).ready(function(){
            {{--//Expand the menu :D--}}
             $('body').removeClass('sidebar-collapse');
        });

    </script>

@endsection