@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| New Role  
@endsection


<!-- Custom CSS !-->
@section('style')
<style type="text/css">
    .dropdown-menu>li>a {
        display: block;
        padding: 3px 40px;
        clear: both;
    }

    .multiselect {
        text-align: left;
    }
    .multiselect b.caret {
        position: absolute;
        top: 14px;
        right: 8px;
    }
    .multiselect-group {
        font-weight: bold;
        text-decoration: underline;
    }
</style>
{{ HTML::style('css/bootstrap-multiselect/bootstrap-multiselect.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Role
    <small>Add New</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')

<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>

        <form id="role" name="role" method="post" action="{{URL::to('role/save');}}" role="form">
            {{ Form::token(); }}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif


                <div class="col-lg-6 form-group">
                    {{Form::label('name','Name')}}
                    {{Form::text('name','',array('class'=>'form-control','required'=>'required','placeholder'=>'Please enter the Role Name'))}}
                </div>
                <div class="col-lg-6 form-group">
                    {{Form::label('permissions','Permissions')}}
                    <select name="permissions[]" required="required" class="form-control multiselect" id="permissions" multiple="multiple">
                        <optgroup label="Resource Permissions">
                            @foreach($permissions as $permission)
                            @if($permission->type == 0)
                            <option value="{{$permission->permission_id}}">{{$permission->display_name}}</option>
                            @endif
                            @endforeach
                        </optgroup>
                        <optgroup label="Functional Permissions">
                            @foreach($permissions as $permission)
                            @if($permission->type == 1)
                            <option value="{{$permission->permission_id}}">{{$permission->display_name}}</option>
                            @endif
                            @endforeach
                        </optgroup>
                        
                    </select>
                </div>
            </div><!-- /.box-body -->
            <div  class="box-footer">
                <div style="padding: 13px;" class="form-group"> 
                    <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i>  Submit</button>
                </div>
            </div>
        </form>

    </div>  



</div>

@endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
<script type="text/javascript">
    $(function() {
        $('#permissions').multiselect({
            numberDisplayed: 2,
            includeSelectAllOption: true,
            selectAllText: 'Grant All'
        });
        /*Width Hack*/
        $('.dropdown-toggle').css({width:'100%'});
        $('.btn-group').css({width:'100%'});
    });
    $(document).ready(function(){

        {{--//Expand the menu :D--}}
         $('body').removeClass('sidebar-collapse');
    });
</script>
@endsection 