@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Roles
@endsection


<!-- Custom CSS !-->
@section('style')
<style type="text/css">
    small{
        margin-left: 2px;
    }
</style>
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
{{ HTML::style('js/plugins/noty/animate.css')}}

<style type="text/css">
    .modal-dialog{
        width:300px !important;
    }
</style>
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Roles and Permissions

</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">

        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped dataTable" id="roles">
                <thead>
                <th>Name</th>
                <th>Permission(s)</th>
                <th>Actions</th>
                </thead>
                <tbody>
                    @if( count($roles) > 0 )
                    <?php $i = 0; ?>
                    @foreach($roles as $role)
                    <tr>
                        <td>
                            <span class="pull-left">{{$role['name']}}</span>
                            {{-- For Native Roles --}}
                            @if( $role['type'] == 1 )
                                @if( $role['sys_type'] == 1 ) 
                                    <small title="Can access ALL Department Information" class="badge pull-right bg-light-blue"> System Wide</small>  
                                @elseif ($role['sys_type'] == 2)
                                    <small title="Can Access Timetable" class="badge pull-right bg-purple"> Regular User</small>
                                @else
                                    <small title="Can Access only assigned Department(s)" class="badge pull-right bg-purple"> Department Wide</small>
                                @endif
                            @endif
                            {{-- End of Native Roles --}}
                            
                            @if( $role['type'] == 0 )
                            <small data-placement="left" title="User Defined Role" class="badge pull-right bg-navy"> Custom</small>  
                            @endif
                            @if( $role['type'] == 1 )
                            <small data-placement="left" title="Native Role" class="badge pull-right bg-maroon"> <span class="fa fa-lock "></span></small>  
                            @endif
                            
                            {{-- Display Number of Users in Role --}}
                            @if( $role_users[$i]['count'] == 0 )
                                <small data-placement="right" title="{{$role_users[$i]['names']}}" class="badge pull-left bg-gray"> {{$role_users[$i]['count']}}</small>
                           @else
                                <small data-placement="right" title="{{$role_users[$i]['names']}}" class="badge pull-left bg-green"> {{$role_users[$i]['count']}}</small>
                           @endif
                            {{-- End of --}}
                            <?php $deletable = $role_users[$i]['count']; ?>
                        </td>
                        <td>
                            @if( is_string ($role['permissions']) )
                            {{$role['permissions']}}
                            @endif    
                            @if( is_array ($role['permissions']) )
                            {{implode('<br>',$role['permissions']);}}
                            @endif
                            
                        </td>
                        <td>
                            @if( $role['type'] == 0 )
                            <form name="delform_role" id="delform{{$role['role_id']}}" method="post" action="{{URL::to('role/delete/'.$role['role_id'])}}">
                                {{Form::token()}}
                                <a data-rel="tooltip" title="Edit {{$role['name']}}" href="{{URL::to('role/'.$role['role_id'].'/edit')}}" >
                                    <span class="fa fa-edit"/>
                                </a>
                                @if( $deletable == 0 )
                                <a href='#' class="delete_entry" id="delete_role{{$role['role_id']}}" data-rel="tooltip" title="Delete {{$role['name']}}" >
                                    <span class="fa fa-trash-o"/>
                                </a>
                                @endif
                            </form>
                            @endif
                            @if( $role['type'] == 1 )
                            {{Form::token()}}
                            <a style="cursor:not-allowed;" data-rel="tooltip" title="You can neither edit nor delete this role" href="#" >
                                <span class="fa fa-ban"/>
                            </a>
                            @endif
                        </td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="box-footer">

        </div>
    </div>

</div>


@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
{{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
{{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
{{ HTML::script('js/plugins/bootstrap-tooltip/bootstrap-tooltip.js')}}
{{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

<script type="text/javascript">
    $(function() {

        $('#roles').dataTable({
            "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "oLanguage": {
                "sLengthMenu": "_MENU_ Roles per page"
            },
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "gotoURL",
                        "sButtonText": '<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>  Add New Role</button>',
                        "sGoToURL": "{{URL::to('role/add')}}"
                    },
                    {
                        "sExtends": "pdf",
                        "sFileName": "Roles.pdf",
                        "sPdfOrientation": "landscape",
                        "sPdfMessage": "SBS Planner System Roles",
                        "sButtonText": "Save to PDF"
                    },
                    {
                        "sExtends": "csv",
                        "sFileName": "Roles.csv",
                        "sButtonText": "Save to CSV"
                    },
                    {
                        "sExtends": "xls",
                        "sFileName": "Roles.xls",
                        "sButtonText": "Save to Excel"
                    }
                ]
            }
        });
        $('select,input').addClass('form-control input-sm');
        $('.alert').fadeOut(5000);

        @if ( Session::get('notice') )

           noty({
            text: "Success! {{ Session::get('notice') }}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 3000,
            layout : 'topRight',
            type: 'success',
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight', // Animate.css class names
                easing: 'swing', // unavailable - no need
                speed: 500 // unavailable - no need
            }
        });

        @endif

        @if ( Session::get('error') )
           noty({
            text: "Error, {{ Session::get('error')}}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 10000,
            layout : 'topRight',
            type: 'error',
            animation: {
                open: 'animated swing', // Animate.css class names
                close: 'animated tada', // Animate.css class names
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            }
        });
        @endif

    });
</script>
@endsection