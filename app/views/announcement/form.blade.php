@extends('layouts.master')

<!-- Page Title !-->
@section('title')
 | New Announcement  
@endsection


<!-- Custom CSS !-->
@section('style')
 {{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Announcement
    <small>Add New</small>
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>


            {{Form::open(['method'=>'POST','action'=>'AnnouncementController@store'])}}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class='form-group col-md-6'>
                {{Form::label('text','Announcement')}}
                {{Form::text('text','',['class'=>'form-control','required'=>'required','placeholder'=>'Announcement'])}}
                </div>
                <div class='form-group col-md-6'>
                {{Form::label('priority','')}}
                {{Form::select('priority',$priorities,'',['class'=>'form-control','required'=>'required','placeholder'=>''])}}
                </div>
                <div class='form-group col-md-6'>
                {{Form::label('start_date','Start Date')}}
                {{Form::text('start_date','',['id'=>'start','required'=>'required','class'=>'form-control start','placeholder'=>'Announcement start date'])}}
                </div>
                <div class='form-group col-md-6'>
                {{Form::label('end_date','End Date')}}
                {{Form::text('end_date','',['id'=>'end','required'=>'required','class'=>'form-control end','placeholder'=>'Announcement end date'])}}
                </div>
                    <div style="padding-left: 15px;" class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i>  Submit</button>
                    </div>
                </div>
                {{Form::close()}}
                </div>
                </div>
@endsection

<!-- Custom Scripts !-->
@section('scripts')
{{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
{{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}
<script>
    $(document).ready(function(){

        {{--//Expand the menu :D--}}
         $('body').removeClass('sidebar-collapse');
    });

$('.start').datetimepicker({
    pickTime:false,
    minDate:moment().subtract('days',1)

});
$('.end').datetimepicker({
    pickTime:false,
    minDate:moment().subtract('days',1)

});
$(".start").on("dp.change", function (e) {$('.end').data("DateTimePicker").setMinDate(e.date);});
$(".end").on("dp.change", function (e) {$('.start').data("DateTimePicker").setMaxDate(e.date);});
</script>
@endsection