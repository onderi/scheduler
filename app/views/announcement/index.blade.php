@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Announcements
@endsection


<!-- Custom CSS !-->
@section('style')
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
{{ HTML::style('js/plugins/noty/animate.css')}}

<style type="text/css">
    .modal-dialog{
        width:300px !important;
    }
</style>
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Announcements
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection

<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">

        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped dataTable" id="announcements">
                <thead>
                <th>Text</th>
                <th>Priority</th>
                <th>Created By</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Actions</th>
                </thead>
                <tbody>
                @if(count($announcements)>0)
                @foreach($announcements as $announce)
                <tr>
                <td>{{$announce->text}}</td>
                <td>{{$announce->priority}}</td>
                <td>{{$announce->user->username}}</td>
                <td>{{date('d-m-Y',strtotime($announce->start_date))}}</td>
                <td>{{date('d-m-Y',strtotime($announce->end_date))}}</td>
                <td>
                {{Form::open(['id'=>'delform'.$announce->announcement_id,'method'=>'post','url'=>'announcement/delete/'.$announce->announcement_id])}}

                        <a data-rel="tooltip" title="Edit {{$announce->text}}" href="{{URL::to('announcement/'.$announce->announcement_id.'/edit')}}">
                        <span class="fa fa-edit"/>
                        </a>
                        <a href="#" class="delete_entry" id="delete{{$announce->announcement_id}}" data-rel="tooltip" title="Delete {{$announce->text}}">
                        <span class="fa fa-trash"/>
                        </a>

                        {{Form::close()}}
                </td>
                </tr>
                @endforeach
                @endif
                 </tbody>

            </table>

        </div>
        <div class="box-footer">

        </div>
    </div>

</div>


@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
{{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
{{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
{{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

<script type="text/javascript">
    $(function() {

        $('#announcements').dataTable({
            "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "oLanguage": {
                "sLengthMenu": "_MENU_ Announcements per page"
            },
            "oTableTools": {
                "aButtons": [
                {
                        "sExtends": "gotoURL",
                        "sButtonText": '<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>  Add Announcement</button>',
                        "sGoToURL": "{{URL::to('announcement/create')}}"
                    }/*,
                    {
                        "sExtends": "pdf",
                        "sFileName": "Announcements.pdf",
                        "sPdfOrientation": "landscape",
                        "sPdfMessage": "SBS Planner System announcements",
                        "sButtonText": "Save to PDF"
                    },
                    {
                        "sExtends": "csv",
                        "sFileName": "announcements.csv",
                        "sButtonText": "Save to CSV"
                    },
                    {
                        "sExtends": "xls",
                        "sFileName": "announcements.xls",
                        "sButtonText": "Save to Excel"
                    }*/
                ]
            }
        });
        $('select,input').addClass('form-control input-sm');
        $('.alert').fadeOut(12000);

        @if ( Session::get('notice') )

           noty({
            text: "Success! {{ Session::get('notice') }}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 3000,
            layout : 'topRight',
            type: 'success',
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight', // Animate.css class names
                easing: 'swing', // unavailable - no need
                speed: 500 // unavailable - no need
            }
        });

        @endif

        @if ( Session::get('error') )
           noty({
            text: "Error, {{ Session::get('error')}}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 10000,
            layout : 'topRight',
            type: 'error',
            animation: {
                open: 'animated swing', // Animate.css class names
                close: 'animated tada', // Animate.css class names
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            }
        });
        @endif


    });
</script>

@endsection