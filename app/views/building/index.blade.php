@extends('layouts.master')

<!-- Page Title !-->
@section('title')
| Buildings
@endsection


<!-- Custom CSS !-->
@section('style')
<!-- CSS Datatables !-->
{{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{ HTML::style('css/bootstrap-dialog/bootstrap-dialog.css')}}
{{ HTML::style('css/datatables/TableTools.css')}}
{{ HTML::style('js/plugins/noty/animate.css')}}

<style type="text/css">
    .modal-dialog{
        width:300px !important;
    }
</style>
@endsection

<!-- Content Heading !-->
@section('content_head')
<h1>
    Buildings
</h1>
@endsection


<!-- Breadcrumbs !-->
@section('breadcrumb')
{{ Breadcrumbs::render() }}
@endsection


<!-- Page Content !-->
@section('page_content')
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">

        </div>
        <div class="box-body">

            <table class="table table-bordered table-striped dataTable" id="buildings">
                <thead>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>
                </thead>
                <tbody>
                    @if( count($buildings) > 0 )
                    @foreach($buildings as $building)
                    <tr>
                        <td>
                            <span class='pull-left'>{{$building->building_name}}</span>
                            <?php $deletable = count( $building->areas ); ?>
                            @if( count( $building->rooms ) > 0 )
                                <?php    
                                    $prefix = '';
                                    $list = ""; 
                                 ?>
                                @foreach( $building->rooms as $r )
                                    <?php
                                        $list .= $prefix.$r->room_name;
                                        $prefix = '&#013;'; 
                                    ?>
                                @endforeach
                                <small data-placement="left" title="{{$list}}" class="badge pull-right bg-green"> {{ count( $building->rooms )}}</small>
                            @else
                                <small data-placement="left" title="No Rooms" class="badge pull-right bg-gray"> 0</small>
                            @endif
                        </td>
                        <td>{{$building->building_description}}
                            @if( $building->type == "Internal" )
                                <small data-placement="left" title="Type / Classification" class="badge pull-right bg-blue">{{strtoupper($building->type)}}</small>
                            @else
                                <small data-placement="left" title="Type / Classification" class="badge pull-right bg-orange">{{strtoupper($building->type)}}</small>
                            @endif
                        </td>
                        <td>
                            {{--
                            <a data-rel="tooltip" title="Add New" href="{{URL::to('building/add')}}" >
                            <span class="fa fa-plus-square-o"/>
                            </a>
                            --}}
                            <form id="delform{{$building->building_id}}" method="post" action="{{URL::to('building/delete/'.$building->building_id)}}">
                                {{Form::token()}}
                                <a data-rel="tooltip" title="Edit {{$building->building_name}}" href="{{URL::to('building/'.$building->building_id.'/edit')}}" >
                                    <span class="fa fa-edit"/>
                                </a>
                                @if( $deletable == 0)
                                <a href='#' class="delete_entry" id="delete{{$building->building_id}}" data-rel="tooltip" title="Delete {{$building->building_name}}" >
                                    <span class="fa fa-trash-o"/>
                                </a>
                                @endif
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>

            </table>

        </div>
        <div class="box-footer">

        </div>
    </div>

</div>


@endsection

<!-- Custom Scripts !-->
@section('scripts')
<!-- JS Datatables !-->
{{ HTML::script('js/jquery-ui-1.10.3.js')}}
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
{{ HTML::script('js/plugins/bootstrap-modal/bootstrap-modal.js')}}
{{ HTML::script('js/plugins/datatables/TableTools.min.js')}}
{{ HTML::script('js/plugins/datatables/custom_buttons.js')}}
{{ HTML::script('js/plugins/datatables/ZeroClipboard.js')}}
{{ HTML::script('js/plugins/noty/jquery.noty.packaged.min.js')}}

<script type="text/javascript">
    $(function() {

        $('#buildings').dataTable({
            "sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row' <'col-xs-6'l><'col-xs-6'p> >",
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "oLanguage": {
                "sLengthMenu": "_MENU_ Buildings per page"
            },
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "gotoURL",
                        "sButtonText": '<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>  Add New Building</button>',
                        "sGoToURL": "{{URL::to('building/add')}}"
                    },
                    {
                        "sExtends": "pdf",
                        "sFileName": "Buildings.pdf",
                        "sPdfOrientation": "landscape",
                        "sPdfMessage": "SBS Planner System Buildings",
                        "sButtonText": "Save to PDF"
                    },
                    {
                        "sExtends": "csv",
                        "sFileName": "Buildings.csv",
                        "sButtonText": "Save to CSV"
                    },
                    {
                        "sExtends": "xls",
                        "sFileName": "Buildings.xls",
                        "sButtonText": "Save to Excel"
                    }
                ]
            }
        });
        $('select,input').addClass('form-control input-sm');
        $('.alert').fadeOut(5000);

        @if ( Session::get('notice') )

           noty({
            text: "Success! {{ Session::get('notice') }}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 3000,
            layout : 'topRight',
            type: 'success',
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight', // Animate.css class names
                easing: 'swing', // unavailable - no need
                speed: 500 // unavailable - no need
            }
        });

        @endif

        @if ( Session::get('error') )
           noty({
            text: "Error, {{ Session::get('error')}}",
            theme: "relax",
            dismissQueue: true,
            progressBar : true,
            timeout     : 10000,
            layout : 'topRight',
            type: 'error',
            animation: {
                open: 'animated swing', // Animate.css class names
                close: 'animated tada', // Animate.css class names
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            }
        });
        @endif
    });
</script>

@endsection

