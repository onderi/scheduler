<!DOCTYPE html>
<html>
<head>
<title>
SBS Scheduler| Reservation Confirmation
</title>

<link rel="shortcut icon" href="{{ URL::to('img/favicon.ico') }}">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


</head>
<body class="skin-blue" style="padding-left:1%;padding-right:1%;">
	<header class="header">
		<h2><a href="{{URL::to('/')}}" class="logo" style="text-decoration:none">
			SBS Scheduler
		</a></h2>

		</header>
	
	<section class="content">
		<div class="container-fluid row">

<h3>Booking Confirmation</h3>
<p>Dear {{$user->fname.' '.$user->lname}},</p>
<p>We would like you to confirm (or reject) the bookings made for: </p>
<p>Room:{{$reservation->room->room_name}}</p>


<p>The Booking is meant to run from {{$reservation->start_time}} to {{$reservation->end_time}}</p>

<p>Please click on this link <a href="{{URL::to('reservation/details/create').'/'.$reservation->reservation_room_id}}">here</a> or copy and paste the link below to your browser</p>
<p>{{URL::to('reservation/details/create').'/'.$reservation->reservation_room_id}}</p>

<i>You may be required to login</i>



<p><b>N.B:</b><i>To change any of the details please contact the Booking administrator<i></p>
</div>
	</section>
	<br><br><br>
	<footer class="footer">
		<address>
		<small><b>Strathmore Business School</b></small><br>
		<small>Ole Sangale Road, Madaraka Estate</small><br>
		<small>P.O. Box 59857-00200 Nairobi, Kenya</small><br>
		<small><a href="http://www.sbs.ac.ke">www.sbs.ac.ke</a></small>
		</address>
	</footer>

	
</body>
</html>