<!DOCTYPE html>
<html>
<head>
</head>
<body>
<table border='0' width='100%'>
<tr>
    <td>
    <table border='0' width='100%'>
    <tr>
      <td width='120px'>
        <h3>SFPlanner</h3>
      </td>

      <td>
        <h3><small>Room Reservation Approval</small></h3>
      </td>
      </tr>
      </table>
      </td>
    </tr>
    <tr>
    <td>Dear {{$user->fname.' '.$user->lname}},</td>
    </tr>
    <tr>
    <td>Your reservation request for {{$room->room_name}} from {{$reservation->start_time}} to {{$reservation->end_time}} has been
    <strong>{{$reservation->approval}}</strong><br></td>
    </tr>

    <tr>
    @if($reservation->approval=='Approved')
    <td>You can segment the session from this
    <a href='{{URL::to('reservation/details/create').'/'.$reservation->reservation_room_id}}'>link</a>
    <br>
    or copy link to your browser http://my.sbs.ac.ke/scheduler/reservation/details/create/{{$reservation->reservation_room_id}}
    </td>
    @else
    <td>
    You may log into the system and attempt to make another reservation or contact the booking administrator
    </td>
    @endif
    </tr>

</table>
<br>
  <i>You may be required to login</i><br>
  <br>

  <footer>
    <address>
      <small><b>Strathmore Business School</b></small><br>
      <small>Ole Sangale Road, Madaraka Estate</small><br>
      <small>P.O. Box 59857-00200 Nairobi, Kenya</small><br>
      <small><a href="http://www.sbs.ac.ke">www.sbs.ac.ke</a></small>
    </address>
  </footer>
</body>
</html>