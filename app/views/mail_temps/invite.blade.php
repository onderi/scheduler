<!DOCTYPE html>
<html style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
<head>
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>SBS Scheduler</title>
</head>
<body bgcolor="#f6f6f6" style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; -webkit-font-smoothing: antialiased; height: 100%; -webkit-text-size-adjust: none; width: 100% !important; margin: 0; padding: 0;">

<!-- body -->
<table class="body-wrap" bgcolor="#f6f6f6" style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
		<td style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;"></td>
		<td class="container" bgcolor="#FFFFFF" style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; clear: both !important; display: block !important; max-width: 600px !important; Margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">

			<!-- content -->
			<div class="content" style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; display: block; max-width: 600px; margin: 0 auto; padding: 0;">
				<table style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; width: 100%; margin: 0; padding: 0;">
					<tr style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
						<td align="center" style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
							<img style='width:15%;margin-top:2px;margin-left:5px;' alt='SBS Logo' src='{{URL::to('img/logo.png')}}'/>
						</td>
					</tr>
					<tr style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
						<td style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
							<p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Dear {{$name}},</p>
							<p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">  Kindly take note of your class session at the Strathmore Business School; </p>
							<ul>
								<li><p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Participant Group : MBA Year 1 Trimester 1 </p></li>
								<li><p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Course : MBA 8101 - Managerial Accounting</p></li>
								<li><p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Duration : {{  date('g:iA',strtotime($start)) }} to {{  date('g:iA',strtotime($end)) }} on {{  date('j/n/Y',strtotime($start))  }}</p></li>
								<ul>
									<li><p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;"><small>NB: If you are using gmail,please click on the time above to add it to your calendar</small></p></li>
								</ul>
							</ul> <br/>

							<p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">In case you have any changes in your schedule, please contact the program manager.

							</p>

							<p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Kind regards,</p>
							<p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">{{$user->fname.' '.$user->lname}} via <a href="http://my.sbs.ac.ke/scheduler/public/login">SBS Scheduler</a></p>

						</td>
					</tr></table>
			</div>
			<!-- /content -->

		</td>
		<td style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;"></td>
	</tr></table>
<!-- /body --><!-- footer -->
<table class="footer-wrap" style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; clear: both !important; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
		<td style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;"></td>
		<td class="container" style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; clear: both !important; display: block !important; max-width: 600px !important; margin: 0 auto; padding: 0;">

			<!-- content -->
			<div class="content" style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; display: block; max-width: 600px; margin: 0 auto; padding: 0;">
				<table style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
						<td align="center" style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
							<p style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.6em; color: #666666; font-weight: normal; margin: 0 0 10px; padding: 0;">This is an automated email.
							</p>
						</td>
					</tr></table>
			</div>
			<!-- /content -->

		</td>
		<td style="font-family: 'Comic Sans Ms', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;"></td>
	</tr></table>
<!-- /footer -->
</body>
</html>