@extends('mail_temps.outline')

@section('title')
SFPlanner Mail
@endsection


@section('heading')
Room Reservation
@endsection

@section('style')
<style type="text/css">
    .dropdown-menu>li>a {
        display: block;
        padding: 3px 40px;
        clear: both;
    }

    .multiselect {
        text-align: left;
    }
    .multiselect b.caret {
        position: absolute;
        top: 14px;
        right: 8px;
    }
    .multiselect-group {
        font-weight: bold;
        text-decoration: underline;
    }
    
</style>
{{ HTML::style('css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}
{{ HTML::style('css/bootstrap-multiselect/bootstrap-multiselect.css')}}
@endsection


@section('content')


<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <!-- <h3 class="box-title">User Form</h3> !-->
        </div>

        {{Form::open(array('method'=>'POST','action'=>'ReservationDetailsController@store'))}}
            <div class="box-body">
                @if ( $errors->count() > 0 )
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>Error!</b>
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
                
                
                @endif
                
                <div class="form-group col-md-12">
                {{Form::label('rr','Reservation: ')}}
                {{Form::text('rr_name',$reservation->user->username,array('class'=>'form-control','readonly'=>'readonly','required'=>'required'))}}
                {{Form::hidden('rr',$reservation->reservation_room_id)}}
                </div>
                

                <div class="form-group col-md-6">
                {{Form::label('owner','Owner')}}
                {{Form::text('owner',$reservation->user->fname.' '.$reservation->user->lname,array('class'=>'form-control','readonly'=>'readonly','required'=>'required'))}}
                {{Form::hidden('user_id',$reservation->user_id)}}
                </div>
                
                
                <div class="form-group col-md-6">
                {{Form::label('ref','Reference Number')}}
                {{Form::text('ref','Ref: '.$ref,array('class'=>'form-control','readonly'=>'readonly','required'=>'required'))}}
                </div>
                <div id="segments">
                     <div class="container-fluid col-md-12 group" style="border:1px solid blue;border-radius:4px;margin-bottom:13px;">
                          <div class="form-group col-md-6">
                              {{Form::label('course','Course')}}
                              {{Form::select('course[]',$courses,'',array("class"=>"form-control",'required'=>'required'))}}
                          </div>
                          <div class="form-group col-md-6">
                              {{Form::label('group','Group')}}
                              {{Form::select('group[]',$groups,'',array('class'=>'form-control','required'=>'required'))}}
                          </div>
                          <div class="form-group col-md-6">
                              {{Form::label('start','Start Time')}}
                              {{Form::text('start[]',$reservation->start_time,array('id'=>'start','required'=>'required','class'=>'form-control start','data-date-format' => "YYYY-MM-DD HH:mm"))}}
                          </div>
                          <div class="form-group col-md-6">
                              {{Form::label('end','Finish Time')}}
                              {{Form::text('end[]',$reservation->end_time,array('id'=>'end','class'=>'form-control end','data-date-format' => "HH:mm",'required'=>'required'))}}
                          </div>
                          <div class="form-group col-md-6">
                              {{Form::label('desc','Description')}}
                              {{Form::textarea('desc[]','',array('class'=>'form-control','placeholder'=>'Reservation Detail Description','maxlength'=>'200','rows'=>'2','required'=>'required'))}}
                          </div>
                          
                      
                          <div class="form-group col-md-6"> 
                              {{Form::label('status','Status')}}
                              {{Form::select('status[]',$statuses,'',array('class'=>'form-control','required'=>'required'))}}
                          </div>
                     </div>
                </div>
                <div class="input-group">
                     
                     <input type="button" onClick="add_segment();" value="Add segment" class="btn btn-info" style="margin-bottom:13px" />
                     
                     
                </div>

                {{Form::submit('Add Details',array('class'=>'btn btn-primary form-control'))}}
                </div>
                {{Form::close()}}
                
                
</div>
</div>



<script type="text/javascript">
    //Date Time Picker
    var min_date=<?php echo(json_encode($reservation->start_time));?>;
    var max_date=<?php echo(json_encode($reservation->end_time));?>;
    
                    $('.start').datetimepicker({
                        minuteStepping: 30,
                        pick12HourFormat: false,
                        minDate:moment(min_date).subtract('hours',1)
                      });
                    
                    //End date
                    $('.end').datetimepicker({
                        pickDate: false,
                        minuteStepping: 30,
                        pick12HourFormat: false,
                        maxDate:moment(max_date)
                    });
                    var seg_num=0;
                    function add_segment() {
                    seg_num++;
                    var segment="<div id='seg_"+seg_num+"' class='container-fluid col-md-12 group' style='border:1px solid blue;border-radius:4px;margin-bottom:13px;'><button class='close'  aria-hidden='true' onClick= 'remove_segment("+seg_num+");'>x</button><div class='form-group col-md-6'><label for='course[]'>Course</label><select name='course[]' class='form-control' required><?php foreach($courses as $key=>$value){ echo '<option value='.$key.'>'.$value.'</option>'; }?></select></div><div class='form-group col-md-6'><label for='group[]'>Group</label><select name='group[]' class='form-control' required><?php foreach($groups as $key=>$value){ echo '<option value='.$key.'>'.$value.'</option>';} ?></select></div><div class='form-group col-md-6'><label for='start'>Start Time</label><input type='text' name='start[]' data-date-format= 'YYYY-MM-DD HH:mm' id='seg_"+seg_num+"_start' class='form-control' value='<?=$reservation->start_time?>' required/></div><div class='form-group col-md-6'><label for='end'>Completion Time</label><input type='text' id='seg_"+seg_num+"_end' name='end[]' data-date-format='HH:mm' class='form-control end' value='<?=$reservation->end_time?>' required/></div><div class='form-group col-md-6'><label for='desc[]'>Segment Description</label><textarea name='desc[]' placeholder='Reservation Description' maxlength='200' rows='2' class='form-control' required></textarea></div><div class='form-group col-md-6'><label for='status[]'>Segment Status</label><select name='status[]' class='form-control' required><?php foreach($statuses as $key=>$value){ echo '<option value='.$key.'>'.$value.'</option>';}?></select></div></div>";

                    jQuery('#segments').append(segment);

                    $('#seg_'+seg_num+'_start').datetimepicker({
                      minuteStepping:30,
                      pick12HourFormat:false,
                      minDate:moment(min_date).subtract('hours',1)
                      
                    });
                    $('#seg_'+seg_num+'_end').datetimepicker({
                      pick12HourFormat:false,
                      pickDate:false,
                      minuteStepping:30,
                    maxDate:moment(max_date)
                  });

                    

                    }
                    function remove_segment(seg_num) {
                         jQuery('#seg_'+seg_num).remove();
                    }

                    
</script>

@endsection
@section('scripts')

{{ HTML::script('js/plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}
{{ HTML::script('js/plugins/moment/moment-with-langs.min.js')}}
{{ HTML::script('js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}


@endsection