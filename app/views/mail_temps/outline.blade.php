<html>
<head>
<title>
SF Planner| @yield('title')
</title>
<link rel="shortcut icon" href="{{ URL::to('img/favicon.ico') }}">


{{ HTML::style('css/bootstrap.min.css')}}
{{ HTML::style('css/font-awesome.min.css')}}
       {{ HTML::style('css/AdminLTE.css')}}

@yield('style')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        
         <!-- jQuery 2.0.2 -->
		 
{{ HTML::script('js/jquery.min.js')}}
</head>
<body class="skin-blue" style="padding-left:1%;padding-right:1%;">
	<header class="header">
		<a href="my.sbs.ac.ke/scheduler" class="logo">
			SBS Scheduler
		</a>

		<h3 class='navbar navbar-static-top' style="padding:0.75%">
			
			@yield('heading')
		
		</h3>
	</header>
	<br>
	<section class="content">
		<div class="container-fluid row">
			@yield('content')
		</div>
	</section>
	<br><br><br>
	<footer class="footer">
		<address>
		<small><b>Strathmore Business School</b></small><br>
		<small>Ole Sangale Road, Madaraka Estate</small><br>
		<small>P.O. Box 59857-00200 Nairobi, Kenya</small><br>
		<small><a href="http://www.sbs.ac.ke">www.sbs.ac.ke</a></small>
		</address>
	</footer>

	
</body>
<!-- Bootstrap -->
    {{ HTML::script('js/bootstrap.min.js')}}
    <!-- AdminLTE App -->
    {{ HTML::script('js/AdminLTE/app.js')}}
 @yield('scripts')
</html>