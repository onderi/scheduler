<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
                $this->call('StatusTableSeeder');
		$this->call('UserTableSeeder');
	}
}

//Seed the Statuses
class StatusTableSeeder extends Seeder {
    
    public function run()
    {
        DB::table('statuses')->delete();

        Status::create(array('status_name'=>'Active','status_desc'=>'Active User'));
    }

}

//Seed the users
class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array('fname' => 'Wallace','lname'=>'Kinyua','username'=>'wkinyua','email'=>'kinyuawallace@gmail.com','password'=>'test123','salt'=>'salty','phone'=>'0722911496','status_id'=>1));
    }

}