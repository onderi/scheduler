#Get room ids of a certain area - to be used in getting rooms below
SELECT group_concat(room_id) FROM scheduler.sfp_rooms WHERE area_id IN (7,6);


# Select room reservations for a certain year
SELECT reservation_room_id as id,reservation_name,reservation_desc,start_time,end_time,d.dept_abbrev as department,room.room_name as room,IF(rr_type = "0","Internal","External") as type FROM scheduler.sfp_reservation_rooms r
INNER JOIN sfp_depts d ON d.dept_id = r.dept_id
INNER JOIN sfp_rooms room ON room.room_id = r.room_id
where DATE_FORMAT(start_time,'%Y') = '2016' and r.room_id NOT IN (13,17,31,32,18,19,43);