/*
 |--------------------------------------------------------------------------
 | All rights reserved - The Strathmore Business School 
 |--------------------------------------------------------------------------
 |
 | The SBS Faculty Planner
 | @Author - kinyuawallace<at>gmail<dot>com
 */

(function ($, undefined) {
    //Extend the stuff
    $.fn.extend
    ({
        // FullCalendar Extendable Plugin
        FullCalendarExt: function (options) {
            // Default Configurations (General)#

            var defaults =
            {
                defaultView: 'resourceDay', // resourceDay,month,basicWeek or basicDay or agendaWeek
                calendarSelector: '#calendar',
                ajaxJsonFetch: '',
                ajaxJsonResourceFetch: '', //Set resource to default
                resourceFilterField: '#filter',
                ajaxEventSave: '',
                ajaxEventQuickSave: '',
                ajaxEventDelete: '',
                ajaxEventEdit: '',
                ajaxEventExport: '',
                ajaxRepeatCheck: '',
                ajaxRetrieveDescription: '',
                modalViewSelector: '',
                modalEditSelector: '',
                modalQuickSaveSelector: '',
                modalPromptSelector: '',
                modalEditPromptSelector: '',
                formAddEventSelector: '',
                formFilterSelector: '',
                formEditEventSelector: '',
                formSearchSelector: "",
                successAddEventMessage: '',
                successDeleteEventMessage: '',
                successUpdateEventMessage: '',
                failureAddEventMessage: '',
                failureDeleteEventMessage: '',
                failureUpdateEventMessage: '',
                generalFailureMessage: '',
                ajaxError: '',
                visitUrl: '',
                titleText: '',
                descriptionText: '',
                colorText: '',
                startDateText: '',
                startTimeText: '',
                endDateText: '',
                endTimeText: '',
                categoryText: '',
                eventText: '',
                repetitiveEventActionText: '',
                isRTL: false,
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                today: 'Today',
                month: 'Month',
                week: 'Week',
                day: 'Day',
                weekNumberTitle: 'W',
                allDayText: 'all-day',
                defaultColor: '#587ca3',
                weekType: 'agendaWeek', // agendaWeek basicWeek
                dayType: 'resourceDay', // resourceDay

                editable: false,
                ignoreTimezone: true,
                lazyFetching: true,
                filter: true,
                quickSave: true,
                firstDay: 1,
                gcal: false,
                version: 'links', //modal
                quickSaveCategory: '',
                aspectRatio: 1.35, // will make day boxes bigger
                weekends: true, // show (true) the weekend or not (false)
                weekNumbers: false, // show week numbers (true) or not (false)
                weekNumberCalculation: 'iso',
                hiddenDays: [], // [0,1,2,3,4,5,6] to hide days as you wish

                theme: false,
                themePrev: 'circle-triangle-w',
                themeNext: 'circle-triangle-e',
                titleFormatMonth: 'MMMM yyyy',
                titleFormatWeek: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}",
                titleFormatDay: 'dddd, MMM d, yyyy',
                columnFormatMonth: 'ddd',
                columnFormatWeek: 'ddd M/d',
                columnFormatDay: 'dddd M/d',
                timeFormat: 'H:mm - {H:mm}',
                weekMode: 'fixed', // 'fixed', 'liquid', 'variable'

                allDaySlot: false, // true, false
                axisFormat: 'h(:mm)tt',
                slotMinutes: 30,
                minTime: 6,
                maxTime: 22,
                slotEventOverlap: true,
                savedRedirect: '',
                removedRedirect: '',
                updatedRedirect: '',
                newReservationLink: '',
                ajaxLoaderMarkup: '<div class="loadingDiv"></div>',
                prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
                next: "<span class='fc-text-arrow'>&rsaquo;</span>",
                prevYear: "<span class='fc-text-arrow'>&laquo;</span>",
                nextYear: "<span class='fc-text-arrow'>&raquo;</span>",
                timezone: 'local'
            };

            var options = $.extend(defaults, options);

            var opt = options;

            if (opt.gcal == true) {
                opt.weekType = '';
                opt.dayType = '';
            }

            //Start the calendar definition.. @GO
            // fullCalendar
            $(opt.calendarSelector).fullCalendar
            ({
                allDaySlot: opt.allDaySlot,
                defaultView: opt.defaultView,
                header: {
                    left: 'prev,next  today',
                    center: 'title',
                    right: 'month,' + opt.weekType + ',' + opt.dayType
                },
                titleFormat: {
                    month: opt.titleFormatMonth
                },
                columnFormat: {
                    month: opt.columnFormatMonth,
                    week: opt.columnFormatWeek,
                    day: opt.columnFormatDay
                },
                timeFormat: opt.timeFormat,
                monthNames: opt.monthNames,
                hiddenDays: opt.hiddenDays,
                theme: opt.theme,
                monthNamesShort: opt.monthNamesShort,
                dayNames: opt.dayNames,
                dayNamesShort: opt.dayNamesShort,
                buttonText: {
                    prev: opt.prev,
                    next: opt.next,
                    prevYear: opt.prevYear,
                    nextYear: opt.nextYear,
                    today: opt.today,
                    month: opt.month,
                    week: opt.week,
                    day: opt.day
                },
                minTime: opt.minTime,
                maxTime: opt.maxTime,
                editable: opt.editable,
                selectable: opt.quickSave,
                selectHelper: opt.quickSave,
                timezone: opt.timezone,
                slotEventOverlap: true,
                //The reservation data i.e. Event sources
                events: opt.ajaxJsonFetch,

                lazyFetching: true,
                resources: opt.ajaxJsonResourceFetch,
                dayClick: function (date, allDay, jsEvent, view) {

                    var check = $.fullCalendar.formatDate(date, 'yyyy-MM-dd HH');
                    var today = $.fullCalendar.formatDate(new Date(), 'yyyy-MM-dd HH');
                    var view_name = view.name;
                    //we do not want to navigate from the ResourceDayView plus when the date is previous, take them directly to the date
                    if  ( view_name !== "resourceDay" && check < today )
                        $('#calendar').fullCalendar('changeView', 'agendaDay').fullCalendar('gotoDate', date.getFullYear(), date.getMonth(), date.getDate());
                },
                eventRender: function (event, element, view) {
                    //Attach Tooltip - for description

                    var status = event.status;

                    //Show descriptive text except for resourceDay
                    var view_name = view.name;
                    var stat  = "";
                    store.set('view',view_name);
                    if (  view_name === "resourceDay" )
                        stat = status;
                    else
                        stat = status + " for " + event.title;

                    //var tool=event.room+'\n'+'Status: '+status;
                    element.qtip({
                        content: {
                            title: event.room,
                            text: stat
                        },
                        style: 'qtip-cluetip qtip-rounded ',
                        position : {
                            target: "mouse",
                            my: 'bottom left'
                        }
                    });

                    //Limit the number of event in month view
                    if (view_name === 'month' )  {
                        $(element).removeClass('max_height');

                        $(element).addClass('max_height');

                        var year = event.start.getFullYear(), month = event.start.getMonth() + 1, date = event.start.getDate();
                        var result = year + '-' + (month < 10 ? '0' + month : month) + '-' + (date < 10 ? '0' + date : date);

                        $(element).addClass(result);

                        var ele = $('td[data-date="' + result + '"]'), count = $('.' + result).length;

                        $(ele).find('.view_more').remove();

                        if (count > 3) {
                            $('.' + result + ':gt(2)').remove();
                            $(ele).find('.fc-day-number').after('<a class="view_more"> More...</a>');

                        }

                    }

                    if (view_name==='resourceDay'){
                        //      console.log('day');
                        $('#filt').css('display','inline');
                        $('#cont').removeClass('col-md-12').addClass('col-md-10');
                    }

                    //Our Filter - Added a param to the result array to include the area for filtering.
                    if( view_name==='agendaWeek' || view_name==='month' || view_name==='agendaDay' ){
                        console.log(view_name);
                        if( event.area != $('#filter').val() ){
                            //hide it from UI
                            $(element).hide();
                        }
                    }

                },
                eventAfterAllRender: function (view) {
                    var view_name = view.name;
                    if (view_name === 'month') {

                        $('td.fc-day').each(function () {
                            var EventCount = $('.' + $(this).attr('data-date')).length;

                            if (EventCount == 0)

                                $(this).find('.view_more').remove();
                        });

                    }

                },

                //The calendar actions
                select: function (start, end, ev, view, resource, pview) {

                    var check = $.fullCalendar.formatDate(start, 'yyyy-MM-dd HH');
                    var today = $.fullCalendar.formatDate(new Date(), 'yyyy-MM-dd HH');

                    //Clean the dates for url passings
                    var clean_start = $.fullCalendar.formatDate(start, 'yyyy-MM-dd|HH:mm');
                    var clean_end = $.fullCalendar.formatDate(end, 'yyyy-MM-dd|HH:mm');
                    var resourceId = resource.id;

                    var addReservationLink = opt.newReservationLink + '/' + resourceId + '/' + clean_start + '/' + clean_end;
                    //get the current view
                    var pview_current = pview.name;



                    if (check < today) {

                        if (!ev && (pview_current === "agendaDay" || pview_current === "resourceDay")) {

                            var message = '<b style="text-align: center;">Reservations forbidden for past date and/or time.</b> <br/>';
                            BootstrapDialog.show({
                                title: 'Error Message',
                                message: message,
                                closable: true,
                                type: BootstrapDialog.TYPE_DANGER,
                                cssClass: "small-dialog"
                            });
                        }

                    } else {
                        var diag = new BootstrapDialog({
                            type: BootstrapDialog.TYPE_PRIMARY,
                            title: "Please Select Action",
                            closable: true,
                            cssClass: "small-dialog",
                            buttons: [{
                                label: 'Add New Reservation',
                                cssClass: 'btn-primary',
                                icon: 'glyphicon glyphicon-plus',
                                action: function(dialog) {
                                    //go to location
                                    window.location = addReservationLink;
                                    dialog.close();
                                }
                            }, {
                                label: 'Go To Date',
                                cssClass: 'btn-success',
                                icon: 'glyphicon glyphicon-check',
                                action: function(dialog) {
                                    $('#calendar').fullCalendar('changeView', 'agendaDay').fullCalendar('gotoDate', start.getFullYear(), start.getMonth(), start.getDate());
                                    dialog.close();
                                }
                            }]


                        });

                        diag.realize();
                        diag.getModalBody().hide();
                        diag.getModalBody().remove();
                        diag.open();

                    }
                },
                eventClick: function (event) {
                    var start=event.start;
                    //$('#calendar').fullCalendar('changeView', 'agendaDay').fullCalendar('gotoDate', start.getFullYear(), start.getMonth(), start.getDate());
                },
                eventDrop: function (event, delta, revertFunc) {
                }
            });
        }
    });
})(jQuery);


