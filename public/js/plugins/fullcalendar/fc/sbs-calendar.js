/**
 * Update the rooms filter
 * @param list_of_rooms
 */
function updateRoomsFilter(rooms_div){
    //prevent reloading on first load
    if( store.get('filter_changed') === '1'){
        //slideUp
        $('#rooms').slideUp('fast');
        //append to div
        $('#rooms').html(rooms_div);
        //slideDown
        $('#rooms').slideDown('slow');
    }

}

/**
 *
 * @param rooms_div
 */
function updateRoomsFilterReport(rooms_div){
    //prevent reloading on first load
        //slideUp
        $('#rooms').slideUp('fast');
        //append to div
        $('#rooms').html(rooms_div);
        //slideDown
        $('#rooms').slideDown('slow');
}
/**
 *
 * @param rooms
 */
function setRooms(rooms){
    store.set("current_report_rooms",rooms);
}
/**
 *
 * @param rooms
 */
function unsetRooms(rooms){
    store.set("current_report_rooms",rooms);
}

function getRooms(){
    return store.get('current_report_rooms');
}